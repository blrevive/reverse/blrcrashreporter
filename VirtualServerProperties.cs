﻿// Decompiled with JetBrains decompiler
// Type: VirtualServerProperties
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System.Runtime.InteropServices;

[ComVisible(true)]
public enum VirtualServerProperties
{
  VIRTUALSERVER_UNIQUE_IDENTIFIER,
  VIRTUALSERVER_NAME,
  VIRTUALSERVER_WELCOMEMESSAGE,
  VIRTUALSERVER_PLATFORM,
  VIRTUALSERVER_VERSION,
  VIRTUALSERVER_MAXCLIENTS,
  VIRTUALSERVER_PASSWORD,
  VIRTUALSERVER_CLIENTS_ONLINE,
  VIRTUALSERVER_CHANNELS_ONLINE,
  VIRTUALSERVER_CREATED,
  VIRTUALSERVER_UPTIME,
  VIRTUALSERVER_ENDMARKER,
}
