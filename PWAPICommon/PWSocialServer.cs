﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.PWSocialServer
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Queries.Clan;
using PWAPICommon.Queries.Party;
using PWAPICommon.Queries.Player;
using PWAPICommon.Resources;
using PWAPICommon.Servers;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace PWAPICommon
{
  [ComVisible(true)]
  public class PWSocialServer : PWServer
  {
    private ConcurrentDictionary<int, ChatRoom> ChatChannels;
    private ushort NextRoomIndex;
    private ConcurrentDictionary<string, ClanChatRoom> ClanChatChannels;
    private List<PartyInfo> PartyList;
    private ConcurrentDictionary<string, SocialLoginData> KnownClientNames;
    private ConcurrentDictionary<long, SocialLoginData> KnownClientIds;
    private ConcurrentDictionary<PWConnectionBase, SocialLoginData> KnownClientConnections;
    private MessageSerializer Serializer;

    public PWSocialServer(
      int InMaxConnections,
      string InServerName,
      string InRegionName,
      int InGameID,
      int InServerID,
      PWServerBase.AddLogMessageCallback InCallback,
      ELoggingLevel InLogLevel,
      PWServerApp InApp)
      : base(InMaxConnections, InServerName, InRegionName, InGameID, InServerID, InCallback, InLogLevel, InApp)
    {
      this.KnownClientNames = new ConcurrentDictionary<string, SocialLoginData>();
      this.KnownClientIds = new ConcurrentDictionary<long, SocialLoginData>();
      this.KnownClientConnections = new ConcurrentDictionary<PWConnectionBase, SocialLoginData>();
      this.ChatChannels = new ConcurrentDictionary<int, ChatRoom>();
      this.ClanChatChannels = new ConcurrentDictionary<string, ClanChatRoom>();
      int int32_1 = Convert.ToInt32((object) ChatMode.CM_GLOBAL);
      int int32_2 = Convert.ToInt32((object) ChatMode.CM_HELP);
      this.ChatChannels.TryAdd(int32_1, new ChatRoom("Global", int32_1, true));
      this.ChatChannels.TryAdd(int32_2, new ChatRoom("Help", int32_2, true));
      this.PartyList = new List<PartyInfo>();
      this.NextRoomIndex = Convert.ToUInt16((object) ChatMode.CM_MAX);
      this.Serializer = new MessageSerializer();
    }

    public void SendMessageToClient(EMessageType Type, string Message, PWConnectionBase Connection) => Connection.SendMessage(Type, this.Serializer.Serialize(Message));

    public void SendMessageToAllClients(EMessageType Type, byte[] Message)
    {
      byte[] FinalBlob = (byte[]) null;
      MessageSerializer.Encode(Type, Message, out FinalBlob);
      lock (this.KnownClientIds)
      {
        foreach (SocialLoginData socialLoginData in (IEnumerable<SocialLoginData>) this.KnownClientIds.Values)
          socialLoginData.Connection.SendEncodedMessage(Type, FinalBlob);
      }
    }

    public SocialLoginData GetLoginData(PWConnectionBase ToFind)
    {
      SocialLoginData socialLoginData = (SocialLoginData) null;
      this.KnownClientConnections.TryGetValue(ToFind, out socialLoginData);
      return socialLoginData;
    }

    public SocialLoginData GetLoginData(string ToFind)
    {
      SocialLoginData socialLoginData = (SocialLoginData) null;
      this.KnownClientNames.TryGetValue(ToFind.ToLower(), out socialLoginData);
      return socialLoginData;
    }

    public SocialLoginData GetLoginData(long ToFind)
    {
      SocialLoginData socialLoginData = (SocialLoginData) null;
      this.KnownClientIds.TryGetValue(ToFind, out socialLoginData);
      return socialLoginData;
    }

    public bool AddLoginData(SocialLoginData InClient)
    {
      SocialLoginData socialLoginData;
      if (!this.KnownClientNames.TryAdd(InClient.PlayerInfo.UserName.ToLower(), InClient) && (!this.KnownClientNames.TryRemove(InClient.PlayerInfo.UserName.ToLower(), out socialLoginData) || !this.KnownClientNames.TryAdd(InClient.PlayerInfo.UserName.ToLower(), InClient)))
      {
        this.Log("Adding logindata failed for KnownClientNames, cannot complete social login for player " + InClient.PlayerInfo.UserName + " " + (object) InClient.PlayerInfo.UserID, (PWConnectionBase) null, false, ELoggingLevel.ELL_Errors);
        return false;
      }
      if (!this.KnownClientIds.TryAdd(InClient.PlayerInfo.UserID, InClient) && (!this.KnownClientIds.TryRemove(InClient.PlayerInfo.UserID, out socialLoginData) || !this.KnownClientIds.TryAdd(InClient.PlayerInfo.UserID, InClient)))
      {
        this.Log("Adding logindata failed for KnownClientIds, cannot complete social login for player " + InClient.PlayerInfo.UserName + " " + (object) InClient.PlayerInfo.UserID, (PWConnectionBase) null, false, ELoggingLevel.ELL_Errors);
        return false;
      }
      if (this.KnownClientConnections.TryAdd(InClient.Connection, InClient) || this.KnownClientConnections.TryRemove(InClient.Connection, out socialLoginData) && this.KnownClientConnections.TryAdd(InClient.Connection, InClient))
        return true;
      this.Log("Adding logindata failed for KnownClientConnections, cannot complete social login for player " + InClient.PlayerInfo.UserName + " " + (object) InClient.PlayerInfo.UserID, (PWConnectionBase) null, false, ELoggingLevel.ELL_Errors);
      return false;
    }

    public void RemoveLoginData(SocialLoginData ToRemove)
    {
      this.RemoveLoginData(ToRemove.PlayerInfo.UserName, ToRemove.PlayerInfo.UserID, ToRemove.Connection);
      this.PurgeFromChats(ToRemove);
    }

    public void RemoveLoginData(string OldPlayerName, long OldUserId, PWConnectionBase Conn)
    {
      SocialLoginData socialLoginData;
      this.KnownClientNames.TryRemove(OldPlayerName.ToLower(), out socialLoginData);
      this.KnownClientIds.TryRemove(OldUserId, out socialLoginData);
      this.KnownClientConnections.TryRemove(Conn, out socialLoginData);
    }

    public void PurgeFromChats(SocialLoginData InClient)
    {
      if (InClient == null)
        return;
      lock (InClient)
      {
        this.RemoveFromAllRooms(InClient);
        this.RemoveClanListener(InClient);
      }
    }

    public PlayerSearchData[] GetPartialNameMatches(string ToMatch)
    {
      List<PlayerSearchData> playerSearchDataList = new List<PlayerSearchData>();
      ToMatch = ToMatch.ToLower();
      lock (this.KnownClientNames)
      {
        foreach (string key in (IEnumerable<string>) this.KnownClientNames.Keys)
        {
          if (key.Contains(ToMatch) && key != ToMatch)
          {
            SocialLoginData knownClientName = this.KnownClientNames[key];
            playerSearchDataList.Add(new PlayerSearchData(knownClientName.PlayerInfo.UserName, knownClientName.PlayerInfo.Experience));
          }
        }
      }
      return playerSearchDataList.ToArray();
    }

    public bool UpdateStatus(long InUserId, SocialStatus InStatus)
    {
      SocialLoginData loginData = this.GetLoginData(InUserId);
      if (loginData == null)
        return false;
      loginData.UpdateLocation(InStatus);
      return true;
    }

    public void RemoveClanListener(SocialLoginData ClientData)
    {
      if (ClientData == null)
        return;
      this.Log("Removing client " + (object) ClientData + "from clan chat, if he is in one", ClientData.Connection, false, ELoggingLevel.ELL_Verbose);
      if (ClientData.ClanChat == null || ClientData.ClanChat.Listeners == null || !ClientData.ClanChat.Listeners.Contains(ClientData))
        return;
      lock (ClientData.ClanChat.Listeners)
        ClientData.ClanChat.Listeners.Remove(ClientData);
      if (ClientData.ClanChat.Listeners.Count == 0 && ClientData.ClanInfo != null)
      {
        this.Log("Clan Chat for clan " + ClientData.ClanInfo.ToString() + "is empty, removing.", (PWConnectionBase) null, false, ELoggingLevel.ELL_Verbose);
        this.ClanChatChannels.TryRemove(ClientData.ClanInfo.ClanItem.ClanTag, out ClanChatRoom _);
      }
      ClientData.ClanChat = (ClanChatRoom) null;
    }

    public void DisbandClan(string ClanTag, byte[] Message)
    {
      ClanChatRoom clanChatRoom;
      if (this.ClanChatChannels.TryGetValue(ClanTag, out clanChatRoom))
      {
        byte[] FinalBlob = (byte[]) null;
        MessageSerializer.Encode(EMessageType.EMT_ClanDisband, Message, out FinalBlob);
        lock (clanChatRoom.Listeners)
        {
          foreach (SocialLoginData listener in clanChatRoom.Listeners)
          {
            listener.ClanChat = (ClanChatRoom) null;
            listener.ClanInfo = (PWClanInfo) null;
            listener.Connection.SendEncodedMessage(EMessageType.EMT_ClanDisband, FinalBlob);
          }
        }
        this.ClanChatChannels.TryRemove(ClanTag, out clanChatRoom);
      }
      else
        this.Log("Attempted to disband non existant clan chat.", (PWConnectionBase) null, false, ELoggingLevel.ELL_Errors);
    }

    public void SendClanMemberUpdate(SocialLoginData TheirData, EClanMemberUpdateType UpdateType) => this.SendClanMemberUpdate(TheirData.ClanInfo.FindMember(TheirData.PlayerInfo.UserID), TheirData.ClanChat.Listeners, UpdateType);

    public void SendClanMemberUpdate(
      PWClanMemberInfo Member,
      List<SocialLoginData> ClanMembers,
      EClanMemberUpdateType UpdateType)
    {
      if (Member == null)
        return;
      lock (ClanMembers)
      {
        foreach (SocialLoginData clanMember in ClanMembers)
          new PWUpdateClanMemberPushReq(clanMember.Connection, clanMember.PlayerInfo.UserID, Member)
          {
            UpdateType = UpdateType
          }.SubmitServerQuery();
      }
    }

    public void AddClanListener(SocialLoginData ClientData)
    {
      if (ClientData.ClanChat != null && ClientData.ClanChat.Listeners.Contains(ClientData))
      {
        this.Log("Refreshing clan information, no changes to chat performed", ClientData.Connection, false, ELoggingLevel.ELL_Verbose);
      }
      else
      {
        ClanChatRoom clanChatRoom1;
        if (this.ClanChatChannels.TryGetValue(ClientData.ClanInfo.ClanItem.ClanTag, out clanChatRoom1))
        {
          ClientData.ClanChat = clanChatRoom1;
          lock (clanChatRoom1.Listeners)
            clanChatRoom1.Listeners.Add(ClientData);
        }
        else
        {
          this.Log("Restoring abandoned clan chat", ClientData.Connection, false, ELoggingLevel.ELL_Verbose);
          ClanChatRoom clanChatRoom2 = new ClanChatRoom(ClientData.ClanInfo.ClanItem.ClanTag);
          this.ClanChatChannels.TryAdd(ClientData.ClanInfo.ClanItem.ClanTag, clanChatRoom2);
          lock (clanChatRoom2.Listeners)
            clanChatRoom2.Listeners.Add(ClientData);
          ClientData.ClanChat = clanChatRoom2;
        }
      }
    }

    public void SendMessageToClanMates(
      EMessageType Type,
      ref byte[] Message,
      SocialLoginData ClientData)
    {
      byte[] FinalBlob = (byte[]) null;
      MessageSerializer.Encode(Type, Message, out FinalBlob);
      if (ClientData.ClanChat == null)
        this.Log("ClanChat doesn't exist when trying to send clan message", ClientData.Connection, false, ELoggingLevel.ELL_Errors);
      lock (ClientData.ClanChat.Listeners)
      {
        foreach (SocialLoginData listener in ClientData.ClanChat.Listeners)
          listener.Connection.SendEncodedMessage(Type, FinalBlob);
      }
    }

    public int CreateChannel(string Name)
    {
      ushort uint16 = Convert.ToUInt16((object) ChatMode.CM_MAX);
      bool flag = false;
      lock (this.ChatChannels)
      {
        for (int index = 0; index < (int) ushort.MaxValue; ++index)
        {
          if (!flag)
          {
            if (this.NextRoomIndex >= (ushort) 0 && (int) this.NextRoomIndex < (int) uint16)
              this.NextRoomIndex = uint16;
            flag = this.ChatChannels.TryAdd((int) this.NextRoomIndex, new ChatRoom(Name, (int) this.NextRoomIndex));
            if (!flag)
              ++this.NextRoomIndex;
          }
          else
            break;
        }
      }
      if (flag)
        return (int) this.NextRoomIndex++;
      this.Log("Failed to make new chat room, there are " + (object) this.ChatChannels.Count + "channels. Last attempted index was " + (object) this.NextRoomIndex, (PWConnectionBase) null, false, ELoggingLevel.ELL_Errors);
      return -1;
    }

    public int FindChannel(string Name)
    {
      lock (this.ChatChannels)
      {
        foreach (KeyValuePair<int, ChatRoom> chatChannel in this.ChatChannels)
        {
          if (chatChannel.Value.RoomName == Name)
            return chatChannel.Key;
        }
      }
      return -1;
    }

    public bool AddListener(int ChannelID, ref SocialLoginData NewListener)
    {
      ChatRoom chatRoom;
      if (!this.ChatChannels.TryGetValue(ChannelID, out chatRoom))
      {
        this.Log("Bad channel ID: " + Convert.ToString(ChannelID) + " in add listener to", (PWConnectionBase) null, false, ELoggingLevel.ELL_Warnings);
        return false;
      }
      if (!chatRoom.Listeners.TryAdd(NewListener.PlayerInfo.UserID, NewListener.Connection))
      {
        chatRoom.Listeners.TryRemove(NewListener.PlayerInfo.UserID, out PWConnectionBase _);
        chatRoom.Listeners.TryAdd(NewListener.PlayerInfo.UserID, NewListener.Connection);
      }
      if (!NewListener.ListeningRooms.Contains(chatRoom))
        NewListener.ListeningRooms.Add(chatRoom);
      return true;
    }

    public bool RemoveListener(int ChannelID, ref SocialLoginData ToRemove)
    {
      ChatRoom CR;
      if (!this.ChatChannels.TryGetValue(ChannelID, out CR))
      {
        this.Log("Bad channel ID: " + Convert.ToString(ChannelID) + " in remove listener from", (PWConnectionBase) null, false, ELoggingLevel.ELL_Warnings);
        return false;
      }
      if (CR.Listeners.ContainsKey(ToRemove.PlayerInfo.UserID))
      {
        this.RemoveListenerSecure(CR, ToRemove.PlayerInfo.UserID);
        ToRemove.ListeningRooms.Remove(CR);
        return true;
      }
      this.Log("Attempted to remove non existent listener", (PWConnectionBase) null, false, ELoggingLevel.ELL_Warnings);
      return false;
    }

    private void RemoveListenerSecure(ChatRoom CR, long UserID)
    {
      CR.Listeners.TryRemove(UserID, out PWConnectionBase _);
      if (CR.Listeners.Count != 0 || CR.bIsPersistent)
        return;
      this.Log("Channel: " + CR.RoomName + " with ID: " + (object) CR.RoomID + " is empty, closing", (PWConnectionBase) null, false, ELoggingLevel.ELL_Verbose);
      this.ChatChannels.TryRemove(CR.RoomID, out ChatRoom _);
    }

    public void RemoveFromAllRooms(SocialLoginData Client)
    {
      if (Client.ListeningRooms == null)
        return;
      if (Client.Connection != null)
      {
        lock (Client.ListeningRooms)
        {
          foreach (ChatRoom listeningRoom in Client.ListeningRooms)
            this.RemoveListenerSecure(listeningRoom, Client.PlayerInfo.UserID);
        }
      }
      Client.ListeningRooms.Clear();
    }

    public void SendMessageToChannel(EMessageType Type, ref byte[] Message, int ChannelID)
    {
      byte[] FinalBlob = (byte[]) null;
      ChatRoom chatRoom;
      if (!this.ChatChannels.TryGetValue(ChannelID, out chatRoom))
      {
        this.Log("Bad channel ID:" + Convert.ToString(ChannelID) + " in send message", (PWConnectionBase) null, false, ELoggingLevel.ELL_Warnings);
      }
      else
      {
        MessageSerializer.Encode(Type, Message, out FinalBlob);
        this.Log("Sending message to " + (object) chatRoom.Listeners.Count + " clients", (PWConnectionBase) null, false, ELoggingLevel.ELL_Verbose);
        lock (chatRoom.Listeners)
        {
          foreach (PWConnectionBase pwConnectionBase in (IEnumerable<PWConnectionBase>) chatRoom.Listeners.Values)
            pwConnectionBase.SendEncodedMessage(Type, FinalBlob);
        }
      }
    }

    public EPartyError CreateParty(SocialLoginData Leader, long FirstMemberPlayerId)
    {
      SocialLoginData loginData = this.GetLoginData(FirstMemberPlayerId);
      if (Leader == null || loginData == null)
        return EPartyError.EPE_UnknownError;
      if (Leader.Party != null)
        return EPartyError.EPE_YouAlreadyInParty;
      EPartyError epartyError = EPartyError.EPE_Ok;
      lock (this.PartyList)
      {
        PartyInfo partyInfo = new PartyInfo(Leader, EPartyMode.EPM_Open, 8);
        this.PartyList.Add(partyInfo);
        epartyError = this.InvitePendingPartyMember(Leader, FirstMemberPlayerId);
        if (epartyError != EPartyError.EPE_Ok)
        {
          Leader.Party = (PartyInfo) null;
          partyInfo.Disband();
          this.PartyList.Remove(partyInfo);
        }
      }
      return epartyError;
    }

    public EPartyError InvitePendingPartyMember(
      SocialLoginData Inviter,
      long NewPendingMemberID)
    {
      SocialLoginData loginData = this.GetLoginData(NewPendingMemberID);
      if (Inviter == null || loginData == null)
        return EPartyError.EPE_UnknownError;
      if (Inviter.Party != null)
        Inviter.Party.IsPartyLeader(Inviter);
      if (loginData.Party == null)
        return Inviter.Party.InviteMember(loginData);
      if (!loginData.Party.IsPendingParty(loginData.Party.GetPartyMember(loginData)))
        return EPartyError.EPE_TheyAlreadyInParty;
      return loginData.Party == Inviter.Party ? EPartyError.EPE_TheyPendingOwnParty : EPartyError.EPE_TheyPendingOtherParty;
    }

    public EPartyError AcceptPartyInvite(SocialLoginData NewMember)
    {
      if (NewMember == null)
        return EPartyError.EPE_UnknownError;
      return NewMember.Party == null ? EPartyError.EPE_PartyDoesNotExist : NewMember.Party.AcceptPending(NewMember);
    }

    public EPartyError DenyPartyInvite(SocialLoginData DenyMember)
    {
      if (DenyMember == null)
        return EPartyError.EPE_UnknownError;
      if (DenyMember.Party == null)
        return EPartyError.EPE_PartyDoesNotExist;
      PartyInfo party = DenyMember.Party;
      EPartyError epartyError = DenyMember.Party.DenyPending(DenyMember);
      if (epartyError == EPartyError.EPE_Ok)
      {
        if (party.MemberList.Count <= 1)
        {
          int num = (int) this.DisbandParty(party);
        }
        else
          this.PushRemovedPlayerToParty(party, DenyMember, DenyMember);
      }
      return epartyError;
    }

    public EPartyError RemoveMemberFromParty(
      SocialLoginData Remover,
      long RemoveMemberID)
    {
      SocialLoginData socialLoginData = Remover.PlayerInfo.UserID == RemoveMemberID ? Remover : this.GetLoginData(RemoveMemberID);
      if (Remover == null || socialLoginData == null)
        return EPartyError.EPE_UnknownError;
      if (socialLoginData.Party == null)
        return EPartyError.EPE_PartyDoesNotExist;
      PartyInfo party = socialLoginData.Party;
      bool flag1 = party.IsPartyLeader(Remover);
      bool flag2 = party.IsPartyLeader(socialLoginData);
      if (!flag1 && Remover != socialLoginData)
        return EPartyError.EPE_NotLeader;
      EPartyError epartyError = socialLoginData.Party.RemoveMember(socialLoginData);
      if (epartyError == EPartyError.EPE_Ok)
      {
        if (party.MemberList.Count <= 1)
        {
          int num = (int) this.DisbandParty(party);
          this.SendMessageToClient(EMessageType.EMT_PartyMemberRemoved, "T:" + Remover.PlayerInfo.UserID.ToString() + ":" + socialLoginData.PlayerInfo.UserID.ToString(), socialLoginData.Connection);
        }
        else
        {
          this.PushRemovedPlayerToParty(party, Remover, socialLoginData);
          if (flag2)
          {
            int num = (int) this.PromoteMember(party.MemberList[0].LoginInfo, party.MemberList[0].MemberID, true);
          }
        }
      }
      return epartyError;
    }

    public EPartyError PromoteMember(
      SocialLoginData Promoter,
      long PromotedMemberId,
      bool bForce = false)
    {
      SocialLoginData loginData = this.GetLoginData(PromotedMemberId);
      if (Promoter == null || loginData == null)
        return EPartyError.EPE_UnknownError;
      if (loginData.Party == null)
        return EPartyError.EPE_PartyDoesNotExist;
      PartyInfo party = loginData.Party;
      if (!party.IsPartyLeader(Promoter) && !bForce)
        return EPartyError.EPE_NotLeader;
      EPartyError epartyError = party.PromoteMember(loginData);
      if (epartyError == EPartyError.EPE_Ok)
        this.SendMessageToParty(EMessageType.EMT_PartyLeaderPromoted, PromotedMemberId.ToString(), party);
      return epartyError;
    }

    public EPartyError DisbandParty(PartyInfo Party)
    {
      if (Party == null)
        return EPartyError.EPE_UnknownError;
      lock (this.PartyList)
        this.PartyList.Remove(Party);
      this.SendMessageToParty(EMessageType.EMT_PartyDisbanded, "", Party);
      Party.Disband();
      return EPartyError.EPE_Ok;
    }

    public void NotifyPartyOfPublicGame(SocialLoginData ClientData, string IPString, int Port)
    {
      byte[] BinaryData = this.Serializer.Serialize("T:" + IPString + ":" + (object) Port);
      byte[] FinalBlob = (byte[]) null;
      MessageSerializer.Encode(EMessageType.EMT_PartyReceivePublicGameInvite, BinaryData, out FinalBlob);
      if (ClientData == null || ClientData.Party == null)
        return;
      foreach (PartyMember member in ClientData.Party.MemberList)
      {
        if (member.MemberID != ClientData.PlayerInfo.UserID)
          member.LoginInfo.Connection.SendEncodedMessage(EMessageType.EMT_PartyReceivePublicGameInvite, FinalBlob);
      }
    }

    public void PushUpdatedPlayerToParty(SocialLoginData ChangedMember)
    {
      if (ChangedMember.Party == null)
        return;
      PartyInfo party = ChangedMember.Party;
      PartyMember partyMember1 = party.GetPartyMember(ChangedMember);
      List<PartyMember> memberList = party.MemberList;
      if (partyMember1 == null)
        return;
      foreach (PartyMember partyMember2 in memberList)
        new PartyListUpdateReq(partyMember2.LoginInfo.Connection)
        {
          MembersToUpdate = {
            partyMember1
          }
        }.SubmitServerQuery();
    }

    public void PushRemovedPlayerToParty(
      PartyInfo Party,
      SocialLoginData Remover,
      SocialLoginData RemovedMember)
    {
      string Message = Remover.PlayerInfo.UserID.ToString() + ":" + RemovedMember.PlayerInfo.UserID.ToString();
      this.SendMessageToClient(EMessageType.EMT_PartyMemberRemoved, "T:" + Message, RemovedMember.Connection);
      this.SendMessageToParty(EMessageType.EMT_PartyMemberRemoved, Message, Party);
    }

    public void SendMessageToParty(EMessageType Type, string Message, PartyInfo Party)
    {
      byte[] SerializedMessage = this.Serializer.Serialize("T:" + Message);
      this.SendMessageToParty(Type, ref SerializedMessage, Party);
    }

    public void SendMessageToParty(
      EMessageType Type,
      ref byte[] SerializedMessage,
      PartyInfo Party)
    {
      byte[] FinalBlob = (byte[]) null;
      MessageSerializer.Encode(Type, SerializedMessage, out FinalBlob);
      foreach (PartyMember member in Party.MemberList)
        member.LoginInfo.Connection.SendEncodedMessage(Type, FinalBlob);
    }

    public override void DisconnectClient(PWConnectionBase InClientConnection)
    {
      base.DisconnectClient(InClientConnection);
      this.Log("Chat Disconnected", InClientConnection, false, ELoggingLevel.ELL_Verbose);
      SocialLoginData loginData = this.GetLoginData(InClientConnection);
      if (loginData != null)
      {
        new PWUpdateLastSeenTime((PWConnectionBase) this.LocalConnection)
        {
          UserID = loginData.PlayerInfo.UserID
        }.SubmitServerQuery();
        loginData.PlayerInfo.LastSeenTime = PWServerBase.CurrentTime;
        loginData.UpdateLocation(new SocialStatus()
        {
          StatusContext = (byte) 0
        });
        this.RemoveLoginData(loginData);
      }
      this.GetResource<PWPresenceCache<SocialStatus>>()?.NotifyDisconnected(InClientConnection);
    }

    public static bool HasClanPermissions(PWClanMemberInfo TheirInfo)
    {
      if (TheirInfo == null)
        return false;
      EClanRank clanPosition = (EClanRank) TheirInfo.ClanPosition;
      return clanPosition == EClanRank.CR_LEADER || clanPosition == EClanRank.CR_OFFICER;
    }
  }
}
