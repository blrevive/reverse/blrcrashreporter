﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Connections.PWConnectionUDPServer
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Queries;
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Runtime.InteropServices;

namespace PWAPICommon.Connections
{
  [ComVisible(true)]
  public class PWConnectionUDPServer : PWConnectionUDPBase
  {
    private List<PWRequestBase> PendingMessages;

    public PWConnectionUDPServer(PWServerBase InOwningServer, Socket InSocket)
      : base(InOwningServer, InSocket)
      => this.PendingMessages = new List<PWRequestBase>();

    private void ProcessPendingMessages()
    {
      if (this.PendingMessages.Count <= 0)
        return;
      this.OwningServer.Log("Processing out of order messages", (PWConnectionBase) this, false, ELoggingLevel.ELL_Informative);
      foreach (PWRequestBase pendingMessage in this.PendingMessages)
        this.ReceiveValidMessage(pendingMessage, (byte[]) null);
      this.PendingMessages.Clear();
    }

    protected override bool ReceiveValidMessage(PWRequestBase InMessage, byte[] InOriginalData) => this.SubmitQuery(InMessage);

    protected override bool ParseMessage(PWRequestBase InRequest, byte[] InMessage) => InRequest.ParseServerQuery(InMessage);

    public override bool SubmitQuery(PWRequestBase InQuery)
    {
      try
      {
        return InQuery.SubmitServerQuery();
      }
      catch (Exception ex)
      {
        this.Log("UDP SubmitQuery Exception: " + ex.ToString(), false, ELoggingLevel.ELL_Errors);
      }
      return false;
    }
  }
}
