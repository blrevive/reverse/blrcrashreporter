﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Connections.PWConnectionUDPBase
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Queries;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;

namespace PWAPICommon.Connections
{
  [ComVisible(true)]
  public class PWConnectionUDPBase : PWConnectionBase
  {
    private Socket SendSocket;
    private IPEndPoint SendEP;
    private List<byte> TransferBuffer;

    public override string EndPoint => "UDP";

    public PWConnectionUDPBase(PWServerBase InOwningServer, Socket InUsedSocket)
      : base(InOwningServer)
    {
      this.SendSocket = InUsedSocket;
      this.SendEP = (IPEndPoint) null;
      this.TransferBuffer = new List<byte>(1024);
    }

    public override string ToString() => "[" + this.Name + "]" + this.EndPoint;

    public override bool SendMessage(EMessageType MessageType, byte[] BinaryData)
    {
      byte[] FinalBlob = (byte[]) null;
      MessageSerializer.Encode(MessageType, BinaryData, out FinalBlob);
      base.SendMessage(MessageType, FinalBlob);
      return this.SendEncodedMessage(MessageType, FinalBlob);
    }

    public override bool SendEncodedMessage(EMessageType MessageType, byte[] FinalBlob)
    {
      base.SendEncodedMessage(MessageType, FinalBlob);
      if (this.SendEP == null)
      {
        this.OwningServer.Log("SendEncodedMessage: Endpoint NULL", (PWConnectionBase) this, false, ELoggingLevel.ELL_Verbose);
        return false;
      }
      lock (this.SendSocket)
      {
        SocketAsyncEventArgs connection = this.OwningServer.GetConnection();
        if (connection == null)
        {
          this.Log("New Args are NULL, aborting send!", false, ELoggingLevel.ELL_Errors);
          return false;
        }
        connection.SetBuffer(FinalBlob, 0, FinalBlob.Length);
        connection.UserToken = (object) this;
        connection.RemoteEndPoint = (System.Net.EndPoint) this.SendEP;
        try
        {
          return this.SendSocket.SendToAsync(connection);
        }
        catch (InvalidOperationException ex)
        {
          this.Log("SendAsync Exception: " + ex.ToString() + " LastOp:" + connection.LastOperation.ToString(), false, ELoggingLevel.ELL_Errors);
          return this.SendEncodedMessage(MessageType, FinalBlob);
        }
      }
    }

    public void ReceivedData(byte[] InData, IPEndPoint FromEP)
    {
      this.SendEP = FromEP;
      lock (this.TransferBuffer)
      {
        this.TransferBuffer.AddRange((IEnumerable<byte>) InData);
        byte[] array = this.TransferBuffer.ToArray();
        do
          ;
        while (this.ProcessData(ref array));
        this.TransferBuffer.RemoveRange(0, this.TransferBuffer.Count - array.Length);
      }
    }

    public override bool SubmitQuery(PWRequestBase InQuery) => throw new NotImplementedException();

    protected override bool ReceiveValidMessage(PWRequestBase InMessage, byte[] InOriginalData) => throw new NotImplementedException();

    protected override bool ParseMessage(PWRequestBase InRequest, byte[] InMessage) => throw new NotImplementedException();
  }
}
