﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.ELoggingLevel
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System.ComponentModel;
using System.Runtime.InteropServices;

namespace PWAPICommon
{
  [ComVisible(true)]
  public enum ELoggingLevel
  {
    [Description("None")] ELL_None,
    [Description("Critical")] ELL_Critical,
    [Description("Error")] ELL_Errors,
    [Description("Warning")] ELL_Warnings,
    [Description("Information")] ELL_Informative,
    [Description("Verbose")] ELL_Verbose,
  }
}
