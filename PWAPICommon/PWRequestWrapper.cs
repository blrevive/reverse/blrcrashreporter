﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.PWRequestWrapper
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Queries;
using System.Runtime.InteropServices;

namespace PWAPICommon
{
  [ComVisible(true)]
  public abstract class PWRequestWrapper : PWRequestBase
  {
    public int RetriesLeft;

    public bool CanRetry
    {
      get
      {
        --this.RetriesLeft;
        return this.RetriesLeft > 0;
      }
    }

    public PWRequestWrapper()
      : base((PWConnectionBase) null)
    {
    }

    public PWRequestWrapper(PWConnectionBase InConnection)
      : base(InConnection)
      => this.RetriesLeft = 2;
  }
}
