﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.PWInventory
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Xml.Serialization;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public class PWInventory
  {
    [XmlAttribute("IC")]
    public int MaxItems;
    private List<PWInventoryItem> InventoryItems;

    [XmlIgnore]
    public bool CapReached => this.SlotsLeft <= 0;

    [XmlIgnore]
    public long UserId => this.Items.Count <= 0 ? PWServerBase.EmptyUserID : this.Items.First<PWInventoryItem>().UserId;

    [XmlIgnore]
    public int SlotsLeft => this.MaxItems - this.Items.Count;

    [XmlElement("II")]
    public List<PWInventoryItem> Items
    {
      get => this.InventoryItems;
      set => this.InventoryItems = value ?? new List<PWInventoryItem>();
    }

    public PWInventory()
      : this((List<PWInventoryItem>) null)
    {
    }

    public PWInventory(List<PWInventoryItem> InItems)
    {
      this.Items = InItems;
      this.MaxItems = 800;
    }

    public PWInventory(List<PWInventoryItem> InItems, int InventoryCap)
      : this(InItems)
      => this.MaxItems = InventoryCap;
  }
}
