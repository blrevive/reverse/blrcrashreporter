﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.PWMessageItem
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System.Runtime.InteropServices;
using System.Xml.Serialization;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public class PWMessageItem
  {
    [XmlAttribute("MID")]
    public EMessageType Type;
    [XmlAttribute("MSG")]
    public string Message;

    public PWMessageItem()
      : this(EMessageType.EMT_Invalid, string.Empty)
    {
    }

    public PWMessageItem(EMessageType InType, string InMessage)
    {
      this.Type = InType;
      this.Message = InMessage;
    }
  }
}
