﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.XmlSerializerCache
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System;
using System.Collections.Concurrent;
using System.Runtime.InteropServices;
using System.Xml.Serialization;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public static class XmlSerializerCache
  {
    private static readonly ConcurrentDictionary<string, XmlSerializer> cache = new ConcurrentDictionary<string, XmlSerializer>();

    public static XmlSerializer Create(Type type)
    {
      string key = type.ToString();
      XmlSerializer xmlSerializer1 = (XmlSerializer) null;
      if (XmlSerializerCache.cache.TryGetValue(key, out xmlSerializer1))
        return xmlSerializer1;
      XmlSerializer xmlSerializer2 = new XmlSerializer(type);
      XmlSerializerCache.cache[key] = xmlSerializer2;
      return xmlSerializer2;
    }

    public static XmlSerializer Create(Type type, Type[] ExtraTypes)
    {
      string key = type.ToString() + ":";
      for (int index = 0; index < ExtraTypes.Length; ++index)
        key = key + (object) ExtraTypes[index] + ":";
      XmlSerializer xmlSerializer = (XmlSerializer) null;
      if (XmlSerializerCache.cache.TryGetValue(key, out xmlSerializer))
        return xmlSerializer;
      xmlSerializer = new XmlSerializer(type, ExtraTypes);
      XmlSerializerCache.cache[key] = xmlSerializer;
      return xmlSerializer;
    }

    public static XmlSerializer Create(Type type, XmlTinyAttribute[] IgnoreAttributes)
    {
      string key = type.ToString() + ":";
      for (int index = 0; index < IgnoreAttributes.Length; ++index)
        key = key + IgnoreAttributes[index].LocalName + ":";
      XmlSerializer xmlSerializer = (XmlSerializer) null;
      if (XmlSerializerCache.cache.TryGetValue(key, out xmlSerializer))
        return xmlSerializer;
      XmlAttributeOverrides overrides = new XmlAttributeOverrides();
      XmlAttributes attributes = new XmlAttributes();
      attributes.XmlIgnore = true;
      foreach (XmlTinyAttribute ignoreAttribute in IgnoreAttributes)
        overrides.Add(ignoreAttribute.ParentType, ignoreAttribute.LocalName, attributes);
      xmlSerializer = new XmlSerializer(type, overrides);
      XmlSerializerCache.cache[key] = xmlSerializer;
      return xmlSerializer;
    }
  }
}
