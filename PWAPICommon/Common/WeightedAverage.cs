﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.WeightedAverage
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public class WeightedAverage
  {
    private List<float> Samples;
    private int CurrentSample;

    public WeightedAverage(int MaxSamples)
    {
      this.Samples = new List<float>(MaxSamples);
      for (int index = 0; index < MaxSamples; ++index)
        this.Samples.Add(0.0f);
      this.CurrentSample = 0;
    }

    public float Average
    {
      get
      {
        float num = 0.0f;
        foreach (float sample in this.Samples)
          num += sample;
        return num / (float) this.Samples.Count;
      }
    }

    public void AddSample(float NewSample)
    {
      this.Samples[this.CurrentSample] = NewSample;
      this.CurrentSample = (this.CurrentSample + 1) % this.Samples.Count;
    }
  }
}
