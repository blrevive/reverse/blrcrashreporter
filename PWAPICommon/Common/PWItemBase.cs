﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.PWItemBase
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public class PWItemBase
  {
    public PWItemBase()
    {
    }

    public PWItemBase(object[] InValues) => this.ParseFromArray(InValues);

    public virtual bool ParseFromArray(object[] InArray) => false;

    protected bool Equal(double a, double b) => Math.Abs(a - b) < 1E-05;

    protected bool ValidatedAssign(ref string Output, object Input)
    {
      if (Input == null || !(Input.GetType() != typeof (DBNull)))
        return false;
      Output = Input.ToString();
      return true;
    }

    protected bool ValidatedAssign(ref Guid Output, object Input)
    {
      if (Input == null || !(Input.GetType() != typeof (DBNull)))
        return false;
      Output = new Guid((byte[]) Input);
      return true;
    }

    protected bool ValidatedAssign(ref DateTime Output, object Input)
    {
      if (Input == null || !(Input.GetType() != typeof (DBNull)))
        return false;
      Output = DateTime.Parse(Input.ToString());
      return true;
    }

    protected bool ValidatedAssign(ref long Output, object Input)
    {
      if (Input == null || !(Input.GetType() != typeof (DBNull)))
        return false;
      Output = Convert.ToInt64(Input);
      return true;
    }

    protected bool ValidatedAssign(ref uint Output, object Input)
    {
      if (Input == null || !(Input.GetType() != typeof (DBNull)))
        return false;
      Output = Convert.ToUInt32(Input);
      return true;
    }

    protected bool ValidatedAssign(ref PWMachineID Output, object Input) => Input != null && Input.GetType() == typeof (string) && Output.TryParse(Input as string);

    protected bool ValidatedAssign<T>(ref T Output, object Input)
    {
      if (Input == null || !(Input.GetType() != typeof (DBNull)))
        return false;
      Output = (T) Input;
      return true;
    }

    public virtual SqlCommand CreateAddCommand() => throw new NotImplementedException();

    public virtual SqlCommand CreateUpdateCommand() => throw new NotImplementedException();

    public virtual SqlCommand CreateRemoveCommand() => throw new NotImplementedException();

    public virtual SqlCommand CreateQueryByOwnerCommand() => throw new NotImplementedException();

    public virtual SqlCommand CreateQueryAllCommand() => throw new NotImplementedException();
  }
}
