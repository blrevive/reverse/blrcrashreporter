﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.EPurchaseLength
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System.Runtime.InteropServices;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public enum EPurchaseLength
  {
    EPL_OneDay = 0,
    EPL_ThreeDay = 1,
    EPL_SevenDay = 2,
    EPL_ThirtyDay = 3,
    EPL_NinetyDay = 4,
    EPL_Perm = 5,
    EPL_Invalid = 999, // 0x000003E7
  }
}
