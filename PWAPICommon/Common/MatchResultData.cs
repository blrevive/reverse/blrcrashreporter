﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.MatchResultData
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System.Runtime.InteropServices;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public class MatchResultData : RandomItemData, IOfferData<MatchResultData>, IOfferData
  {
    public EMatchResult RequiredResult;

    public MatchResultData()
      : this(EMatchResult.EMR_None)
    {
    }

    public MatchResultData(EMatchResult InRequiredResult) => this.RequiredResult = InRequiredResult;

    public bool Matches(MatchResultData Other)
    {
      if (this.RequiredResult == EMatchResult.EMR_None)
        return this.Matches((RandomItemData) Other);
      return Other != null && this.RequiredResult == Other.RequiredResult && this.Matches((RandomItemData) Other);
    }

    public override bool Matches(object Other) => this.Matches(Other as MatchResultData);
  }
}
