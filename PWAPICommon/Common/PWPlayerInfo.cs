﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.PWPlayerInfo
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System;
using System.Runtime.InteropServices;
using System.Xml.Serialization;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public class PWPlayerInfo : PWItemBase
  {
    [XmlAttribute("UN")]
    public string UserName;
    [XmlAttribute("UID")]
    public long UserID;
    [XmlAttribute("BD")]
    public long BadgeData;
    [XmlAttribute("TD")]
    public int TitleData;
    [XmlAttribute("XP")]
    public int Experience;
    [XmlAttribute("GD")]
    public bool bIsFemale;
    [XmlAttribute("LS")]
    public DateTime LastSeenTime;
    [XmlElement("OS")]
    public SocialStatus OnlineStatus;
    public bool IsDeveloper;

    public PWPlayerInfo()
      : this(0L)
    {
    }

    public PWPlayerInfo(long InUserID)
      : this(InUserID, new SocialStatus())
    {
    }

    public PWPlayerInfo(long InUserID, SocialStatus InOnlineStatus)
    {
      this.UserID = InUserID;
      this.OnlineStatus = InOnlineStatus;
    }

    public void CopyFromUserProfile(UserProfile InProfile)
    {
      this.UserID = InProfile.UserId;
      this.UserName = InProfile.UserName;
      this.TitleData = InProfile.TitleData;
      this.Experience = InProfile.Experience;
      this.BadgeData = InProfile.BadgeData;
      this.bIsFemale = InProfile.bIsFemale;
      this.LastSeenTime = InProfile.LastSeenDate;
      if (InProfile.QueryType != UserProfile.EProfileQueryType.EPQT_Full)
        return;
      int num = 16;
      this.IsDeveloper = (InProfile.Unlocks & num) != 0;
    }
  }
}
