﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.PWLevelMap
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public class PWLevelMap
  {
    private static Dictionary<int, LevelData> LevelMap;
    private static int MaxLevel;

    public static bool GetLevelData(int ForLevel, out LevelData OutData)
    {
      if (PWLevelMap.LevelMap == null)
        PWLevelMap.InitLevelMap();
      return PWLevelMap.LevelMap.TryGetValue(ForLevel, out OutData);
    }

    public static bool PassedLevelThreshold(
      int PreviousXP,
      int NewXP,
      out int PreviousLevel,
      out int NewLevel)
    {
      NewLevel = -1;
      PreviousLevel = -1;
      if (PWLevelMap.LevelMap == null)
        PWLevelMap.InitLevelMap();
      if (PreviousXP < NewXP)
      {
        foreach (LevelData levelData in PWLevelMap.LevelMap.Values)
        {
          if (PreviousXP >= levelData.XPRequired)
            PreviousLevel = Math.Max(levelData.LevelIndex, PreviousLevel);
          else if (NewXP >= levelData.XPRequired)
            NewLevel = Math.Max(levelData.LevelIndex, NewLevel);
        }
      }
      return PreviousLevel >= 0 && NewLevel >= 0;
    }

    public static int AdjustForLevelCap(int PreviousXP, int XPAdded)
    {
      if (PWLevelMap.LevelMap == null)
        PWLevelMap.InitLevelMap();
      int num = PreviousXP + XPAdded;
      int xpRequired = PWLevelMap.LevelMap[PWLevelMap.MaxLevel].XPRequired;
      return num > xpRequired ? xpRequired - PreviousXP : XPAdded;
    }

    private static void InitLevelMap()
    {
      PWLevelMap.LevelMap = new Dictionary<int, LevelData>(30);
      PWLevelMap.LevelMap[1] = new LevelData(1, 0);
      PWLevelMap.LevelMap[2] = new LevelData(2, 5000);
      PWLevelMap.LevelMap[3] = new LevelData(3, 12500);
      PWLevelMap.LevelMap[4] = new LevelData(4, 23000);
      PWLevelMap.LevelMap[5] = new LevelData(5, 37700);
      PWLevelMap.LevelMap[6] = new LevelData(6, 56810);
      PWLevelMap.LevelMap[7] = new LevelData(7, 79742);
      PWLevelMap.LevelMap[8] = new LevelData(8, 104967);
      PWLevelMap.LevelMap[9] = new LevelData(9, 132463);
      PWLevelMap.LevelMap[10] = new LevelData(10, 162433);
      PWLevelMap.LevelMap[11] = new LevelData(11, 195100);
      PWLevelMap.LevelMap[12] = new LevelData(12, 230708);
      PWLevelMap.LevelMap[13] = new LevelData(13, 269520);
      PWLevelMap.LevelMap[14] = new LevelData(14, 311825);
      PWLevelMap.LevelMap[15] = new LevelData(15, 357937);
      PWLevelMap.LevelMap[16] = new LevelData(16, 408200);
      PWLevelMap.LevelMap[17] = new LevelData(17, 462987);
      PWLevelMap.LevelMap[18] = new LevelData(18, 522704);
      PWLevelMap.LevelMap[19] = new LevelData(19, 587796);
      PWLevelMap.LevelMap[20] = new LevelData(20, 658746);
      PWLevelMap.LevelMap[21] = new LevelData(21, 736081);
      PWLevelMap.LevelMap[22] = new LevelData(22, 820377);
      PWLevelMap.LevelMap[23] = new LevelData(23, 912259);
      PWLevelMap.LevelMap[24] = new LevelData(24, 1012411);
      PWLevelMap.LevelMap[25] = new LevelData(25, 1121577);
      PWLevelMap.LevelMap[26] = new LevelData(26, 1240567);
      PWLevelMap.LevelMap[27] = new LevelData(27, 1370266);
      PWLevelMap.LevelMap[28] = new LevelData(28, 1511639);
      PWLevelMap.LevelMap[29] = new LevelData(29, 1665735);
      PWLevelMap.LevelMap[30] = new LevelData(30, 1833699);
      PWLevelMap.LevelMap[31] = new LevelData(31, 2015101);
      PWLevelMap.LevelMap[32] = new LevelData(32, 2209201);
      PWLevelMap.LevelMap[33] = new LevelData(33, 2414946);
      PWLevelMap.LevelMap[34] = new LevelData(34, 2630979);
      PWLevelMap.LevelMap[35] = new LevelData(35, 2855654);
      PWLevelMap.MaxLevel = 35;
    }
  }
}
