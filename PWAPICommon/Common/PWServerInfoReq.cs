﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.PWServerInfoReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public class PWServerInfoReq : ICloneable
  {
    public string ServerName;
    public string Password;
    public string OwnerName;
    public int MaxPlayers;
    public int MaxSpectators;
    public string Playlist;
    public int[] PlaylistBuildInfo;
    public string MapName;
    public string GameType;
    public bool bPrivate;
    public long CreatorId;
    public bool bArbitrated;
    public List<long> ArbitratedTeamAPlayers;
    public List<long> ArbitratedTeamBPlayers;
    public List<string> ServerProperties;
    public int SeasonID;
    public int MatchID;
    public Guid RequestId;

    public bool HasPassword => this.Password != null && this.Password != "";

    public PWServerInfoReq()
    {
      this.ArbitratedTeamAPlayers = new List<long>(8);
      this.ArbitratedTeamBPlayers = new List<long>(8);
      this.ServerName = "New Server";
      this.OwnerName = "PerfectWorld";
      this.Playlist = "";
      this.Password = "";
      this.MaxPlayers = 16;
      this.MaxSpectators = 16;
      this.MatchID = -1;
      this.SeasonID = -1;
      this.bPrivate = false;
      this.PlaylistBuildInfo = new int[4];
      this.ServerProperties = new List<string>();
    }

    public object Clone() => (object) new PWServerInfoReq()
    {
      ServerName = this.ServerName,
      Password = this.Password,
      MaxPlayers = this.MaxPlayers,
      MaxSpectators = this.MaxSpectators,
      Playlist = this.Playlist,
      MapName = this.MapName,
      GameType = this.GameType,
      bArbitrated = this.bArbitrated,
      ArbitratedTeamAPlayers = this.ArbitratedTeamAPlayers,
      ArbitratedTeamBPlayers = this.ArbitratedTeamBPlayers,
      RequestId = this.RequestId,
      MatchID = this.MatchID,
      OwnerName = this.OwnerName,
      SeasonID = this.SeasonID,
      bPrivate = this.bPrivate,
      CreatorId = this.CreatorId,
      ServerProperties = this.ServerProperties
    };

    public override bool Equals(object obj) => obj != null && obj is PWServerInfoReq pwServerInfoReq && this.RequestId == pwServerInfoReq.RequestId;

    public bool Equals(PWServerInfoReq other) => other != null && this.RequestId == other.RequestId;

    public override int GetHashCode() => this.RequestId.GetHashCode();
  }
}
