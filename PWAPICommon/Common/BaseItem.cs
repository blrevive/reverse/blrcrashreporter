﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.BaseItem
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Xml.Serialization;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public class BaseItem
  {
    [XmlAttribute("ID")]
    public int ID;
    [XmlAttribute("ACT")]
    [DefaultValue(false)]
    public bool bActivated;
    [XmlAttribute("Quantity")]
    [DefaultValue(1)]
    public int Quantity;
    [XmlAttribute("Duration")]
    [DefaultValue(-1)]
    public int Duration;

    public BaseItem()
    {
      this.ID = -1;
      this.bActivated = false;
      this.Duration = -1;
    }
  }
}
