﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.EnumHelper
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System;
using System.Runtime.InteropServices;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public static class EnumHelper
  {
    public static int[] ToIntArray<T>(T[] value)
    {
      int[] numArray = new int[value.Length];
      for (int index = 0; index < value.Length; ++index)
        numArray[index] = Convert.ToInt32((object) value[index]);
      return numArray;
    }

    public static T[] FromIntArray<T>(int[] value)
    {
      T[] objArray = new T[value.Length];
      for (int index = 0; index < value.Length; ++index)
        objArray[index] = (T) Enum.ToObject(typeof (T), value[index]);
      return objArray;
    }

    internal static T Parse<T>(string value, T defaultValue)
    {
      if (Enum.IsDefined(typeof (T), (object) value))
        return (T) Enum.Parse(typeof (T), value);
      int result;
      return int.TryParse(value, out result) && Enum.IsDefined(typeof (T), (object) result) ? (T) Enum.ToObject(typeof (T), result) : defaultValue;
    }

    public static bool TryParse<T>(string value, out T OutValue)
    {
      if (Enum.IsDefined(typeof (T), (object) value))
      {
        OutValue = (T) Enum.Parse(typeof (T), value);
        return true;
      }
      int result;
      if (int.TryParse(value, out result) && Enum.IsDefined(typeof (T), (object) result))
      {
        OutValue = (T) Enum.ToObject(typeof (T), result);
        return true;
      }
      OutValue = default (T);
      return false;
    }
  }
}
