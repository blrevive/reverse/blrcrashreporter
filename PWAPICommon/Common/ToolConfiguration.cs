﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.ToolConfiguration
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Xml;
using System.Xml.Serialization;

namespace PWAPICommon.Common
{
  [XmlRoot]
  [ComVisible(true)]
  public abstract class ToolConfiguration
  {
    public static bool Save(IConfigurable InConfigurable) => ToolConfiguration.Save(InConfigurable, false);

    public static bool Save(IConfigurable InConfigurable, bool bSaveDefault)
    {
      try
      {
        FileStream fileStream = new FileStream(InConfigurable.ConfigurationFileName, FileMode.Create);
        XmlSerializer xmlSerializer = new XmlSerializer(InConfigurable.ConfigurationType);
        ToolConfiguration instance = (ToolConfiguration) Activator.CreateInstance(InConfigurable.ConfigurationType);
        if (!bSaveDefault)
          InConfigurable.SaveTo(instance);
        xmlSerializer.Serialize((Stream) fileStream, (object) instance);
        fileStream.Close();
        InConfigurable.Log("Config saved successfully", ELoggingLevel.ELL_Informative);
      }
      catch (Exception ex)
      {
        InConfigurable.Log("Failed to save config:\n" + ex.ToString(), ELoggingLevel.ELL_Errors);
        return false;
      }
      return true;
    }

    public static bool Load(IConfigurable InConfigurable)
    {
      try
      {
        FileStream fileStream = new FileStream(InConfigurable.ConfigurationFileName, FileMode.Open);
        ToolConfiguration Config = (ToolConfiguration) new XmlSerializer(InConfigurable.ConfigurationType).Deserialize(XmlReader.Create((Stream) fileStream));
        fileStream.Close();
        InConfigurable.LoadFrom(Config);
        InConfigurable.Log("Config loaded successfully", ELoggingLevel.ELL_Informative);
      }
      catch (FileNotFoundException ex)
      {
        InConfigurable.Log("No config file found, creating...", ELoggingLevel.ELL_Informative);
        return ToolConfiguration.Save(InConfigurable, true) && ToolConfiguration.Load(InConfigurable);
      }
      catch (Exception ex)
      {
        InConfigurable?.Log("Failed to load config:\n" + ex.ToString(), ELoggingLevel.ELL_Errors);
        return false;
      }
      return true;
    }
  }
}
