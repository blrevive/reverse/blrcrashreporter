﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.MailOfferData
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System.Runtime.InteropServices;
using System.Xml.Serialization;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public class MailOfferData : OfferDataBase<MailOfferData>
  {
    public string MailSubject;
    [XmlElement]
    public string[] MailBody;

    public override bool Matches(MailOfferData Other) => Other != null;
  }
}
