﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.PWInventoryItem
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Caches;
using PWAPICommon.Wrappers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.InteropServices;
using System.Xml.Serialization;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public class PWInventoryItem : 
    PWSqlItemBase<PWInventoryItem>,
    IPWSqlItemKeyed<long>,
    IComparable<PWInventoryItem>,
    ICloneable
  {
    [XmlAttribute("UID")]
    public long UserId;
    [XmlAttribute("ID")]
    public int ItemId;
    [DefaultValue(-1)]
    [XmlAttribute("IN")]
    public int ItemName;
    [XmlAttribute("GID")]
    public Guid InstanceId;
    [XmlAttribute("SID")]
    public Guid StoreItemId;
    [DefaultValue(-1)]
    [XmlAttribute("USE")]
    public int UsesLeft;
    [XmlAttribute("AC")]
    [DefaultValue(false)]
    public bool Activated;
    [XmlAttribute("PD")]
    public DateTime PurchaseDate;
    [XmlAttribute("XD")]
    public DateTime ExpirationDate;
    [XmlAttribute("AID")]
    public Guid AffectedInvId;
    [XmlAttribute("ATD")]
    public DateTime ActivationDate;
    [XmlAttribute("PTD")]
    public byte PurchaseDuration;
    [DefaultValue(0)]
    [XmlAttribute("PT")]
    public byte PurchaseType;

    public PWInventoryItem()
    {
      this.InstanceId = Guid.NewGuid();
      this.UserId = -1L;
      this.ItemId = -1;
      this.ItemName = -1;
      this.UsesLeft = -1;
      this.Activated = false;
      this.PurchaseDate = PWServerBase.CurrentTime;
      this.ExpirationDate = PWServerBase.CurrentTime;
      this.ActivationDate = PWServerBase.CurrentTime;
      this.PurchaseType = (byte) 0;
      this.PurchaseDuration = byte.MaxValue;
    }

    public PWInventoryItem(object[] InValues)
      : base(InValues)
    {
    }

    public object Clone() => (object) new PWInventoryItem()
    {
      UserId = this.UserId,
      ItemId = this.ItemId,
      ItemName = this.ItemName,
      InstanceId = this.InstanceId,
      UsesLeft = this.UsesLeft,
      PurchaseDate = this.PurchaseDate,
      ExpirationDate = this.ExpirationDate,
      StoreItemId = this.StoreItemId,
      Activated = this.Activated,
      AffectedInvId = this.AffectedInvId,
      PurchaseType = this.PurchaseType,
      PurchaseDuration = this.PurchaseDuration,
      ActivationDate = this.ActivationDate
    };

    public string Describe => this.ToString();

    public override bool ParseFromArray(object[] InArray)
    {
      if (InArray == null || InArray.Length != 13 || (!this.ValidatedAssign(ref this.UserId, InArray[0]) || !this.ValidatedAssign<int>(ref this.ItemId, InArray[1])) || (!this.ValidatedAssign<int>(ref this.ItemName, InArray[2]) || !this.ValidatedAssign(ref this.InstanceId, InArray[3]) || (!this.ValidatedAssign<int>(ref this.UsesLeft, InArray[4]) || !this.ValidatedAssign(ref this.PurchaseDate, InArray[5]))) || (!this.ValidatedAssign(ref this.ExpirationDate, InArray[6]) || !this.ValidatedAssign(ref this.StoreItemId, InArray[7]) || (!this.ValidatedAssign<bool>(ref this.Activated, InArray[8]) || !this.ValidatedAssign(ref this.AffectedInvId, InArray[9])) || !this.ValidatedAssign<byte>(ref this.PurchaseType, InArray[10])))
        return false;
      this.ValidatedAssign(ref this.ActivationDate, InArray[11]);
      this.ValidatedAssign<byte>(ref this.PurchaseDuration, InArray[12]);
      return true;
    }

    public override string ToString() => this.UserId.ToString() + "," + (object) this.ItemId + "," + (object) this.ItemName;

    public int CompareTo(PWInventoryItem Other) => this.InstanceId.CompareTo(Other.InstanceId);

    public override SqlCommand GetCommand(ESqlItemOp SqlOp, ref string LogOutput)
    {
      SqlCommand command;
      switch (SqlOp)
      {
        case ESqlItemOp.ESIO_Add:
          command = new SqlCommand("SP_Inventory_Item_Add");
          command.CommandType = CommandType.StoredProcedure;
          command.AddParameter<long>("@UserID", SqlDbType.BigInt, this.UserId);
          command.AddParameter<int>("@ItemID", SqlDbType.Int, this.ItemId);
          command.AddParameter<byte[]>("@InstanceID", SqlDbType.VarBinary, this.InstanceId.ToByteArray());
          command.AddParameter<byte[]>("@StoreItemID", SqlDbType.VarBinary, this.StoreItemId.ToByteArray());
          command.AddParameter<byte[]>("@AffectedInvID", SqlDbType.VarBinary, this.AffectedInvId.ToByteArray());
          command.AddParameter<int>("@ItemName", SqlDbType.Int, this.ItemName);
          command.AddParameter<bool>("@Activated", SqlDbType.Bit, this.Activated);
          command.AddParameter<int>("@UsesLeft", SqlDbType.Int, this.UsesLeft);
          command.AddParameter<DateTime>("@PurchaseDate", SqlDbType.DateTime, this.PurchaseDate);
          command.AddParameter<DateTime>("@ExpirationDate", SqlDbType.DateTime, this.ExpirationDate);
          command.AddParameter<DateTime>("@ActivationDate", SqlDbType.DateTime, this.ActivationDate);
          command.AddParameter<byte>("@PurchaseType", SqlDbType.TinyInt, this.PurchaseType);
          command.AddParameter<byte>("@PurchaseLength", SqlDbType.TinyInt, this.PurchaseDuration);
          LogOutput = "Adding Inventory Item [" + this.UserId.ToString() + ", " + this.ItemId.ToString() + ", " + this.InstanceId.ToString() + "]";
          break;
        case ESqlItemOp.ESIO_Update:
          command = new SqlCommand("SP_Inventory_Item_Update");
          command.CommandType = CommandType.StoredProcedure;
          command.AddParameter<long>("@UserID", SqlDbType.BigInt, this.UserId);
          command.AddParameter<int>("@ItemID", SqlDbType.Int, this.ItemId);
          command.AddParameter<byte[]>("@InstanceID", SqlDbType.VarBinary, this.InstanceId.ToByteArray());
          command.AddParameter<byte[]>("@StoreItemID", SqlDbType.VarBinary, this.StoreItemId.ToByteArray());
          command.AddParameter<byte[]>("@AffectedInvID", SqlDbType.VarBinary, this.AffectedInvId.ToByteArray());
          command.AddParameter<int>("@ItemName", SqlDbType.Int, this.ItemName);
          command.AddParameter<bool>("@Activated", SqlDbType.Bit, this.Activated);
          command.AddParameter<int>("@UsesLeft", SqlDbType.Int, this.UsesLeft);
          command.AddParameter<DateTime>("@PurchaseDate", SqlDbType.DateTime, this.PurchaseDate);
          command.AddParameter<DateTime>("@ExpirationDate", SqlDbType.DateTime, this.ExpirationDate);
          command.AddParameter<DateTime>("@ActivationDate", SqlDbType.DateTime, this.ActivationDate);
          command.AddParameter<byte>("@PurchaseLength", SqlDbType.TinyInt, this.PurchaseDuration);
          LogOutput = "Updating Inventory Item [" + this.UserId.ToString() + ", " + this.ItemId.ToString() + ", " + this.InstanceId.ToString() + "]";
          break;
        case ESqlItemOp.ESIO_Remove:
          command = new SqlCommand("SP_Inventory_Item_Remove");
          command.CommandType = CommandType.StoredProcedure;
          command.AddParameter<byte[]>("@InstanceID", SqlDbType.VarBinary, this.InstanceId.ToByteArray());
          LogOutput = "Removing Inventory Item [" + this.InstanceId.ToString() + "]";
          break;
        default:
          command = base.GetCommand(SqlOp, ref LogOutput);
          break;
      }
      return command;
    }

    public StoreItem FindParentStoreItem(List<StoreItem> AllStoreItems)
    {
      StoreItem storeItem = AllStoreItems.Find((Predicate<StoreItem>) (x => x.ItemGuid == this.StoreItemId && x.ItemId == this.ItemId));
      if (storeItem == (StoreItem) null || storeItem.ItemId != this.ItemId)
        storeItem = AllStoreItems.Find((Predicate<StoreItem>) (x => x.ItemId == this.ItemId));
      return storeItem;
    }

    public override bool ParseServerResult(PWSqlItemReqBase<PWInventoryItem> SqlReq)
    {
      if (SqlReq == null || SqlReq.Connection == null || SqlReq.Connection.OwningServer == null)
        return false;
      PWPlayerItemCache<PWInventory> resource = SqlReq.Connection.OwningServer.GetResource<PWPlayerItemCache<PWInventory>>();
      if (resource != null)
      {
        switch (SqlReq.SqlOp)
        {
          case ESqlItemOp.ESIO_Add:
          case ESqlItemOp.ESIO_Update:
          case ESqlItemOp.ESIO_Remove:
            resource.MarkDirtyFor(this.UserId);
            break;
          case ESqlItemOp.ESIO_QueryKeyed:
            if (SqlReq is PWSqlItemQueryKeyedReq<PWInventoryItem, long> itemQueryKeyedReq)
            {
              resource.SetCachedItem(itemQueryKeyedReq.Key, new PWInventory(itemQueryKeyedReq.ResponseItems.ToList<PWInventoryItem>()));
              break;
            }
            break;
        }
      }
      return base.ParseServerResult(SqlReq);
    }

    public SqlCommand GetCommand(ESqlItemOp SqlOp, long Key, ref string LogOutput)
    {
      SqlCommand command = (SqlCommand) null;
      if (SqlOp == ESqlItemOp.ESIO_QueryKeyed)
      {
        command = new SqlCommand("SP_Inventory_Item_Get_By_UserId");
        command.CommandType = CommandType.StoredProcedure;
        command.AddParameter<long>("@UserID", SqlDbType.BigInt, Key);
        LogOutput = "Querying Inventory Items [" + Key.ToString() + "]";
      }
      return command;
    }
  }
}
