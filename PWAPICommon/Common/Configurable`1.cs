﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.Configurable`1
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System;
using System.Runtime.InteropServices;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public abstract class Configurable<T> : IConfigurable, ILogger where T : ToolConfiguration, new()
  {
    public abstract string ConfigurationFileName { get; }

    public Type ConfigurationType => typeof (T);

    public void SaveTo(ToolConfiguration Config) => this.SaveTo((T) Config);

    public void LoadFrom(ToolConfiguration Config) => this.LoadFrom((T) Config);

    public abstract void Log(string Message, ELoggingLevel InLogLevel);

    public abstract void Log(string Message, string Filter);

    public abstract void SaveTo(T Config);

    public abstract void LoadFrom(T Config);
  }
}
