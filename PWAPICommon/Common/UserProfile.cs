﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.UserProfile
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System;
using System.Runtime.InteropServices;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public class UserProfile : PWItemBase, ICloneable
  {
    public long UserId;
    public string UserName;
    public int Experience;
    public bool bIsFemale;
    public byte[] ProfileData;
    public byte[] Skills;
    public int Unlocks;
    public long BadgeData;
    public int TitleData;
    public DateTime LastSeenDate;
    public UserProfile.EProfileQueryType QueryType;

    public string ResponseString => this.UserName + ":" + this.Experience.ToString() + ":" + this.bIsFemale.ToString() + ":" + this.Unlocks.ToString() + ":" + this.TitleData.ToString() + ":" + this.BadgeData.ToString() + ":";

    public UserProfile()
    {
      this.UserId = 0L;
      this.UserName = "";
      this.Experience = 0;
      this.bIsFemale = false;
      this.Unlocks = 0;
      this.BadgeData = 0L;
      this.TitleData = 0;
      this.LastSeenDate = PWServerBase.CurrentTime;
      this.QueryType = UserProfile.EProfileQueryType.EPQT_None;
    }

    public bool ParseFromArray(object[] InValues, UserProfile.EProfileQueryType InQueryType)
    {
      this.QueryType = InQueryType;
      switch (InQueryType)
      {
        case UserProfile.EProfileQueryType.EPQT_Basic:
          return this.ParseBasicFromArray(InValues);
        case UserProfile.EProfileQueryType.EPQT_Full:
          return this.ParseFullFromArray(InValues);
        default:
          return false;
      }
    }

    public object Clone()
    {
      UserProfile userProfile = new UserProfile();
      userProfile.UserId = this.UserId;
      userProfile.UserName = (string) this.UserName.Clone();
      userProfile.Experience = this.Experience;
      userProfile.bIsFemale = this.bIsFemale;
      userProfile.Unlocks = this.Unlocks;
      if (this.ProfileData != null)
        userProfile.ProfileData = (byte[]) this.ProfileData.Clone();
      if (this.Skills != null)
        userProfile.Skills = (byte[]) this.Skills.Clone();
      userProfile.Unlocks = this.Unlocks;
      userProfile.BadgeData = this.BadgeData;
      userProfile.TitleData = this.TitleData;
      userProfile.LastSeenDate = this.LastSeenDate;
      return (object) userProfile;
    }

    private bool ParseBasicFromArray(object[] InArray)
    {
      if (InArray == null || InArray.Length < 7 || (!this.ValidatedAssign(ref this.UserId, InArray[0]) || !this.ValidatedAssign(ref this.UserName, InArray[1])) || (!this.ValidatedAssign<int>(ref this.Experience, InArray[2]) || !this.ValidatedAssign<bool>(ref this.bIsFemale, InArray[3])))
        return false;
      this.ValidatedAssign<int>(ref this.TitleData, InArray[4]);
      this.ValidatedAssign(ref this.BadgeData, InArray[5]);
      this.ValidatedAssign(ref this.LastSeenDate, InArray[6]);
      return true;
    }

    private bool ParseFullFromArray(object[] InArray)
    {
      if (!this.ParseBasicFromArray(InArray) || InArray.Length != 10 || (!this.ValidatedAssign<byte[]>(ref this.Skills, InArray[7]) || !this.ValidatedAssign<int>(ref this.Unlocks, InArray[8])) || !this.ValidatedAssign<byte[]>(ref this.ProfileData, InArray[9]))
        return false;
      this.ValidatedAssign(ref this.LastSeenDate, InArray[6]);
      return true;
    }

    public enum EProfileQueryType
    {
      EPQT_None,
      EPQT_Basic,
      EPQT_Full,
    }
  }
}
