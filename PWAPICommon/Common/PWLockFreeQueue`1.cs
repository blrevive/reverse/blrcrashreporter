﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.PWLockFreeQueue`1
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System.Runtime.InteropServices;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public class PWLockFreeQueue<T>
  {
    private PWSingleLinkNode<T> head;
    private PWSingleLinkNode<T> tail;

    public PWLockFreeQueue()
    {
      this.head = new PWSingleLinkNode<T>();
      this.tail = this.head;
    }

    public void Enqueue(T item)
    {
      PWSingleLinkNode<T> comparand = (PWSingleLinkNode<T>) null;
      PWSingleLinkNode<T> newValue = new PWSingleLinkNode<T>();
      newValue.Item = item;
      bool flag = false;
      while (!flag)
      {
        comparand = this.tail;
        PWSingleLinkNode<T> next = comparand.Next;
        if (this.tail == comparand)
        {
          if (next == null)
            flag = SyncMethods.CAS<PWSingleLinkNode<T>>(ref this.tail.Next, (PWSingleLinkNode<T>) null, newValue);
          else
            SyncMethods.CAS<PWSingleLinkNode<T>>(ref this.tail, comparand, next);
        }
      }
      SyncMethods.CAS<PWSingleLinkNode<T>>(ref this.tail, comparand, newValue);
    }

    public bool Dequeue(out T item)
    {
      item = default (T);
      bool flag = false;
      while (!flag)
      {
        PWSingleLinkNode<T> head = this.head;
        PWSingleLinkNode<T> tail = this.tail;
        PWSingleLinkNode<T> next = head.Next;
        if (head == this.head)
        {
          if (head == tail)
          {
            if (next == null)
              return false;
            SyncMethods.CAS<PWSingleLinkNode<T>>(ref this.tail, tail, next);
          }
          else
          {
            item = next.Item;
            flag = SyncMethods.CAS<PWSingleLinkNode<T>>(ref this.head, head, next);
          }
        }
      }
      return true;
    }

    public T Dequeue()
    {
      T obj;
      this.Dequeue(out obj);
      return obj;
    }
  }
}
