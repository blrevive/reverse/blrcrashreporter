﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.PWMachineID
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public class PWMachineID
  {
    public string MachineGuid;
    public List<string> Nics;

    public override string ToString()
    {
      string str = this.MachineGuid + ";";
      foreach (string nic in this.Nics)
        str = str + nic + ";";
      if (str.EndsWith(";"))
        str.Remove(str.Length - 1);
      return str;
    }

    public PWMachineID() => this.Nics = new List<string>(1);

    public bool TryParse(string InValue)
    {
      string[] strArray = InValue.Split(new char[1]{ ';' }, StringSplitOptions.RemoveEmptyEntries);
      if (strArray == null || strArray.Length <= 0)
        return false;
      this.MachineGuid = strArray[0];
      for (int index = 1; index < strArray.Length; ++index)
        this.Nics.Add(strArray[index]);
      return true;
    }

    public bool Equals(PWMachineID rhs)
    {
      if (!(this.MachineGuid == rhs.MachineGuid) || this.Nics.Count != rhs.Nics.Count)
        return false;
      for (int index = 0; index < this.Nics.Count; ++index)
      {
        if (!rhs.Nics.Contains(this.Nics[index]))
          return false;
      }
      return true;
    }
  }
}
