﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.NameGenerator
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public class NameGenerator
  {
    private static List<char> CharacterList;

    private static void GenerateList()
    {
      NameGenerator.CharacterList = new List<char>();
      for (int index = 65; index <= 90; ++index)
        NameGenerator.CharacterList.Add((char) index);
      for (int index = 97; index <= 122; ++index)
        NameGenerator.CharacterList.Add((char) index);
      for (int index = 48; index <= 57; ++index)
        NameGenerator.CharacterList.Add((char) index);
    }

    public static string Generate(int length)
    {
      if (NameGenerator.CharacterList == null)
        NameGenerator.GenerateList();
      Random random = new Random();
      StringBuilder stringBuilder = new StringBuilder(length);
      for (int index1 = 0; index1 < length; ++index1)
      {
        int index2 = random.Next(0, NameGenerator.CharacterList.Count);
        char character = NameGenerator.CharacterList[index2];
        stringBuilder.Append(character);
      }
      return stringBuilder.ToString();
    }
  }
}
