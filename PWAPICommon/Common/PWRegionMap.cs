﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.PWRegionMap
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public static class PWRegionMap
  {
    private static readonly Dictionary<string, PWRegionMap.RegionInfo> KnownRegions = new Dictionary<string, PWRegionMap.RegionInfo>();

    static PWRegionMap()
    {
      PWRegionMap.RegionInfo regionInfo1 = new PWRegionMap.RegionInfo("TEST", "INTEST", (int) byte.MaxValue, EServerLevel.ESL_Test, new PWRegionMap.DNSInfo("bltest.perfectworld.com", 8221), new PWRegionMap.DNSInfo("bltest.perfectworld.com", 9221), new PWRegionMap.DNSInfo("bltest.perfectworld.com", 10221), new PWRegionMap.DNSInfo("bltest.perfectworld.com", 11337), new PWRegionMap.DNSInfo("bltest.perfectworld.com", 8655), new PWRegionMap.DNSInfo("bltest.perfectworld.com", 8899), new string[0]);
      PWRegionMap.RegionInfo regionInfo2 = new PWRegionMap.RegionInfo("INTEST", "INTEST", (int) byte.MaxValue, EServerLevel.ESL_Test, new PWRegionMap.DNSInfo("127.0.0.1", 8221), new PWRegionMap.DNSInfo("127.0.0.1", 9221), new PWRegionMap.DNSInfo("127.0.0.1", 10221), new PWRegionMap.DNSInfo("127.0.0.1", 11337), new PWRegionMap.DNSInfo("127.0.0.1", 8655), new PWRegionMap.DNSInfo("127.0.0.1", 8899), new string[0]);
      PWRegionMap.RegionInfo regionInfo3 = new PWRegionMap.RegionInfo("TEST2", "INTEST2", 17, EServerLevel.ESL_Test, new PWRegionMap.DNSInfo("bltest2.perfectworld.com", 9000), new PWRegionMap.DNSInfo("bltest2.perfectworld.com", 9001), new PWRegionMap.DNSInfo("bltest2.perfectworld.com", 9002), new PWRegionMap.DNSInfo("bltest2.perfectworld.com", 9003), new PWRegionMap.DNSInfo("bltest2.perfectworld.com", 9004), new PWRegionMap.DNSInfo("bltest2.perfectworld.com", 9005), new string[0]);
      PWRegionMap.RegionInfo regionInfo4 = new PWRegionMap.RegionInfo("INTEST2", "INTEST2", 17, EServerLevel.ESL_Test, new PWRegionMap.DNSInfo("127.0.0.1", 9000), new PWRegionMap.DNSInfo("127.0.0.1", 9001), new PWRegionMap.DNSInfo("127.0.0.1", 9002), new PWRegionMap.DNSInfo("127.0.0.1", 9003), new PWRegionMap.DNSInfo("127.0.0.1", 9004), new PWRegionMap.DNSInfo("127.0.0.1", 9005), new string[0]);
      PWRegionMap.RegionInfo regionInfo5 = new PWRegionMap.RegionInfo("LOCAL", "LOCAL", 13, EServerLevel.ESL_Test, new PWRegionMap.DNSInfo("127.0.0.1", 8221), new PWRegionMap.DNSInfo("127.0.0.1", 9221), new PWRegionMap.DNSInfo("127.0.0.1", 10221), new PWRegionMap.DNSInfo("127.0.0.1", 11337), new PWRegionMap.DNSInfo("127.0.0.1", 8655), new PWRegionMap.DNSInfo("127.0.0.1", 8899), new string[0]);
      PWRegionMap.RegionInfo regionInfo6 = new PWRegionMap.RegionInfo("WEST", "INWEST", 0, EServerLevel.ESL_Production, new PWRegionMap.DNSInfo("auth.blwest1.perfectworld.com", 9000), new PWRegionMap.DNSInfo("store.blwest1.perfectworld.com", 9001), new PWRegionMap.DNSInfo("master.blwest1.perfectworld.com", 9002), new PWRegionMap.DNSInfo("chat.blwest1.perfectworld.com", 9003), new PWRegionMap.DNSInfo("chat.blwest1.perfectworld.com", 9004), new PWRegionMap.DNSInfo("master.blwest1.perfectworld.com", 9005), new string[0]);
      PWRegionMap.RegionInfo regionInfo7 = new PWRegionMap.RegionInfo("INWEST", "INWEST", 0, EServerLevel.ESL_Production, new PWRegionMap.DNSInfo("auth.blwest1.private", 9000), new PWRegionMap.DNSInfo("store.blwest1.private", 9001), new PWRegionMap.DNSInfo("master.blwest1.private", 9002), new PWRegionMap.DNSInfo("chat.blwest1.private", 9003), new PWRegionMap.DNSInfo("chat.blwest1.private", 9004), new PWRegionMap.DNSInfo("master.blwest1.private", 9005), new string[0]);
      PWRegionMap.RegionInfo regionInfo8 = new PWRegionMap.RegionInfo("EAST", "INEAST", 1, EServerLevel.ESL_Production, new PWRegionMap.DNSInfo("auth.bleast1.perfectworld.com", 9000), new PWRegionMap.DNSInfo("store.bleast1.perfectworld.com", 9001), new PWRegionMap.DNSInfo("master.bleast1.perfectworld.com", 9002), new PWRegionMap.DNSInfo("chat.bleast1.perfectworld.com", 9003), new PWRegionMap.DNSInfo("chat.bleast1.perfectworld.com", 9004), new PWRegionMap.DNSInfo("master.bleast1.perfectworld.com", 9005), new string[0]);
      PWRegionMap.RegionInfo regionInfo9 = new PWRegionMap.RegionInfo("INEAST", "INEAST", 1, EServerLevel.ESL_Production, new PWRegionMap.DNSInfo("auth.bleast1.private", 9000), new PWRegionMap.DNSInfo("store.bleast1.private", 9001), new PWRegionMap.DNSInfo("master.bleast1.private", 9002), new PWRegionMap.DNSInfo("chat.bleast1.private", 9003), new PWRegionMap.DNSInfo("chat.bleast1.private", 9004), new PWRegionMap.DNSInfo("master.bleast1.private", 9005), new string[0]);
      PWRegionMap.RegionInfo regionInfo10 = new PWRegionMap.RegionInfo("EU", "INEU", 2, EServerLevel.ESL_Production, new PWRegionMap.DNSInfo("auth.bleu1.perfectworld.eu", 9000), new PWRegionMap.DNSInfo("store.bleu1.perfectworld.eu", 9001), new PWRegionMap.DNSInfo("master.bleu1.perfectworld.eu", 9002), new PWRegionMap.DNSInfo("chat.bleu1.perfectworld.eu", 9003), new PWRegionMap.DNSInfo("chat.bleu1.perfectworld.eu", 9004), new PWRegionMap.DNSInfo("master.bleu1.perfectworld.eu", 9005), new string[0]);
      PWRegionMap.RegionInfo regionInfo11 = new PWRegionMap.RegionInfo("INEU", "INEU", 2, EServerLevel.ESL_Production, new PWRegionMap.DNSInfo("auth.bleu1.private", 9000), new PWRegionMap.DNSInfo("store.bleu1.private", 9001), new PWRegionMap.DNSInfo("master.bleu1.private", 9002), new PWRegionMap.DNSInfo("chat.bleu1.private", 9003), new PWRegionMap.DNSInfo("chat.bleu1.private", 9004), new PWRegionMap.DNSInfo("master.bleu1.private", 9005), new string[0]);
      PWRegionMap.RegionInfo regionInfo12 = new PWRegionMap.RegionInfo("STAG", "INSTAG", 19, EServerLevel.ESL_Staging, new PWRegionMap.DNSInfo("auth.blstaging1.perfectworld.com", 9000), new PWRegionMap.DNSInfo("store.blstaging1.perfectworld.com", 9001), new PWRegionMap.DNSInfo("master.blstaging1.perfectworld.com", 9002), new PWRegionMap.DNSInfo("chat.blstaging1.perfectworld.com", 9003), new PWRegionMap.DNSInfo("chat.blstaging1.perfectworld.com", 9004), new PWRegionMap.DNSInfo("master.blstaging1.perfectworld.com", 9005), new string[0]);
      PWRegionMap.RegionInfo regionInfo13 = new PWRegionMap.RegionInfo("INSTAG", "INSTAG", 19, EServerLevel.ESL_Staging, new PWRegionMap.DNSInfo("auth.blstaging1.private", 9000), new PWRegionMap.DNSInfo("store.blstaging1.private", 9001), new PWRegionMap.DNSInfo("master.blstaging1.private", 9002), new PWRegionMap.DNSInfo("chat.blstaging1.private", 9003), new PWRegionMap.DNSInfo("chat.blstaging1.private", 9004), new PWRegionMap.DNSInfo("master.blstaging1.private", 9005), new string[0]);
      PWRegionMap.KnownRegions.Add("TEST", regionInfo1);
      PWRegionMap.KnownRegions.Add("INTEST", regionInfo2);
      PWRegionMap.KnownRegions.Add("TEST2", regionInfo3);
      PWRegionMap.KnownRegions.Add("INTEST2", regionInfo4);
      PWRegionMap.KnownRegions.Add("LOCAL", regionInfo5);
      PWRegionMap.KnownRegions.Add("STAG", regionInfo12);
      PWRegionMap.KnownRegions.Add("INSTAG", regionInfo13);
      PWRegionMap.KnownRegions.Add("WEST", regionInfo6);
      PWRegionMap.KnownRegions.Add("EAST", regionInfo8);
      PWRegionMap.KnownRegions.Add("EU", regionInfo10);
      PWRegionMap.KnownRegions.Add("INWEST", regionInfo7);
      PWRegionMap.KnownRegions.Add("INEAST", regionInfo9);
      PWRegionMap.KnownRegions.Add("INEU", regionInfo11);
    }

    public static List<string> GetRegionNames() => PWRegionMap.KnownRegions.Select<KeyValuePair<string, PWRegionMap.RegionInfo>, string>((Func<KeyValuePair<string, PWRegionMap.RegionInfo>, string>) (x => x.Key)).ToList<string>();

    public static bool IsVisible(string RegionName, EServerLevel Level)
    {
      PWRegionMap.RegionInfo OutInfo;
      return Level != EServerLevel.ESL_Hidden && PWRegionMap.GetRegionInfo(RegionName, out OutInfo) && OutInfo.Level >= Level;
    }

    public static bool GetRegionInfo(string RegionName, out PWRegionMap.RegionInfo OutInfo)
    {
      if (!string.IsNullOrEmpty(RegionName))
        return PWRegionMap.KnownRegions.TryGetValue(RegionName.ToUpper(), out OutInfo);
      OutInfo = new PWRegionMap.RegionInfo();
      return false;
    }

    public struct DNSInfo
    {
      private string dns;
      private int port;
      private IPEndPoint EndPoint;

      public DNSInfo(string InDNS, int InPort)
      {
        this.dns = InDNS;
        this.port = InPort;
        this.EndPoint = (IPEndPoint) null;
      }

      public IPEndPoint Location
      {
        get
        {
          if (this.EndPoint == null)
          {
            try
            {
              this.EndPoint = new IPEndPoint(PWServerBase.StaticGetIPAddressFromString(this.dns), this.port);
            }
            catch (Exception ex)
            {
            }
          }
          return this.EndPoint;
        }
      }

      public string Address => this.dns;

      public int Port => this.port;
    }

    public struct RegionInfo
    {
      private int id;
      private string name;
      private string internalName;
      private PWRegionMap.DNSInfo[] dnsInfo;
      private string[] visibleRegions;
      private EServerLevel level;

      public RegionInfo(
        string InName,
        string InInternalName,
        int InId,
        EServerLevel InLevel,
        PWRegionMap.DNSInfo InAuth,
        PWRegionMap.DNSInfo InItem,
        PWRegionMap.DNSInfo InSuper,
        PWRegionMap.DNSInfo InChat,
        PWRegionMap.DNSInfo InVoip,
        PWRegionMap.DNSInfo InReport,
        string[] InVisibleRegionNames)
      {
        this.id = InId;
        this.name = InName;
        this.internalName = InInternalName;
        this.dnsInfo = new PWRegionMap.DNSInfo[System.Enum.GetValues(typeof (EServerType)).Length];
        this.dnsInfo[0] = InAuth;
        this.dnsInfo[1] = InItem;
        this.dnsInfo[2] = InSuper;
        this.dnsInfo[3] = InChat;
        this.dnsInfo[4] = InVoip;
        this.dnsInfo[5] = InReport;
        this.visibleRegions = Utility<string>.CloneList((IEnumerable<string>) InVisibleRegionNames).ToArray<string>();
        this.level = InLevel;
      }

      public string Name => this.name;

      public string InternalName => this.internalName;

      public EServerLevel Level => this.level;

      public string[] VisibleRegions => this.visibleRegions;

      public PWRegionMap.DNSInfo GetDNSInfo(EServerType ServerIndex) => this.dnsInfo != null && ServerIndex >= EServerType.EST_Auth && ServerIndex < (EServerType) ((IEnumerable<PWRegionMap.DNSInfo>) this.dnsInfo).Count<PWRegionMap.DNSInfo>() ? this.dnsInfo[(int) ServerIndex] : new PWRegionMap.DNSInfo();

      public PWRegionMap.DNSInfo GetDNSInfo(string ServerName)
      {
        if (this.dnsInfo != null)
        {
          if (ServerName.Equals("auth", StringComparison.CurrentCultureIgnoreCase))
            return this.Auth;
          if (ServerName.Equals("item", StringComparison.CurrentCultureIgnoreCase))
            return this.Item;
          if (ServerName.Equals("super", StringComparison.CurrentCultureIgnoreCase))
            return this.Super;
          if (ServerName.Equals("social", StringComparison.CurrentCultureIgnoreCase))
            return this.Chat;
          if (ServerName.Equals("voip", StringComparison.CurrentCultureIgnoreCase))
            return this.Voip;
          if (ServerName.Equals("crash", StringComparison.CurrentCultureIgnoreCase))
            return this.Report;
        }
        return new PWRegionMap.DNSInfo();
      }

      public PWRegionMap.DNSInfo Auth => this.GetDNSInfo(EServerType.EST_Auth);

      public PWRegionMap.DNSInfo Item => this.GetDNSInfo(EServerType.EST_Item);

      public PWRegionMap.DNSInfo Super => this.GetDNSInfo(EServerType.EST_Super);

      public PWRegionMap.DNSInfo Chat => this.GetDNSInfo(EServerType.EST_Social);

      public PWRegionMap.DNSInfo Voip => this.GetDNSInfo(EServerType.EST_Voice);

      public PWRegionMap.DNSInfo Report => this.GetDNSInfo(EServerType.EST_Report);

      public int ID => this.id;
    }
  }
}
