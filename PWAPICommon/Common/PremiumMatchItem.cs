﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.PremiumMatchItem
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Config;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public class PremiumMatchItem : PWSqlItemBase<PremiumMatchItem>, IPWSqlItemKeyed<long>
  {
    public Guid UniqueID;
    public long OwnerID;
    public DateTime CreationDate;
    public DateTime ExpirationDate;
    public string SubRegion;
    public string GameRules;

    public PWServerInfoReq GenerateRequestObject()
    {
      KVPDictionary kvpDictionary = new KVPDictionary();
      kvpDictionary.FromString(this.GameRules);
      PWServerInfoReq pwServerInfoReq = new PWServerInfoReq();
      pwServerInfoReq.bArbitrated = false;
      pwServerInfoReq.RequestId = this.UniqueID;
      pwServerInfoReq.CreatorId = this.OwnerID;
      pwServerInfoReq.ServerName = kvpDictionary.GetValue("ServerName", "Custom Server");
      pwServerInfoReq.Password = kvpDictionary.GetValue("GamePassword", "");
      pwServerInfoReq.bPrivate = kvpDictionary["PublicGame"] == "0";
      pwServerInfoReq.PlaylistBuildInfo[0] = kvpDictionary.GetValue("PI1", 0);
      pwServerInfoReq.PlaylistBuildInfo[1] = kvpDictionary.GetValue("PI2", 0);
      pwServerInfoReq.PlaylistBuildInfo[2] = kvpDictionary.GetValue("PI3", 0);
      pwServerInfoReq.PlaylistBuildInfo[3] = kvpDictionary.GetValue("PI4", 0);
      pwServerInfoReq.OwnerName = kvpDictionary.GetValue("OwnerName", "Custom Owner");
      pwServerInfoReq.MaxPlayers = kvpDictionary.GetValue("MaxPlayers", 16);
      pwServerInfoReq.MaxSpectators = kvpDictionary.GetValue("MaxSpectators", 0);
      return pwServerInfoReq;
    }

    public override bool ParseFromArray(object[] InArray)
    {
      if (InArray == null || InArray.Length != 6 || (!this.ValidatedAssign(ref this.UniqueID, InArray[0]) || !this.ValidatedAssign(ref this.OwnerID, InArray[1])) || (!this.ValidatedAssign(ref this.CreationDate, InArray[2]) || !this.ValidatedAssign(ref this.ExpirationDate, InArray[3])))
        return false;
      this.SubRegion = (string) InArray[4];
      this.GameRules = (string) InArray[5];
      return true;
    }

    public override SqlCommand GetCommand(ESqlItemOp SqlOp, ref string LogOutput)
    {
      switch (SqlOp)
      {
        case ESqlItemOp.ESIO_Add:
          return this.CreateAddCommand();
        case ESqlItemOp.ESIO_Update:
          return this.CreateUpdateCommand();
        case ESqlItemOp.ESIO_Remove:
          return this.CreateRemoveCommand();
        case ESqlItemOp.ESIO_QueryKeyed:
          return this.CreateQueryByOwnerCommand();
        case ESqlItemOp.ESIO_QueryAll:
          return this.CreateQueryAllCommand();
        default:
          return (SqlCommand) null;
      }
    }

    public SqlCommand GetCommand(ESqlItemOp SqlOp, long Key, ref string LogOutput) => SqlOp == ESqlItemOp.ESIO_QueryKeyed ? this.CreateQueryByOwnerCommand() : (SqlCommand) null;

    public override SqlCommand CreateAddCommand()
    {
      SqlCommand command = new SqlCommand("SP_PrivateMatch_Add");
      command.CommandType = CommandType.StoredProcedure;
      command.AddParameter<byte[]>("@UniqueID", SqlDbType.VarBinary, this.UniqueID.ToByteArray());
      command.AddParameter<long>("@OwnerID", SqlDbType.BigInt, this.OwnerID);
      command.AddParameter<DateTime>("@CreationDate", SqlDbType.DateTime, this.CreationDate);
      command.AddParameter<DateTime>("@ExpirationDate", SqlDbType.DateTime, this.ExpirationDate);
      command.AddParameter<string>("@SubRegion", SqlDbType.VarChar, this.SubRegion);
      command.AddParameter<string>("@GameData", SqlDbType.Xml, this.GameRules);
      return command;
    }

    public override SqlCommand CreateUpdateCommand()
    {
      SqlCommand command = new SqlCommand("SP_PrivateMatch_Update");
      command.CommandType = CommandType.StoredProcedure;
      command.AddParameter<byte[]>("@UniqueID", SqlDbType.VarBinary, this.UniqueID.ToByteArray());
      command.AddParameter<DateTime>("@ExpirationDate", SqlDbType.DateTime, this.ExpirationDate);
      command.AddParameter<string>("@SubRegion", SqlDbType.VarChar, this.SubRegion);
      command.AddParameter<string>("@GameData", SqlDbType.Xml, this.GameRules);
      return command;
    }

    public override SqlCommand CreateQueryByOwnerCommand()
    {
      SqlCommand command = new SqlCommand("SP_PrivateMatch_Select_By_UserID");
      command.CommandType = CommandType.StoredProcedure;
      command.AddParameter<long>("@OwnerID", SqlDbType.BigInt, this.OwnerID);
      return command;
    }

    public override SqlCommand CreateQueryAllCommand()
    {
      SqlCommand sqlCommand = new SqlCommand("SP_PrivateMatch_Select_All");
      sqlCommand.CommandType = CommandType.StoredProcedure;
      return sqlCommand;
    }

    public override SqlCommand CreateRemoveCommand()
    {
      SqlCommand command = new SqlCommand("SP_PrivateMatch_Remove");
      command.CommandType = CommandType.StoredProcedure;
      command.AddParameter<byte[]>("@UniqueID", SqlDbType.VarBinary, this.UniqueID.ToByteArray());
      return command;
    }
  }
}
