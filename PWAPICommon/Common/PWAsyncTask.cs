﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.PWAsyncTask
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public class PWAsyncTask
  {
    private Task TaskItem;
    private Action Action;
    private TaskCreationOptions CreationOptions;
    private bool bBlockSubsequent;

    public bool Running => this.TaskItem != null && this.TaskItem.Status < TaskStatus.RanToCompletion;

    public bool BlockSubsequent
    {
      get => this.bBlockSubsequent;
      set => this.bBlockSubsequent = value;
    }

    public PWAsyncTask(Action PushAction) => this.Action = PushAction;

    public bool Start() => this.Start(this.Action, this.CreationOptions);

    public bool Start(Action TaskAction) => this.Start(TaskAction, this.CreationOptions);

    public bool Start(Action TaskAction, TaskCreationOptions TaskOptions)
    {
      lock (this)
      {
        if (this.Running)
        {
          if (this.BlockSubsequent)
            return false;
          this.Wait();
        }
        this.CreationOptions = TaskOptions;
        this.Action = TaskAction;
        this.TaskItem = Task.Factory.StartNew(this.Action, this.CreationOptions);
      }
      return true;
    }

    public void Stop()
    {
      lock (this)
      {
        if (this.TaskItem != null)
          this.Wait();
        this.TaskItem = (Task) null;
      }
    }

    private void Wait()
    {
      try
      {
        this.TaskItem.Wait();
      }
      catch (AggregateException ex)
      {
      }
      catch (ObjectDisposedException ex)
      {
      }
    }
  }
}
