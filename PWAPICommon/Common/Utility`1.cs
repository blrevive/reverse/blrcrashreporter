﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.Utility`1
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public static class Utility<T> where T : ICloneable
  {
    public static IEnumerable<T> CloneList(IEnumerable<T> ToClone)
    {
      foreach (T obj in ToClone)
        yield return (T) obj.Clone();
    }
  }
}
