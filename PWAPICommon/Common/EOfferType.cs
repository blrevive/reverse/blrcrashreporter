﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.EOfferType
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System.Runtime.InteropServices;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public enum EOfferType
  {
    EOT_None,
    EOT_Featured,
    EOT_Discount,
    EOT_FreeOnLogin,
    EOT_EndRankedMatchItem,
    EOT_ExperienceThreshold,
    EOT_EndNormalMatchItem,
    EOT_AARAdvertise,
    EOT_Popular,
    EOT_DealOfTheDay,
    EOT_ProfileFlagReward,
    EOT_EndPrivateMatchItem,
    EOT_NewsItem,
    EOT_EventItem,
  }
}
