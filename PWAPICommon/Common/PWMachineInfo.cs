﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.PWMachineInfo
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public class PWMachineInfo : PWSqlItemBase<PWMachineInfo>, IPWSqlItemKeyed<PWMachineID>
  {
    public Guid UniqueID;
    public PWMachineID MachineID;
    public PWOSInfo OSInfo;
    public PWMemInfo MemInfo;
    public PWCPUInfo CPUInfo;
    public PWVideoInfo VideoInfo;

    public PWMachineInfo() => this.MachineID = new PWMachineID();

    public override bool Equals(object obj)
    {
      if (obj == null || this.GetType() != obj.GetType())
        return false;
      PWMachineInfo pwMachineInfo = obj as PWMachineInfo;
      return this.MachineID.Equals(pwMachineInfo.MachineID) && this.OSInfo.Equals((object) pwMachineInfo.OSInfo) && (this.MemInfo.Equals((object) pwMachineInfo.MemInfo) && this.CPUInfo.Equals((object) pwMachineInfo.CPUInfo)) && this.VideoInfo.Equals((object) pwMachineInfo.VideoInfo);
    }

    public override int GetHashCode() => this.MachineID.GetHashCode();

    public override bool ParseFromArray(object[] InArray)
    {
      if (InArray == null || InArray.Length != 15)
        return false;
      this.ValidatedAssign(ref this.UniqueID, InArray[0]);
      this.ValidatedAssign(ref this.MachineID, InArray[1]);
      this.ValidatedAssign(ref this.CPUInfo.Description, InArray[2]);
      this.ValidatedAssign(ref this.CPUInfo.ClockSpeed, InArray[3]);
      this.ValidatedAssign(ref this.CPUInfo.NumCores, InArray[4]);
      this.ValidatedAssign(ref this.CPUInfo.NumProcessors, InArray[5]);
      this.ValidatedAssign(ref this.OSInfo.Name, InArray[6]);
      this.ValidatedAssign(ref this.OSInfo.Version, InArray[7]);
      this.ValidatedAssign(ref this.OSInfo.Locale, InArray[8]);
      this.ValidatedAssign(ref this.MemInfo.RamMB, InArray[9]);
      this.ValidatedAssign(ref this.VideoInfo.Name, InArray[10]);
      this.ValidatedAssign(ref this.VideoInfo.Description, InArray[11]);
      this.ValidatedAssign(ref this.VideoInfo.DriverDate, InArray[12]);
      this.ValidatedAssign(ref this.VideoInfo.DriverVersion, InArray[13]);
      this.ValidatedAssign<int>(ref this.VideoInfo.RamMB, InArray[14]);
      return true;
    }

    public override SqlCommand GetCommand(ESqlItemOp SqlOp, ref string LogOutput)
    {
      SqlCommand command;
      switch (SqlOp)
      {
        case ESqlItemOp.ESIO_Add:
          command = new SqlCommand("SP_MachineInfo_Add");
          command.CommandType = CommandType.StoredProcedure;
          command.AddParameter<byte[]>("@UniqueID", SqlDbType.VarBinary, this.UniqueID.ToByteArray());
          command.AddParameter<string>("@MachineID", SqlDbType.VarChar, this.MachineID.ToString());
          command.AddParameter<string>("@CPUDesc", SqlDbType.VarChar, this.CPUInfo.Description);
          command.AddParameter<uint>("@CPUClock", SqlDbType.Int, this.CPUInfo.ClockSpeed);
          command.AddParameter<uint>("@CPUCores", SqlDbType.Int, this.CPUInfo.NumCores);
          command.AddParameter<uint>("@CPUProcs", SqlDbType.Int, this.CPUInfo.NumProcessors);
          command.AddParameter<string>("@OSName", SqlDbType.VarChar, this.OSInfo.Name);
          command.AddParameter<string>("@OSVer", SqlDbType.VarChar, this.OSInfo.Version);
          command.AddParameter<string>("@OSLocale", SqlDbType.VarChar, this.OSInfo.Locale);
          command.AddParameter<uint>("@RamAmount", SqlDbType.Int, this.MemInfo.RamMB);
          command.AddParameter<string>("@VidName", SqlDbType.VarChar, this.VideoInfo.Name);
          command.AddParameter<string>("@VidDesc", SqlDbType.VarChar, this.VideoInfo.Description);
          command.AddParameter<string>("@VidDriverDate", SqlDbType.VarChar, this.VideoInfo.DriverDate.ToString());
          command.AddParameter<string>("@VidDriverVersion", SqlDbType.VarChar, this.VideoInfo.DriverVersion);
          command.AddParameter<int>("@VidRam", SqlDbType.Int, this.VideoInfo.RamMB);
          break;
        case ESqlItemOp.ESIO_Update:
          command = new SqlCommand("SP_MachineInfo_Update");
          command.CommandType = CommandType.StoredProcedure;
          command.AddParameter<byte[]>("@UniqueID", SqlDbType.VarBinary, this.UniqueID.ToByteArray());
          command.AddParameter<string>("@MachineID", SqlDbType.VarChar, this.MachineID.ToString());
          command.AddParameter<string>("@CPUDesc", SqlDbType.VarChar, this.CPUInfo.Description);
          command.AddParameter<uint>("@CPUClock", SqlDbType.Int, this.CPUInfo.ClockSpeed);
          command.AddParameter<uint>("@CPUCores", SqlDbType.Int, this.CPUInfo.NumCores);
          command.AddParameter<uint>("@CPUProcs", SqlDbType.Int, this.CPUInfo.NumProcessors);
          command.AddParameter<string>("@OSName", SqlDbType.VarChar, this.OSInfo.Name);
          command.AddParameter<string>("@OSVer", SqlDbType.VarChar, this.OSInfo.Version);
          command.AddParameter<string>("@OSLocale", SqlDbType.VarChar, this.OSInfo.Locale);
          command.AddParameter<uint>("@RamAmount", SqlDbType.Int, this.MemInfo.RamMB);
          command.AddParameter<string>("@VidName", SqlDbType.VarChar, this.VideoInfo.Name);
          command.AddParameter<string>("@VidDesc", SqlDbType.VarChar, this.VideoInfo.Description);
          command.AddParameter<string>("@VidDriverDate", SqlDbType.VarChar, this.VideoInfo.DriverDate.ToString());
          command.AddParameter<string>("@VidDriverVersion", SqlDbType.VarChar, this.VideoInfo.DriverVersion);
          command.AddParameter<int>("@VidRam", SqlDbType.Int, this.VideoInfo.RamMB);
          break;
        default:
          command = base.GetCommand(SqlOp, ref LogOutput);
          break;
      }
      return command;
    }

    public SqlCommand GetCommand(ESqlItemOp SqlOp, PWMachineID Key, ref string LogOutput)
    {
      SqlCommand command = (SqlCommand) null;
      if (SqlOp == ESqlItemOp.ESIO_QueryKeyed)
      {
        command = new SqlCommand("SP_MachineInfo_Select_By_MachineID");
        command.CommandType = CommandType.StoredProcedure;
        command.AddParameter<string>("@MachineID", SqlDbType.VarChar, Key.ToString());
      }
      return command;
    }
  }
}
