﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.IConfigurable
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System;
using System.Runtime.InteropServices;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public interface IConfigurable : ILogger
  {
    string ConfigurationFileName { get; }

    Type ConfigurationType { get; }

    void SaveTo(ToolConfiguration Config);

    void LoadFrom(ToolConfiguration Config);
  }
}
