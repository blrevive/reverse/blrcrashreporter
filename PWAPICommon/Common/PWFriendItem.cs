﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.PWFriendItem
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System;
using System.Runtime.InteropServices;
using System.Xml.Serialization;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public class PWFriendItem : PWItemBase, IComparable<PWFriendItem>
  {
    [XmlAttribute("UID")]
    public Guid UniqueID;
    [XmlAttribute("OW")]
    public long OwnerID;
    [XmlAttribute("FID")]
    public long FriendID;
    [XmlAttribute("AT")]
    public DateTime FriendAddTime;
    [XmlAttribute("FS")]
    public byte State;
    [XmlElement("PI")]
    public PWPlayerInfo PlayerInfo;

    public PWFriendItem() => this.PlayerInfo = new PWPlayerInfo();

    public override bool ParseFromArray(object[] InArray) => InArray.Length == 5 && this.ValidatedAssign(ref this.UniqueID, InArray[0]) && (this.ValidatedAssign(ref this.OwnerID, InArray[1]) && this.ValidatedAssign(ref this.FriendID, InArray[2])) && (this.ValidatedAssign(ref this.FriendAddTime, InArray[3]) && this.ValidatedAssign<byte>(ref this.State, InArray[4]));

    public int CompareTo(PWFriendItem Other) => this.UniqueID.CompareTo(Other.UniqueID);

    public enum FriendState
    {
      EFS_Pending,
      EFS_Accepted,
      EFS_Denied,
    }
  }
}
