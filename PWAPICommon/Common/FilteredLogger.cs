﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.FilteredLogger
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public class FilteredLogger : ILogger
  {
    public const int DefaultLogCapacity = 1000;
    public const string DefaultRootFilter = "System";
    public RefreshLogCallback RefreshLog;
    private int LogCapacity;
    private string RootFilter;
    private string ActiveFilter;
    private Dictionary<string, List<string>> LogFilters;

    public string[] this[string Filter]
    {
      get
      {
        List<string> stringList;
        return !this.LogFilters.TryGetValue(Filter, out stringList) ? (string[]) null : stringList.ToArray();
      }
    }

    public FilteredLogger()
      : this("System", 1000)
    {
    }

    public FilteredLogger(string InRootFilter)
      : this(InRootFilter, 1000)
    {
    }

    public FilteredLogger(int InLogCapacity)
      : this("System", InLogCapacity)
    {
    }

    public FilteredLogger(string InRootFilter, int InLogCapacity)
    {
      switch (InRootFilter)
      {
        case "":
        case null:
          InRootFilter = "System";
          break;
      }
      this.LogCapacity = InLogCapacity;
      this.RootFilter = InRootFilter;
      this.ActiveFilter = this.RootFilter;
      this.LogFilters = new Dictionary<string, List<string>>();
    }

    public void Log(string Message, ELoggingLevel InLogLevel) => this.Log(Message, this.RootFilter);

    public void Log(string Message, string Filter)
    {
      switch (Filter)
      {
        case "":
        case null:
          Filter = this.RootFilter;
          break;
      }
      string FullMessage = "[" + Filter + "] " + Message;
      if (Filter != this.RootFilter)
        this.AddLogMessage(FullMessage, Filter);
      this.AddLogMessage(FullMessage, this.RootFilter);
      if (this.RefreshLog == null || !this.IsActiveFilter(Filter))
        return;
      this.RefreshLog(this[this.ActiveFilter]);
    }

    public bool IsActiveFilter(string Filter)
    {
      if (Filter == this.ActiveFilter)
        return true;
      return Filter == "" && this.RootFilter == this.ActiveFilter;
    }

    public void SetActiveFilter(string Filter)
    {
      switch (Filter)
      {
        case "":
        case null:
          Filter = this.RootFilter;
          break;
      }
      if (this.IsActiveFilter(Filter))
        return;
      this.ActiveFilter = Filter;
      if (this.RefreshLog == null)
        return;
      this.RefreshLog(this[this.ActiveFilter]);
    }

    private void AddLogMessage(string FullMessage, string Filter)
    {
      List<string> stringList;
      if (!this.LogFilters.TryGetValue(Filter, out stringList))
      {
        stringList = new List<string>(this.LogCapacity);
        this.LogFilters[Filter] = stringList;
      }
      if (stringList.Count > 0 && stringList.Count >= stringList.Capacity)
        stringList.RemoveAt(0);
      stringList.Add(FullMessage);
    }
  }
}
