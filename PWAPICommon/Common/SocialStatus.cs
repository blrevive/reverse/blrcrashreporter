﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.SocialStatus
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Queries.General;
using System;
using System.Runtime.InteropServices;
using System.Xml.Serialization;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public class SocialStatus : PWPresenceInfo
  {
    [XmlAttribute("SC")]
    public byte StatusContext;
    [XmlAttribute("PL")]
    public string PresenceLocation;

    public SocialStatus() => this.StatusContext = Convert.ToByte((object) SocialStatus.EOnlineContext.EOT_Offline);

    public override void CopyFrom(PWPresenceInfo Presence)
    {
      base.CopyFrom(Presence);
      if (!(Presence is SocialStatus socialStatus))
        return;
      this.StatusContext = socialStatus.StatusContext;
      this.PresenceLocation = socialStatus.PresenceLocation;
    }

    public override string ToString() => base.ToString() + ", " + ((SocialStatus.EOnlineContext) this.StatusContext).ToString();

    public enum EOnlineContext
    {
      EOT_Offline,
      EOT_Away,
      EOT_Busy,
      EOT_MainMenu,
      EOT_Customizing,
      EOT_Shopping,
      EOT_PreGame,
      EOT_Playing,
      EOT_EndRound,
      EOT_Intermission,
      EOT_RankedMatch,
    }
  }
}
