﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.PWMailItem
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using System.Xml.Serialization;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public class PWMailItem : PWSqlItemBase<PWMailItem>, IPWSqlItemKeyed<long>, IComparable<PWMailItem>
  {
    [XmlAttribute("UID")]
    public Guid UniqueID;
    [XmlAttribute("RID")]
    public long RecipientID;
    [XmlAttribute("SID")]
    public long SenderID;
    [XmlAttribute("SN")]
    public string SenderName;
    [XmlAttribute("TS")]
    public DateTime TimeSent;
    [DefaultValue(false)]
    [XmlAttribute("RD")]
    public bool Read;
    [DefaultValue("")]
    [XmlAttribute("SB")]
    public string Subject;
    [XmlAttribute("MS")]
    [DefaultValue("")]
    public string Message;
    [XmlAttribute("IA")]
    public Guid ItemA;
    [XmlAttribute("IB")]
    public Guid ItemB;
    [XmlAttribute("IC")]
    public Guid ItemC;
    [XmlAttribute("ID")]
    public Guid ItemD;
    [XmlAttribute("IAU")]
    [DefaultValue(-1)]
    public int ItemAUnlockID;
    [XmlAttribute("IBU")]
    [DefaultValue(-1)]
    public int ItemBUnlockID;
    [XmlAttribute("ICU")]
    [DefaultValue(-1)]
    public int ItemCUnlockID;
    [DefaultValue(-1)]
    [XmlAttribute("IDU")]
    public int ItemDUnlockID;
    [DefaultValue(0)]
    [XmlAttribute("GP")]
    public int GPAmount;
    [XmlAttribute("ZP")]
    [DefaultValue(0)]
    public int ZPAmount;
    [DefaultValue(255)]
    [XmlAttribute("DR")]
    public byte Duration;

    public PWMailItem()
    {
      this.TimeSent = PWServerBase.CurrentTime;
      this.UniqueID = Guid.NewGuid();
      this.ItemAUnlockID = -1;
      this.ItemBUnlockID = -1;
      this.ItemCUnlockID = -1;
      this.ItemDUnlockID = -1;
      this.Duration = byte.MaxValue;
      this.Read = false;
    }

    public override bool ParseFromArray(object[] InArray)
    {
      bool flag = this.ValidatedAssign(ref this.UniqueID, InArray[0]) && this.ValidatedAssign(ref this.RecipientID, InArray[1]) && this.ValidatedAssign(ref this.Subject, InArray[2]) && this.ValidatedAssign(ref this.Message, InArray[3]) && this.ValidatedAssign(ref this.ItemA, InArray[4]) && this.ValidatedAssign(ref this.ItemB, InArray[5]) && this.ValidatedAssign(ref this.ItemC, InArray[6]) && this.ValidatedAssign(ref this.ItemD, InArray[7]) && this.ValidatedAssign<bool>(ref this.Read, InArray[8]) && this.ValidatedAssign(ref this.SenderName, InArray[9]) && this.ValidatedAssign(ref this.SenderID, InArray[10]) && this.ValidatedAssign(ref this.TimeSent, InArray[11]) && this.ValidatedAssign<int>(ref this.GPAmount, InArray[12]) && this.ValidatedAssign<byte>(ref this.Duration, InArray[13]);
      return true;
    }

    public int CompareTo(PWMailItem Other) => this.UniqueID.CompareTo(Other.UniqueID);

    public override SqlCommand GetCommand(ESqlItemOp SqlOp, ref string LogOutput)
    {
      SqlCommand command;
      switch (SqlOp)
      {
        case ESqlItemOp.ESIO_Add:
          command = new SqlCommand("SP_Mail_Create");
          command.CommandType = CommandType.StoredProcedure;
          command.AddParameter<byte[]>("@UniqueID", SqlDbType.VarBinary, this.UniqueID.ToByteArray());
          command.AddParameter<long>("@UserID", SqlDbType.BigInt, this.RecipientID);
          command.AddParameter<long>("@SenderID", SqlDbType.BigInt, this.SenderID);
          command.AddParameter<string>("@Subject", SqlDbType.VarChar, this.Subject);
          command.AddParameter<string>("@Message", SqlDbType.VarChar, this.Message);
          command.AddParameter<string>("@SenderName", SqlDbType.VarChar, this.SenderName);
          command.AddParameter<DateTime>("@SendDate", SqlDbType.DateTime, this.TimeSent);
          command.AddParameter<byte[]>("@ItemA", SqlDbType.VarBinary, this.ItemA.ToByteArray());
          command.AddParameter<byte[]>("@ItemB", SqlDbType.VarBinary, this.ItemB.ToByteArray());
          command.AddParameter<byte[]>("@ItemC", SqlDbType.VarBinary, this.ItemC.ToByteArray());
          command.AddParameter<byte[]>("@ItemD", SqlDbType.VarBinary, this.ItemD.ToByteArray());
          command.AddParameter<bool>("@Read", SqlDbType.Bit, this.Read);
          command.AddParameter<int>("@GPAmount", SqlDbType.Int, this.GPAmount);
          command.AddParameter<byte>("@Duration", SqlDbType.TinyInt, this.Duration);
          LogOutput = "Adding Mail Item [" + this.RecipientID.ToString() + " ," + this.SenderID.ToString() + " ," + this.UniqueID.ToString() + "]";
          break;
        case ESqlItemOp.ESIO_Update:
          command = new SqlCommand("SP_Mail_Update");
          command.CommandType = CommandType.StoredProcedure;
          command.AddParameter<byte[]>("@UniqueID", SqlDbType.VarBinary, this.UniqueID.ToByteArray());
          command.AddParameter<long>("@UserID", SqlDbType.BigInt, this.RecipientID);
          command.AddParameter<long>("@SenderID", SqlDbType.BigInt, this.SenderID);
          command.AddParameter<string>("@Subject", SqlDbType.VarChar, this.Subject);
          command.AddParameter<string>("@Message", SqlDbType.VarChar, this.Message);
          command.AddParameter<string>("@SenderName", SqlDbType.VarChar, this.SenderName);
          command.AddParameter<DateTime>("@SendDate", SqlDbType.DateTime, this.TimeSent);
          command.AddParameter<byte[]>("@ItemA", SqlDbType.VarBinary, this.ItemA.ToByteArray());
          command.AddParameter<byte[]>("@ItemB", SqlDbType.VarBinary, this.ItemB.ToByteArray());
          command.AddParameter<byte[]>("@ItemC", SqlDbType.VarBinary, this.ItemC.ToByteArray());
          command.AddParameter<byte[]>("@ItemD", SqlDbType.VarBinary, this.ItemD.ToByteArray());
          command.AddParameter<bool>("@Read", SqlDbType.Bit, this.Read);
          command.AddParameter<int>("@GPAmount", SqlDbType.Int, this.GPAmount);
          command.AddParameter<byte>("@Duration", SqlDbType.TinyInt, this.Duration);
          LogOutput = "Updating Mail Item [" + this.RecipientID.ToString() + " ," + this.SenderID.ToString() + " ," + this.UniqueID.ToString() + "]";
          break;
        case ESqlItemOp.ESIO_Remove:
          command = new SqlCommand("SP_Mail_Delete");
          command.CommandType = CommandType.StoredProcedure;
          command.AddParameter<byte[]>("@UniqueID", SqlDbType.VarBinary, this.UniqueID.ToByteArray());
          LogOutput = "Removing Mail Item [" + this.UniqueID.ToString() + "]";
          break;
        case ESqlItemOp.ESIO_QueryAll:
          command = new SqlCommand("SP_Mail_Select_All");
          command.CommandType = CommandType.StoredProcedure;
          LogOutput = "Querying Mail Items [All]";
          break;
        default:
          command = base.GetCommand(SqlOp, ref LogOutput);
          break;
      }
      return command;
    }

    public SqlCommand GetCommand(ESqlItemOp SqlOp, long Key, ref string LogOutput)
    {
      SqlCommand command = (SqlCommand) null;
      if (SqlOp == ESqlItemOp.ESIO_QueryKeyed)
      {
        command = new SqlCommand("SP_Mail_Select_By_UserID");
        command.CommandType = CommandType.StoredProcedure;
        command.AddParameter<long>("@UserID", SqlDbType.BigInt, Key);
        LogOutput = "Querying Mail Items [" + Key.ToString() + "]";
      }
      return command;
    }
  }
}
