﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.OfferDataBase
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Xml.Serialization;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public abstract class OfferDataBase
  {
    [DefaultValue(EPurchaseLength.EPL_Perm)]
    [XmlElement("PurchaseLength")]
    public EPurchaseLength Duration;
    [DefaultValue(1)]
    public int Quantity = 1;
    [DefaultValue(true)]
    public bool bAutoActivate = true;
    protected EPurchaseLength PurchaseLength = EPurchaseLength.EPL_Perm;

    public abstract bool Matches(object Other);
  }
}
