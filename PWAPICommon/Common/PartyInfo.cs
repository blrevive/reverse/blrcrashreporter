﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.PartyInfo
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public class PartyInfo
  {
    private long LeaderPlayerId;
    private List<PartyMember> Members;
    private int MaxPartySize;
    private string CurrentPartyLocation;
    private EPartyMode partyMode;

    public List<PartyMember> MemberList => this.Members;

    public EPartyMode PartyMode => this.partyMode;

    public PartyInfo(SocialLoginData InLeader, EPartyMode Mode, int MaxSize)
    {
      this.partyMode = Mode;
      this.MaxPartySize = MaxSize;
      this.LeaderPlayerId = InLeader.PlayerInfo.UserID;
      this.Members = new List<PartyMember>(MaxSize);
      this.CreatePartyMemberFromLoginInfo(InLeader, true);
    }

    private PartyMember CreatePartyMemberFromLoginInfo(
      SocialLoginData InLoginData,
      bool bAccepted)
    {
      PartyMember partyMember = new PartyMember();
      partyMember.OwnerID = this.LeaderPlayerId;
      partyMember.MemberID = InLoginData.PlayerInfo.UserID;
      partyMember.State = bAccepted ? (byte) 1 : (byte) 0;
      partyMember.LoginInfo = InLoginData;
      partyMember.PlayerInfo = InLoginData.PlayerInfo;
      InLoginData.Party = this;
      this.Members.Add(partyMember);
      return partyMember;
    }

    public EPartyError InviteMember(SocialLoginData InLoginData)
    {
      if (this.IsPartyFull())
        return EPartyError.EPE_PartyAtCapacity;
      PartyMember partyMember = this.GetPartyMember(InLoginData);
      if (partyMember != null)
        return this.IsPendingParty(partyMember) ? EPartyError.EPE_TheyPendingOwnParty : EPartyError.EPE_TheyAlreadyInParty;
      this.CreatePartyMemberFromLoginInfo(InLoginData, false);
      return EPartyError.EPE_Ok;
    }

    public EPartyError AcceptPending(SocialLoginData InLoginData)
    {
      PartyMember partyMember = this.GetPartyMember(InLoginData);
      if (partyMember == null || !this.IsPendingParty(partyMember))
        return EPartyError.EPE_UnknownError;
      partyMember.State = (byte) 1;
      return EPartyError.EPE_Ok;
    }

    public EPartyError DenyPending(SocialLoginData InLoginData)
    {
      PartyMember partyMember = this.GetPartyMember(InLoginData);
      return partyMember == null || !this.IsPendingParty(partyMember) ? EPartyError.EPE_UnknownError : this.RemoveMember(InLoginData);
    }

    public EPartyError PromoteMember(SocialLoginData InLoginData)
    {
      PartyMember partyMember = this.GetPartyMember(InLoginData);
      if (!this.IsInParty(partyMember))
        return EPartyError.EPE_UnknownError;
      this.LeaderPlayerId = partyMember.MemberID;
      foreach (PartyMember member in this.Members)
        member.OwnerID = this.LeaderPlayerId;
      return EPartyError.EPE_Ok;
    }

    public EPartyError RemoveMember(SocialLoginData InLoginData)
    {
      PartyMember partyMember = this.GetPartyMember(InLoginData);
      if (partyMember == null)
        return EPartyError.EPE_UnknownError;
      InLoginData.Party = (PartyInfo) null;
      this.Members.Remove(partyMember);
      return EPartyError.EPE_Ok;
    }

    public void Disband()
    {
      foreach (PartyMember member in this.Members)
        member.LoginInfo.Party = (PartyInfo) null;
      this.Members.Clear();
    }

    public PartyMember GetPartyMember(SocialLoginData InLoginData)
    {
      if (InLoginData == null || InLoginData.PlayerInfo == null)
        return (PartyMember) null;
      foreach (PartyMember member in this.Members)
      {
        if (member.MemberID == InLoginData.PlayerInfo.UserID)
          return member;
      }
      return (PartyMember) null;
    }

    public bool IsPartyLeader(SocialLoginData InLoginData) => InLoginData != null && InLoginData.PlayerInfo.UserID == this.LeaderPlayerId;

    public bool IsInParty(PartyMember PM) => PM != null && PM.State == (byte) 1;

    public bool IsPendingParty(PartyMember PM) => PM != null && PM.State == (byte) 0;

    public bool IsPartyFull() => this.Members.Count >= this.MaxPartySize;

    public int GetNumActiveMembers()
    {
      int num = 0;
      for (int index = 0; index < this.Members.Count; ++index)
      {
        if (this.Members[index].State == (byte) 1)
          ++num;
      }
      return num;
    }

    private bool PartyTravel(string ToServer)
    {
      this.CurrentPartyLocation = ToServer;
      return true;
    }
  }
}
