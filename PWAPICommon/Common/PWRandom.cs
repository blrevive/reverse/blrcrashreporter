﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.PWRandom
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System;
using System.Runtime.InteropServices;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public static class PWRandom
  {
    private static Random RandomObject = new Random();

    public static int Next()
    {
      lock (PWRandom.RandomObject)
        return PWRandom.RandomObject.Next();
    }

    public static int Next(int Max)
    {
      lock (PWRandom.RandomObject)
        return PWRandom.RandomObject.Next(Max);
    }

    public static int Next(int Min, int Max)
    {
      lock (PWRandom.RandomObject)
        return PWRandom.RandomObject.Next(Min, Max);
    }

    public static void NextBytes(byte[] Buffer)
    {
      lock (PWRandom.RandomObject)
        PWRandom.RandomObject.NextBytes(Buffer);
    }

    public static double NextDouble()
    {
      lock (PWRandom.RandomObject)
        return PWRandom.RandomObject.NextDouble();
    }
  }
}
