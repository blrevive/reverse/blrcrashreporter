﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.PWOfferItem
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Xml;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public class PWOfferItem : PWSqlItemBase<PWOfferItem>, ICloneable
  {
    public Guid UniqueID;
    public byte OfferType;
    public EOfferType OfferTypeEnum;
    public string OfferName;
    public DateTime StartDate;
    public DateTime EndDate;
    public double Discount;
    public Guid TargetItem;
    public int SortIndex;
    public string CustomData;
    public DateTime CreationDate;
    private object CustomDataObject;
    private static readonly Dictionary<EOfferType, Type> CustomDataMap = new Dictionary<EOfferType, Type>()
    {
      {
        EOfferType.EOT_ExperienceThreshold,
        typeof (ExperienceThresholdData)
      },
      {
        EOfferType.EOT_EndNormalMatchItem,
        typeof (RandomItemData)
      },
      {
        EOfferType.EOT_EndRankedMatchItem,
        typeof (RandomItemData)
      },
      {
        EOfferType.EOT_EndPrivateMatchItem,
        typeof (RandomItemData)
      },
      {
        EOfferType.EOT_ProfileFlagReward,
        typeof (ProfileFlagData)
      },
      {
        EOfferType.EOT_NewsItem,
        typeof (MailOfferData)
      }
    };
    private static readonly Type[] CustomDataTypesUnique = PWOfferItem.CustomDataMap.Values.Distinct<Type>().Concat<Type>((IEnumerable<Type>) new Type[1]
    {
      typeof (MatchResultData)
    }).ToArray<Type>();

    public T GetCustomData<T>() where T : class => this.CustomDataObject as T;

    public static IEnumerable<Type> CustomDataTypes => (IEnumerable<Type>) PWOfferItem.CustomDataTypesUnique;

    public PWOfferItem()
    {
      this.UniqueID = Guid.NewGuid();
      this.OfferTypeEnum = EOfferType.EOT_None;
      this.OfferType = (byte) 0;
      this.OfferName = "New offer";
      this.StartDate = PWServerBase.CurrentTime;
      this.EndDate = PWServerBase.CurrentTime.AddYears(10);
      this.CreationDate = PWServerBase.CurrentTime;
      this.Discount = 0.0;
      this.SortIndex = -1;
      this.CustomData = "";
    }

    public PWOfferItem(object[] InArray) => this.ParseFromArray(InArray);

    public override bool ParseFromArray(object[] InArray)
    {
      if (InArray.Length != 10 || !this.ValidatedAssign(ref this.UniqueID, InArray[0]) || (!this.ValidatedAssign<byte>(ref this.OfferType, InArray[1]) || !this.ValidatedAssign(ref this.OfferName, InArray[2])) || (!this.ValidatedAssign(ref this.StartDate, InArray[3]) || !this.ValidatedAssign(ref this.EndDate, InArray[4]) || (!this.ValidatedAssign<double>(ref this.Discount, InArray[5]) || !this.ValidatedAssign(ref this.TargetItem, InArray[6]))) || !this.ValidatedAssign<int>(ref this.SortIndex, InArray[7]))
        return false;
      this.ValidatedAssign(ref this.CustomData, InArray[8]);
      this.ValidatedAssign(ref this.CreationDate, InArray[9]);
      this.OfferTypeEnum = (EOfferType) this.OfferType;
      if (this.CustomData != "" && PWOfferItem.CustomDataMap.ContainsKey(this.OfferTypeEnum))
      {
        XmlReader xmlReader = (XmlReader) new XmlTextReader((TextReader) new StringReader(this.CustomData));
        this.CustomDataObject = XmlSerializerCache.Create(PWOfferItem.CustomDataMap[this.OfferTypeEnum], PWOfferItem.CustomDataTypesUnique).Deserialize(xmlReader);
      }
      return true;
    }

    public override bool Equals(object obj)
    {
      if (obj.GetType() == this.GetType())
      {
        PWOfferItem pwOfferItem = (PWOfferItem) obj;
        if (this.UniqueID == pwOfferItem.UniqueID && (int) this.OfferType == (int) pwOfferItem.OfferType && (this.OfferName == pwOfferItem.OfferName && this.StartDate == pwOfferItem.StartDate) && (this.EndDate == pwOfferItem.EndDate && this.Equal(this.Discount, pwOfferItem.Discount) && (this.TargetItem == pwOfferItem.TargetItem && this.SortIndex == pwOfferItem.SortIndex)) && this.CustomData == pwOfferItem.CustomData)
          return true;
      }
      return false;
    }

    public override int GetHashCode() => this.UniqueID.GetHashCode();

    public object Clone() => (object) new PWOfferItem()
    {
      UniqueID = this.UniqueID,
      OfferName = this.OfferName,
      OfferType = this.OfferType,
      OfferTypeEnum = this.OfferTypeEnum,
      StartDate = this.StartDate,
      EndDate = this.EndDate,
      Discount = this.Discount,
      TargetItem = this.TargetItem,
      SortIndex = this.SortIndex,
      CustomData = this.CustomData,
      CreationDate = this.CreationDate
    };

    public override SqlCommand GetCommand(ESqlItemOp SqlOp, ref string LogOutput)
    {
      SqlCommand command;
      switch (SqlOp)
      {
        case ESqlItemOp.ESIO_Add:
          command = new SqlCommand("SP_Offers_Add");
          command.CommandType = CommandType.StoredProcedure;
          command.AddParameter<byte[]>("@UniqueID", SqlDbType.VarBinary, this.UniqueID.ToByteArray());
          command.AddParameter<byte>("@Type", SqlDbType.TinyInt, this.OfferType);
          command.AddParameter<string>("@Name", SqlDbType.VarChar, this.OfferName);
          command.AddParameter<DateTime>("@StartDate", SqlDbType.DateTime, this.StartDate);
          command.AddParameter<DateTime>("@EndDate", SqlDbType.DateTime, this.EndDate);
          command.AddParameter<double>("@Discount", SqlDbType.Float, this.Discount);
          command.AddParameter<byte[]>("@Target", SqlDbType.VarBinary, this.TargetItem.ToByteArray());
          command.AddParameter<int>("@SortValue", SqlDbType.Int, this.SortIndex);
          command.AddParameter<SqlXml>("@CustomData", SqlDbType.Xml, !string.IsNullOrEmpty(this.CustomData) ? new SqlXml((XmlReader) new XmlTextReader((TextReader) new StringReader(this.CustomData))) : new SqlXml());
          command.AddParameter<DateTime>("@CreationDate", SqlDbType.DateTime, this.CreationDate);
          LogOutput = "Adding Offer Item [" + this.UniqueID.ToString() + "]";
          break;
        case ESqlItemOp.ESIO_Update:
          command = new SqlCommand("SP_Offers_Update");
          command.CommandType = CommandType.StoredProcedure;
          command.AddParameter<byte[]>("@UniqueID", SqlDbType.VarBinary, this.UniqueID.ToByteArray());
          command.AddParameter<byte>("@Type", SqlDbType.TinyInt, this.OfferType);
          command.AddParameter<string>("@Name", SqlDbType.VarChar, this.OfferName);
          command.AddParameter<DateTime>("@StartDate", SqlDbType.DateTime, this.StartDate);
          command.AddParameter<DateTime>("@EndDate", SqlDbType.DateTime, this.EndDate);
          command.AddParameter<double>("@Discount", SqlDbType.Float, this.Discount);
          command.AddParameter<byte[]>("@Target", SqlDbType.VarBinary, this.TargetItem.ToByteArray());
          command.AddParameter<int>("@SortValue", SqlDbType.Int, this.SortIndex);
          command.AddParameter<SqlXml>("@CustomData", SqlDbType.Xml, !string.IsNullOrEmpty(this.CustomData) ? new SqlXml((XmlReader) new XmlTextReader((TextReader) new StringReader(this.CustomData))) : new SqlXml());
          command.AddParameter<DateTime>("@CreationDate", SqlDbType.DateTime, this.CreationDate);
          LogOutput = "Updating Offer Item [" + this.UniqueID.ToString() + "]";
          break;
        case ESqlItemOp.ESIO_Remove:
          command = new SqlCommand("SP_Offers_Remove");
          command.CommandType = CommandType.StoredProcedure;
          command.AddParameter<byte[]>("@UniqueID", SqlDbType.VarBinary, this.UniqueID.ToByteArray());
          LogOutput = "Removing Offer Item [" + this.UniqueID.ToString() + "]";
          break;
        case ESqlItemOp.ESIO_QueryAll:
          command = new SqlCommand("SP_Offers_Select_All");
          command.CommandType = CommandType.StoredProcedure;
          LogOutput = "Querying Offer Items [All]";
          break;
        default:
          command = base.GetCommand(SqlOp, ref LogOutput);
          break;
      }
      return command;
    }
  }
}
