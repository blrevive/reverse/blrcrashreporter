﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.PWClanItem
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System;
using System.Runtime.InteropServices;
using System.Xml.Serialization;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public class PWClanItem : PWItemBase, ICloneable
  {
    [XmlAttribute("CID")]
    public Guid ClanID;
    [XmlAttribute("CN")]
    public string ClanName;
    [XmlAttribute("CT")]
    public string ClanTag;
    [XmlAttribute("OID")]
    public long OwnerID;
    [XmlAttribute("MOTD")]
    public string MOTD;
    [XmlAttribute("MC")]
    public int MemberCount;
    [XmlAttribute("DF")]
    public DateTime Founded;

    public PWClanItem()
    {
      this.ClanID = Guid.NewGuid();
      this.ClanName = "";
      this.ClanTag = "";
      this.OwnerID = 1337L;
      this.MOTD = "";
      this.Founded = new DateTime(1988, 3, 3);
    }

    public PWClanItem(object[] InArray) => this.ParseFromArray(InArray);

    public override bool ParseFromArray(object[] InArray) => this.ValidatedAssign(ref this.ClanID, InArray[0]) && this.ValidatedAssign(ref this.ClanName, InArray[1]) && (this.ValidatedAssign(ref this.ClanTag, InArray[2]) && this.ValidatedAssign(ref this.MOTD, InArray[3])) && (this.ValidatedAssign(ref this.OwnerID, InArray[4]) && this.ValidatedAssign(ref this.Founded, InArray[5]));

    public object Clone() => (object) new PWClanItem()
    {
      ClanID = this.ClanID,
      ClanName = this.ClanName,
      ClanTag = this.ClanTag,
      OwnerID = this.OwnerID,
      MOTD = this.MOTD
    };

    public string ToPWString() => "clanid=" + Convert.ToString((object) this.ClanID) + ", clanname=" + this.ClanName + ", clantag=" + this.ClanTag;

    public override string ToString() => this.ClanName + ":" + this.ClanTag + ":" + this.ClanID.ToString() + ":" + this.Founded.ToString("d") + ":" + (object) this.MemberCount;
  }
}
