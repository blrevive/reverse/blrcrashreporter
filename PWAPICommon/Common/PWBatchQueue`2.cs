﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.PWBatchQueue`2
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public abstract class PWBatchQueue<T, U>
  {
    private ConcurrentQueue<U> BatchItems = new ConcurrentQueue<U>();

    public void Enqueue(T Item) => this.BatchItems.Enqueue(this.Generate(Item));

    public void Enqueue(IEnumerable<T> Items)
    {
      foreach (T obj in Items ?? (IEnumerable<T>) new T[0])
        this.Enqueue(obj);
    }

    public T[] Dequeue()
    {
      if (this.BatchItems.Count <= 0)
        return (T[]) null;
      U result = default (U);
      List<U> CoalescedItems = new List<U>(this.BatchItems.Count);
      while (this.BatchItems.TryDequeue(out result))
        this.Coalesce(result, CoalescedItems);
      return this.FormatOutput((IEnumerable<U>) CoalescedItems);
    }

    protected abstract U Generate(T Item);

    protected abstract void Coalesce(U BatchItem, List<U> CoalescedItems);

    protected abstract T[] FormatOutput(IEnumerable<U> CoalescedItems);
  }
}
