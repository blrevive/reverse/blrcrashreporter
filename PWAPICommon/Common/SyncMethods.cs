﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.SyncMethods
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System.Runtime.InteropServices;
using System.Threading;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public static class SyncMethods
  {
    public static bool CAS<T>(ref T location, T comparand, T newValue) where T : class => (object) comparand == (object) Interlocked.CompareExchange<T>(ref location, newValue, comparand);
  }
}
