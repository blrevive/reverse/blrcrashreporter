﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Common.PWServerInfoRunning
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace PWAPICommon.Common
{
  [ComVisible(true)]
  public class PWServerInfoRunning : IComparable<PWServerInfoRunning>
  {
    public PWServerInfoFull BaseInfo;
    public PWConnectionBase GameConnection;
    private PWServerInfoUpdate UpdatedInfo;
    public bool bMatchInProgress;
    public string CurrentMapName;
    private Process proc;
    private string procInstanceName;
    private DateTime StartTime;
    private PerformanceCounter ProcCpuCounter;
    private PerformanceCounter ProcRAMCounter;
    private WeightedAverage CPUAverage;
    private WeightedAverage CPUFrameTimeAverage;
    private float _TimeInHighFrameRate;
    private string AdvertisedString;
    public static TimeSpan MinUptimeForWarning = new TimeSpan(0, 0, 30);

    public bool ExitAllowed => this.BaseInfo.BaseInfo.bArbitrated || this.BaseInfo.BaseInfo.Password != "";

    public Process Proc
    {
      get => this.proc;
      set
      {
        this.proc = value;
        this.StartTime = PWServerBase.CurrentTime;
        this.procInstanceName = this.GetInstanceName();
        this.ProcCpuCounter = new PerformanceCounter("Process", "% Processor Time", this.procInstanceName);
        this.ProcRAMCounter = new PerformanceCounter("Process", "Private Bytes", this.procInstanceName);
        this.CPUAverage = new WeightedAverage(10);
        this.CPUFrameTimeAverage = new WeightedAverage(10);
      }
    }

    public float CpuUsage
    {
      get
      {
        try
        {
          if (this.proc != null)
          {
            if (!this.proc.HasExited)
            {
              if (this.ProcCpuCounter != null)
                this.CPUAverage.AddSample(this.ProcCpuCounter.NextValue() / (float) Environment.ProcessorCount);
            }
          }
        }
        catch (Exception ex)
        {
        }
        return this.CPUAverage.Average;
      }
    }

    public float CpuFrameTime
    {
      get => this.CPUFrameTimeAverage.Average;
      set => this.CPUFrameTimeAverage.AddSample(value);
    }

    public float RamUsage
    {
      get
      {
        try
        {
          if (this.ProcRAMCounter != null)
            return (float) ((double) this.ProcRAMCounter.NextValue() / 1024.0 / 1024.0);
        }
        catch (Exception ex)
        {
        }
        return 0.0f;
      }
    }

    public float TimeInHighFrameRate => this._TimeInHighFrameRate;

    public TimeSpan UpTime => PWServerBase.CurrentTime - this.StartTime;

    public byte NumPlayers
    {
      get => this.UpdatedInfo.PlayerNum;
      set => this.UpdatedInfo.PlayerNum = value;
    }

    public PWServerInfoRunning(PWConnectionBase InConn, PWServerInfoFull InBaseInfo)
    {
      this.bMatchInProgress = false;
      this.BaseInfo = InBaseInfo;
      this.GameConnection = InConn;
    }

    public int CompareTo(PWServerInfoRunning other) => this.BaseInfo.ServerID != other.BaseInfo.ServerID ? 1 : 0;

    public string GetAdvertiseString()
    {
      if (this.AdvertisedString == null || this.AdvertisedString == "")
        this.RefreshAdvertisedString();
      return this.AdvertisedString;
    }

    public override string ToString() => this.GetAdvertiseString();

    private void RefreshAdvertisedString()
    {
      this.AdvertisedString = this.BaseInfo.ServerID.ToString() + ":" + this.BaseInfo.BaseInfo.ServerName + ":" + this.BaseInfo.PublicIP + ":" + (object) this.BaseInfo.GamePort + ":" + (object) this.UpdatedInfo.PlayerNum + ":" + (object) this.BaseInfo.BaseInfo.MaxPlayers + ":" + (this.BaseInfo.BaseInfo.Password != "" ? (object) "true" : (object) "false") + ":" + this.BaseInfo.BaseInfo.OwnerName + ":";
      foreach (KeyValuePair<string, string> serverProperty in this.BaseInfo.ServerProperties)
      {
        PWServerInfoRunning serverInfoRunning = this;
        serverInfoRunning.AdvertisedString = serverInfoRunning.AdvertisedString + "," + serverProperty.Key + "=" + serverProperty.Value;
      }
    }

    public void UpdateWith(PWServerInfoUpdate NewInfo, Dictionary<string, string> NewProperties)
    {
      this.UpdatedInfo = NewInfo;
      foreach (KeyValuePair<string, string> newProperty in NewProperties)
        this.BaseInfo.ServerProperties[newProperty.Key] = newProperty.Value;
      this.RefreshAdvertisedString();
    }

    public void UpdateTimeInHighFrameRate(float TimeElapsed) => this._TimeInHighFrameRate += TimeElapsed;

    public void ClearTimeInHighFrameRate() => this._TimeInHighFrameRate = 0.0f;

    private string FormatInstanceName(string processName, int count)
    {
      string empty = string.Empty;
      return count != 0 ? string.Format("{0}#{1}", (object) processName, (object) count) : processName;
    }

    private string GetInstanceName()
    {
      string instanceName = "";
      try
      {
        if (this.proc != null)
        {
          if (!this.proc.HasExited)
          {
            Process[] processesByName = Process.GetProcessesByName(this.proc.ProcessName);
            if (processesByName.Length > 0)
            {
              int count = 0;
              foreach (Process process in processesByName)
              {
                instanceName = this.FormatInstanceName(process.ProcessName, count);
                if (PerformanceCounterCategory.CounterExists("ID Process", "Process"))
                {
                  if ((long) this.proc.Id == new PerformanceCounter("Process", "ID Process", instanceName).RawValue)
                    break;
                }
                ++count;
              }
            }
          }
        }
      }
      catch (Exception ex)
      {
      }
      return instanceName;
    }
  }
}
