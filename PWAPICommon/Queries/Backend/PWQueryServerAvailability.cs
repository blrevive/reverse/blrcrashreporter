﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Backend.PWQueryServerAvailability
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Caches;
using PWAPICommon.Clients;
using System;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Backend
{
  [ComVisible(true)]
  public class PWQueryServerAvailability : PWRequestBase
  {
    private PWLoginCache LoginCache;

    public PWQueryServerAvailability(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_QueryServerAvailability;

    public override bool ParseServerQuery(byte[] InMessage) => true;

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      this.LoginCache = this.OwningServer.GetResource<PWLoginCache>();
      return this.LoginCache != null;
    }

    protected override bool ProcessServerResult()
    {
      if (!this.Successful)
        return false;
      int num1 = 0;
      int num2 = 0;
      PWLoginQueueCache resource = this.OwningServer.GetResource<PWLoginQueueCache>();
      if (resource != null)
      {
        num1 = resource.TotalQueueSize;
        num2 = (int) resource.NewUserWaitTime;
      }
      string ExtraString;
      if (this.LoginCache.WhiteListEnabled)
        ExtraString = this.EnumToString((Enum) PWQueryServerAvailability.EServerAvailability.ESA_Maintenance);
      else if (num1 > 0 || this.LoginCache.ShouldQueueUser)
        ExtraString = this.EnumToString((Enum) PWQueryServerAvailability.EServerAvailability.ESA_Full) + ":" + (object) num1 + ":" + (object) num2;
      else
        ExtraString = this.EnumToString((Enum) PWQueryServerAvailability.EServerAvailability.ESA_Online);
      return this.SendMessage(this.SerializeMessage(ExtraString));
    }

    private enum EServerAvailability
    {
      ESA_Unreachable,
      ESA_Maintenance,
      ESA_Full,
      ESA_Online,
    }
  }
}
