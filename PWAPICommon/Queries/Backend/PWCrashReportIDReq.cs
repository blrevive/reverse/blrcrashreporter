﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Backend.PWCrashReportIDReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;

namespace PWAPICommon.Queries.Backend
{
  [ComVisible(true)]
  public class PWCrashReportIDReq : PWRequestBase
  {
    public string Name;
    public string Email;
    public string Description;
    public string CrashID;
    public string PlayerName;
    public PWMachineID MachineID;
    public string DestinationDir;
    public PWCrashReportIDReq.ECrashReportResult ResultCode;

    public PWCrashReportIDReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWCrashReportIDReq(PWConnectionBase InClient)
      : base(InClient)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_CrashReportID;

    public override bool SubmitClientQuery() => this.SendMessage(this.CompressMessage(this.SerializeSelf()));

    public override bool ParseServerQuery(byte[] InMessage)
    {
      byte[] InMessage1 = this.Serializer.Decompress(InMessage);
      if (InMessage1 != null)
      {
        PWCrashReportIDReq crashReportIdReq = this.DeserializeSelf<PWCrashReportIDReq>(InMessage1);
        if (crashReportIdReq != null)
        {
          this.Name = crashReportIdReq.Name;
          this.Email = crashReportIdReq.Email;
          this.Description = crashReportIdReq.Description;
          this.MachineID = crashReportIdReq.MachineID;
          this.PlayerName = crashReportIdReq.PlayerName;
          this.CrashID = crashReportIdReq.CrashID;
          return true;
        }
      }
      return false;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      string CrashHash = this.HashSignature(this.CrashID);
      this.OwningServer.GetConfigValue<string>("CrashDumpDir", out this.DestinationDir);
      PWCrashReportIDReq crashReportIdReq = this;
      crashReportIdReq.DestinationDir = crashReportIdReq.DestinationDir + "\\" + PWServerBase.Version + "\\" + CrashHash + "\\";
      this.ProcessCrashReportTo(this.DestinationDir, CrashHash);
      return this.ProcessServerResultDefault();
    }

    protected void GetCrashPatternInfo(DirectoryInfo Info, out long LargestSize, out int Count)
    {
      FileInfo[] files = Info.GetFiles("*.dmp");
      LargestSize = 0L;
      Count = 0;
      foreach (FileInfo fileInfo in files)
      {
        if (LargestSize < fileInfo.Length)
          LargestSize = fileInfo.Length;
        ++Count;
      }
    }

    protected virtual bool ProcessCrashReportTo(string DestDir, string CrashHash)
    {
      PWCrashReportIDReq crashReportIdReq = this;
      crashReportIdReq.DestinationDir = crashReportIdReq.DestinationDir + PWServerBase.CurrentTime.ToString("yyyy-MM-dd") + "\\" + Guid.NewGuid().ToString() + "\\";
      if (Directory.Exists(DestDir))
      {
        this.Log("Received Duplicate Crash Report Signature from Name [" + this.Name + "] Email [" + this.Email + "] Hash [" + CrashHash + "]", false, ELoggingLevel.ELL_Informative);
        IEnumerable<string> strings = Directory.EnumerateDirectories(DestDir);
        int OutValue1 = 0;
        this.OwningServer.GetConfigValue<int>("CrashDumpMaxReports", out OutValue1, 5);
        long OutValue2 = 0;
        this.OwningServer.GetConfigValue<long>("CrashDumpFullDumpSizeThreshold", out OutValue2, 10L);
        OutValue2 *= 1048576L;
        int num = 0;
        long val2 = 0;
        foreach (string path in strings)
        {
          foreach (string enumerateDirectory in Directory.EnumerateDirectories(path))
          {
            DirectoryInfo directoryInfo = new DirectoryInfo(enumerateDirectory);
            long LargestSize = 0;
            int Count = 0;
            this.GetCrashPatternInfo(new DirectoryInfo(enumerateDirectory), out LargestSize, out Count);
            val2 = Math.Max(LargestSize, val2);
            num += Count;
          }
        }
        this.ResultCode = val2 >= OutValue2 ? (num >= OutValue1 ? PWCrashReportIDReq.ECrashReportResult.ECRR_Full : PWCrashReportIDReq.ECrashReportResult.ECRR_NeedMore) : PWCrashReportIDReq.ECrashReportResult.ECRR_New;
      }
      else
      {
        this.Log("Received New Crash Report Signature from Name [" + this.Name + "] Email [" + this.Email + "] Hash [" + CrashHash + "]", false, ELoggingLevel.ELL_Informative);
        this.ResultCode = PWCrashReportIDReq.ECrashReportResult.ECRR_New;
      }
      Directory.CreateDirectory(this.DestinationDir);
      string InString = "Full Name: " + this.Name + Environment.NewLine + "Player Name: " + this.PlayerName + Environment.NewLine + "Email: " + this.Email + Environment.NewLine + "Description: " + this.Description + Environment.NewLine + "Hash: " + CrashHash + Environment.NewLine + "Signature: " + Environment.NewLine + this.CrashID + Environment.NewLine + Environment.NewLine;
      return this.OwningServer.SendQuery((PWRequestBase) new PWFileUploadReq()
      {
        OutputFilePath = (this.DestinationDir + "Info.txt"),
        OutputFileData = this.Serializer.SerializeRaw(InString)
      });
    }

    private string HashSignature(string Instring)
    {
      Regex regex1 = new Regex("(0x[0-9a-fA-F]+)");
      Regex regex2 = new Regex("(BLR|FoxGame-Win32-Release|FoxGame-Win32-Debug|FoxGame-Win32-Shipping).exe");
      string[] strArray = Instring.Split(new char[1]{ '\n' }, StringSplitOptions.RemoveEmptyEntries);
      string InString = "";
      foreach (string input in strArray)
      {
        Match match1 = regex1.Match(input);
        Match match2 = regex2.Match(input);
        if (match1.Success && match2.Success)
          InString = InString + match1.Value + Environment.NewLine;
      }
      return this.Serializer.HashStringMD5(InString);
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => true;

    protected override bool ProcessServerResult()
    {
      string ExtraString = this.ResultCode.ToString();
      if (this.ResultCode != PWCrashReportIDReq.ECrashReportResult.ECRR_Full)
        ExtraString = ExtraString + ":" + this.DestinationDir.Replace(":", ";");
      return this.SendMessage(this.SerializeMessage(ExtraString));
    }

    public override bool ParseClientQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      if (strArray == null || strArray.Length < 2 || (!(strArray[0] == "T") || !Enum.TryParse<PWCrashReportIDReq.ECrashReportResult>(strArray[1], out this.ResultCode)))
        return false;
      this.DestinationDir = strArray.Length > 2 ? strArray[2].Replace(";", ":") : "";
      return true;
    }

    public enum ECrashReportResult
    {
      ECRR_New,
      ECRR_NeedMore,
      ECRR_Full,
    }
  }
}
