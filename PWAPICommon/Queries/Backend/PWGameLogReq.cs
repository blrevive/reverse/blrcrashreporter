﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Backend.PWGameLogReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Backend
{
  [ComVisible(true)]
  public class PWGameLogReq : PWRequestBase
  {
    public string LogString;

    public PWGameLogReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWGameLogReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_GameLog;

    public override bool ParseClientQuery(byte[] InMessage)
    {
      this.LogString = this.Serializer.DeserializeToString(InMessage);
      return true;
    }

    public override bool SubmitClientQuery()
    {
      base.SubmitClientQuery();
      return this.ProcessClientResultBase((PWRequestBase) null);
    }
  }
}
