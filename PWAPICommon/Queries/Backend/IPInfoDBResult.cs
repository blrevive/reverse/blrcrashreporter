﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Backend.IPInfoDBResult
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System.Runtime.InteropServices;
using System.Xml.Serialization;

namespace PWAPICommon.Queries.Backend
{
  [XmlRoot("Response")]
  [ComVisible(true)]
  public class IPInfoDBResult
  {
    public string statusCode;
    public string statusMessage;
    public string ipAddress;
    public string countryCode;
    public string countryName;
    public string regionName;
    public string cityName;
    public string zipCode;
    public string latitude;
    public string longitude;
    public string timeZone;
  }
}
