﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Backend.PWQueryIPInfoDBReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Backend
{
  [ComVisible(true)]
  public class PWQueryIPInfoDBReq : PWRequestBase
  {
    public string InIP;
    public IPInfoDBResult OutResult;

    public PWQueryIPInfoDBReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWQueryIPInfoDBReq(PWConnectionBase InConn)
      : base(InConn)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_QueryIPLocation;

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      if (this.InIP == null || this.InIP == "")
        this.InIP = this.Connection.EndPoint;
      this.Log("Getting GeoLoc for " + this.InIP, false, ELoggingLevel.ELL_Informative);
      string OutValue1;
      string OutValue2;
      if (this.InIP == null || !(this.InIP != "") || (!this.OwningServer.GetConfigValue<string>("IPInfoDBURL", out OutValue1) || !this.OwningServer.GetConfigValue<string>("IPInfoDBAPIKey", out OutValue2)))
        return this.ProcessServerResultDefault();
      PWWebReq pwWebReq = new PWWebReq(this.Connection, OutValue1 + "?key=" + OutValue2 + "&ip=" + this.InIP + "&format=xml", false, EWebRequestType.EWRT_GET, (object) null, typeof (IPInfoDBResult));
      pwWebReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return pwWebReq.SubmitServerQuery();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      if (!(ForRequest is PWWebReq pwWebReq) || !pwWebReq.Successful || (pwWebReq.Result == null || !(pwWebReq.Result.GetType() == typeof (IPInfoDBResult))))
        return false;
      this.OutResult = pwWebReq.Result as IPInfoDBResult;
      float result1;
      float result2;
      if (float.TryParse(this.OutResult.latitude, out result1) && float.TryParse(this.OutResult.longitude, out result2))
      {
        this.Connection.GeoLoc = new GeoLocation((double) result1, (double) result2);
        this.Connection.GeoLoc.Country = this.OutResult.countryName;
        this.Connection.GeoLoc.City = this.OutResult.cityName;
        this.Log("GeoLoc for " + this.InIP + " is Lat:" + (object) result1 + " Long:" + (object) result2, false, ELoggingLevel.ELL_Informative);
      }
      return true;
    }

    protected override bool ProcessServerResult() => true;
  }
}
