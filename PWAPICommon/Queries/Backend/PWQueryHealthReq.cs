﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Backend.PWQueryHealthReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Xml.Serialization;

namespace PWAPICommon.Queries.Backend
{
  [ComVisible(true)]
  public class PWQueryHealthReq : PWRequestBase
  {
    public List<string> HealthStrings;
    [XmlIgnore]
    private Dictionary<string, string> HealthDict;

    public override string WebResponse
    {
      get
      {
        string str = "";
        foreach (string healthString in this.HealthStrings)
          str = str + healthString + Environment.NewLine;
        return str;
      }
    }

    [XmlIgnore]
    public Dictionary<string, string> Health => this.HealthDict;

    public PWQueryHealthReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWQueryHealthReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_QueryHealth;

    public override bool SubmitClientQuery()
    {
      base.SubmitClientQuery();
      return this.SendDefaultMessage();
    }

    public override bool ParseServerQuery(byte[] InMessage) => true;

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      this.HealthDict = this.OwningServer.ServerHealth;
      this.HealthStrings = new List<string>(this.HealthDict.Count);
      foreach (KeyValuePair<string, string> keyValuePair in this.HealthDict)
        this.HealthStrings.Add(keyValuePair.Key + "=" + keyValuePair.Value);
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => true;

    protected override bool ProcessServerResult() => this.SendMessage(this.SerializeSelf());

    public override bool ParseClientQuery(byte[] InMessage)
    {
      this.HealthStrings = this.DeserializeSelf<PWQueryHealthReq>(InMessage).HealthStrings;
      this.HealthDict = new Dictionary<string, string>();
      foreach (string healthString in this.HealthStrings)
      {
        char[] separator = new char[1]{ '=' };
        string[] strArray = healthString.Split(separator, StringSplitOptions.RemoveEmptyEntries);
        this.HealthDict[strArray[0]] = strArray[1];
      }
      return true;
    }

    public override bool ParseClientResult(PWRequestBase ForRequest) => true;
  }
}
