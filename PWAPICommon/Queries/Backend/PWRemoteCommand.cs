﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Backend.PWRemoteCommand
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Backend
{
  [ComVisible(true)]
  public class PWRemoteCommand : PWRequestBase
  {
    public string ConsoleCommand;

    public PWRemoteCommand()
      : base((PWConnectionBase) null)
    {
    }

    public PWRemoteCommand(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_RemoteCommand;

    public override bool SubmitClientQuery()
    {
      base.SubmitClientQuery();
      return this.ProcessClientResultBase((PWRequestBase) null);
    }

    public override bool ParseClientResult(PWRequestBase ForRequest) => true;

    protected override bool ProcessClientResult(PWRequestBase ForRequest) => this.SendMessage(this.SerializeMessage(this.ConsoleCommand));
  }
}
