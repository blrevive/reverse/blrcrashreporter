﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Backend.PWFileUploadReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System;
using System.IO;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Backend
{
  [ComVisible(true)]
  public class PWFileUploadReq : PWRequestBase
  {
    public long CurrentReadBytes;
    public long TargetReadBytes;
    public string InputFilePath;
    public byte[] InputFileData;
    public string OutputFilePath;
    public byte[] OutputFileData;
    public int StreamBufferSize = 524288;
    private BinaryReader StreamReader;
    private byte[] StreamBuffer;
    private bool bReceivedStreamChunk;
    private bool bReadComplete;
    private ProcessDelegate ExternalClientResultProcessor;

    public override bool Completed
    {
      get
      {
        if (!base.Completed)
          return false;
        return this.bReadComplete || !this.bReceivedStreamChunk;
      }
    }

    public PWFileUploadReq()
      : this((PWConnectionBase) null)
    {
    }

    public PWFileUploadReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_FileUpload;

    private Stream OpenFileStream(string FileName, FileMode Mode)
    {
      try
      {
        return (Stream) new FileStream(FileName, Mode);
      }
      catch (Exception ex)
      {
        this.Log("Failed to Open File " + FileName + ": " + ex.ToString(), false, ELoggingLevel.ELL_Warnings);
        return (Stream) null;
      }
    }

    private Stream OpenMemoryStream(byte[] FileData)
    {
      try
      {
        return (Stream) new MemoryStream(FileData);
      }
      catch (Exception ex)
      {
        this.Log("Failed to Open Memory Stream: " + ex.ToString(), false, ELoggingLevel.ELL_Warnings);
        return (Stream) null;
      }
    }

    private BinaryReader OpenInputReader()
    {
      Stream input;
      if (this.InputFileData != null)
        input = this.OpenMemoryStream(this.InputFileData);
      else if (!string.IsNullOrEmpty(this.InputFilePath))
      {
        input = this.OpenFileStream(this.InputFilePath, FileMode.Open);
      }
      else
      {
        this.Log("Failed to Open Input Stream: No Target Path/Data", false, ELoggingLevel.ELL_Errors);
        return (BinaryReader) null;
      }
      if (input != null)
      {
        try
        {
          return new BinaryReader(input);
        }
        catch (Exception ex)
        {
          this.Log("Failed to Open Input Stream Writer: " + ex.ToString(), false, ELoggingLevel.ELL_Errors);
          input.Dispose();
        }
      }
      return (BinaryReader) null;
    }

    private BinaryWriter OpenOutputWriter()
    {
      if (!string.IsNullOrEmpty(this.OutputFilePath))
      {
        int startIndex = this.OutputFilePath.LastIndexOf("\\");
        string path = this.OutputFilePath.Remove(startIndex, this.OutputFilePath.Length - startIndex);
        if (!Directory.Exists(path))
          Directory.CreateDirectory(path);
        Stream output = this.OpenFileStream(this.OutputFilePath, FileMode.Append);
        if (output != null)
        {
          try
          {
            return new BinaryWriter(output);
          }
          catch (Exception ex)
          {
            this.Log("Failed to Open Output Stream Writer: " + ex.ToString(), false, ELoggingLevel.ELL_Errors);
            output.Dispose();
          }
        }
      }
      else
        this.Log("Failed to Open Output Stream: No Target Path", false, ELoggingLevel.ELL_Errors);
      return (BinaryWriter) null;
    }

    private bool ReadStreamChunk()
    {
      try
      {
        if (this.StreamReader == null)
        {
          this.StreamReader = this.OpenInputReader();
          if (this.StreamReader == null)
            return false;
          this.TargetReadBytes = this.StreamReader.BaseStream.Length;
        }
        long position = this.StreamReader.BaseStream.Position;
        Array.Resize<byte>(ref this.StreamBuffer, this.StreamBufferSize);
        int newSize = this.StreamReader.Read(this.StreamBuffer, 0, this.StreamBuffer.Length);
        this.CurrentReadBytes += (long) newSize;
        this.bReadComplete = newSize <= 0;
        if (!this.bReadComplete)
        {
          this.Log("Reading Stream Chunk: [" + (object) position + ", " + this.StreamReader.BaseStream.Position.ToString() + "]", false, ELoggingLevel.ELL_Verbose);
          Array.Resize<byte>(ref this.StreamBuffer, newSize);
          return true;
        }
      }
      catch (Exception ex)
      {
        this.Log("Failed to Read File " + this.InputFilePath + ": " + ex.ToString(), false, ELoggingLevel.ELL_Warnings);
      }
      return false;
    }

    private bool WriteStreamChunk()
    {
      try
      {
        using (BinaryWriter binaryWriter = this.OpenOutputWriter())
        {
          if (binaryWriter != null)
          {
            if (this.StreamBuffer == null)
              this.StreamBuffer = this.OutputFileData;
            this.Log("Writing Stream Chunk: [" + this.OutputFilePath + ", 0, " + this.StreamBuffer.Length.ToString() + "]", false, ELoggingLevel.ELL_Verbose);
            binaryWriter.Write(this.StreamBuffer, 0, this.StreamBuffer.Length);
            return true;
          }
        }
      }
      catch (Exception ex)
      {
        this.Log("Failed to Write File " + this.OutputFilePath + ": " + ex.ToString(), false, ELoggingLevel.ELL_Warnings);
      }
      return false;
    }

    public override bool ParseClientQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      if (strArray.Length != 1)
        return false;
      this.bReceivedStreamChunk = strArray[0] == "T";
      return true;
    }

    public override bool SubmitClientQuery()
    {
      base.SubmitClientQuery();
      if (this.StreamReader == null)
      {
        this.ExternalClientResultProcessor = this.ClientResultProcessor;
        this.ClientResultProcessor = (ProcessDelegate) null;
      }
      if (!this.ReadStreamChunk())
        return this.ProcessClientResultBase((PWRequestBase) null);
      byte[] numArray = this.Serializer.Serialize(this.OutputFilePath.Replace(":", ";") + ":");
      byte[] InData = new byte[numArray.Length + this.StreamBuffer.Length];
      numArray.CopyTo((Array) InData, 0);
      this.StreamBuffer.CopyTo((Array) InData, numArray.Length);
      return this.SendMessage(this.CompressMessage(InData));
    }

    public bool ReadNextStreamChunk(PWRequestBase ForRequest) => this.OwningServer.SendQuery((PWRequestBase) this);

    public override bool ParseClientResult(PWRequestBase ForRequest)
    {
      if (this.bReadComplete || !this.bReceivedStreamChunk)
      {
        if (this.StreamReader != null)
        {
          this.StreamReader.Dispose();
          this.StreamReader = (BinaryReader) null;
        }
        this.ClientResultProcessor = this.ExternalClientResultProcessor;
      }
      else
        this.ClientResultProcessor = new ProcessDelegate(this.ReadNextStreamChunk);
      return this.bReadComplete || this.bReceivedStreamChunk;
    }

    protected override bool ProcessClientResult(PWRequestBase ForRequest) => base.ProcessClientResult(ForRequest);

    public override bool ParseServerQuery(byte[] InMessage)
    {
      byte[] BinaryData = this.Serializer.Decompress(InMessage);
      if (BinaryData != null)
      {
        string[] strArray = this.Serializer.Deserialize(BinaryData);
        if (strArray.Length >= 2)
        {
          this.OutputFilePath = strArray[0].Replace(";", ":");
          int sourceIndex = strArray[0].Length + 1;
          if (sourceIndex > 0 && sourceIndex < BinaryData.Length)
          {
            this.StreamBuffer = new byte[BinaryData.Length - sourceIndex];
            Array.Copy((Array) BinaryData, sourceIndex, (Array) this.StreamBuffer, 0, this.StreamBuffer.Length);
            return true;
          }
        }
      }
      return false;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => this.WriteStreamChunk();

    protected override bool ProcessServerResult() => this.Connection == null || this.SendDefaultMessage();
  }
}
