﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Backend.PWQueryPlayerList
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Backend
{
  [ComVisible(true)]
  public class PWQueryPlayerList : PWRequestBase
  {
    public List<PlayerInfo> Players;

    public PWQueryPlayerList()
      : base((PWConnectionBase) null)
    {
    }

    public PWQueryPlayerList(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    public override bool ParseClientQuery(byte[] InMessage)
    {
      this.Players = new List<PlayerInfo>();
      PlayerInfo playerInfo = new PlayerInfo();
      string[] array = this.Serializer.Deserialize(InMessage).OfType<string>().ToArray<string>();
      if (array.Length <= 0 || !(array[0] == "T"))
        return false;
      for (int index = 1; index < array.Length; ++index)
      {
        playerInfo.CharacterName = array[index];
        this.Players.Add(playerInfo);
      }
      return true;
    }

    public override bool SubmitClientQuery()
    {
      base.SubmitClientQuery();
      return this.ProcessClientResultBase((PWRequestBase) null);
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_QueryPlayerList;
  }
}
