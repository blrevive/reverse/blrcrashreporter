﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Offers.PWOfferQueryReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Caches;
using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Wrappers;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Offers
{
  [ComVisible(true)]
  public class PWOfferQueryReq : PWRequestBase
  {
    public List<PWOfferItem> Offers;

    public PWOfferQueryReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWOfferQueryReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_OfferQuery;

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      this.Log("Querying Store Offers", false, ELoggingLevel.ELL_Informative);
      PWOfferCache resource = this.Connection.OwningServer.GetResource<PWOfferCache>();
      if (resource != null && !resource.IsDirty)
        this.Offers = resource.GetCachedItems();
      if (this.Offers != null)
        return this.ProcessServerResultDefault();
      PWSqlItemQueryAllReq<PWOfferItem> sqlItemQueryAllReq = new PWSqlItemQueryAllReq<PWOfferItem>(this.Connection);
      sqlItemQueryAllReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return sqlItemQueryAllReq.SubmitServerQuery();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      if (this.Offers == null && ForRequest != null)
      {
        PWSQLReq pwsqlReq = (PWSQLReq) ForRequest;
        this.Offers = new List<PWOfferItem>();
        if (pwsqlReq.ResponseValues != null && pwsqlReq.ResponseValues.Count > 0)
        {
          for (int index = 0; index < pwsqlReq.ResponseValues.Count; ++index)
          {
            object[] responseValue = (object[]) pwsqlReq.ResponseValues[index];
            PWOfferItem pwOfferItem = new PWOfferItem();
            if (pwOfferItem.ParseFromArray(responseValue))
              this.Offers.Add(pwOfferItem);
          }
        }
      }
      return this.Offers.Count > 0;
    }

    protected override bool ProcessServerResult() => throw new NotImplementedException();

    public override bool SubmitClientQuery() => throw new NotImplementedException();

    protected override bool ProcessClientResult(PWRequestBase ForRequest) => throw new NotImplementedException();
  }
}
