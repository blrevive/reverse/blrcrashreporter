﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Friends.PWFriendRespondReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Queries.Player;
using PWAPICommon.Resources;
using System;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Friends
{
  [ComVisible(true)]
  public class PWFriendRespondReq : PWRequestBase
  {
    private long UserID;
    private long FriendID;
    private bool bAccepted;
    private PWFriendQueryInternalReq QueryReq;
    private PWFriendItem FoundItem;
    private PWFriendUpdateReq UpdateReq;
    private PWProfileQueryBasicReq TheirProfileLookupReq;
    private PWProfileQueryBasicReq MyBasicProfileLookupReq;

    public PWFriendRespondReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWFriendRespondReq(PWConnectionBase InClient)
      : base(InClient)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_FriendRespond;

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      int result;
      if (strArray.Length != 3 || !long.TryParse(strArray[0], out this.UserID) || (!long.TryParse(strArray[1], out this.FriendID) || !int.TryParse(strArray[2], out result)))
        return false;
      this.bAccepted = Convert.ToBoolean(result);
      return true;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      if (this.QueryReq == null)
      {
        this.QueryReq = new PWFriendQueryInternalReq(this.Connection);
        this.QueryReq.UserId = this.UserID;
        this.QueryReq.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
        return this.QueryReq.SubmitServerQuery();
      }
      if (this.QueryReq.Successful && this.FoundItem == null)
      {
        this.FoundItem = this.QueryReq.AllFriends.Find((Predicate<PWFriendItem>) (x => x.OwnerID == this.FriendID && x.FriendID == this.UserID));
        if (this.FoundItem == null || (int) this.FoundItem.State != (int) Convert.ToByte((object) PWFriendItem.FriendState.EFS_Pending))
          return this.ProcessServerResultDefault();
        this.FoundItem.State = this.bAccepted ? Convert.ToByte((object) PWFriendItem.FriendState.EFS_Accepted) : Convert.ToByte((object) PWFriendItem.FriendState.EFS_Denied);
        return this.SubmitServerQuery();
      }
      if (this.TheirProfileLookupReq == null)
      {
        this.TheirProfileLookupReq = new PWProfileQueryBasicReq(this.Connection);
        this.TheirProfileLookupReq.ForUserID = this.FriendID;
        this.TheirProfileLookupReq.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
        return this.TheirProfileLookupReq.SubmitServerQuery();
      }
      if (this.MyBasicProfileLookupReq == null)
      {
        this.MyBasicProfileLookupReq = new PWProfileQueryBasicReq(this.Connection);
        this.MyBasicProfileLookupReq.ForUserID = this.UserID;
        this.MyBasicProfileLookupReq.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
        return this.MyBasicProfileLookupReq.SubmitServerQuery();
      }
      if (this.FoundItem != null && this.UpdateReq == null)
      {
        if (this.bAccepted)
          this.Log("friendaccept userid=" + (object) this.UserID + ", friendid=" + (object) this.FriendID, true, ELoggingLevel.ELL_Verbose);
        else
          this.Log("frienddeny userid=" + (object) this.UserID + ", friendid=" + (object) this.FriendID, true, ELoggingLevel.ELL_Verbose);
        this.UpdateReq = new PWFriendUpdateReq(this.Connection);
        this.UpdateReq.Item = this.FoundItem;
        this.UpdateReq.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
        return this.UpdateReq.SubmitServerQuery();
      }
      if (this.UpdateReq != null && this.UpdateReq.Successful)
      {
        if (this.Connection.OwningServer != null)
        {
          PWSocialServer owningServer = (PWSocialServer) this.Connection.OwningServer;
        }
        PWPresenceCache<SocialStatus> resource = this.OwningServer.GetResource<PWPresenceCache<SocialStatus>>();
        PWFriendItem InUpdatedItem1 = new PWFriendItem();
        InUpdatedItem1.State = this.FoundItem.State;
        InUpdatedItem1.FriendID = this.FoundItem.FriendID;
        InUpdatedItem1.OwnerID = this.FoundItem.OwnerID;
        SocialStatus InOnlineStatus1 = (SocialStatus) null;
        if (resource != null)
          InOnlineStatus1 = resource.GetPresence(this.FriendID);
        InUpdatedItem1.PlayerInfo = new PWPlayerInfo(this.FriendID, InOnlineStatus1);
        InUpdatedItem1.PlayerInfo.CopyFromUserProfile(this.TheirProfileLookupReq.BasicProfile);
        new PWSingleFriendUpdatePushReq(this.Connection, this.UserID, InUpdatedItem1).SubmitServerQuery();
        PWFriendItem InUpdatedItem2 = new PWFriendItem();
        InUpdatedItem2.State = this.FoundItem.State;
        InUpdatedItem2.FriendID = this.FoundItem.FriendID;
        InUpdatedItem2.OwnerID = this.FoundItem.OwnerID;
        SocialStatus InOnlineStatus2 = (SocialStatus) null;
        if (resource != null)
          InOnlineStatus2 = resource.GetPresence(this.UserID);
        InUpdatedItem2.PlayerInfo = new PWPlayerInfo(this.UserID, InOnlineStatus2);
        InUpdatedItem2.PlayerInfo.CopyFromUserProfile(this.MyBasicProfileLookupReq.BasicProfile);
        new PWSingleFriendUpdatePushReq(this.Connection, this.FriendID, InUpdatedItem2).SubmitServerQuery();
      }
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => this.UpdateReq != null && this.UpdateReq.Successful;

    protected override bool ProcessServerResult() => this.SendDefaultMessage();
  }
}
