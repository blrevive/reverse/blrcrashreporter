﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Friends.PWFriendQueryInternalReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Caches;
using PWAPICommon.Clients;
using PWAPICommon.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Friends
{
  [ComVisible(true)]
  public class PWFriendQueryInternalReq : PWRequestBase
  {
    public long UserId;
    public List<PWFriendItem> AllFriends;
    private PWSQLReq SqlReq;

    public PWFriendQueryInternalReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWFriendQueryInternalReq(PWConnectionBase InClient)
      : base(InClient)
    {
    }

    protected override EMessageType GetMessageType() => throw new NotImplementedException();

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      PWPlayerItemCache<List<PWFriendItem>> resource = this.OwningServer.GetResource<PWPlayerItemCache<List<PWFriendItem>>>();
      if (resource != null)
      {
        this.AllFriends = resource.GetCachedItem(this.UserId);
        if (this.AllFriends != null)
          return this.ProcessServerResultDefault();
      }
      SqlCommand sqlCommand = new SqlCommand("SP_Friend_Select_By_OwnerID");
      sqlCommand.CommandType = CommandType.StoredProcedure;
      sqlCommand.AddParameter<long>("@owner ", SqlDbType.BigInt, this.UserId);
      this.SqlReq = new PWSQLReq(sqlCommand, this.Connection);
      this.SqlReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return this.SqlReq.SubmitServerQuery();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      if (this.AllFriends != null)
        return true;
      this.AllFriends = new List<PWFriendItem>();
      if (this.SqlReq == null || this.SqlReq.ResponseValues == null || this.SqlReq.ResponseValues.Count <= 0)
        return false;
      string str = ((object[]) this.SqlReq.ResponseValues[0])[0].ToString();
      if (str.Contains("Failed"))
        return false;
      if (str.Contains("not exists"))
        return true;
      for (int index = 0; index < this.SqlReq.ResponseValues.Count; ++index)
      {
        object[] responseValue = (object[]) this.SqlReq.ResponseValues[index];
        PWFriendItem pwFriendItem = new PWFriendItem();
        if (pwFriendItem.ParseFromArray(responseValue))
          this.AllFriends.Add(pwFriendItem);
      }
      this.OwningServer.GetResource<PWPlayerItemCache<List<PWFriendItem>>>()?.SetCachedItem(this.UserId, this.AllFriends);
      return true;
    }

    protected override bool ProcessServerResult() => this.SendMessage(this.CompressMessage(this.SerializeMessage(this.Serializer.SerializeObject<List<PWFriendItem>>(this.AllFriends))));
  }
}
