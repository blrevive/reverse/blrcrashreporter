﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Friends.PWFriendUpdateReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Caches;
using PWAPICommon.Clients;
using PWAPICommon.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Friends
{
  [ComVisible(true)]
  public class PWFriendUpdateReq : PWRequestBase
  {
    public PWFriendItem Item;

    public PWFriendUpdateReq(PWConnectionBase InClient)
      : base(InClient)
    {
    }

    protected override EMessageType GetMessageType() => throw new NotImplementedException();

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      SqlCommand sqlCommand = new SqlCommand("SP_Friend_Update");
      sqlCommand.CommandType = CommandType.StoredProcedure;
      sqlCommand.AddParameter<byte[]>("@ID ", SqlDbType.VarBinary, this.Item.UniqueID.ToByteArray());
      sqlCommand.AddParameter<long>("@Owner ", SqlDbType.BigInt, this.Item.OwnerID);
      sqlCommand.AddParameter<long>("@Friend ", SqlDbType.BigInt, this.Item.FriendID);
      sqlCommand.AddParameter<DateTime>("@FriendTime ", SqlDbType.DateTime, this.Item.FriendAddTime);
      sqlCommand.AddParameter<byte>("@State ", SqlDbType.TinyInt, this.Item.State);
      PWSQLReq pwsqlReq = new PWSQLReq(sqlCommand, this.Connection);
      pwsqlReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return pwsqlReq.SubmitServerQuery();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      if (!(ForRequest is PWSQLReq pwsqlReq) || !pwsqlReq.Successful)
        return false;
      PWPlayerItemCache<List<PWFriendItem>> resource = this.OwningServer.GetResource<PWPlayerItemCache<List<PWFriendItem>>>();
      if (resource != null)
      {
        resource.MarkDirtyFor(this.Item.OwnerID);
        resource.MarkDirtyFor(this.Item.FriendID);
      }
      return true;
    }

    protected override bool ProcessServerResult() => this.SendDefaultMessage();
  }
}
