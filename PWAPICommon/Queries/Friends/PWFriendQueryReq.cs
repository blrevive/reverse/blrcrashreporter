﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Friends.PWFriendQueryReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Queries.General;
using PWAPICommon.Queries.Player;
using PWAPICommon.Resources;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Friends
{
  [ComVisible(true)]
  public class PWFriendQueryReq : PWSocialReqBase
  {
    public long UserId;
    private PWFriendQueryInternalReq FriendQuery;
    private List<PWProfileQueryBasicReq> LookupRequests;
    private PWCombinedReq BasicLookupReq;

    public PWFriendQueryReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWFriendQueryReq(PWConnectionBase InClient)
      : base(InClient)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_FriendQuery;

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      return strArray.Length == 1 && long.TryParse(strArray[0], out this.UserId) && this.UserId > 0L;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      if (this.FriendQuery == null)
      {
        this.FriendQuery = new PWFriendQueryInternalReq(this.Connection);
        this.FriendQuery.UserId = this.UserId;
        this.FriendQuery.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
        return this.FriendQuery.SubmitServerQuery();
      }
      if (this.FriendQuery.Successful && this.FriendQuery.AllFriends.Count > 0 && this.BasicLookupReq == null)
      {
        PWPresenceCache<SocialStatus> resource = this.OwningServer.GetResource<PWPresenceCache<SocialStatus>>();
        if (this.SocialServer != null && resource != null)
        {
          foreach (PWFriendItem allFriend in this.FriendQuery.AllFriends)
          {
            long num = this.UserId == allFriend.OwnerID ? allFriend.FriendID : allFriend.OwnerID;
            SocialStatus presence = resource.GetPresence(num);
            if (presence != null)
              allFriend.PlayerInfo = new PWPlayerInfo(num, presence);
          }
        }
        this.BasicLookupReq = new PWCombinedReq(this.Connection);
        this.BasicLookupReq.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
        this.LookupRequests = new List<PWProfileQueryBasicReq>(this.FriendQuery.AllFriends.Count);
        foreach (PWFriendItem allFriend in this.FriendQuery.AllFriends)
        {
          PWProfileQueryBasicReq profileQueryBasicReq = new PWProfileQueryBasicReq(this.Connection);
          profileQueryBasicReq.ForUserID = this.UserId == allFriend.OwnerID ? allFriend.FriendID : allFriend.OwnerID;
          this.LookupRequests.Add(profileQueryBasicReq);
          this.BasicLookupReq.AddSubRequest((PWRequestBase) profileQueryBasicReq);
        }
        return this.BasicLookupReq.SubmitServerQuery();
      }
      if (this.BasicLookupReq != null && this.BasicLookupReq.Completed)
      {
        using (List<PWProfileQueryBasicReq>.Enumerator enumerator = this.LookupRequests.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            PWProfileQueryBasicReq subreq = enumerator.Current;
            if (subreq.Successful)
              this.FriendQuery.AllFriends.Find((Predicate<PWFriendItem>) (x => x.FriendID == subreq.ForUserID || x.OwnerID == subreq.ForUserID))?.PlayerInfo.CopyFromUserProfile(subreq.BasicProfile);
          }
        }
      }
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => this.FriendQuery != null && this.FriendQuery.AllFriends != null;

    protected override bool ProcessServerResult() => this.SendMessage(this.CompressMessage(this.SerializeMessage(this.Serializer.SerializeObject<List<PWFriendItem>>(this.FriendQuery.AllFriends))));
  }
}
