﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Friends.PWSingleFriendUpdatePushReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Queries.General;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Friends
{
  [ComVisible(true)]
  public class PWSingleFriendUpdatePushReq : PWNotificationReq<PWFriendItem>
  {
    public PWSingleFriendUpdatePushReq()
    {
    }

    public PWSingleFriendUpdatePushReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    public PWSingleFriendUpdatePushReq(
      PWConnectionBase InConnection,
      long InUserID,
      PWFriendItem InUpdatedItem)
      : this(InConnection, InUserID, new List<PWFriendItem>()
      {
        InUpdatedItem
      })
    {
    }

    public PWSingleFriendUpdatePushReq(
      PWConnectionBase InConnection,
      long InUserID,
      List<PWFriendItem> InUpdatedItems)
      : base(InConnection, InUserID, (IEnumerable<PWFriendItem>) InUpdatedItems)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_FriendUpdate;

    public override object Clone()
    {
      PWSingleFriendUpdatePushReq friendUpdatePushReq = new PWSingleFriendUpdatePushReq();
      friendUpdatePushReq.UserID = this.UserID;
      friendUpdatePushReq.Items = this.Items;
      return (object) friendUpdatePushReq;
    }

    protected override bool ProcessServerResult() => this.SendMessage(this.CompressMessage(this.SerializeMessage(this.Serializer.SerializeObject<PWFriendItem[]>(this.Items))));
  }
}
