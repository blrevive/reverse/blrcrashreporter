﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Chat.PWJoinChatChannel
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Chat
{
  [ComVisible(true)]
  public class PWJoinChatChannel : PWSocialReqBase
  {
    private int ChannelID;
    private string ChannelName;

    public PWJoinChatChannel()
      : base((PWConnectionBase) null)
    {
    }

    public PWJoinChatChannel(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_JoinChatChannel;

    public override bool ParseServerQuery(byte[] InData)
    {
      base.ParseServerQuery(InData);
      if (this.MsgArray.Length != 2)
        return false;
      if (!int.TryParse(Convert.ToString(this.MsgArray[0]), out this.ChannelID))
        this.ChannelID = -1;
      this.ChannelName = Convert.ToString(this.MsgArray[1]);
      return true;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      bool flag;
      if (this.ChannelID != -1)
      {
        flag = this.SocialServer.AddListener(this.ChannelID, ref this.ClientData);
      }
      else
      {
        this.ChannelID = this.SocialServer.FindChannel(this.ChannelName);
        if (this.ChannelID == -1)
          this.ChannelID = this.SocialServer.CreateChannel(this.ChannelName);
        flag = this.SocialServer.AddListener(this.ChannelID, ref this.ClientData);
      }
      if (flag)
        this.Log("join_channel userid=" + Convert.ToString(this.ClientData.PlayerInfo.UserID) + ", roomid=" + (object) this.ChannelID, true, ELoggingLevel.ELL_Verbose);
      return flag;
    }

    protected override bool ProcessServerResult() => this.SendMessage(this.SerializeMessage(Convert.ToString(this.ChannelID) + ":" + this.ChannelName));
  }
}
