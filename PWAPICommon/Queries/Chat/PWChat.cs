﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Chat.PWChat
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Chat
{
  [ComVisible(true)]
  public class PWChat : PWSocialReqBase
  {
    private const byte ContentIndex = 1;
    private bool IsClanChat;
    private bool IsLobbyOrTeamChat;
    private bool IsPartyChat;
    public int ChannelIdent;
    public string ContentMsg;

    public PWChat()
      : base((PWConnectionBase) null)
    {
    }

    public PWChat(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_Chat;

    public override bool ParseServerQuery(byte[] InData)
    {
      base.ParseServerQuery(InData);
      return this.MsgArray.Length >= 2;
    }

    public override bool SubmitClientQuery() => this.SendMessage(this.Serializer.Serialize(this.ChannelIdent.ToString() + ":" + this.ContentMsg));

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      this.ChannelIdent = Convert.ToInt32(this.MsgArray[0]);
      this.ContentMsg = Convert.ToString(this.MsgArray[1]);
      for (int index = 2; index < this.MsgArray.Length; ++index)
      {
        PWChat pwChat = this;
        pwChat.ContentMsg = pwChat.ContentMsg + ":" + Convert.ToString(this.MsgArray[index]);
      }
      if (this.ClientData == null)
      {
        this.Log("Found null client data during chat", false, ELoggingLevel.ELL_Warnings);
        return false;
      }
      string LogMessage;
      if (!this.ProcessChatMessage(out LogMessage))
        return true;
      this.Log(LogMessage, true, ELoggingLevel.ELL_Verbose);
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => true;

    protected override bool ProcessServerResult()
    {
      if (this.IsLobbyOrTeamChat)
        return true;
      byte[] numArray = this.SerializeMessage(this.ChannelIdent.ToString() + ":" + this.ClientData.PlayerInfo.UserName + ":" + this.ContentMsg);
      if (this.IsClanChat)
        this.SendMessageToClanMates(ref numArray);
      else if (this.IsPartyChat)
      {
        if (this.ClientData == null || this.ClientData.Party == null)
          return false;
        this.SocialServer.SendMessageToParty(this.MessageType, ref numArray, this.ClientData.Party);
      }
      else
        this.SocialServer.SendMessageToChannel(this.MessageType, ref numArray, Convert.ToInt32(this.ChannelIdent));
      return true;
    }

    private bool ProcessChatMessage(out string LogMessage)
    {
      string str = this.Encode64(this.ContentMsg);
      LogMessage = "chat userid=" + Convert.ToString(this.ClientData.PlayerInfo.UserID) + ", roomid=";
      if (this.ChannelIdent == Convert.ToInt32((object) ChatMode.CM_CLAN))
      {
        if (this.ClientData.ClanInfo == null)
          return false;
        this.IsClanChat = true;
        ref string local = ref LogMessage;
        local = local + "Clan, clantag=" + this.ClientData.ClanInfo.ClanItem.ClanTag;
      }
      else if (this.ChannelIdent == Convert.ToInt32((object) ChatMode.CM_PARTY))
      {
        this.IsPartyChat = true;
        LogMessage += "Party";
      }
      else if (this.ChannelIdent == Convert.ToInt32((object) ChatMode.CM_GLOBAL))
        LogMessage += "Global";
      else if (this.ChannelIdent == Convert.ToInt32((object) ChatMode.CM_LOBBY))
      {
        this.IsLobbyOrTeamChat = true;
        LogMessage += "Lobby";
      }
      else if (this.ChannelIdent == Convert.ToInt32((object) ChatMode.CM_TEAM))
      {
        this.IsLobbyOrTeamChat = true;
        LogMessage += "Team";
      }
      else if (this.ChannelIdent == Convert.ToInt32((object) ChatMode.CM_HELP))
        LogMessage += "Help";
      else
        LogMessage += (string) (object) this.ChannelIdent;
      ref string local1 = ref LogMessage;
      local1 = local1 + ", content=\"" + str + "\"";
      return true;
    }
  }
}
