﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Chat.PWSocialSetInfo
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using System;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Chat
{
  [ComVisible(true)]
  public class PWSocialSetInfo : PWSocialReqBase
  {
    public PWSocialSetInfo()
      : base((PWConnectionBase) null)
    {
    }

    public PWSocialSetInfo(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    public override bool ParseServerQuery(byte[] InData)
    {
      base.ParseServerQuery(InData);
      if (this.MsgArray.Length == 2)
        return true;
      this.Log("Incorrect parameter count to update social info, ignoring", false, ELoggingLevel.ELL_Warnings);
      return false;
    }

    public override bool SubmitServerQuery()
    {
      if (this.ClientData != null)
      {
        SocialStatus InStatus = new SocialStatus();
        this.Log("Updating Social Info for player " + this.ClientData.ToString(), false, ELoggingLevel.ELL_Verbose);
        InStatus.StatusContext = Convert.ToByte(this.MsgArray[0]);
        InStatus.PresenceLocation = Convert.ToString(this.MsgArray[1]);
        this.ClientData.UpdateLocation(InStatus);
      }
      else
        this.Log("Null client data during social set info", false, ELoggingLevel.ELL_Warnings);
      return true;
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => true;

    protected override bool ProcessServerResult() => true;
  }
}
