﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Chat.PWLeaveChatChannel
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Chat
{
  [ComVisible(true)]
  public class PWLeaveChatChannel : PWSocialReqBase
  {
    private int ChannelID;

    public PWLeaveChatChannel()
      : base((PWConnectionBase) null)
    {
    }

    public PWLeaveChatChannel(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_LeaveChatChannel;

    public override bool ParseServerQuery(byte[] InData)
    {
      base.ParseServerQuery(InData);
      return this.MsgArray.Length == 1 && int.TryParse(Convert.ToString(this.MsgArray[0]), out this.ChannelID);
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => this.SocialServer.RemoveListener(this.ChannelID, ref this.ClientData);

    protected override bool ProcessServerResult() => true;
  }
}
