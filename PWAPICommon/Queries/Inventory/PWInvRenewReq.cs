﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Inventory.PWInvRenewReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Queries.Currency;
using PWAPICommon.Queries.General;
using PWAPICommon.Queries.Store;
using PWAPICommon.Wrappers;
using System;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Inventory
{
  [ComVisible(true)]
  public class PWInvRenewReq : PWRequestBase
  {
    private long userid;
    private Guid ItemGuid;
    private EPurchaseLength PurchaseLength;
    private EPurchaseLocation PurchaseLocation;
    private ECurrencyType Currency;
    private StoreItem PurchasedStoreItem;
    private PWCombinedReq LookupRequest;
    private PWInvQueryReq InvReq;
    private PWStoreQueryUserReq StoreReq;
    private PWInventoryItem CurrentInventoryItem;
    private PWRequestBase FundRemovalRequest;
    private PWSqlItemUpdateReq<PWInventoryItem> InvUpdateRequest;
    private PWGPQueryReq GPRequest;
    private int Cost;
    private EPurchaseResult PurchaseResult;

    public PWInvRenewReq(PWConnectionBase InConnection)
      : base(InConnection)
      => this.PurchaseResult = EPurchaseResult.EPR_UnknownError;

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] stringArray = this.Serializer.DeserializeToStringArray(InMessage);
      return stringArray.Length == 5 && long.TryParse(stringArray[0], out this.userid) && (Guid.TryParse(stringArray[1], out this.ItemGuid) && EnumHelper.TryParse<EPurchaseLength>(stringArray[2], out this.PurchaseLength)) && (EnumHelper.TryParse<EPurchaseLocation>(stringArray[3], out this.PurchaseLocation) && EnumHelper.TryParse<ECurrencyType>(stringArray[4], out this.Currency));
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_InventoryRenew;

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      if (this.LookupRequest == null)
      {
        this.InvReq = new PWInvQueryReq(this.Connection);
        this.InvReq.UserId = this.userid;
        this.StoreReq = new PWStoreQueryUserReq(this.Connection);
        this.StoreReq.UserId = this.userid;
        this.StoreReq.UserType = (byte) 0;
        this.StoreReq.Filter = "";
        this.LookupRequest = new PWCombinedReq(this.Connection);
        this.LookupRequest.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
        this.LookupRequest.AddSubRequest((PWRequestBase) this.InvReq);
        this.LookupRequest.AddSubRequest((PWRequestBase) this.StoreReq);
        if (this.Currency == ECurrencyType.EC_GP)
        {
          this.GPRequest = new PWGPQueryReq();
          this.GPRequest.UserId = this.userid;
          this.LookupRequest.AddSubRequest((PWRequestBase) this.GPRequest);
        }
        return this.LookupRequest.SubmitServerQuery();
      }
      if (this.LookupRequest.Successful && this.CurrentInventoryItem == null && (this.PurchasedStoreItem == (StoreItem) null && this.InvReq.Successful))
      {
        this.CurrentInventoryItem = this.InvReq.ReadInventory.Items.Find((Predicate<PWInventoryItem>) (x => x.InstanceId == this.ItemGuid));
        if (this.CurrentInventoryItem == null)
        {
          this.PurchaseResult = EPurchaseResult.EPR_InvalidItem;
          return this.ProcessServerResultDefault();
        }
        this.PurchasedStoreItem = this.StoreReq.Items.Find((Predicate<StoreItem>) (x => x.ItemGuid == this.CurrentInventoryItem.StoreItemId));
        if (this.PurchasedStoreItem == (StoreItem) null)
        {
          this.Log("Store Item not found, error!", false, ELoggingLevel.ELL_Warnings);
          this.PurchaseResult = EPurchaseResult.EPR_InvalidItem;
          return this.ProcessServerResultDefault();
        }
        if (this.PurchasedStoreItem.ItemId != this.CurrentInventoryItem.ItemId)
          this.PurchasedStoreItem = this.StoreReq.Items.Find((Predicate<StoreItem>) (x => x.ItemId == this.CurrentInventoryItem.ItemId && x.ActivationType != (byte) 3 && x.ActivationType != (byte) 4));
        if (!(this.PurchasedStoreItem == (StoreItem) null))
          return this.SubmitServerQuery();
        this.Log("Store Item not found, error!", false, ELoggingLevel.ELL_Warnings);
        this.PurchaseResult = EPurchaseResult.EPR_InvalidItem;
        return this.ProcessServerResultDefault();
      }
      if (this.CurrentInventoryItem != null && this.PurchasedStoreItem != (StoreItem) null && this.FundRemovalRequest == null)
      {
        this.Log("Purchase Item Validating with Store", false, ELoggingLevel.ELL_Verbose);
        this.PurchaseResult = this.PurchasedStoreItem.ValdiateRenewal(this.InvReq.ReadInventory, this.PurchaseLength, this.Currency);
        switch (this.PurchaseResult)
        {
          case EPurchaseResult.EPR_Success:
            this.Cost = this.PurchasedStoreItem.CalculateCostFor(this.PurchaseLength, this.Currency, 1);
            switch (this.Currency)
            {
              case ECurrencyType.EC_GP:
                PWGPRemoveReq pwgpRemoveReq = new PWGPRemoveReq(this.Connection);
                pwgpRemoveReq.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
                pwgpRemoveReq.UserId = this.userid;
                pwgpRemoveReq.Amount = this.Cost;
                this.FundRemovalRequest = (PWRequestBase) pwgpRemoveReq;
                break;
              case ECurrencyType.EC_ZP:
                PWZPRemoveReq pwzpRemoveReq = new PWZPRemoveReq(this.Connection);
                pwzpRemoveReq.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
                pwzpRemoveReq.userid = this.userid;
                pwzpRemoveReq.amount = this.Cost;
                pwzpRemoveReq.count = 1;
                pwzpRemoveReq.server = this.Connection.OwningServer.PWServerID;
                pwzpRemoveReq.game = this.Connection.OwningServer.PWGameID;
                pwzpRemoveReq.itemid = this.CurrentInventoryItem.ItemId;
                pwzpRemoveReq.uniqueid = this.CurrentInventoryItem.InstanceId.ToString();
                pwzpRemoveReq.charid = 0;
                this.FundRemovalRequest = (PWRequestBase) pwzpRemoveReq;
                break;
            }
            this.Log("Renew Item starting Fund Removal Request of" + (object) this.Cost + this.Currency.ToString(), false, ELoggingLevel.ELL_Verbose);
            return this.FundRemovalRequest.SubmitServerQuery();
          case EPurchaseResult.EPR_InvalidLength:
            this.Log("Invalid Renewal Parameters for Item " + this.PurchasedStoreItem.ItemId.ToString() + ": " + this.PurchaseLength.ToString() + ", " + this.Currency.ToString(), false, ELoggingLevel.ELL_Warnings);
            return this.ProcessServerResultDefault();
          default:
            this.Log("Store Item Renewal Failed for Unknown or Unhandled Reason: " + this.PurchaseResult.ToString(), false, ELoggingLevel.ELL_Warnings);
            return this.ProcessServerResultDefault();
        }
      }
      else
      {
        if (this.FundRemovalRequest != null && this.InvUpdateRequest == null)
        {
          if (this.FundRemovalRequest.Successful)
          {
            this.Log("Renew Item Fund Removal succeeded, starting Inventory Update", false, ELoggingLevel.ELL_Verbose);
            this.InvUpdateRequest = new PWSqlItemUpdateReq<PWInventoryItem>(this.Connection);
            this.InvUpdateRequest.Item = this.CurrentInventoryItem;
            this.CurrentInventoryItem.ExpirationDate = StoreItem.GetExpirationDate(this.PurchaseLength);
            this.CurrentInventoryItem.PurchaseDuration = (byte) this.PurchaseLength;
            this.InvUpdateRequest.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
            return this.InvUpdateRequest.SubmitServerQuery();
          }
          this.Log("Renew Item Error, sending Failed Response", false, ELoggingLevel.ELL_Verbose);
          this.PurchaseResult = EPurchaseResult.EPR_InsufficientFunds;
          return this.ProcessServerResultDefault();
        }
        this.LogRenewItem();
        this.PurchaseResult = EPurchaseResult.EPR_Success;
        return this.ProcessServerResultDefault();
      }
    }

    private void LogRenewItem()
    {
      string str = "renewitem" + " serverid=" + (object) this.Connection.OwningServer.PWServerID + " userid=" + (object) this.userid + " itemid=" + (object) this.ItemGuid + " unlockid=" + (object) this.CurrentInventoryItem.ItemId + " time=" + (object) this.PurchaseLength + " location=" + (object) this.PurchaseLocation + " usegp=" + (this.Currency == ECurrencyType.EC_GP ? "true" : "false") + " cost=" + (object) this.Cost;
      int num = -1;
      if (this.Currency == ECurrencyType.EC_GP)
        num = this.GPRequest.Funds - this.Cost;
      else if (this.Currency == ECurrencyType.EC_ZP)
        num = (this.FundRemovalRequest as PWZPRemoveReq).TotalFunds;
      this.Log(str + " balance=" + (object) num, true, ELoggingLevel.ELL_Verbose);
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => this.FundRemovalRequest != null && this.InvUpdateRequest != null && this.PurchaseResult == EPurchaseResult.EPR_Success;

    protected override bool ProcessServerResult()
    {
      if (this.Successful)
      {
        PWTransactionAddReq transactionAddReq = new PWTransactionAddReq(this.Connection);
        transactionAddReq.Item = new PWTransaction(this.userid, this.Currency == ECurrencyType.EC_GP ? ETransactionType.ETT_RenewGP : ETransactionType.ETT_RenewZP, this.PurchaseLength, this.CurrentInventoryItem.InstanceId);
        transactionAddReq.SubmitServerQuery();
        new PWInvUpdatedReq(this.Connection, this.userid, this.CurrentInventoryItem).SubmitServerQuery();
      }
      return this.SendMessage(this.SerializeMessage(((byte) this.PurchaseResult).ToString() + ":" + this.ItemGuid.ToString()));
    }
  }
}
