﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Inventory.PWInvFindReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Queries.General;
using PWAPICommon.Wrappers;
using System;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Inventory
{
  [ComVisible(true)]
  public class PWInvFindReq : PWRequestBase
  {
    public long OwnerID;
    public Guid GuidToFind;
    public StoreItem FoundStoreItem;
    public PWInventoryItem FoundInvItem;
    private PWSqlItemQueryAllReq<StoreItem> StoreReq;
    private PWInvQueryReq InvReq;
    private PWCombinedReq LookupReq;

    public PWInvFindReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWInvFindReq(PWConnectionBase InConn)
      : base(InConn)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_FindInvItem;

    public override bool SubmitClientQuery() => this.SendMessage(this.SerializeSelf());

    public override bool ParseServerQuery(byte[] InMessage)
    {
      PWInvFindReq pwInvFindReq = this.DeserializeSelf<PWInvFindReq>(InMessage);
      if (pwInvFindReq == null)
        return false;
      this.OwnerID = pwInvFindReq.OwnerID;
      this.GuidToFind = pwInvFindReq.GuidToFind;
      return true;
    }

    public override bool ParseClientQuery(byte[] InMessage)
    {
      PWInvFindReq pwInvFindReq = this.DeserializeSelf<PWInvFindReq>(InMessage);
      if (pwInvFindReq == null)
        return false;
      this.OwnerID = pwInvFindReq.OwnerID;
      this.GuidToFind = pwInvFindReq.GuidToFind;
      this.FoundInvItem = pwInvFindReq.FoundInvItem;
      this.FoundStoreItem = pwInvFindReq.FoundStoreItem;
      return true;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      if (this.LookupReq == null)
      {
        this.InvReq = new PWInvQueryReq(this.Connection);
        this.InvReq.UserId = this.OwnerID;
        this.StoreReq = new PWSqlItemQueryAllReq<StoreItem>(this.Connection);
        this.LookupReq = new PWCombinedReq(this.Connection);
        this.LookupReq.AddSubRequest((PWRequestBase) this.InvReq);
        this.LookupReq.AddSubRequest((PWRequestBase) this.StoreReq);
        this.LookupReq.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
        return this.LookupReq.SubmitServerQuery();
      }
      if (this.LookupReq.Successful)
      {
        this.FoundInvItem = this.InvReq.ReadInventory.Items.Find((Predicate<PWInventoryItem>) (x => x.InstanceId == this.GuidToFind));
        if (this.FoundInvItem != null)
          this.FoundStoreItem = this.StoreReq.ResponseItems.Find((Predicate<StoreItem>) (x => x.ItemGuid == this.FoundInvItem.StoreItemId));
      }
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => this.FoundStoreItem != (StoreItem) null && this.FoundInvItem != null;

    protected override bool ProcessServerResult() => this.SendMessage(this.SerializeSelf());
  }
}
