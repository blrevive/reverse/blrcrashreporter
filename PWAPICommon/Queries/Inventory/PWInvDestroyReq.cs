﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Inventory.PWInvDestroyReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Wrappers;
using System;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Inventory
{
  [ComVisible(true)]
  public class PWInvDestroyReq : PWRequestBase
  {
    public long UserID;
    public Guid GuidToRemove;
    public PWInventoryItem ItemToRemove;
    private PWSqlItemRemoveReq<PWInventoryItem> RemoveQuery;

    public PWInvDestroyReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWInvDestroyReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_InventoryDestroy;

    public override bool ParseClientQuery(byte[] InMessage)
    {
      string[] stringArray = this.Serializer.DeserializeToStringArray(InMessage);
      return stringArray.Length == 2 && stringArray[0] == "T" && Guid.TryParse(stringArray[1], out this.GuidToRemove);
    }

    public override bool SubmitClientQuery() => this.Connection.SendMessage(this.GetMessageType(), this.Serializer.SerializeRaw(this.UserID.ToString() + ":" + this.GuidToRemove.ToString()));

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] stringArray = this.Serializer.DeserializeToStringArray(InMessage);
      return stringArray.Length == 2 && long.TryParse(stringArray[0], out this.UserID) && Guid.TryParse(stringArray[1], out this.GuidToRemove);
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      if (this.RemoveQuery == null)
      {
        if (this.ItemToRemove == null)
        {
          this.ItemToRemove = new PWInventoryItem();
          this.ItemToRemove.UserId = this.UserID;
          this.ItemToRemove.InstanceId = this.GuidToRemove;
        }
        this.RemoveQuery = new PWSqlItemRemoveReq<PWInventoryItem>(this.Connection);
        this.RemoveQuery.Item = this.ItemToRemove;
        this.RemoveQuery.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
        return this.RemoveQuery.SubmitServerQuery();
      }
      if (this.RemoveQuery == null || !this.RemoveQuery.Completed || !this.RemoveQuery.Successful)
        return this.ProcessServerResultDefault();
      PWTransactionAddReq transactionAddReq = new PWTransactionAddReq(this.Connection);
      transactionAddReq.Item = new PWTransaction(this.UserID, ETransactionType.ETT_Destroy, this.GuidToRemove);
      transactionAddReq.SubmitServerQuery();
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      if (this.RemoveQuery == null || !this.RemoveQuery.Successful)
        return false;
      this.ItemToRemove.UserId = PWServerBase.AdminUserID;
      return true;
    }

    protected override bool ProcessServerResult()
    {
      if (this.Successful)
        new PWInvUpdatedReq(this.Connection, this.UserID, this.ItemToRemove).SubmitServerQuery();
      return this.SendMessage(this.SerializeMessage(this.GuidToRemove.ToString()));
    }
  }
}
