﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.PWSocialReqBase
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using System;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries
{
  [ComVisible(true)]
  public class PWSocialReqBase : PWRequestBase
  {
    protected PWSocialServer SocialServer;
    protected SocialLoginData ClientData;
    protected object[] MsgArray;

    public PWSocialReqBase()
      : base((PWConnectionBase) null)
    {
    }

    public PWSocialReqBase(PWConnectionBase InConnection)
      : base(InConnection)
      => this.StartUpReq();

    protected virtual void StartUpReq()
    {
      try
      {
        this.SocialServer = this.OwningServer as PWSocialServer;
      }
      catch (InvalidCastException ex)
      {
        this.SocialServer = (PWSocialServer) null;
      }
      if (this.SocialServer == null || this.ClientData != null)
        return;
      this.ClientData = this.SocialServer.GetLoginData(this.Connection);
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_Invalid;

    public override bool ParseServerQuery(byte[] InData)
    {
      this.MsgArray = (object[]) this.Serializer.Deserialize(InData);
      return true;
    }

    public override bool SubmitServerQuery() => base.SubmitServerQuery();

    protected void SendMessageToClanMates(ref byte[] Message)
    {
      byte[] FinalBlob = (byte[]) null;
      MessageSerializer.Encode(this.MessageType, Message, out FinalBlob);
      if (this.ClientData.ClanChat == null)
      {
        this.Log("ClanChat doesn't exist when trying to send clan chat, trying to repair", false, ELoggingLevel.ELL_Verbose);
        this.Log("User info: " + this.PrintClientInfo(), false, ELoggingLevel.ELL_Verbose);
        if (this.ClientData.ClanInfo != null)
        {
          lock (this.ClientData)
            this.SocialServer.AddClanListener(this.ClientData);
          if (this.ClientData.ClanChat != null)
          {
            this.Log("Clan chat found and issue repaired.", false, ELoggingLevel.ELL_Verbose);
          }
          else
          {
            this.Log("Client attempted to clan chat, appears to have valid clan info, but cannot find clan chat.", false, ELoggingLevel.ELL_Errors);
            return;
          }
        }
        else
        {
          this.Log("Client attempted to send clan chat, but has no clan info nor clan chat.", false, ELoggingLevel.ELL_Errors);
          return;
        }
      }
      foreach (SocialLoginData listener in this.ClientData.ClanChat.Listeners)
        listener.Connection.SendEncodedMessage(this.MessageType, FinalBlob);
    }

    protected string Encode64(string ToEncode) => Convert.ToBase64String(this.Serializer.Serialize(ToEncode));

    protected string Decode64(string ToDecode) => this.Serializer.DeserializeToString(Convert.FromBase64String(ToDecode));

    protected string PrintClientInfo()
    {
      string str = "No player info could be retrieved (ClientData and/or PlayerInfo was null)";
      if (this.ClientData != null && this.ClientData.PlayerInfo != null)
        str = "Username: " + this.ClientData.PlayerInfo.UserName + " UserID: " + (object) this.ClientData.PlayerInfo.UserID;
      if (this.ClientData.ClanInfo != null)
        str = str + " ClanName: " + this.ClientData.ClanInfo.ClanItem.ClanName + " ClanTag: " + this.ClientData.ClanInfo.ClanItem.ClanTag + " ClanID " + (object) this.ClientData.ClanInfo.ClanItem.ClanID;
      return str;
    }
  }
}
