﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.PWTransaction
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Common;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries
{
  [ComVisible(true)]
  public class PWTransaction : 
    PWSqlItemBase<PWTransaction>,
    IPWSqlItemKeyed<long>,
    IComparable<PWTransaction>
  {
    public DateTime Time;
    public Guid UniqueId;
    public long Instigator;
    public Guid TargetItem;
    public ETransactionType Type;
    public EPurchaseLength Duration;
    public long RecipientUser;
    public Guid RecipientItem;
    public int RecipientItemID;
    public int TargetItemID;

    public PWTransaction()
    {
      this.Time = PWServerBase.CurrentTime;
      this.UniqueId = Guid.NewGuid();
      this.Duration = EPurchaseLength.EPL_Invalid;
      this.RecipientItemID = -1;
      this.TargetItemID = -1;
    }

    public PWTransaction(long InInstigator, ETransactionType InType)
      : this()
    {
      this.Instigator = InInstigator;
      this.Type = InType;
    }

    public PWTransaction(long InInstigator, ETransactionType InType, Guid InTargetItem)
      : this(InInstigator, InType)
      => this.TargetItem = InTargetItem;

    public PWTransaction(
      long InInstigator,
      ETransactionType InType,
      EPurchaseLength InDuration,
      Guid InTargetItem)
      : this(InInstigator, InType)
    {
      this.TargetItem = InTargetItem;
      this.Duration = InDuration;
    }

    public PWTransaction(
      long InInstigator,
      ETransactionType InType,
      Guid InTargetItem,
      long InRecipientUser)
      : this(InInstigator, InType)
    {
      this.TargetItem = InTargetItem;
      this.RecipientUser = InRecipientUser;
    }

    public PWTransaction(
      long InInstigator,
      ETransactionType InType,
      EPurchaseLength InDuration,
      Guid InTargetItem,
      long InRecipientUser)
      : this(InInstigator, InType)
    {
      this.TargetItem = InTargetItem;
      this.RecipientUser = InRecipientUser;
      this.Duration = InDuration;
    }

    public PWTransaction(
      long InInstigator,
      ETransactionType InType,
      Guid InTargetItem,
      Guid InRecipientItem)
      : this(InInstigator, InType)
    {
      this.TargetItem = InTargetItem;
      this.RecipientItem = InRecipientItem;
    }

    public PWTransaction(
      long InInstigator,
      ETransactionType InType,
      EPurchaseLength InDuration,
      Guid InTargetItem,
      Guid InRecipientItem)
      : this(InInstigator, InType)
    {
      this.Duration = InDuration;
      this.TargetItem = InTargetItem;
      this.RecipientItem = InRecipientItem;
    }

    public override SqlCommand GetCommand(ESqlItemOp SqlOp, ref string LogOutput)
    {
      SqlCommand command;
      switch (SqlOp)
      {
        case ESqlItemOp.ESIO_Add:
          command = new SqlCommand("SP_Transact_Add");
          command.CommandType = CommandType.StoredProcedure;
          command.AddParameter<byte[]>("@ID", SqlDbType.VarBinary, this.UniqueId.ToByteArray());
          command.AddParameter<long>("@Instigator", SqlDbType.BigInt, this.Instigator);
          command.AddParameter<long>("@RecipientUser", SqlDbType.BigInt, this.RecipientUser);
          command.AddParameter<DateTime>("@Time", SqlDbType.DateTime, this.Time);
          command.AddParameter<byte[]>("@TargetItem", SqlDbType.VarBinary, this.TargetItem.ToByteArray());
          command.AddParameter<int>("@TargetItemID", SqlDbType.Int, this.TargetItemID);
          command.AddParameter<byte[]>("@RecipientItem", SqlDbType.VarBinary, this.RecipientItem.ToByteArray());
          command.AddParameter<int>("@RecipientItemID", SqlDbType.Int, this.RecipientItemID);
          command.AddParameter<byte>("@Type", SqlDbType.TinyInt, (byte) this.Type);
          command.AddParameter<byte>("@Duration", SqlDbType.TinyInt, (byte) this.Duration);
          LogOutput = "Adding Transaction Item [" + this.Instigator.ToString() + ", " + this.UniqueId.ToString() + "]";
          break;
        case ESqlItemOp.ESIO_QueryAll:
          command = new SqlCommand("SP_Transact_Select_All");
          command.CommandType = CommandType.StoredProcedure;
          LogOutput = "Querying Transaction Items [All]";
          break;
        default:
          command = base.GetCommand(SqlOp, ref LogOutput);
          break;
      }
      return command;
    }

    public SqlCommand GetCommand(ESqlItemOp SqlOp, long Key, ref string LogOutput)
    {
      SqlCommand command = (SqlCommand) null;
      if (SqlOp == ESqlItemOp.ESIO_QueryKeyed)
      {
        command = new SqlCommand("SP_Transact_Select_By_UserID");
        command.CommandType = CommandType.StoredProcedure;
        command.AddParameter<long>("@Instigator", SqlDbType.BigInt, Key);
        LogOutput = "Querying Transaction Items [" + Key.ToString() + "]";
      }
      return command;
    }

    private EPurchaseLength ToDuration(byte InValue)
    {
      switch (InValue)
      {
        case 0:
          return EPurchaseLength.EPL_OneDay;
        case 1:
          return EPurchaseLength.EPL_ThreeDay;
        case 2:
          return EPurchaseLength.EPL_SevenDay;
        case 3:
          return EPurchaseLength.EPL_ThirtyDay;
        case 4:
          return EPurchaseLength.EPL_NinetyDay;
        case 5:
          return EPurchaseLength.EPL_Perm;
        default:
          return EPurchaseLength.EPL_OneDay;
      }
    }

    public override bool ParseFromArray(object[] InValues)
    {
      this.Time = (DateTime) InValues[0];
      this.UniqueId = new Guid((byte[]) InValues[1]);
      this.Instigator = (long) InValues[2];
      this.TargetItem = new Guid((byte[]) InValues[3]);
      this.Type = (ETransactionType) (byte) InValues[4];
      this.RecipientUser = (long) InValues[5];
      this.RecipientItem = new Guid((byte[]) InValues[6]);
      this.Duration = InValues[7].GetType() == typeof (byte) ? this.ToDuration((byte) InValues[7]) : this.ToDuration((byte) 0);
      if (InValues[8].GetType() == typeof (int))
        this.TargetItemID = (int) InValues[8];
      if (InValues[9].GetType() == typeof (int))
        this.RecipientItemID = (int) InValues[9];
      return true;
    }

    public int CompareTo(PWTransaction Other)
    {
      if (this.Time > Other.Time)
        return 1;
      return this.Time < Other.Time ? -1 : 0;
    }
  }
}
