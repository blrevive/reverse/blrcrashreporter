﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.PWFuseInventory
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Queries.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries
{
  [ComVisible(true)]
  public class PWFuseInventory : PWRequestBase
  {
    private FuseInventoryHandlerReqBase FusionHandler;
    private List<PWFuseInventory.PWFuseInventoryHandler> FusionHandlers;
    private PWInvQueryReq InvQueryReq;
    public long ForUserID;
    private bool bFuseErrorFound;
    private Guid InventoryGuidA;
    private Guid InventoryGuidB;
    private string AdditionalFuseInfo;
    public PWInventoryItem InventoryItemA;
    public PWInventoryItem InventoryItemB;
    public Guid ResultingInventoryGuid;

    public PWFuseInventory()
      : base((PWConnectionBase) null)
    {
    }

    public PWFuseInventory(PWConnectionBase InClient)
      : base(InClient)
    {
    }

    protected bool AddFusionHandler(int ItemIdA, int ItemIdB, Type RequestType) => this.AddFusionHandler(ItemIdA, ItemIdA, ItemIdB, ItemIdB, RequestType);

    protected bool AddFusionHandler(
      int ItemIdBeginA,
      int ItemIdEndA,
      int ItemIdBeginB,
      int ItemIdEndB,
      Type RequestType)
    {
      if (this.FusionHandlers == null)
        this.FusionHandlers = new List<PWFuseInventory.PWFuseInventoryHandler>();
      if (this.FusionHandlers != null && !this.FusionHandlers.Any<PWFuseInventory.PWFuseInventoryHandler>((Func<PWFuseInventory.PWFuseInventoryHandler, bool>) (x => x.Intersects(ItemIdBeginA, ItemIdEndA, ItemIdBeginB, ItemIdEndB))))
      {
        this.FusionHandlers.Add(new PWFuseInventory.PWFuseInventoryHandler(ItemIdBeginA, ItemIdEndA, ItemIdBeginB, ItemIdEndB, RequestType));
        return true;
      }
      this.Log("Colliding fusion handler range: [" + (object) ItemIdBeginA + ", " + (object) ItemIdEndA + "] - [" + (object) ItemIdBeginB + ", " + (object) ItemIdEndB + "]", false, ELoggingLevel.ELL_Errors);
      return false;
    }

    protected PWFuseInventory.PWFuseInventoryHandler GetFusionHandler(
      int ItemIdA,
      int ItemIdB)
    {
      try
      {
        if (this.FusionHandlers != null)
          return this.FusionHandlers.SingleOrDefault<PWFuseInventory.PWFuseInventoryHandler>((Func<PWFuseInventory.PWFuseInventoryHandler, bool>) (x => x.Contained(ItemIdA, ItemIdB)));
      }
      catch (InvalidOperationException ex)
      {
        this.Log("Colliding fusion handler ranges!", false, ELoggingLevel.ELL_Errors);
      }
      return (PWFuseInventory.PWFuseInventoryHandler) null;
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_FuseInventory;

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      if (strArray.Length < 3 || strArray.Length > 4 || (!long.TryParse(strArray[0], out this.ForUserID) || !Guid.TryParse(strArray[1], out this.InventoryGuidA)) || !Guid.TryParse(strArray[2], out this.InventoryGuidB))
        return false;
      this.AdditionalFuseInfo = strArray.Length == 4 ? strArray[3] : "";
      return true;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      this.InvQueryReq = new PWInvQueryReq(this.Connection);
      this.InvQueryReq.UserId = this.ForUserID;
      this.InvQueryReq.ServerResultProcessor = new ProcessDelegate(this.OnInventoryQueryComplete);
      this.Log("Query inventory for fuse inv", false, ELoggingLevel.ELL_Informative);
      return this.InvQueryReq.SubmitServerQuery();
    }

    private bool OnInventoryQueryComplete(PWRequestBase ForRequest)
    {
      this.InventoryItemA = this.InvQueryReq.ReadInventory.Items.Find((Predicate<PWInventoryItem>) (x => x.InstanceId == this.InventoryGuidA));
      this.InventoryItemB = this.InvQueryReq.ReadInventory.Items.Find((Predicate<PWInventoryItem>) (x => x.InstanceId == this.InventoryGuidB));
      this.Log("Query inventory for fuse inv done", false, ELoggingLevel.ELL_Informative);
      if (this.InventoryItemA == null)
      {
        this.Log("Failed to Find Inventory Item A to fuse", false, ELoggingLevel.ELL_Informative);
        this.bFuseErrorFound = true;
        this.ProcessServerResultDefault();
        return false;
      }
      if (this.InventoryItemB == null)
      {
        this.Log("Failed to Find Inventory Item B to fuse", false, ELoggingLevel.ELL_Informative);
        this.bFuseErrorFound = true;
        this.ProcessServerResultDefault();
        return false;
      }
      this.AddFusionHandler(2000, 3999, 2000, 3999, typeof (PWFuseDataNodesReq));
      PWFuseInventory.PWFuseInventoryHandler fusionHandler = this.GetFusionHandler(this.InventoryItemA.ItemId, this.InventoryItemB.ItemId);
      if (fusionHandler != null)
      {
        this.FusionHandler = (FuseInventoryHandlerReqBase) Activator.CreateInstance(fusionHandler.HandlerType, (object) this.Connection, (object) this.ForUserID, (object) this.InventoryItemA, (object) this.InventoryItemB, (object) this.AdditionalFuseInfo);
        this.FusionHandler.ServerResultProcessor = new ProcessDelegate(this.OnFusionComplete);
        this.FusionHandler.SubmitServerQuery();
      }
      return true;
    }

    private bool OnFusionComplete(PWRequestBase ForRequest)
    {
      this.ResultingInventoryGuid = this.FusionHandler.ResultingInventoryGuid;
      this.Log("PWFuseInventory complete", false, ELoggingLevel.ELL_Informative);
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForReq)
    {
      if (this.bFuseErrorFound || this.FusionHandler != null && this.FusionHandler.Completed && this.FusionHandler.Successful)
        this.Log("Parsing server result for fuse inventory. Finished", false, ELoggingLevel.ELL_Informative);
      else
        this.Log("Parsing server result for fuse inventory. Not finished", false, ELoggingLevel.ELL_Informative);
      if (this.bFuseErrorFound)
        return true;
      return this.FusionHandler != null && this.FusionHandler.Completed && this.FusionHandler.Successful;
    }

    protected override bool ProcessServerResult()
    {
      this.Log("Fuse inventory processing server result", false, ELoggingLevel.ELL_Informative);
      return this.SendMessage(this.SerializeMessage(this.ResultingInventoryGuid.ToString()));
    }

    protected class PWFuseInventoryHandler
    {
      public Type HandlerType;
      private int RangeBeginA;
      private int RangeEndA;
      private int RangeBeginB;
      private int RangeEndB;

      public PWFuseInventoryHandler(int ItemIdA, int ItemIDB, Type RequestType)
        : this(ItemIdA, ItemIdA, ItemIDB, ItemIDB, RequestType)
      {
      }

      public PWFuseInventoryHandler(
        int ItemIdBeginA,
        int ItemIdEndA,
        int ItemIdBeginB,
        int ItemIdEndB,
        Type RequestType)
      {
        this.RangeBeginA = ItemIdBeginA;
        this.RangeEndA = this.RangeBeginA + Math.Max(ItemIdEndA - ItemIdBeginA, 0);
        this.RangeBeginB = ItemIdBeginB;
        this.RangeEndB = this.RangeBeginB + Math.Max(ItemIdEndB - ItemIdBeginB, 0);
        this.HandlerType = RequestType;
      }

      public bool Contained(int ItemIdA, int ItemIdB)
      {
        if (ItemIdA >= this.RangeBeginA && ItemIdA <= this.RangeEndA && (ItemIdA >= this.RangeBeginB && ItemIdA <= this.RangeEndB))
          return true;
        return ItemIdB >= this.RangeBeginA && ItemIdB <= this.RangeEndA && ItemIdB >= this.RangeBeginB && ItemIdB <= this.RangeEndB;
      }

      public bool Intersects(int ItemIdBeginA, int ItemIdEndA, int ItemIdBeginB, int ItemIdEndB)
      {
        ItemIdEndA = ItemIdBeginA + Math.Max(ItemIdEndA - ItemIdBeginA, 0);
        ItemIdEndB = ItemIdBeginB + Math.Max(ItemIdEndB - ItemIdBeginB, 0);
        if (ItemIdBeginA >= this.RangeBeginA && ItemIdBeginA <= this.RangeEndA || ItemIdEndA >= this.RangeBeginA && ItemIdEndA <= this.RangeEndA || (this.RangeBeginA >= ItemIdBeginA && this.RangeBeginA <= ItemIdEndA || this.RangeEndA >= ItemIdBeginA && this.RangeEndA <= ItemIdEndA) || (ItemIdBeginA >= this.RangeBeginB && ItemIdBeginA <= this.RangeEndB || ItemIdEndA >= this.RangeBeginB && ItemIdEndA <= this.RangeEndB || (this.RangeBeginB >= ItemIdBeginA && this.RangeBeginB <= ItemIdEndA || this.RangeEndB >= ItemIdBeginA && this.RangeEndB <= ItemIdEndA)) || (ItemIdBeginB >= this.RangeBeginB && ItemIdBeginB <= this.RangeEndB || ItemIdEndB >= this.RangeBeginB && ItemIdEndB <= this.RangeEndB || (this.RangeBeginB >= ItemIdBeginB && this.RangeBeginB <= ItemIdEndB || this.RangeEndB >= ItemIdBeginB && this.RangeEndB <= ItemIdEndB) || (ItemIdBeginB >= this.RangeBeginA && ItemIdBeginB <= this.RangeEndA || ItemIdEndB >= this.RangeBeginA && ItemIdEndB <= this.RangeEndA || this.RangeBeginA >= ItemIdBeginB && this.RangeBeginA <= ItemIdEndB)))
          return true;
        return this.RangeEndA >= ItemIdBeginB && this.RangeEndA <= ItemIdEndB;
      }
    }
  }
}
