﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Currency.PWZPAddReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Xml.Serialization;

namespace PWAPICommon.Queries.Currency
{
  [ComVisible(true)]
  public class PWZPAddReq : PWZPChangeReq
  {
    public long UserID;
    public int TotalFunds;

    [XmlIgnore]
    public override KeyValuePair<long, int> UserBalance => new KeyValuePair<long, int>(this.UserID, this.TotalFunds);

    public PWZPAddReq()
      : this((PWConnectionBase) null)
    {
    }

    public PWZPAddReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_ZPAdd;

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      return strArray.Length == 2 && long.TryParse(strArray[0], out this.UserID) && int.TryParse(strArray[1], out this.TotalFunds) || base.ParseServerQuery(InMessage);
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => this.UserID != PWServerBase.EmptyUserID && this.TotalFunds >= 0;

    protected override bool ProcessServerResult()
    {
      base.ProcessServerResult();
      return this.SendDefaultMessage();
    }

    public override bool ParseClientQuery(byte[] InMessage) => this.Serializer.Deserialize(InMessage).Length == 1;

    public override bool SubmitClientQuery()
    {
      base.SubmitClientQuery();
      return this.SendMessage(this.Serializer.SerializeRaw(this.UserID.ToString() + ":" + this.TotalFunds.ToString()));
    }
  }
}
