﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Currency.PWGPChangeReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Queries.General;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Xml.Serialization;

namespace PWAPICommon.Queries.Currency
{
  [ComVisible(true)]
  public abstract class PWGPChangeReq : PWRequestBase
  {
    [XmlIgnore]
    public abstract KeyValuePair<long, int> UserBalance { get; }

    public PWGPChangeReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWGPChangeReq(PWConnectionBase InConnection)
      : base(InConnection)
      => this.ForceProcessResult = true;

    protected override bool ProcessServerResult()
    {
      if (this.Successful)
      {
        KeyValuePair<long, int> userBalance = this.UserBalance;
        if (userBalance.Key != PWServerBase.EmptyUserID && userBalance.Value >= 0)
          new PWRouteMessageReq(this.Connection, userBalance.Key, new PWGPQueryReq(this.Connection)
          {
            UserId = userBalance.Key,
            Funds = userBalance.Value
          }.GetMessagePairing(true)).SubmitServerQuery();
      }
      return true;
    }
  }
}
