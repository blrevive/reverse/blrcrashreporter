﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Currency.PWZPQueryReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System.Runtime.InteropServices;
using System.Xml.Serialization;

namespace PWAPICommon.Queries.Currency
{
  [XmlRoot("currency-request", Namespace = "")]
  [ComVisible(true)]
  public class PWZPQueryReq : PWRequestBase
  {
    public long userid;
    public int game;
    public int server;
    [XmlIgnore]
    public int Funds;

    public PWZPQueryReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWZPQueryReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] stringArray = this.Serializer.DeserializeToStringArray(InMessage);
      this.game = this.Connection.OwningServer.PWGameID;
      this.server = this.Connection.OwningServer.PWServerID;
      return stringArray.Length == 1 && long.TryParse(stringArray[0], out this.userid);
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_ZPQuery;

    protected override string GetMessageString(bool bSuccess) => base.GetMessageString(bSuccess) + ":" + (object) this.userid + ":" + (object) this.Funds;

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      this.Log("Querying ZP for user " + (object) this.userid, false, ELoggingLevel.ELL_Informative);
      string OutValue;
      if (!this.OwningServer.GetConfigValue<string>("ItemURL", out OutValue))
        return this.ProcessServerResultDefault();
      PWWebReq pwWebReq = new PWWebReq(this.Connection, OutValue, true, EWebRequestType.EWRT_POST, (object) this, typeof (PWZPQueryResult));
      pwWebReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return pwWebReq.SubmitServerQuery();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      PWZPQueryResult result = (PWZPQueryResult) ((PWWebReq) ForRequest).Result;
      if (result == null)
        return false;
      int.TryParse(result.balance, out this.Funds);
      return true;
    }

    protected override bool ProcessServerResult() => this.SendDefaultMessage();
  }
}
