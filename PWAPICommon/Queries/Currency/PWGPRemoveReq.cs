﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Currency.PWGPRemoveReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using System.Xml.Serialization;

namespace PWAPICommon.Queries.Currency
{
  [ComVisible(true)]
  public class PWGPRemoveReq : PWGPChangeReq
  {
    public long UserId;
    public int Amount;
    private int TotalFunds = -1;

    [XmlIgnore]
    public override KeyValuePair<long, int> UserBalance => new KeyValuePair<long, int>(this.UserId, this.TotalFunds);

    public PWGPRemoveReq()
      : this((PWConnectionBase) null)
    {
    }

    public PWGPRemoveReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] stringArray = this.Serializer.DeserializeToStringArray(InMessage);
      return stringArray.Length == 2 && long.TryParse(stringArray[0], out this.UserId) && int.TryParse(stringArray[1], out this.Amount);
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_GPRemove;

    public override bool SubmitServerQuery()
    {
      if (this.Amount <= 0)
        return this.ProcessServerResultDefault();
      base.SubmitServerQuery();
      this.Log("Removing " + (object) this.Amount + " GP from user " + (object) this.UserId, false, ELoggingLevel.ELL_Informative);
      SqlCommand InQuery = new SqlCommand("SP_Fund_Deduct_Money");
      InQuery.CommandType = CommandType.StoredProcedure;
      InQuery.Parameters.Add("@userid", SqlDbType.BigInt);
      InQuery.Parameters["@userid"].Value = (object) this.UserId;
      InQuery.Parameters.Add("@zombie_money ", SqlDbType.BigInt);
      InQuery.Parameters["@zombie_money "].Value = (object) this.Amount;
      PWSQLReq pwsqlReq = new PWSQLReq(InQuery, this.Connection);
      pwsqlReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return pwsqlReq.SubmitServerQuery();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      if (this.Amount == 0)
        return true;
      PWSQLReq pwsqlReq = (PWSQLReq) ForRequest;
      if (!pwsqlReq.Successful)
        return false;
      string[] strArray = ((string) ((object[]) pwsqlReq.ResponseValues[0])[0]).Split(new char[1]
      {
        ' '
      }, StringSplitOptions.RemoveEmptyEntries);
      int result = -1;
      if (strArray.Length == 8 && int.TryParse(strArray[7], out result))
        this.TotalFunds = result;
      return true;
    }

    protected override bool ProcessServerResult() => this.Amount <= 0 || base.ProcessServerResult();
  }
}
