﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Currency.PWZPRemoveReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Xml.Serialization;

namespace PWAPICommon.Queries.Currency
{
  [XmlRoot("item-purchase-request", Namespace = "")]
  [ComVisible(true)]
  public class PWZPRemoveReq : PWZPChangeReq
  {
    public long userid;
    public int charid;
    public int game;
    public int server;
    [DefaultValue(-1)]
    public int amount;
    public int itemid;
    public int count;
    public string uniqueid;
    [XmlIgnore]
    public int TotalFunds = -1;

    [XmlIgnore]
    public override KeyValuePair<long, int> UserBalance => new KeyValuePair<long, int>(this.userid, this.TotalFunds);

    public PWZPRemoveReq()
      : this((PWConnectionBase) null)
    {
    }

    public PWZPRemoveReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] stringArray = this.Serializer.DeserializeToStringArray(InMessage);
      if (stringArray.Length != 3 || !long.TryParse(stringArray[0], out this.userid) || (!int.TryParse(stringArray[1], out this.itemid) || !int.TryParse(stringArray[2], out this.count)))
        return false;
      this.game = this.Connection.OwningServer.PWGameID;
      this.charid = 0;
      this.server = this.Connection.OwningServer.PWServerID;
      return true;
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_ZPRemove;

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      if (this.amount <= 0)
        return this.ProcessServerResultDefault();
      this.Log("Purchasing " + (object) this.count + " of Item: " + (object) this.itemid + " for user " + (object) this.userid, false, ELoggingLevel.ELL_Informative);
      string OutValue;
      if (!this.OwningServer.GetConfigValue<string>("ItemURL", out OutValue))
        return this.ProcessServerResultDefault();
      PWWebReq pwWebReq = new PWWebReq(this.Connection, OutValue, true, EWebRequestType.EWRT_POST, (object) this, typeof (PWZPRemoveResp));
      pwWebReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return pwWebReq.SubmitServerQuery();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => this.amount == 0 || ForRequest is PWWebReq pwWebReq && pwWebReq.Result is PWZPRemoveResp result && (result.status != null && result.status != "failed") && int.TryParse(result.newbalance, out this.TotalFunds);

    protected override bool ProcessServerResult() => this.amount <= 0 || base.ProcessServerResult();
  }
}
