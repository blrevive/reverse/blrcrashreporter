﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Currency.PWGPQueryReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Currency
{
  [ComVisible(true)]
  public class PWGPQueryReq : PWRequestBase
  {
    public long UserId;
    public int Funds;

    public PWGPQueryReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWGPQueryReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] stringArray = this.Serializer.DeserializeToStringArray(InMessage);
      return stringArray.Length == 1 && long.TryParse(stringArray[0], out this.UserId);
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_GPQuery;

    protected override string GetMessageString(bool bSuccess)
    {
      string str = base.GetMessageString(bSuccess);
      if (bSuccess)
        str = str + ":" + (object) this.UserId + ":" + (object) this.Funds;
      return str;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      this.Log("Querying GP for user " + (object) this.UserId, false, ELoggingLevel.ELL_Informative);
      SqlCommand InQuery = new SqlCommand("SP_Fund_Get_Money_By_Userid");
      InQuery.CommandType = CommandType.StoredProcedure;
      InQuery.Parameters.Add("@userid", SqlDbType.BigInt);
      InQuery.Parameters["@userid"].Value = (object) this.UserId;
      PWSQLReq pwsqlReq = new PWSQLReq(InQuery, this.Connection);
      pwsqlReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return pwsqlReq.SubmitServerQuery();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      if (ForRequest is PWSQLReq pwsqlReq)
      {
        IList<object> objectList = pwsqlReq.ResponseValues ?? (IList<object>) new List<object>();
        if (objectList.Count == 1 && objectList[0] is object[] objArray && objArray.Length > 0)
        {
          string s = (objArray[0] ?? (object) string.Empty).ToString();
          if (int.TryParse(s, out this.Funds))
            return true;
          if (s.IndexOf("Not Exists", StringComparison.CurrentCultureIgnoreCase) >= 0)
          {
            this.Funds = 0;
            return true;
          }
        }
      }
      return false;
    }

    protected override bool ProcessServerResult() => this.SendDefaultMessage();
  }
}
