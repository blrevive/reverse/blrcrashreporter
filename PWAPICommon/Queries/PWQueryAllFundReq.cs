﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.PWQueryAllFundReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Queries.Currency;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries
{
  [ComVisible(true)]
  public class PWQueryAllFundReq : PWRequestBase
  {
    public PWZPQueryReq ZPReq;
    public PWGPQueryReq GPReq;
    public long UserId;
    private int ZPFunds;
    private int GPFunds;

    public PWQueryAllFundReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWQueryAllFundReq(PWConnectionBase InConnection, long ForUserId)
      : base(InConnection)
      => this.UserId = ForUserId;

    public PWQueryAllFundReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] stringArray = this.Serializer.DeserializeToStringArray(InMessage);
      return stringArray.Length == 1 && long.TryParse(stringArray[0], out this.UserId);
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      this.ZPReq = new PWZPQueryReq(this.Connection);
      this.ZPReq.userid = this.UserId;
      this.ZPReq.game = this.Connection.OwningServer.PWGameID;
      this.ZPReq.server = this.Connection.OwningServer.PWServerID;
      this.ZPReq.ServerResultProcessor = new ProcessDelegate(this.ZPRequestFinished);
      this.GPReq = new PWGPQueryReq(this.Connection);
      this.GPReq.UserId = this.UserId;
      this.GPReq.ServerResultProcessor = new ProcessDelegate(this.GPRequestFinished);
      bool flag1 = this.ZPReq.SubmitServerQuery();
      bool flag2 = this.GPReq.SubmitServerQuery();
      return flag1 && flag2;
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_QueryAllFunds;

    private bool ZPRequestFinished(PWRequestBase ForRequest)
    {
      this.ZPFunds = this.ZPReq.Funds;
      lock (this)
      {
        if (this.ZPReq.Completed)
        {
          if (this.GPReq.Completed)
            return this.ProcessServerResultDefault();
        }
      }
      return true;
    }

    private bool GPRequestFinished(PWRequestBase ForRequest)
    {
      this.GPFunds = this.GPReq.Funds;
      lock (this)
      {
        if (this.ZPReq.Completed)
        {
          if (this.GPReq.Completed)
            return this.ProcessServerResultDefault();
        }
      }
      return true;
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => this.ZPReq.Successful || this.GPReq.Successful;

    protected override bool ProcessServerResult()
    {
      if (!this.Successful)
        return this.Connection.SendMessage(this.MessageType, this.SerializeMessage(this.UserId.ToString()));
      return this.Connection.SendMessage(this.MessageType, this.SerializeMessage(this.UserId.ToString() + ":" + (object) this.ZPFunds + ":" + (object) this.GPFunds));
    }
  }
}
