﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.PWTransactionQueryByUserReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Wrappers;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries
{
  [ComVisible(true)]
  public class PWTransactionQueryByUserReq : PWTransactionQueryReq
  {
    private long UserID;

    public PWTransactionQueryByUserReq(PWConnectionBase InConnection, long InUserID)
      : base(InConnection)
      => this.UserID = InUserID;

    public override bool SubmitServerQuery()
    {
      this.bSubmitted = true;
      PWSqlItemQueryKeyedReq<PWTransaction, long> itemQueryKeyedReq = new PWSqlItemQueryKeyedReq<PWTransaction, long>(this.Connection);
      itemQueryKeyedReq.Key = this.UserID;
      itemQueryKeyedReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return itemQueryKeyedReq.SubmitServerQuery();
    }
  }
}
