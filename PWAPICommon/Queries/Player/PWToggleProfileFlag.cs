﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Player.PWToggleProfileFlag
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Caches;
using PWAPICommon.Clients;
using PWAPICommon.Common;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Player
{
  [ComVisible(true)]
  public class PWToggleProfileFlag : PWRequestBase
  {
    private static ProfileFlag[] ProtectedProfileFlags = new ProfileFlag[3]
    {
      ProfileFlag.PF_Developer,
      ProfileFlag.PF_Press,
      ProfileFlag.PF_SteamLogin
    };
    public long UserId;
    public ProfileFlag FlagIndex;
    private PWQueryProfileReq ProfileQueryReq;
    private PWCreateProfileReq ProfileCreateReq;
    private PWToggleProfileFlagHelper HelperReq;

    public PWToggleProfileFlag()
      : this((PWConnectionBase) null)
    {
    }

    public PWToggleProfileFlag(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_ToggleProfileFlag;

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      byte result;
      if (strArray.Length != 2 || !long.TryParse(strArray[0], out this.UserId) || !byte.TryParse(strArray[1], out result))
        return false;
      this.FlagIndex = (ProfileFlag) result;
      return true;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      if (this.ProfileQueryReq == null)
      {
        if (((IEnumerable<ProfileFlag>) PWToggleProfileFlag.ProtectedProfileFlags).Contains<ProfileFlag>(this.FlagIndex) && this.UserId == this.Connection.UserID)
        {
          this.Log("Profile Flag Toggle Failed for Protected Profile Flag " + this.FlagIndex.ToString() + " on Profile " + (object) this.UserId, false, ELoggingLevel.ELL_Errors);
          return this.ProcessServerResultDefault();
        }
        this.ProfileQueryReq = new PWQueryProfileReq(this.Connection);
        this.ProfileQueryReq.ForUserID = this.UserId;
        this.ProfileQueryReq.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
        return this.ProfileQueryReq.SubmitServerQuery();
      }
      if (this.ProfileQueryReq.Completed && this.ProfileCreateReq == null && this.HelperReq == null)
      {
        if (this.ProfileQueryReq.Successful)
        {
          this.HelperReq = new PWToggleProfileFlagHelper(this.Connection);
          this.HelperReq.Profile = this.ProfileQueryReq.FoundProfile;
          this.HelperReq.FlagIndex = this.FlagIndex;
          this.HelperReq.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
          return this.HelperReq.SubmitServerQuery();
        }
        if (this.ProfileQueryReq.ProfileMissing)
        {
          this.Log("Profile For User " + (object) this.UserId + " Missing. Creating...", false, ELoggingLevel.ELL_Informative);
          this.ProfileCreateReq = new PWCreateProfileReq(this.Connection);
          this.ProfileCreateReq.UserId = this.UserId;
          this.ProfileCreateReq.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
          return this.ProfileCreateReq.SubmitServerQuery();
        }
        this.Log("Profile Flag Toggle Failed to Find Profile " + (object) this.UserId, false, ELoggingLevel.ELL_Errors);
        return this.ProcessServerResultDefault();
      }
      if (this.ProfileCreateReq != null && this.ProfileCreateReq.Completed && this.HelperReq == null)
      {
        if (this.ProfileCreateReq.Successful)
        {
          this.HelperReq = new PWToggleProfileFlagHelper(this.Connection);
          this.HelperReq.Profile = this.ProfileQueryReq.FoundProfile;
          this.HelperReq.FlagIndex = this.FlagIndex;
          this.HelperReq.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
          return this.HelperReq.SubmitServerQuery();
        }
        this.Log("Profile Flag Toggle Failed to Create Profile " + (object) this.UserId, false, ELoggingLevel.ELL_Errors);
        return this.ProcessServerResultDefault();
      }
      if (this.HelperReq != null && this.HelperReq.Completed)
      {
        if (this.HelperReq.Successful)
        {
          PWOfferCache resource = this.OwningServer.GetResource<PWOfferCache>();
          if (resource != null)
            resource.AwardMatchingOffers(this.UserId, EOfferType.EOT_ProfileFlagReward, (OfferDataBase) new ProfileFlagData()
            {
              ProfileTrigger = this.FlagIndex
            }, this.Connection);
        }
        else
        {
          this.Log("Profile Flag " + (object) this.FlagIndex + " Already Toggled", false, ELoggingLevel.ELL_Warnings);
          return this.ProcessServerResultDefault();
        }
      }
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => this.HelperReq != null && this.HelperReq.Successful;

    protected override bool ProcessServerResult() => this.Submitted ? this.SendMessage(this.SerializeMessage(this.UserId.ToString() + ":" + ((byte) this.FlagIndex).ToString())) : this.SendDefaultMessage();

    public override bool ParseClientQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      return strArray.Length == 3 && strArray[0] == "T" && long.TryParse(strArray[1], out this.UserId) && byte.TryParse(strArray[2], out byte _);
    }

    public override bool SubmitClientQuery() => this.Connection.SendMessage(this.GetMessageType(), this.Serializer.SerializeRaw(this.UserId.ToString() + ":" + ((byte) this.FlagIndex).ToString()));
  }
}
