﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Player.PWLoginItemServer
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Queries.General;
using PWAPICommon.Queries.Machine;
using PWAPICommon.Wrappers;
using System;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Player
{
  [ComVisible(true)]
  public class PWLoginItemServer : PWRequestBase
  {
    private long UserID;
    private PWMachineInfo MachineInfo;
    private PWCombinedReq CombinedReq;
    private PWQueryAllFundReq FundQuery;
    private PWMachineInfoQueryReq MachineQueryReq;
    private PWMachineInfoVerifyQueryUserReq VerifyRequest;
    private PWSqlItemAddReq<PWVerifiedMachine> NewVerifyReq;
    private EVerifyResult ResultCode;

    public PWLoginItemServer()
      : base((PWConnectionBase) null)
    {
    }

    public PWLoginItemServer(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_Login;

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      this.MachineInfo = new PWMachineInfo();
      if (strArray.Length != 18 || !long.TryParse(strArray[0], out this.UserID) || (!uint.TryParse(strArray[7], out this.MachineInfo.CPUInfo.ClockSpeed) || !uint.TryParse(strArray[8], out this.MachineInfo.CPUInfo.NumCores)) || (!uint.TryParse(strArray[9], out this.MachineInfo.CPUInfo.NumProcessors) || !uint.TryParse(strArray[10], out this.MachineInfo.MemInfo.RamMB) || (!uint.TryParse(strArray[11], out this.MachineInfo.MemInfo.PageMB) || !uint.TryParse(strArray[12], out this.MachineInfo.MemInfo.VirtualMB))) || !int.TryParse(strArray[17], out this.MachineInfo.VideoInfo.RamMB))
        return false;
      this.MachineInfo.MachineID.TryParse(strArray[1]);
      this.MachineInfo.OSInfo.Name = strArray[2];
      this.MachineInfo.OSInfo.Description = strArray[3];
      this.MachineInfo.OSInfo.Version = strArray[4];
      this.MachineInfo.OSInfo.Locale = strArray[5];
      this.MachineInfo.CPUInfo.Description = strArray[6];
      this.MachineInfo.VideoInfo.Name = strArray[13];
      this.MachineInfo.VideoInfo.Description = strArray[14];
      this.MachineInfo.VideoInfo.DriverVersion = strArray[15];
      DateTime.TryParse(strArray[16], out this.MachineInfo.VideoInfo.DriverDate);
      return true;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      if (this.CombinedReq == null)
      {
        this.FundQuery = new PWQueryAllFundReq(this.Connection);
        this.FundQuery.UserId = this.UserID;
        this.FundQuery.ForceProcessResult = true;
        this.VerifyRequest = new PWMachineInfoVerifyQueryUserReq(this.Connection);
        this.VerifyRequest.UserID = this.UserID;
        this.MachineQueryReq = new PWMachineInfoQueryReq(this.Connection);
        this.MachineQueryReq.InMachineId = this.MachineInfo.MachineID;
        this.CombinedReq = new PWCombinedReq(this.Connection);
        this.CombinedReq.AddSubRequest((PWRequestBase) this.FundQuery);
        this.CombinedReq.AddSubRequest((PWRequestBase) this.VerifyRequest);
        this.CombinedReq.AddSubRequest((PWRequestBase) this.MachineQueryReq);
        this.CombinedReq.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
        return this.CombinedReq.SubmitServerQuery();
      }
      if (this.CombinedReq.Completed)
      {
        PWVerifiedMachine pwVerifiedMachine = this.VerifyRequest.Successful ? this.VerifyRequest.FoundMachines.Find((Predicate<PWVerifiedMachine>) (x => x.MachineID.Equals(this.MachineInfo.MachineID))) : (PWVerifiedMachine) null;
        if (pwVerifiedMachine != null)
        {
          bool OutValue = false;
          this.OwningServer.GetConfigValue<bool>("RequireMachineVerification", out OutValue, false);
          if (pwVerifiedMachine.bVerified || !OutValue)
            this.ResultCode = EVerifyResult.EVR_OK;
          else if (pwVerifiedMachine.ExpirationDate > PWServerBase.CurrentTime)
          {
            this.ResultCode = EVerifyResult.EVR_ChallengeIssued;
          }
          else
          {
            this.ResultCode = EVerifyResult.EVR_ChallengeExpired;
            PWSqlItemRemoveReq<PWVerifiedMachine> sqlItemRemoveReq = new PWSqlItemRemoveReq<PWVerifiedMachine>(this.Connection);
            sqlItemRemoveReq.Item = pwVerifiedMachine;
            sqlItemRemoveReq.SubmitServerQuery();
            this.NewVerifyReq = new PWSqlItemAddReq<PWVerifiedMachine>(this.Connection);
            this.NewVerifyReq.Item = PWVerifiedMachine.GenerateFor(this.MachineInfo.MachineID, this.UserID);
            this.NewVerifyReq.SubmitServerQuery();
          }
        }
        else
        {
          this.NewVerifyReq = new PWSqlItemAddReq<PWVerifiedMachine>(this.Connection);
          this.NewVerifyReq.Item = PWVerifiedMachine.GenerateFor(this.MachineInfo.MachineID, this.UserID);
          this.NewVerifyReq.SubmitServerQuery();
          this.ResultCode = EVerifyResult.EVR_NewMachine;
        }
        if (!this.MachineQueryReq.Successful)
        {
          PWSqlItemAddReq<PWMachineInfo> pwSqlItemAddReq = new PWSqlItemAddReq<PWMachineInfo>(this.Connection);
          pwSqlItemAddReq.Item = this.MachineInfo;
          pwSqlItemAddReq.Item.UniqueID = Guid.NewGuid();
          pwSqlItemAddReq.SubmitServerQuery();
        }
        else if (!this.MachineQueryReq.OutMachineInfo.Equals((object) this.MachineInfo))
        {
          PWSqlItemUpdateReq<PWMachineInfo> sqlItemUpdateReq = new PWSqlItemUpdateReq<PWMachineInfo>(this.Connection);
          sqlItemUpdateReq.Item = this.MachineInfo;
          sqlItemUpdateReq.Item.UniqueID = this.MachineQueryReq.OutMachineInfo.UniqueID;
          sqlItemUpdateReq.SubmitServerQuery();
        }
        this.Log("loginfunds userid=" + (object) this.UserID + " gp=" + (object) this.FundQuery.GPReq.Funds + " zen=" + (object) this.FundQuery.ZPReq.Funds, true, ELoggingLevel.ELL_Verbose);
      }
      this.OwningServer.ApplyUserIdentity(this.Connection, this.UserID);
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      PWConnectionBase connection = this.Connection;
      return connection != null && connection.UserID == this.UserID && this.ResultCode == EVerifyResult.EVR_OK;
    }

    protected override bool ProcessServerResult() => this.SendDefaultMessage();
  }
}
