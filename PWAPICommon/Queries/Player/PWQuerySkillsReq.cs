﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Player.PWQuerySkillsReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Player
{
  [ComVisible(true)]
  public class PWQuerySkillsReq : PWRequestBase
  {
    private long UserId;
    private byte[] SkillData;

    public PWQuerySkillsReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWQuerySkillsReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] stringArray = this.Serializer.DeserializeToStringArray(InMessage);
      return stringArray.Length == 1 && long.TryParse(stringArray[0], out this.UserId);
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_QuerySkills;

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      this.Log("Querying skills for UserId " + (object) this.UserId, false, ELoggingLevel.ELL_Informative);
      SqlCommand InQuery = new SqlCommand("SP_Account_Get_Skills_By_UserId");
      InQuery.CommandType = CommandType.StoredProcedure;
      InQuery.Parameters.Add("@userid", SqlDbType.BigInt);
      InQuery.Parameters["@userid"].Value = (object) this.UserId;
      PWSQLReq pwsqlReq = new PWSQLReq(InQuery, this.Connection);
      pwsqlReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return pwsqlReq.SubmitServerQuery();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      PWSQLReq pwsqlReq = (PWSQLReq) ForRequest;
      if (pwsqlReq == null || pwsqlReq.ResponseValues == null || pwsqlReq.ResponseValues.Count != 1)
        return false;
      object[] responseValue = (object[]) pwsqlReq.ResponseValues[0];
      this.SkillData = responseValue[0].GetType() == typeof (byte[]) ? (byte[]) responseValue[0] : new byte[20];
      return true;
    }

    protected override bool ProcessServerResult() => this.SendMessage(this.SerializeMessage(this.UserId.ToString(), this.SkillData));

    public override bool SubmitClientQuery() => throw new NotImplementedException();

    protected override bool ProcessClientResult(PWRequestBase ForRequest) => throw new NotImplementedException();
  }
}
