﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Player.PWChangeNameReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Caches;
using PWAPICommon.Clients;
using PWAPICommon.Common;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Player
{
  [ComVisible(true)]
  public class PWChangeNameReq : PWRequestBase
  {
    public long UserId;
    public string NewName;
    public bool bLogResult;
    private PWValidatePlayerName ValidatePlayerNameReq;

    public PWChangeNameReq()
      : base((PWConnectionBase) null)
      => this.bLogResult = true;

    public PWChangeNameReq(PWConnectionBase InConnection)
      : base(InConnection)
      => this.bLogResult = true;

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] stringArray = this.Serializer.DeserializeToStringArray(InMessage);
      if (stringArray.Length != 2 || !long.TryParse(stringArray[0], out this.UserId))
        return false;
      this.NewName = stringArray[1];
      return true;
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_ChangeName;

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      if (this.ValidatePlayerNameReq == null)
      {
        this.ValidatePlayerNameReq = new PWValidatePlayerName(this.Connection);
        this.ValidatePlayerNameReq.UserId = this.UserId;
        this.ValidatePlayerNameReq.PlayerName = this.NewName;
        this.ValidatePlayerNameReq.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
        return this.ValidatePlayerNameReq.SubmitServerQuery();
      }
      if (!this.ValidatePlayerNameReq.Successful)
        return this.ProcessServerResultDefault();
      this.Log("UserId " + (object) this.UserId + " changing name to " + this.NewName, false, ELoggingLevel.ELL_Informative);
      SqlCommand InQuery = new SqlCommand("SP_Account_UserName_Update");
      InQuery.CommandType = CommandType.StoredProcedure;
      InQuery.Parameters.Add("@userid", SqlDbType.BigInt);
      InQuery.Parameters["@userid"].Value = (object) this.UserId;
      InQuery.Parameters.Add("@UserName ", SqlDbType.VarChar);
      InQuery.Parameters["@UserName "].Value = (object) this.NewName;
      PWSQLReq pwsqlReq = new PWSQLReq(InQuery, this.Connection);
      pwsqlReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return pwsqlReq.SubmitServerQuery();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      if (!(ForRequest is PWSQLReq pwsqlReq) || !pwsqlReq.Successful)
        return false;
      this.Log("charid=" + (object) this.UserId + " change-name=" + this.NewName, true, ELoggingLevel.ELL_Verbose);
      return true;
    }

    protected override bool ProcessServerResult()
    {
      if (!this.Successful)
        return this.SendDefaultMessage();
      this.OwningServer.GetResource<PWPlayerItemCache<UserProfile>>()?.MarkDirtyFor(this.UserId);
      return this.SendMessage(this.SerializeMessage(this.NewName));
    }
  }
}
