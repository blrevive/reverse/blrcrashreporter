﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Player.PWGenderChangeReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Player
{
  [ComVisible(true)]
  public class PWGenderChangeReq : PWRequestBase
  {
    public long UserID;
    public bool bNewGender;
    public bool bLogResult;

    public PWGenderChangeReq(PWConnectionBase InConnection)
      : base(InConnection)
      => this.bLogResult = true;

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] stringArray = this.Serializer.DeserializeToStringArray(InMessage);
      if (stringArray.Length != 2 || !long.TryParse(stringArray[0], out this.UserID))
        return false;
      this.bNewGender = stringArray[1] == "T";
      return true;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      this.Log("UserId " + (object) this.UserID + " changing gender to " + (this.bNewGender ? (object) "Female" : (object) "Male"), false, ELoggingLevel.ELL_Informative);
      SqlCommand InQuery = new SqlCommand("SP_Account_Gender_Update");
      InQuery.CommandType = CommandType.StoredProcedure;
      InQuery.Parameters.Add("@UserID", SqlDbType.BigInt);
      InQuery.Parameters["@UserID"].Value = (object) this.UserID;
      InQuery.Parameters.Add("@Gender", SqlDbType.Bit);
      InQuery.Parameters["@Gender"].Value = (object) (this.bNewGender ? 1 : 0);
      PWSQLReq pwsqlReq = new PWSQLReq(InQuery, this.Connection);
      pwsqlReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return pwsqlReq.SubmitServerQuery();
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_GenderChange;

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      if (!((object[]) ((PWSQLReq) ForRequest).ResponseValues[0])[0].ToString().Contains("Succeeded"))
        return false;
      if (this.bLogResult)
        this.Log("charid=" + (object) this.UserID + " change-gender=" + (this.bNewGender ? (object) "female" : (object) "male"), true, ELoggingLevel.ELL_Verbose);
      return true;
    }

    protected override bool ProcessServerResult() => this.SendDefaultMessage();
  }
}
