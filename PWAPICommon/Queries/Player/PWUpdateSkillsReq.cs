﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Player.PWUpdateSkillsReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Player
{
  [ComVisible(true)]
  public class PWUpdateSkillsReq : PWRequestBase
  {
    private long UserId;
    private byte[] Data = new byte[20];

    public PWUpdateSkillsReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWUpdateSkillsReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    public override bool ParseServerQuery(byte[] InMessage)
    {
      this.UserId = (long) int.Parse(this.Serializer.DeserializeToStringArray(InMessage)[0]);
      if (InMessage.Length <= 20)
        return false;
      Array.Copy((Array) InMessage, InMessage.Length - 20, (Array) this.Data, 0, 20);
      return true;
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_UpdateSkills;

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      this.Log("Updating skills for UserId " + (object) this.UserId, false, ELoggingLevel.ELL_Informative);
      SqlCommand InQuery = new SqlCommand("SP_Account_Update_Skills");
      InQuery.CommandType = CommandType.StoredProcedure;
      InQuery.Parameters.Add("@userid", SqlDbType.BigInt);
      InQuery.Parameters["@userid"].Value = (object) this.UserId;
      InQuery.Parameters.Add("@skills", SqlDbType.VarBinary);
      InQuery.Parameters["@skills"].Value = (object) this.Data;
      PWSQLReq pwsqlReq = new PWSQLReq(InQuery, this.Connection);
      pwsqlReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return pwsqlReq.SubmitServerQuery();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => ForRequest.Successful;

    protected override bool ProcessServerResult() => this.SendDefaultMessage();

    public override bool SubmitClientQuery() => throw new NotImplementedException();

    protected override bool ProcessClientResult(PWRequestBase ForRequest) => throw new NotImplementedException();
  }
}
