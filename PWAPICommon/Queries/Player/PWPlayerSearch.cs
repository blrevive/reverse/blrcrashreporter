﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Player.PWPlayerSearch
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Player
{
  [ComVisible(true)]
  public class PWPlayerSearch : PWSocialReqBase
  {
    private string SearchedName;
    private PlayerSearchData[] PartialMatches;
    private PWProfileQueryBasicReq ProfileQuery;

    public PWPlayerSearch()
      : base((PWConnectionBase) null)
    {
    }

    public PWPlayerSearch(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_PlayerSearch;

    public override bool ParseServerQuery(byte[] InData)
    {
      base.ParseServerQuery(InData);
      if (this.MsgArray.Length != 1)
        return false;
      this.SearchedName = Convert.ToString(this.MsgArray[0]);
      return true;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      this.PartialMatches = this.SocialServer.GetPartialNameMatches(this.SearchedName);
      this.ProfileQuery = new PWProfileQueryBasicReq(this.Connection);
      this.ProfileQuery.ForUserName = this.SearchedName;
      this.ProfileQuery.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return this.ProfileQuery.SubmitServerQuery();
    }

    protected override bool ProcessServerResult()
    {
      bool flag = this.ProfileQuery.Successful && this.ProfileQuery.BasicProfile != null;
      if (!flag && this.PartialMatches.Length <= 0)
        return this.SendDefaultMessage();
      string str = flag ? this.ProfileQuery.BasicProfile.UserName : "";
      for (int index = 0; index < this.PartialMatches.Length; ++index)
        str = str + ":" + this.PartialMatches[index].ToString();
      return this.SendMessage(this.Serializer.Serialize("T:" + str));
    }
  }
}
