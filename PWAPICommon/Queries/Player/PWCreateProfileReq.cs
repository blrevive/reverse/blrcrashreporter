﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Player.PWCreateProfileReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Player
{
  [ComVisible(true)]
  public class PWCreateProfileReq : PWRequestBase
  {
    public long UserId;

    public PWCreateProfileReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWCreateProfileReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_QueryProfile;

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      return strArray.Length == 1 && long.TryParse(strArray[0], out this.UserId);
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      this.Log("Creating Profile for UserID " + (object) this.UserId, false, ELoggingLevel.ELL_Informative);
      SqlCommand InQuery = new SqlCommand("SP_Account_Add");
      InQuery.CommandType = CommandType.StoredProcedure;
      InQuery.Parameters.Add("@userid", SqlDbType.BigInt);
      InQuery.Parameters["@userid"].Value = (object) this.UserId;
      InQuery.Parameters.Add("@UserName", SqlDbType.VarChar);
      InQuery.Parameters["@UserName"].Value = (object) "";
      InQuery.Parameters.Add("@Experience", SqlDbType.Int);
      InQuery.Parameters["@Experience"].Value = (object) 0;
      InQuery.Parameters.Add("@profile_data", SqlDbType.VarBinary);
      InQuery.Parameters["@profile_data"].Value = (object) new byte[0];
      InQuery.Parameters.Add("@Skills", SqlDbType.VarBinary);
      InQuery.Parameters["@Skills"].Value = (object) new byte[0];
      InQuery.Parameters.Add("@gender", SqlDbType.Bit);
      InQuery.Parameters["@gender"].Value = (object) 0;
      InQuery.Parameters.Add("@unlocks", SqlDbType.Int);
      InQuery.Parameters["@unlocks"].Value = (object) 0;
      InQuery.Parameters.Add("@TitleData", SqlDbType.Int);
      InQuery.Parameters["@TitleData"].Value = (object) 0;
      InQuery.Parameters.Add("@BadgeData", SqlDbType.Int);
      InQuery.Parameters["@BadgeData"].Value = (object) 0;
      PWSQLReq pwsqlReq = new PWSQLReq(InQuery, this.Connection);
      pwsqlReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return pwsqlReq.SubmitServerQuery();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => (ForRequest as PWSQLReq).Successful;

    protected override bool ProcessServerResult()
    {
      if (this.Successful)
        return this.SendMessage(this.CompressMessage(this.SerializeMessage(new UserProfile().ResponseString)));
      this.Log("Profile Create Failed for UserID " + (object) this.UserId, false, ELoggingLevel.ELL_Warnings);
      return this.SendDefaultMessage();
    }

    public override bool SubmitClientQuery() => this.SendMessage(this.Serializer.SerializeRaw(this.UserId.ToString()));

    public override bool ParseClientQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      return strArray.Length > 0 && strArray[0] == "T";
    }
  }
}
