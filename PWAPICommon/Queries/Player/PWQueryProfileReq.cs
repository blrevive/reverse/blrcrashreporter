﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Player.PWQueryProfileReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Caches;
using PWAPICommon.Clients;
using PWAPICommon.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Player
{
  [ComVisible(true)]
  public class PWQueryProfileReq : PWRequestBase
  {
    public long ForUserID;
    public UserProfile FoundProfile;
    private bool bProfileMissing;

    public bool ProfileMissing => this.bProfileMissing;

    public PWQueryProfileReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWQueryProfileReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] stringArray = this.Serializer.DeserializeToStringArray(InMessage);
      return stringArray.Length == 1 && long.TryParse(stringArray[0], out this.ForUserID);
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_QueryProfile;

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      this.Log("Querying Profile for UserID " + (object) this.ForUserID, false, ELoggingLevel.ELL_Informative);
      SqlCommand InQuery = new SqlCommand("SP_Account_Select");
      InQuery.CommandType = CommandType.StoredProcedure;
      InQuery.Parameters.Add("@userid", SqlDbType.BigInt);
      InQuery.Parameters["@userid"].Value = (object) this.ForUserID;
      PWSQLReq pwsqlReq = new PWSQLReq(InQuery, this.Connection);
      pwsqlReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return pwsqlReq.SubmitServerQuery();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      if (ForRequest is PWSQLReq pwsqlReq)
      {
        IList<object> responseValues = pwsqlReq.ResponseValues;
        if (responseValues != null && responseValues.Count > 0 && pwsqlReq.ResponseValues[0] is object[] responseValue)
        {
          this.bProfileMissing = responseValue.Length == 1;
          this.FoundProfile = new UserProfile();
          if (!this.bProfileMissing && this.FoundProfile.ParseFromArray(responseValue, UserProfile.EProfileQueryType.EPQT_Full))
          {
            this.OwningServer.GetResource<PWPlayerItemCache<UserProfile>>()?.SetCachedItem(this.FoundProfile.UserId, this.FoundProfile);
            return true;
          }
        }
      }
      return false;
    }

    protected override bool ProcessServerResult()
    {
      if (this.Successful)
      {
        byte[] numArray = this.SerializeMessage(this.FoundProfile.ResponseString);
        byte[] InData;
        if (this.FoundProfile.ProfileData != null)
        {
          InData = new byte[numArray.Length + this.FoundProfile.ProfileData.Length];
          Array.Copy((Array) numArray, (Array) InData, numArray.Length);
          Array.Copy((Array) this.FoundProfile.ProfileData, 0, (Array) InData, numArray.Length, this.FoundProfile.ProfileData.Length);
        }
        else
          InData = numArray;
        return this.SendMessage(this.CompressMessage(InData));
      }
      if (this.bProfileMissing)
      {
        this.Log("Profile Missing, Creating for UserID " + (object) this.ForUserID, false, ELoggingLevel.ELL_Informative);
        return new PWCreateProfileReq(this.Connection)
        {
          UserId = this.ForUserID
        }.SubmitServerQuery();
      }
      this.Log("Profile Read Failed for UserID " + (object) this.ForUserID, false, ELoggingLevel.ELL_Informative);
      this.SendDefaultMessage();
      return false;
    }
  }
}
