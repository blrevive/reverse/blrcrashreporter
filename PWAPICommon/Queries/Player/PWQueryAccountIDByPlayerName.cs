﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Player.PWQueryAccountIDByPlayerName
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Player
{
  [ComVisible(true)]
  public class PWQueryAccountIDByPlayerName : PWRequestBase
  {
    private PWProfileQueryBasicReq BasicProfileReq;
    public string ForPlayerName;
    public long ResultId;

    public override string WebResponse => !this.Successful ? base.WebResponse : this.ResultId.ToString();

    public PWQueryAccountIDByPlayerName()
      : base((PWConnectionBase) null)
    {
    }

    public PWQueryAccountIDByPlayerName(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_QueryAccountIdByPlayerName;

    public override bool ParseClientQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      return strArray.Length == 2 && strArray[0] == "T" && long.TryParse(strArray[1], out this.ResultId);
    }

    public override bool SubmitClientQuery() => this.SendMessage(this.Serializer.SerializeRaw(this.ForPlayerName));

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      if (strArray.Length != 1)
        return false;
      this.ForPlayerName = strArray[0];
      return true;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      if (this.BasicProfileReq != null)
        return this.ProcessServerResultDefault();
      this.BasicProfileReq = new PWProfileQueryBasicReq(this.Connection);
      this.BasicProfileReq.ForUserName = this.ForPlayerName;
      this.BasicProfileReq.ServerResultProcessor = new ProcessDelegate(this.OnBasicProfileRead);
      return this.BasicProfileReq.SubmitServerQuery();
    }

    private bool OnBasicProfileRead(PWRequestBase ForRequest)
    {
      if (this.BasicProfileReq != null && this.BasicProfileReq.Successful && this.BasicProfileReq.BasicProfile != null)
        this.ResultId = this.BasicProfileReq.BasicProfile.UserId;
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => this.BasicProfileReq != null && this.BasicProfileReq.Successful && this.BasicProfileReq.Completed;

    protected override bool ProcessServerResult() => this.BasicProfileReq != null ? this.SendMessage(this.SerializeMessage(this.ResultId.ToString())) : this.SendDefaultMessage();
  }
}
