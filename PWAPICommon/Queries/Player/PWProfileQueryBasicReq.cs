﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Player.PWProfileQueryBasicReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Caches;
using PWAPICommon.Clients;
using PWAPICommon.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Player
{
  [ComVisible(true)]
  public class PWProfileQueryBasicReq : PWRequestBase
  {
    public long ForUserID;
    public string ForUserName;
    public UserProfile BasicProfile;

    public override string WebResponse => !this.Successful ? base.WebResponse : this.BasicProfile.ResponseString;

    public PWProfileQueryBasicReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWProfileQueryBasicReq(PWConnectionBase InClient)
      : base(InClient)
    {
    }

    public override bool ParseClientQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      if (strArray.Length >= 7 && strArray[0] == "T")
      {
        this.BasicProfile = new UserProfile();
        if (long.TryParse(strArray[1], out this.BasicProfile.UserId) && int.TryParse(strArray[3], out this.BasicProfile.Experience) && (bool.TryParse(strArray[4], out this.BasicProfile.bIsFemale) && long.TryParse(strArray[5], out this.BasicProfile.BadgeData)) && int.TryParse(strArray[6], out this.BasicProfile.TitleData))
        {
          this.BasicProfile.UserName = strArray[2];
          if (strArray.Length > 7)
            DateTime.TryParse(strArray[7], out this.BasicProfile.LastSeenDate);
          return true;
        }
      }
      return false;
    }

    public override bool SubmitClientQuery()
    {
      base.SubmitClientQuery();
      if (this.ForUserID != 0L)
        return this.SendMessage(this.Serializer.SerializeRaw(this.ForUserID.ToString()));
      return this.ForUserName != null && this.ForUserName != "" && this.SendMessage(this.Serializer.SerializeRaw(this.ForUserName));
    }

    public override bool ParseClientResult(PWRequestBase ForRequest) => this.BasicProfile != null && this.BasicProfile.UserId != 0L;

    protected override EMessageType GetMessageType() => EMessageType.EMT_QueryBasicProfile;

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      return strArray.Length == 1 && long.TryParse(strArray[0], out this.ForUserID);
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      PWPlayerItemCache<UserProfile> resource = this.OwningServer.GetResource<PWPlayerItemCache<UserProfile>>();
      if (resource != null)
      {
        this.BasicProfile = resource.GetCachedItem(this.ForUserID);
        if (this.BasicProfile != null)
          return this.ProcessServerResultDefault();
      }
      SqlCommand InQuery = (SqlCommand) null;
      if (this.ForUserID != 0L)
      {
        InQuery = new SqlCommand("SP_Account_Basic_By_UserID");
        InQuery.Parameters.Add("@userid", SqlDbType.BigInt);
        InQuery.Parameters["@userid"].Value = (object) this.ForUserID;
      }
      else if (this.ForUserName != null && this.ForUserName != "")
      {
        InQuery = new SqlCommand("SP_Account_Basic_By_UserName");
        InQuery.Parameters.Add("@UserName", SqlDbType.VarChar);
        InQuery.Parameters["@UserName"].Value = (object) this.ForUserName;
      }
      if (InQuery == null)
        return this.ProcessServerResultDefault();
      InQuery.CommandType = CommandType.StoredProcedure;
      PWSQLReq pwsqlReq = new PWSQLReq(InQuery, this.Connection);
      pwsqlReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return pwsqlReq.SubmitServerQuery();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      if (this.BasicProfile != null)
        return true;
      if (ForRequest is PWSQLReq pwsqlReq)
      {
        IList<object> responseValues = pwsqlReq.ResponseValues;
        if (responseValues != null && responseValues.Count > 0)
        {
          this.BasicProfile = new UserProfile();
          if (this.BasicProfile.ParseFromArray(responseValues[0] as object[], UserProfile.EProfileQueryType.EPQT_Basic))
          {
            this.OwningServer.GetResource<PWPlayerItemCache<UserProfile>>()?.SetCachedItem(this.BasicProfile.UserId, this.BasicProfile);
            return true;
          }
        }
      }
      return false;
    }

    protected override bool ProcessServerResult()
    {
      if (!this.Successful)
        return this.SendMessage(this.SerializeMessage(this.ForUserID.ToString()));
      return this.SendMessage(this.CompressMessage(this.SerializeMessage(this.BasicProfile.UserId.ToString() + ":" + this.BasicProfile.UserName + ":" + this.BasicProfile.Experience.ToString() + ":" + this.BasicProfile.bIsFemale.ToString() + ":" + this.BasicProfile.BadgeData.ToString() + ":" + this.BasicProfile.TitleData.ToString() + ":" + this.BasicProfile.LastSeenDate.ToString())));
    }
  }
}
