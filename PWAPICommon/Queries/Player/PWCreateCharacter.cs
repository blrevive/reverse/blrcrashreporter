﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Player.PWCreateCharacter
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Player
{
  [ComVisible(true)]
  public class PWCreateCharacter : PWRequestBase
  {
    public long UserId;
    public string PlayerName;
    public string PlayerGender;
    private PWChangeNameReq NameChangeReq;
    private PWGenderChangeReq GenderChangeReq;
    private PWToggleProfileFlag ToggleProfileFlagReq;

    public PWCreateCharacter()
      : this((PWConnectionBase) null)
    {
    }

    public PWCreateCharacter(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_CreateCharacter;

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      if (strArray.Length != 3 || !long.TryParse(strArray[0], out this.UserId))
        return false;
      this.PlayerName = strArray[1];
      this.PlayerGender = strArray[2];
      return true;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      if (this.NameChangeReq == null)
      {
        string str = this.PlayerGender == "T" ? "Female" : "Male";
        this.Log("Create Character For User " + this.UserId.ToString() + ": " + this.PlayerName + ", " + str, false, ELoggingLevel.ELL_Informative);
        this.NameChangeReq = new PWChangeNameReq(this.Connection);
        this.NameChangeReq.NewName = this.PlayerName;
        this.NameChangeReq.UserId = this.UserId;
        this.NameChangeReq.bLogResult = false;
        this.NameChangeReq.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
        return this.NameChangeReq.SubmitServerQuery();
      }
      if (this.GenderChangeReq == null)
      {
        if (this.NameChangeReq.Successful)
        {
          this.GenderChangeReq = new PWGenderChangeReq(this.Connection);
          this.GenderChangeReq.bNewGender = this.PlayerGender == "T";
          this.GenderChangeReq.UserID = this.UserId;
          this.GenderChangeReq.bLogResult = false;
          this.GenderChangeReq.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
          return this.GenderChangeReq.SubmitServerQuery();
        }
        this.Log("Create Character Failed For User " + this.UserId.ToString() + " Failed on Name Change", false, ELoggingLevel.ELL_Informative);
        return this.ProcessServerResultDefault();
      }
      if (this.ToggleProfileFlagReq == null)
      {
        if (this.GenderChangeReq.Successful)
        {
          this.ToggleProfileFlagReq = new PWToggleProfileFlag(this.Connection);
          this.ToggleProfileFlagReq.UserId = this.UserId;
          this.ToggleProfileFlagReq.FlagIndex = ProfileFlag.PF_CreatedCharacter;
          this.ToggleProfileFlagReq.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
          return this.ToggleProfileFlagReq.SubmitServerQuery();
        }
        this.Log("Create Character Failed For User " + this.UserId.ToString() + " Failed on Gender Change", false, ELoggingLevel.ELL_Informative);
        return this.ProcessServerResultDefault();
      }
      this.Log("Create Character Successful For User " + this.UserId.ToString(), false, ELoggingLevel.ELL_Informative);
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      if (this.NameChangeReq == null || !this.NameChangeReq.Successful || (this.GenderChangeReq == null || !this.GenderChangeReq.Successful) || (this.ToggleProfileFlagReq == null || !this.ToggleProfileFlagReq.Completed))
        return false;
      this.Log("charid=" + (object) this.UserId + " change-name=" + this.PlayerName + " change-gender=" + (this.PlayerGender == "T" ? "female" : "male"), true, ELoggingLevel.ELL_Verbose);
      return true;
    }

    protected override bool ProcessServerResult() => this.SendMessage(this.SerializeMessage(this.PlayerName + ":" + this.PlayerGender));

    public override bool SubmitClientQuery() => this.SendMessage(this.Serializer.SerializeRaw(this.UserId.ToString() + ":" + this.PlayerName + ":" + this.PlayerGender));

    public override bool ParseClientQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      return strArray.Length == 3 && strArray[0] == "T";
    }
  }
}
