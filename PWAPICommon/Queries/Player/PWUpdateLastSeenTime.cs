﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Player.PWUpdateLastSeenTime
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Caches;
using PWAPICommon.Clients;
using PWAPICommon.Common;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Player
{
  [ComVisible(true)]
  public class PWUpdateLastSeenTime : PWRequestBase
  {
    public long UserID;

    public PWUpdateLastSeenTime()
      : base((PWConnectionBase) null)
    {
    }

    public PWUpdateLastSeenTime(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => throw new NotImplementedException();

    public override bool ParseServerQuery(byte[] InMessage) => throw new NotImplementedException();

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      PWPlayerItemCache<UserProfile> resource = this.OwningServer.GetResource<PWPlayerItemCache<UserProfile>>();
      if (resource != null)
      {
        UserProfile cachedItem = resource.GetCachedItem(this.UserID);
        if (cachedItem != null)
          cachedItem.LastSeenDate = PWServerBase.CurrentTime;
      }
      this.Log("Setting LastSeenTime for UserID " + this.UserID.ToString() + " to " + PWServerBase.CurrentTime.ToString(), false, ELoggingLevel.ELL_Informative);
      SqlCommand InQuery = new SqlCommand("SP_Account_LastSeenTime_Update");
      InQuery.CommandType = CommandType.StoredProcedure;
      InQuery.Parameters.Add("@userid", SqlDbType.BigInt);
      InQuery.Parameters["@userid"].Value = (object) this.UserID;
      InQuery.Parameters.Add("@lastSeenTime", SqlDbType.DateTime);
      InQuery.Parameters["@lastSeenTime"].Value = (object) PWServerBase.CurrentTime;
      PWSQLReq pwsqlReq = new PWSQLReq(InQuery, this.Connection);
      pwsqlReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return pwsqlReq.SubmitServerQuery();
    }
  }
}
