﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Player.PWQueryProfileBasicList
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Queries.General;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Player
{
  [ComVisible(true)]
  public class PWQueryProfileBasicList : PWRequestBase
  {
    public List<PWPlayerInfo> PlayerList;
    private PWCombinedReq ProfileQueries;

    public PWQueryProfileBasicList()
      : this((PWConnectionBase) null)
    {
    }

    public PWQueryProfileBasicList(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_Invalid;

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      this.ProfileQueries = new PWCombinedReq(this.Connection);
      this.ProfileQueries.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      foreach (PWPlayerInfo player in this.PlayerList)
        this.ProfileQueries.AddSubRequest((PWRequestBase) new PWProfileQueryBasicReq(this.Connection)
        {
          ForUserID = player.UserID
        });
      this.ProfileQueries.SubmitServerQuery();
      return true;
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      using (List<PWRequestBase>.Enumerator enumerator = this.ProfileQueries.SubRequests.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          PWProfileQueryBasicReq ProfReq = (PWProfileQueryBasicReq) enumerator.Current;
          if (ProfReq.BasicProfile != null)
            this.PlayerList.Find((Predicate<PWPlayerInfo>) (x => x.UserID == ProfReq.BasicProfile.UserId))?.CopyFromUserProfile(ProfReq.BasicProfile);
        }
      }
      return true;
    }

    protected override bool ProcessServerResult() => true;
  }
}
