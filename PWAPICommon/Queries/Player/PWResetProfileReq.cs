﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Player.PWResetProfileReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Caches;
using PWAPICommon.Clients;
using PWAPICommon.Common;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Player
{
  [ComVisible(true)]
  public class PWResetProfileReq : PWRequestBase
  {
    public long UserId;

    public PWResetProfileReq()
      : this((PWConnectionBase) null)
    {
    }

    public PWResetProfileReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_ResetProfile;

    public override bool ParseClientQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      return strArray.Length == 1 && strArray[0] == "T";
    }

    public override bool SubmitClientQuery() => this.Connection.SendMessage(this.GetMessageType(), this.Serializer.SerializeRaw(this.UserId.ToString()));

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      return strArray.Length == 1 && long.TryParse(strArray[0], out this.UserId);
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      this.Log("Removing Profile with UserId " + (object) this.UserId, false, ELoggingLevel.ELL_Informative);
      SqlCommand InQuery = new SqlCommand("SP_Account_Remove");
      InQuery.CommandType = CommandType.StoredProcedure;
      InQuery.Parameters.Add("@UserId", SqlDbType.BigInt);
      InQuery.Parameters["@UserId"].Value = (object) this.UserId;
      PWSQLReq pwsqlReq = new PWSQLReq(InQuery, this.Connection);
      pwsqlReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return pwsqlReq.SubmitServerQuery();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => (ForRequest as PWSQLReq).Successful;

    protected override bool ProcessServerResult()
    {
      this.OwningServer.GetResource<PWPlayerItemCache<UserProfile>>()?.MarkDirtyFor(this.UserId);
      this.Log("Profile Data Reset " + (this.Successful ? (object) "Succeeded" : (object) "Failed") + " for UserId " + (object) this.UserId, false, ELoggingLevel.ELL_Informative);
      return this.SendDefaultMessage();
    }
  }
}
