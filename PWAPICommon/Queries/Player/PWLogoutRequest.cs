﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Player.PWLogoutRequest
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Player
{
  [ComVisible(true)]
  public class PWLogoutRequest : PWRequestBase
  {
    public long UserId;

    public PWLogoutRequest()
      : base((PWConnectionBase) null)
    {
    }

    public PWLogoutRequest(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    public override bool ParseClientQuery(byte[] InMessage)
    {
      string[] stringArray = this.Serializer.DeserializeToStringArray(InMessage);
      return stringArray.Length == 1 && long.TryParse(stringArray[0], out this.UserId);
    }

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] stringArray = this.Serializer.DeserializeToStringArray(InMessage);
      return stringArray.Length == 1 && long.TryParse(stringArray[0], out this.UserId);
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_Logout;

    public override bool SubmitClientQuery()
    {
      base.SubmitClientQuery();
      return this.SendMessage(this.Serializer.SerializeRaw(this.UserId.ToString()));
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      PWLoginCache resource = this.Connection.OwningServer.GetResource<PWLoginCache>();
      return resource != null && resource.Logout(this.Connection);
    }

    protected override bool ProcessServerResult() => this.SendDefaultMessage();
  }
}
