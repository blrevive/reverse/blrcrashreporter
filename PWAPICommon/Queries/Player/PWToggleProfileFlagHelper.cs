﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Player.PWToggleProfileFlagHelper
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Player
{
  [ComVisible(true)]
  public class PWToggleProfileFlagHelper : PWRequestBase
  {
    public UserProfile Profile;
    public ProfileFlag FlagIndex;

    public PWToggleProfileFlagHelper()
      : base((PWConnectionBase) null)
    {
    }

    public PWToggleProfileFlagHelper(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_Invalid;

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      this.Log("Toggling Profile Flag " + (object) this.FlagIndex + "...", false, ELoggingLevel.ELL_Informative);
      if ((this.Profile.Unlocks & 1 << (int) (this.FlagIndex & (ProfileFlag) 31)) != 0)
        return this.ProcessServerResultDefault();
      SqlCommand InQuery = new SqlCommand("SP_Account_Set_Unlock_Bit");
      InQuery.CommandType = CommandType.StoredProcedure;
      InQuery.Parameters.Add("@userid", SqlDbType.BigInt);
      InQuery.Parameters["@userid"].Value = (object) this.Profile.UserId;
      InQuery.Parameters.Add("@bitMask", SqlDbType.Int);
      InQuery.Parameters["@bitMask"].Value = (object) (1 << (int) (this.FlagIndex & (ProfileFlag) 31));
      PWSQLReq pwsqlReq = new PWSQLReq(InQuery, this.Connection);
      pwsqlReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return pwsqlReq.SubmitServerQuery();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => ForRequest is PWSQLReq pwsqlReq && pwsqlReq.Successful;
  }
}
