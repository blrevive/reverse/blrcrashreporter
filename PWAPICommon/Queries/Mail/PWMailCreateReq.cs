﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Mail.PWMailCreateReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Queries.Currency;
using PWAPICommon.Queries.General;
using PWAPICommon.Queries.Inventory;
using PWAPICommon.Queries.Player;
using PWAPICommon.Queries.Store;
using PWAPICommon.Wrappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Mail
{
  [ComVisible(true)]
  public class PWMailCreateReq : PWMailReqBase
  {
    public List<Guid> ItemsToSend;
    private PWCombinedReq ProfileQueries;
    private PWProfileQueryBasicReq SenderProfileQuery;
    private PWProfileQueryBasicReq RecipientProfileQuery;
    private PWStoreQueryUserReq StoreReq;
    private bool bFromAdmin;
    private PWInvQueryReq SenderInventory;
    private PWCombinedReq InventoryUpdateReq;
    private List<int> ItemIDsToSend;
    private PWGPRemoveReq GPRemoveReq;

    public PWMailCreateReq()
    {
      this.ItemsToSend = new List<Guid>(4);
      this.ItemIDsToSend = new List<int>(4);
    }

    public PWMailCreateReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
      this.ItemsToSend = new List<Guid>(4);
      this.ItemIDsToSend = new List<int>(4);
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_MailCreate;

    public override bool SubmitClientQuery() => this.SendMessage(this.Serializer.SerializeRaw(this.Item.SenderID.ToString() + ":" + (object) this.Item.RecipientID + ":" + (object) this.Item.Subject.Count<char>((Func<char, bool>) (x => x == ':')) + ":" + this.Item.Subject + ":" + (object) this.Item.Message.Count<char>((Func<char, bool>) (x => x == ':')) + ":" + this.Item.Message + ":" + this.Item.ItemA.ToString() + ":" + this.Item.ItemB.ToString() + ":" + this.Item.ItemC.ToString() + ":" + this.Item.ItemD.ToString() + ":" + this.Item.GPAmount.ToString() + ":" + this.Item.ZPAmount.ToString() + ":" + this.Item.Duration.ToString()));

    public override bool ParseClientQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      return strArray.Length == 1 && strArray[0] == "T";
    }

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] strArray1 = this.Serializer.Deserialize(InMessage);
      this.Item = new PWMailItem();
      if (strArray1.Length <= 10 || !long.TryParse(strArray1[0], out this.Item.SenderID))
        return false;
      if (!long.TryParse(strArray1[1], out this.Item.RecipientID))
        this.RecipientName = strArray1[1];
      int result1;
      if (!int.TryParse(strArray1[2], out result1))
        return false;
      int num1 = 3;
      PWMailItem pwMailItem1 = this.Item;
      string[] strArray2 = strArray1;
      int index1 = num1;
      int num2 = index1 + 1;
      string str1 = strArray2[index1];
      pwMailItem1.Subject = str1;
      for (int index2 = 1; index2 < result1; ++index2)
      {
        PWMailItem pwMailItem2 = this.Item;
        pwMailItem2.Subject = pwMailItem2.Subject + ":" + strArray1[num2++];
      }
      string[] strArray3 = strArray1;
      int index3 = num2;
      int num3 = index3 + 1;
      int result2;
      if (!int.TryParse(strArray3[index3], out result2))
        return false;
      PWMailItem pwMailItem3 = this.Item;
      string[] strArray4 = strArray1;
      int index4 = num3;
      int num4 = index4 + 1;
      string str2 = strArray4[index4];
      pwMailItem3.Message = str2;
      for (int index2 = 1; index2 < result2; ++index2)
      {
        PWMailItem pwMailItem2 = this.Item;
        pwMailItem2.Message = pwMailItem2.Message + ":" + strArray1[num4++];
      }
      string[] strArray5 = strArray1;
      int index5 = num4;
      int num5 = index5 + 1;
      Guid.TryParse(strArray5[index5], out this.Item.ItemA);
      string[] strArray6 = strArray1;
      int index6 = num5;
      int num6 = index6 + 1;
      Guid.TryParse(strArray6[index6], out this.Item.ItemB);
      string[] strArray7 = strArray1;
      int index7 = num6;
      int num7 = index7 + 1;
      Guid.TryParse(strArray7[index7], out this.Item.ItemC);
      string[] strArray8 = strArray1;
      int index8 = num7;
      int num8 = index8 + 1;
      Guid.TryParse(strArray8[index8], out this.Item.ItemD);
      string[] strArray9 = strArray1;
      int index9 = num8;
      int num9 = index9 + 1;
      int.TryParse(strArray9[index9], out this.Item.GPAmount);
      string[] strArray10 = strArray1;
      int index10 = num9;
      int num10 = index10 + 1;
      int.TryParse(strArray10[index10], out this.Item.ZPAmount);
      string[] strArray11 = strArray1;
      int index11 = num10;
      int num11 = index11 + 1;
      byte.TryParse(strArray11[index11], out this.Item.Duration);
      this.ItemsToSend.Add(this.Item.ItemA);
      this.ItemsToSend.Add(this.Item.ItemB);
      this.ItemsToSend.Add(this.Item.ItemC);
      this.ItemsToSend.Add(this.Item.ItemD);
      this.ItemsToSend.RemoveAll((Predicate<Guid>) (x => x == Guid.Empty));
      this.Item.TimeSent = PWServerBase.CurrentTime;
      this.Item.UniqueID = Guid.NewGuid();
      this.Item.SenderName = PWServerBase.AdminName;
      this.Item.Read = false;
      return true;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      if (this.ProfileQueries == null)
      {
        this.Log("Create Mail - Querying Profiles", false, ELoggingLevel.ELL_Verbose);
        this.bFromAdmin = this.Item.SenderID == PWServerBase.AdminUserID;
        this.ProfileQueries = new PWCombinedReq(this.Connection);
        if (this.Item.RecipientID == 0L && this.RecipientName != "")
        {
          this.RecipientProfileQuery = new PWProfileQueryBasicReq(this.Connection);
          this.RecipientProfileQuery.ForUserName = this.RecipientName;
          this.ProfileQueries.AddSubRequest((PWRequestBase) this.RecipientProfileQuery);
        }
        if (!this.bFromAdmin)
        {
          this.SenderProfileQuery = new PWProfileQueryBasicReq(this.Connection);
          this.SenderProfileQuery.ForUserID = this.Item.SenderID;
          this.ProfileQueries.AddSubRequest((PWRequestBase) this.SenderProfileQuery);
        }
        this.StoreReq = new PWStoreQueryUserReq(this.Connection);
        this.StoreReq.UserId = this.Item.RecipientID;
        this.StoreReq.Filter = "";
        this.StoreReq.UserType = (byte) 0;
        this.ProfileQueries.AddSubRequest((PWRequestBase) this.StoreReq);
        this.ProfileQueries.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
        return this.ProfileQueries.SubmitServerQuery();
      }
      if (this.ProfileQueries.Completed && this.ProfileQueries.Successful && (this.Item.RecipientID == 0L || this.Item.SenderName == ""))
      {
        if (this.Item.RecipientID == 0L && this.RecipientProfileQuery != null)
          this.Item.RecipientID = this.RecipientProfileQuery.BasicProfile.UserId;
        if (!this.bFromAdmin)
          this.Item.SenderName = this.SenderProfileQuery.BasicProfile.UserName;
        return this.SubmitServerQuery();
      }
      if (this.ItemsToSend.Count > 0 && this.SenderInventory == null)
      {
        for (int index = 0; index < this.ItemsToSend.Count; ++index)
        {
          Guid g = this.ItemsToSend[index];
          StoreItem storeItem = this.StoreReq.Items.Find((Predicate<StoreItem>) (x => x.ItemGuid == g));
          if (storeItem != (StoreItem) null)
          {
            this.ItemsToSend.RemoveAt(index--);
            this.ItemIDsToSend.Add(storeItem.ItemId);
          }
        }
        if (this.ItemsToSend.Count <= 0)
          return this.SubmitServerQuery();
        this.SenderInventory = new PWInvQueryReq(this.Connection);
        this.SenderInventory.UserId = this.Item.SenderID;
        this.SenderInventory.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
        return this.SenderInventory.SubmitServerQuery();
      }
      if (this.ItemsToSend.Count > 0 && this.SenderInventory != null && (this.SenderInventory.Successful && this.InventoryUpdateReq == null))
      {
        this.InventoryUpdateReq = new PWCombinedReq(this.Connection);
        this.InventoryUpdateReq.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
        using (List<Guid>.Enumerator enumerator = this.ItemsToSend.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            Guid itemGuid = enumerator.Current;
            PWInventoryItem pwInventoryItem = this.SenderInventory.ReadInventory.Items.Find((Predicate<PWInventoryItem>) (x => x.InstanceId == itemGuid));
            if (pwInventoryItem != null)
            {
              PWSqlItemUpdateReq<PWInventoryItem> sqlItemUpdateReq = new PWSqlItemUpdateReq<PWInventoryItem>(this.Connection);
              sqlItemUpdateReq.Item = pwInventoryItem;
              sqlItemUpdateReq.Item.UserId = PWServerBase.AdminUserID;
              this.InventoryUpdateReq.AddSubRequest((PWRequestBase) sqlItemUpdateReq);
              this.ItemIDsToSend.Add(pwInventoryItem.ItemId);
            }
          }
        }
        if (this.InventoryUpdateReq.NumSubRequests == this.ItemsToSend.Count)
          return this.InventoryUpdateReq.SubmitServerQuery();
        this.Log("Could not find all inventory items to send for mail", false, ELoggingLevel.ELL_Warnings);
        return this.ProcessServerResultDefault();
      }
      if (this.Item.GPAmount > 0 && this.GPRemoveReq == null && !this.bFromAdmin)
      {
        this.GPRemoveReq = new PWGPRemoveReq(this.Connection);
        this.GPRemoveReq.UserId = this.Item.SenderID;
        this.GPRemoveReq.Amount = this.Item.GPAmount;
        this.GPRemoveReq.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
        return this.GPRemoveReq.SubmitServerQuery();
      }
      if (!this.ProfileQueries.Successful || this.Item.GPAmount != 0 && !this.bFromAdmin && (this.GPRemoveReq == null || !this.GPRemoveReq.Successful) || this.ItemsToSend.Count != 0 && (this.InventoryUpdateReq == null || !this.InventoryUpdateReq.Successful))
        return this.ProcessServerResultDefault();
      PWSqlItemAddReq<PWMailItem> pwSqlItemAddReq = new PWSqlItemAddReq<PWMailItem>(this.Connection);
      pwSqlItemAddReq.Item = this.Item;
      pwSqlItemAddReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return pwSqlItemAddReq.SubmitServerQuery();
    }

    protected void FindUnlockIDSlot(int UnlockID)
    {
      if (this.Item.ItemA != Guid.Empty && this.Item.ItemAUnlockID == -1)
        this.Item.ItemAUnlockID = UnlockID;
      else if (this.Item.ItemB != Guid.Empty && this.Item.ItemBUnlockID == -1)
        this.Item.ItemBUnlockID = UnlockID;
      else if (this.Item.ItemC != Guid.Empty && this.Item.ItemCUnlockID == -1)
      {
        this.Item.ItemCUnlockID = UnlockID;
      }
      else
      {
        if (!(this.Item.ItemD != Guid.Empty) || this.Item.ItemDUnlockID != -1)
          return;
        this.Item.ItemDUnlockID = UnlockID;
      }
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      if (!(ForRequest is PWSQLReq pwsqlReq) || !pwsqlReq.Successful)
        return false;
      for (int index = 0; index < this.ItemIDsToSend.Count; ++index)
        this.FindUnlockIDSlot(this.ItemIDsToSend[index]);
      new PWMailNotificationReq(this.Connection, this.Item).SubmitServerQuery();
      return true;
    }

    protected override bool ProcessServerResult() => this.SendDefaultMessage();
  }
}
