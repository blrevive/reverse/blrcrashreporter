﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Mail.PWMailQueryUserReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Caches;
using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Queries.General;
using PWAPICommon.Queries.Inventory;
using PWAPICommon.Queries.Store;
using PWAPICommon.Wrappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Mail
{
  [ComVisible(true)]
  public class PWMailQueryUserReq : PWRequestBase
  {
    public long UserID;
    public List<PWMailItem> Items;
    private PWCombinedReq ItemQueries;
    private PWInvQueryReq MailInventory;
    private PWStoreQueryUserReq StoreQuery;

    public PWMailQueryUserReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWMailQueryUserReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_MailQueryUser;

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      return strArray.Length == 1 && long.TryParse(strArray[0], out this.UserID);
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      this.Log("Query Mail for User " + (object) this.UserID, false, ELoggingLevel.ELL_Informative);
      if (this.ItemQueries == null)
      {
        this.ItemQueries = new PWCombinedReq(this.Connection);
        this.ItemQueries.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
        this.MailInventory = new PWInvQueryReq(this.Connection);
        this.MailInventory.UserId = PWServerBase.AdminUserID;
        this.StoreQuery = new PWStoreQueryUserReq(this.Connection);
        this.StoreQuery.UserId = this.UserID;
        this.StoreQuery.UserType = (byte) 0;
        this.StoreQuery.Filter = "";
        this.ItemQueries.AddSubRequest((PWRequestBase) this.MailInventory);
        this.ItemQueries.AddSubRequest((PWRequestBase) this.StoreQuery);
        return this.ItemQueries.SubmitServerQuery();
      }
      if (this.ItemQueries == null || !this.ItemQueries.Successful)
        return this.ProcessServerResultDefault();
      PWOfferCache resource = this.OwningServer.GetResource<PWOfferCache>();
      if (resource != null)
        this.Items = resource.GetCachedNewsItems();
      PWSqlItemQueryKeyedReq<PWMailItem, long> itemQueryKeyedReq = new PWSqlItemQueryKeyedReq<PWMailItem, long>(this.Connection);
      itemQueryKeyedReq.Key = this.UserID;
      itemQueryKeyedReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return itemQueryKeyedReq.SubmitServerQuery();
    }

    private int GetUnlockIDFromMailGuid(Guid InGuid)
    {
      if (InGuid == Guid.Empty)
        return -1;
      if (this.MailInventory != null && this.MailInventory.ReadInventory.Items != null)
      {
        PWInventoryItem pwInventoryItem = new List<PWInventoryItem>((IEnumerable<PWInventoryItem>) this.MailInventory.ReadInventory.Items).Find((Predicate<PWInventoryItem>) (x => x.InstanceId == InGuid));
        if (pwInventoryItem != null)
          return pwInventoryItem.ItemId;
      }
      if (this.StoreQuery != null && this.StoreQuery.Items != null)
      {
        StoreItem storeItem = this.StoreQuery.Items.Find((Predicate<StoreItem>) (x => x.ItemGuid == InGuid));
        if (storeItem != (StoreItem) null)
          return storeItem.ItemId;
      }
      return -1;
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      if (!(ForRequest is PWSqlItemQueryReqBase<PWMailItem> itemQueryReqBase) || !itemQueryReqBase.Successful)
        return false;
      this.Items = itemQueryReqBase.ResponseItems.Concat<PWMailItem>((IEnumerable<PWMailItem>) (this.Items ?? new List<PWMailItem>())).ToList<PWMailItem>();
      foreach (PWMailItem pwMailItem in this.Items)
      {
        pwMailItem.ItemAUnlockID = this.GetUnlockIDFromMailGuid(pwMailItem.ItemA);
        pwMailItem.ItemBUnlockID = this.GetUnlockIDFromMailGuid(pwMailItem.ItemB);
        pwMailItem.ItemCUnlockID = this.GetUnlockIDFromMailGuid(pwMailItem.ItemC);
        pwMailItem.ItemDUnlockID = this.GetUnlockIDFromMailGuid(pwMailItem.ItemD);
      }
      return true;
    }

    protected override bool ProcessServerResult() => this.SendMessage(this.CompressMessage(this.SerializeMessage(this.Serializer.SerializeObject<List<PWMailItem>>(this.Items))));
  }
}
