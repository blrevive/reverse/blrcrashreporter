﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Mail.PWMailOpenReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Queries.Currency;
using PWAPICommon.Queries.General;
using PWAPICommon.Queries.Inventory;
using PWAPICommon.Queries.Store;
using PWAPICommon.Wrappers;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Mail
{
  [ComVisible(true)]
  public class PWMailOpenReq : PWMailReqBase
  {
    private PWCombinedReq LookupRequest;
    private PWMailQueryUserReq UserMail;
    private PWInvQueryReq SourceInventory;
    private PWInvQueryReq DestInventory;
    private PWStoreQueryUserReq SourceStore;
    private PWCombinedReq ResultRequest;
    private PWSqlItemUpdateReq<PWMailItem> MailUpdate;
    private List<PWInventoryItem> GeneratedInventory;
    private PWGPAddReq GPAddReq;
    private Guid TargetGuid;
    private long UserID;

    public PWMailOpenReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWMailOpenReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_MailReceiveItems;

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      return strArray.Length == 2 && long.TryParse(strArray[0], out this.UserID) && Guid.TryParse(strArray[1], out this.TargetGuid);
    }

    private PWRequestBase CreateOpenMailAction(Guid InItemGuid)
    {
      if (InItemGuid == Guid.Empty)
        return (PWRequestBase) null;
      StoreItem storeItem = this.SourceStore.Items.Find((Predicate<StoreItem>) (x => x.ItemGuid == InItemGuid));
      if (storeItem != (StoreItem) null)
      {
        PWInventoryItem purchaseItem = storeItem.GeneratePurchaseItem(this.Item.RecipientID, (EPurchaseLength) this.Item.Duration, 1);
        this.GeneratedInventory.Add(purchaseItem);
        PWSqlItemAddReq<PWInventoryItem> pwSqlItemAddReq = new PWSqlItemAddReq<PWInventoryItem>(this.Connection);
        pwSqlItemAddReq.Item = purchaseItem;
        return (PWRequestBase) pwSqlItemAddReq;
      }
      PWInventoryItem pwInventoryItem = this.SourceInventory.ReadInventory.Items.Find((Predicate<PWInventoryItem>) (x => x.InstanceId == InItemGuid));
      if (pwInventoryItem == null)
        return (PWRequestBase) null;
      PWSqlItemUpdateReq<PWInventoryItem> sqlItemUpdateReq = new PWSqlItemUpdateReq<PWInventoryItem>(this.Connection);
      sqlItemUpdateReq.Item = pwInventoryItem;
      sqlItemUpdateReq.Item.UserId = this.Item.RecipientID;
      this.GeneratedInventory.Add(pwInventoryItem);
      return (PWRequestBase) sqlItemUpdateReq;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      if (this.LookupRequest == null)
      {
        this.LookupRequest = new PWCombinedReq(this.Connection);
        this.UserMail = new PWMailQueryUserReq(this.Connection);
        this.UserMail.UserID = this.UserID;
        this.SourceInventory = new PWInvQueryReq(this.Connection);
        this.SourceInventory.UserId = PWServerBase.AdminUserID;
        this.DestInventory = new PWInvQueryReq(this.Connection);
        this.DestInventory.UserId = this.UserID;
        this.SourceStore = new PWStoreQueryUserReq(this.Connection);
        this.SourceStore.UserId = this.UserID;
        this.SourceStore.Filter = "";
        this.SourceStore.UserType = (byte) 0;
        this.LookupRequest.AddSubRequest((PWRequestBase) this.SourceInventory);
        this.LookupRequest.AddSubRequest((PWRequestBase) this.DestInventory);
        this.LookupRequest.AddSubRequest((PWRequestBase) this.UserMail);
        this.LookupRequest.AddSubRequest((PWRequestBase) this.SourceStore);
        this.LookupRequest.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
        return this.LookupRequest.SubmitServerQuery();
      }
      if (this.LookupRequest.Successful && this.Item == null)
      {
        this.Item = this.UserMail.Items.Find((Predicate<PWMailItem>) (x => x.UniqueID == this.TargetGuid));
        return this.Item != null ? this.SubmitServerQuery() : this.ProcessServerResultDefault();
      }
      if (this.Item != null && this.ResultRequest == null)
      {
        List<PWRequestBase> pwRequestBaseList = new List<PWRequestBase>(4);
        this.GeneratedInventory = new List<PWInventoryItem>(4);
        PWRequestBase openMailAction1 = this.CreateOpenMailAction(this.Item.ItemA);
        if (openMailAction1 != null)
          pwRequestBaseList.Add(openMailAction1);
        PWRequestBase openMailAction2 = this.CreateOpenMailAction(this.Item.ItemB);
        if (openMailAction2 != null)
          pwRequestBaseList.Add(openMailAction2);
        PWRequestBase openMailAction3 = this.CreateOpenMailAction(this.Item.ItemC);
        if (openMailAction3 != null)
          pwRequestBaseList.Add(openMailAction3);
        PWRequestBase openMailAction4 = this.CreateOpenMailAction(this.Item.ItemD);
        if (openMailAction4 != null)
          pwRequestBaseList.Add(openMailAction4);
        if (pwRequestBaseList.Count > 0 && pwRequestBaseList.Count > this.DestInventory.ReadInventory.SlotsLeft)
        {
          this.Log("Inventory Cap Reached for User " + this.UserID.ToString(), false, ELoggingLevel.ELL_Informative);
          return this.ProcessServerResultDefault();
        }
        if (this.Item.GPAmount > 0)
        {
          this.GPAddReq = new PWGPAddReq(this.Connection);
          this.GPAddReq.UserId = this.Item.RecipientID;
          this.GPAddReq.Amount = this.Item.GPAmount;
          pwRequestBaseList.Add((PWRequestBase) this.GPAddReq);
        }
        if (pwRequestBaseList.Count <= 0)
          return this.ProcessServerResultDefault();
        this.ResultRequest = new PWCombinedReq(this.Connection);
        foreach (PWRequestBase NewRequest in pwRequestBaseList)
          this.ResultRequest.AddSubRequest(NewRequest);
        this.ResultRequest.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
        return this.ResultRequest.SubmitServerQuery();
      }
      if (this.ResultRequest == null || !this.ResultRequest.Successful || this.MailUpdate != null)
        return this.ProcessServerResultDefault();
      this.MailUpdate = new PWSqlItemUpdateReq<PWMailItem>(this.Connection);
      this.Item.ItemA = Guid.Empty;
      this.Item.ItemB = Guid.Empty;
      this.Item.ItemC = Guid.Empty;
      this.Item.ItemD = Guid.Empty;
      this.Item.ZPAmount = 0;
      this.Item.GPAmount = 0;
      this.Item.Read = true;
      this.MailUpdate.Item = this.Item;
      this.MailUpdate.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
      return this.MailUpdate.SubmitServerQuery();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      if (this.MailUpdate != null)
        return this.MailUpdate.Successful;
      return this.GeneratedInventory != null && this.GeneratedInventory.Count == 0;
    }

    protected override bool ProcessServerResult()
    {
      if (this.Successful && this.GeneratedInventory != null && this.GeneratedInventory.Count > 0)
        new PWInvUpdatedReq(this.Connection, this.Item.RecipientID, this.GeneratedInventory).SubmitServerQuery();
      return this.SendDefaultMessage();
    }
  }
}
