﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Mail.PWMailReqBase
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using System;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Mail
{
  [ComVisible(true)]
  public abstract class PWMailReqBase : PWRequestBase
  {
    public PWMailItem Item;
    public string RecipientName;

    public PWMailReqBase()
      : base((PWConnectionBase) null)
    {
    }

    public PWMailReqBase(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => throw new NotImplementedException();
  }
}
