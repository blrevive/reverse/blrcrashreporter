﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Mail.PWMailDestroyReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Wrappers;
using System;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Mail
{
  [ComVisible(true)]
  public class PWMailDestroyReq : PWMailReqBase
  {
    public PWMailDestroyReq()
    {
    }

    public PWMailDestroyReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_MailDestroy;

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      this.Item = new PWMailItem();
      return strArray.Length == 2 && long.TryParse(strArray[0], out this.Item.RecipientID) && Guid.TryParse(strArray[1], out this.Item.UniqueID);
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      this.Log("Delete Mail for User " + (object) this.Item.RecipientID, false, ELoggingLevel.ELL_Informative);
      PWSqlItemRemoveReq<PWMailItem> sqlItemRemoveReq = new PWSqlItemRemoveReq<PWMailItem>(this.Connection);
      sqlItemRemoveReq.Item = this.Item;
      sqlItemRemoveReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return sqlItemRemoveReq.SubmitServerQuery();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => ForRequest is PWSQLReq pwsqlReq && pwsqlReq.Successful;

    protected override bool ProcessServerResult() => this.SendDefaultMessage();
  }
}
