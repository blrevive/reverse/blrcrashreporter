﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Mail.PWMailNotificationReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Queries.General;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Mail
{
  [ComVisible(true)]
  public class PWMailNotificationReq : PWNotificationReq<PWMailItem>
  {
    public PWMailNotificationReq()
    {
    }

    public PWMailNotificationReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    public PWMailNotificationReq(PWConnectionBase InConnection, PWMailItem InUpdatedItem)
      : this(InConnection, new List<PWMailItem>()
      {
        InUpdatedItem
      })
    {
    }

    public PWMailNotificationReq(PWConnectionBase InConnection, List<PWMailItem> InUpdatedItem)
      : base(InConnection, 0L, (IEnumerable<PWMailItem>) InUpdatedItem)
    {
      if (this.Items == null || this.Items.Length <= 0)
        return;
      this.UserID = this.Items[0].RecipientID;
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_MailNotification;

    public override object Clone()
    {
      PWMailNotificationReq mailNotificationReq = new PWMailNotificationReq();
      mailNotificationReq.UserID = this.UserID;
      mailNotificationReq.Items = this.Items;
      return (object) mailNotificationReq;
    }

    protected override bool ProcessServerResult() => this.SendMessage(this.CompressMessage(this.SerializeMessage(this.Serializer.SerializeObject<PWMailItem[]>(this.Items))));
  }
}
