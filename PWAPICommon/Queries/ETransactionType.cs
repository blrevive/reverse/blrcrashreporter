﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.ETransactionType
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System.Runtime.InteropServices;

namespace PWAPICommon.Queries
{
  [ComVisible(true)]
  public enum ETransactionType
  {
    ETT_PurchaseZP = 0,
    ETT_PurchaseGP = 1,
    ETT_Activate = 2,
    ETT_Apply = 3,
    ETT_RenewZP = 4,
    ETT_RenewGP = 5,
    ETT_Destroy = 6,
    ETT_Gift = 7,
    ETT_Invalid = 999, // 0x000003E7
  }
}
