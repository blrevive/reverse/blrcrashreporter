﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Machine.PWMachineInfoVerifySetReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Machine
{
  [ComVisible(true)]
  public class PWMachineInfoVerifySetReq : PWRequestBase
  {
    public Guid MachineToSet;

    public PWMachineInfoVerifySetReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWMachineInfoVerifySetReq(PWConnectionBase InConn)
      : base(InConn)
    {
    }

    protected override EMessageType GetMessageType() => throw new NotImplementedException();

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      SqlCommand sqlCommand = new SqlCommand("SP_VerifiedMachines_SetVerified");
      sqlCommand.CommandType = CommandType.StoredProcedure;
      sqlCommand.AddParameter<Guid>("@UniqueID", SqlDbType.VarBinary, this.MachineToSet);
      PWSQLReq pwsqlReq = new PWSQLReq(sqlCommand, this.Connection);
      pwsqlReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return pwsqlReq.SubmitServerQuery();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      PWSQLReq pwsqlReq = (PWSQLReq) ForRequest;
      return pwsqlReq != null && pwsqlReq.Successful;
    }
  }
}
