﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Machine.PWMachineInfoVerifyQueryUserReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Wrappers;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Machine
{
  [ComVisible(true)]
  public class PWMachineInfoVerifyQueryUserReq : PWRequestBase
  {
    public long UserID;
    public List<PWVerifiedMachine> FoundMachines;

    public PWMachineInfoVerifyQueryUserReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWMachineInfoVerifyQueryUserReq(PWConnectionBase InConn)
      : base(InConn)
    {
    }

    protected override EMessageType GetMessageType() => throw new NotImplementedException();

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      PWSqlItemQueryKeyedReq<PWVerifiedMachine, long> itemQueryKeyedReq = new PWSqlItemQueryKeyedReq<PWVerifiedMachine, long>(this.Connection);
      itemQueryKeyedReq.Key = this.UserID;
      itemQueryKeyedReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return itemQueryKeyedReq.SubmitServerQuery();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      PWSQLReq pwsqlReq = (PWSQLReq) ForRequest;
      if (pwsqlReq != null)
      {
        IList<object> responseValues = pwsqlReq.ResponseValues;
        if (responseValues != null && responseValues.Count > 0)
        {
          this.FoundMachines = new List<PWVerifiedMachine>(responseValues.Count);
          for (int index = 0; index < responseValues.Count; ++index)
          {
            PWVerifiedMachine pwVerifiedMachine = new PWVerifiedMachine();
            if (pwVerifiedMachine.ParseFromArray(responseValues[index] as object[]))
              this.FoundMachines.Add(pwVerifiedMachine);
          }
          return true;
        }
      }
      return false;
    }
  }
}
