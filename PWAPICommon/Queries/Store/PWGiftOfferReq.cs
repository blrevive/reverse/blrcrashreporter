﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Store.PWGiftOfferReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Caches;
using PWAPICommon.Clients;
using PWAPICommon.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Store
{
  [ComVisible(true)]
  public class PWGiftOfferReq : PWRequestBase, ICloneable
  {
    public List<long> UserIds;
    public EOfferType OfferType;
    public OfferDataBase OfferData;
    private static List<Type> OfferDataTypes = PWOfferItem.CustomDataTypes.ToList<Type>();

    public PWGiftOfferReq()
      : this((PWConnectionBase) null)
    {
    }

    public PWGiftOfferReq(PWConnectionBase InConnection)
      : base(InConnection)
      => this.UserIds = new List<long>();

    protected override EMessageType GetMessageType() => EMessageType.EMT_GiftOffer;

    protected override List<Type> GetSerializableTypes() => base.GetSerializableTypes().Concat<Type>((IEnumerable<Type>) PWGiftOfferReq.OfferDataTypes).ToList<Type>();

    public object Clone() => (object) new PWGiftOfferReq()
    {
      UserIds = this.UserIds,
      OfferType = this.OfferType,
      OfferData = this.OfferData
    };

    public override bool ParseServerQuery(byte[] InMessage)
    {
      PWGiftOfferReq pwGiftOfferReq = this.DeserializeSelf<PWGiftOfferReq>(InMessage);
      if (pwGiftOfferReq == null)
        return base.ParseServerQuery(InMessage);
      this.UserIds = pwGiftOfferReq.UserIds;
      this.OfferType = pwGiftOfferReq.OfferType;
      this.OfferData = pwGiftOfferReq.OfferData;
      return true;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      PWOfferCache resource = this.Connection.OwningServer.GetResource<PWOfferCache>();
      if (resource != null)
      {
        foreach (long userId in this.UserIds)
          resource.AwardMatchingOffers(userId, this.OfferType, this.OfferData, this.Connection);
      }
      else
        this.OwningServer.SendInternalQuery(this.Clone() as PWRequestBase, EServerType.EST_Item, 5);
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => true;

    protected override bool ProcessServerResult() => this.SendDefaultMessage();

    public override bool ParseClientQuery(byte[] InMessage) => true;

    public override bool SubmitClientQuery()
    {
      base.SubmitClientQuery();
      return this.Connection != null && this.SendMessage(this.SerializeSelf());
    }
  }
}
