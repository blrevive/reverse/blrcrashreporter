﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Store.PWActivationHandler
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Common;
using System;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Store
{
  [ComVisible(true)]
  public class PWActivationHandler
  {
    private Type handlerType;
    private int RangeBegin;
    private int RangeEnd;
    private EPurchaseResult ErrorCode;
    private ActivationHandlerDataGenerator DataGenerator;

    public Type HandlerType => this.handlerType;

    public EPurchaseResult Error => this.ErrorCode;

    public PWActivationHandler(
      int ItemId,
      Type RequestType,
      ActivationHandlerDataGenerator InGenerator,
      EPurchaseResult InErrorCode)
      : this(ItemId, ItemId, RequestType, InGenerator, InErrorCode)
    {
    }

    public PWActivationHandler(
      int ItemIdBegin,
      int ItemIdEnd,
      Type RequestType,
      ActivationHandlerDataGenerator InGenerator,
      EPurchaseResult InErrorCode)
    {
      this.RangeBegin = ItemIdBegin;
      this.RangeEnd = this.RangeBegin + Math.Max(ItemIdEnd - ItemIdBegin, 0);
      this.handlerType = RequestType;
      this.DataGenerator = InGenerator;
      this.ErrorCode = InErrorCode;
    }

    public string GetActivationData(string UserData, PWActivateItemReq Req) => this.DataGenerator(UserData, Req);

    public bool Contained(int ItemId) => ItemId >= this.RangeBegin && ItemId <= this.RangeEnd;

    public bool Intersects(int ItemIdBegin, int ItemIdEnd)
    {
      ItemIdEnd = ItemIdBegin + Math.Max(ItemIdEnd - ItemIdBegin, 0);
      if (ItemIdBegin >= this.RangeBegin && ItemIdBegin <= this.RangeEnd || ItemIdEnd >= this.RangeBegin && ItemIdEnd <= this.RangeEnd || this.RangeBegin >= ItemIdBegin && this.RangeBegin <= ItemIdEnd)
        return true;
      return this.RangeEnd >= ItemIdBegin && this.RangeEnd <= ItemIdEnd;
    }
  }
}
