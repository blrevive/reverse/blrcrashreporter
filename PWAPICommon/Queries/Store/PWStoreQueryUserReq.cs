﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Store.PWStoreQueryUserReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Wrappers;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Store
{
  [ComVisible(true)]
  public class PWStoreQueryUserReq : PWRequestBase
  {
    public long UserId;
    public byte UserType;
    public string Filter;
    public List<StoreItem> Items;
    private byte[] CachedMessage;

    public PWStoreQueryUserReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWStoreQueryUserReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] stringArray = this.Serializer.DeserializeToStringArray(InMessage);
      if (stringArray.Length != 3 || !long.TryParse(stringArray[0], out this.UserId) || !byte.TryParse(stringArray[1], out this.UserType))
        return false;
      this.Filter = stringArray[2];
      return true;
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_QueryStoreUser;

    public override bool SubmitServerQuery()
    {
      this.bSubmitted = true;
      this.Log("Querying the Store for UserId " + (object) this.UserId, false, ELoggingLevel.ELL_Informative);
      PWStoreCache resource = this.Connection.OwningServer.GetResource<PWStoreCache>();
      if (resource != null)
      {
        resource.GetCachedItemsAndMessage((int) this.UserType, this.Filter, out this.Items, out this.CachedMessage);
        return this.ProcessServerResultDefault();
      }
      PWSqlItemQueryKeyedReq<StoreItem, int> itemQueryKeyedReq = new PWSqlItemQueryKeyedReq<StoreItem, int>(this.Connection);
      itemQueryKeyedReq.Key = (int) this.UserType;
      itemQueryKeyedReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return itemQueryKeyedReq.SubmitServerQuery();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      if (!(ForRequest is PWSqlItemQueryKeyedReq<StoreItem, int> itemQueryKeyedReq) || !itemQueryKeyedReq.Successful)
        return this.CachedMessage != null;
      this.Items = itemQueryKeyedReq.ResponseItems.ToList<StoreItem>();
      return true;
    }

    protected override bool ProcessServerResult()
    {
      if (this.Successful)
      {
        if (this.CachedMessage != null)
          return this.Connection.SendEncodedMessage(this.MessageType, this.CachedMessage);
        return this.SendMessage(this.CompressMessage(this.SerializeMessage<List<StoreItem>>("", this.Items, new List<XmlTinyAttribute>()
        {
          new XmlTinyAttribute(typeof (StoreItem), "InitialQuantity"),
          new XmlTinyAttribute(typeof (StoreItem), "UserType"),
          new XmlTinyAttribute(typeof (StoreItem), "FriendlyName"),
          new XmlTinyAttribute(typeof (StoreItem), "ActivationData"),
          new XmlTinyAttribute(typeof (StoreItem), "VisibilityLevel")
        }.ToArray())));
      }
      this.Log("Store Query Failed", false, ELoggingLevel.ELL_Errors);
      return this.SendDefaultMessage();
    }
  }
}
