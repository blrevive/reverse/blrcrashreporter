﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Store.PWPurchaseItemReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Queries.Currency;
using PWAPICommon.Queries.General;
using PWAPICommon.Queries.Inventory;
using PWAPICommon.Queries.Mail;
using PWAPICommon.Queries.Player;
using PWAPICommon.Wrappers;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Store
{
  [ComVisible(true)]
  public class PWPurchaseItemReq : PWRequestBase
  {
    public long userid;
    private Guid ItemGuid;
    private string ActivationData;
    public int Count;
    public EPurchaseLength PurchaseLength;
    public ECurrencyType Currency;
    public EPurchaseType PurchaseType;
    public EPurchaseLocation PurchaseLocation;
    public string ItemRecipient;
    private StoreItem TargetStoreItem;
    private PWStoreQueryUserReq StoreReq;
    private PWQueryProfileReq ProfileReq;
    private PWInvQueryReq InvReq;
    private PWCombinedReq GetDataReq;
    private PWInventoryItem GeneratedInventory;
    private PWRequestBase FundRemovalRequest;
    private PWSqlItemAddReq<PWInventoryItem> InventoryAddRequest;
    private PWRequestBase PostPurchaseRequest;
    private PWGPQueryReq GPRequest;
    private int PurchaseCost;
    private EPurchaseResult PurchaseResult;

    public PWPurchaseItemReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
            this.PurchaseResult = EPurchaseResult.EPR_UnknownError;
            this.PostPurchaseRequest = null;
    }

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] stringArray = this.Serializer.DeserializeToStringArray(InMessage);
      if (stringArray.Length < 7 || !long.TryParse(stringArray[0], out this.userid) || (!Guid.TryParse(stringArray[1], out this.ItemGuid) || !int.TryParse(stringArray[2], out this.Count)) || (!EnumHelper.TryParse<EPurchaseLength>(stringArray[3], out this.PurchaseLength) || !EnumHelper.TryParse<EPurchaseLocation>(stringArray[4], out this.PurchaseLocation) || (!EnumHelper.TryParse<ECurrencyType>(stringArray[5], out this.Currency) || !EnumHelper.TryParse<EPurchaseType>(stringArray[6], out this.PurchaseType))))
        return false;
      this.ItemRecipient = stringArray[7];
      if (stringArray.Length >= 9)
      {
        this.ActivationData = stringArray[8];
        for (int index = 9; index < stringArray.Length; ++index)
        {
          PWPurchaseItemReq pwPurchaseItemReq = this;
          pwPurchaseItemReq.ActivationData = pwPurchaseItemReq.ActivationData + ":" + stringArray[index];
        }
      }
      return true;
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_PurchaseItem;

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      if (this.StoreReq == null)
      {
        this.StoreReq = new PWStoreQueryUserReq(this.Connection);
        this.StoreReq.UserId = this.userid;
        this.StoreReq.UserType = (byte) 0;
        this.StoreReq.Filter = "";
        this.InvReq = new PWInvQueryReq(this.Connection);
        this.InvReq.UserId = this.userid;
        this.GetDataReq = new PWCombinedReq(this.Connection);
        this.GetDataReq.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
        this.ProfileReq = new PWQueryProfileReq(this.Connection);
        this.ProfileReq.ForUserID = this.userid;
        this.GetDataReq.AddSubRequest((PWRequestBase) this.StoreReq);
        this.GetDataReq.AddSubRequest((PWRequestBase) this.InvReq);
        this.GetDataReq.AddSubRequest((PWRequestBase) this.ProfileReq);
        if (this.Currency == ECurrencyType.EC_GP)
        {
          this.GPRequest = new PWGPQueryReq(this.Connection);
          this.GPRequest.UserId = this.userid;
          this.GetDataReq.AddSubRequest((PWRequestBase) this.GPRequest);
        }
        return this.GetDataReq.SubmitServerQuery();
      }
      if (this.GetDataReq.Completed && this.GeneratedInventory == null && this.TargetStoreItem == (StoreItem) null)
      {
        if (!this.GetDataReq.Successful)
        {
          if (!this.GPRequest.Successful)
            this.PurchaseResult = EPurchaseResult.EPR_InsufficientFunds;
          return this.ProcessServerResultDefault();
        }
        this.Log("Purchase Item Validating with Store", false, ELoggingLevel.ELL_Verbose);
        this.TargetStoreItem = this.StoreReq.Items.Find((Predicate<StoreItem>) (x => x.ItemGuid == this.ItemGuid));
        if (this.TargetStoreItem == (StoreItem) null)
        {
          this.Log("Store Item not found, error!", false, ELoggingLevel.ELL_Warnings);
          this.PurchaseResult = EPurchaseResult.EPR_InvalidItem;
          return this.ProcessServerResultDefault();
        }
        long UserId = 0;
        switch (this.PurchaseType)
        {
          case EPurchaseType.EPT_Inventory:
          case EPurchaseType.EPT_Activate:
            UserId = this.userid;
            break;
        }
        this.PurchaseResult = this.TargetStoreItem.ValidatePurchase(UserId, this.InvReq.ReadInventory, this.PurchaseLength, this.Currency, this.Count);
        switch (this.PurchaseResult)
        {
          case EPurchaseResult.EPR_Success:
            this.PurchaseCost = this.TargetStoreItem.CalculateCostFor(this.PurchaseLength, this.Currency, this.Count);
            this.GeneratedInventory = this.TargetStoreItem.GeneratePurchaseItem(this.userid, this.PurchaseLength, this.Count);
            this.Log("Item Generated Successfully", false, ELoggingLevel.ELL_Verbose);
            break;
          case EPurchaseResult.EPR_InvalidLength:
            this.Log("Invalid Purchase Parameters for Item " + this.TargetStoreItem.ItemId.ToString() + ": " + this.PurchaseLength.ToString() + ", " + this.Currency.ToString() + ", " + (object) this.Count, false, ELoggingLevel.ELL_Warnings);
            return this.ProcessServerResultDefault();
          case EPurchaseResult.EPR_InventoryCapped:
            this.Log("Inventory Cap Reached for User " + UserId.ToString(), false, ELoggingLevel.ELL_Informative);
            return this.ProcessServerResultDefault();
          default:
            this.Log("Store Item Purchase Failed for Unknown or Unhandled Reason: " + this.PurchaseResult.ToString(), false, ELoggingLevel.ELL_Warnings);
            return this.ProcessServerResultDefault();
        }
      }
      if (this.GeneratedInventory != null && this.FundRemovalRequest == null)
      {
        switch (this.Currency)
        {
          case ECurrencyType.EC_GP:
            this.FundRemovalRequest = (PWRequestBase) new PWGPRemoveReq(this.Connection)
            {
              UserId = this.userid,
              Amount = this.PurchaseCost
            };
            break;
          case ECurrencyType.EC_ZP:
            this.FundRemovalRequest = (PWRequestBase) new PWZPRemoveReq(this.Connection)
            {
              userid = this.userid,
              amount = this.PurchaseCost,
              count = this.Count,
              server = this.Connection.OwningServer.PWServerID,
              game = this.Connection.OwningServer.PWGameID,
              itemid = this.GeneratedInventory.ItemId,
              uniqueid = this.GeneratedInventory.InstanceId.ToString(),
              charid = 0
            };
            break;
        }
        this.Log("Purchase Item starting Fund Removal Request of" + (object) this.PurchaseCost + this.Currency.ToString(), false, ELoggingLevel.ELL_Verbose);
        this.FundRemovalRequest.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
        return this.FundRemovalRequest.SubmitServerQuery();
      }
      if (this.FundRemovalRequest != null && this.InventoryAddRequest == null)
      {
        if (this.FundRemovalRequest.Successful)
        {
          this.Log("Purchase Item Fund Removal succeeded, starting Inventory Add", false, ELoggingLevel.ELL_Verbose);
          this.InventoryAddRequest = new PWSqlItemAddReq<PWInventoryItem>(this.Connection);
          this.InventoryAddRequest.Item = this.GeneratedInventory;
          this.InventoryAddRequest.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
          return this.InventoryAddRequest.SubmitServerQuery();
        }
        this.Log("Purchase Item Error, sending Failed Response", false, ELoggingLevel.ELL_Verbose);
        this.PurchaseResult = EPurchaseResult.EPR_InsufficientFunds;
        return this.ProcessServerResultDefault();
      }
      if (this.InventoryAddRequest != null && this.InventoryAddRequest.Successful && this.PostPurchaseRequest == null)
      {
        if (this.PurchaseType == EPurchaseType.EPT_Gift || this.PurchaseType == EPurchaseType.EPT_Mail)
        {
          PWMailCreateReq pwMailCreateReq = new PWMailCreateReq(this.Connection);
          this.PostPurchaseRequest = (PWRequestBase) pwMailCreateReq;
          PWMailItem pwMailItem = new PWMailItem();
          pwMailItem.ItemA = this.GeneratedInventory.InstanceId;
          pwMailItem.SenderID = this.userid;
          pwMailItem.SenderName = this.PurchaseType != EPurchaseType.EPT_Mail ? this.ProfileReq.FoundProfile.UserName : PWServerBase.PurchaseSenderName;
          pwMailItem.ItemAUnlockID = this.GeneratedInventory.ItemId;
          pwMailItem.Subject = "You got an item!";
          pwMailItem.Message = "Purchased for " + (object) this.PurchaseCost + " " + (this.Currency == ECurrencyType.EC_GP ? (object) "GP" : (object) "Zen");
          if (this.PurchaseType == EPurchaseType.EPT_Gift)
          {
            pwMailCreateReq.RecipientName = this.ItemRecipient;
          }
          else
          {
            pwMailItem.RecipientID = this.userid;
            pwMailCreateReq.RecipientName = this.ProfileReq.FoundProfile.UserName;
          }
          pwMailCreateReq.Item = pwMailItem;
          pwMailCreateReq.ItemsToSend = new List<Guid>();
          pwMailCreateReq.ItemsToSend.Add(this.GeneratedInventory.InstanceId);
          pwMailCreateReq.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
          return pwMailCreateReq.SubmitServerQuery();
        }
        if (this.PurchaseType == EPurchaseType.EPT_Activate)
        {
          PWActivateItemReq pwActivateItemReq = new PWActivateItemReq(this.Connection);
          pwActivateItemReq.UserId = this.userid;
          pwActivateItemReq.ActivationData = this.ActivationData;
          pwActivateItemReq.TargetInstanceId = this.GeneratedInventory.InstanceId;
          pwActivateItemReq.ActivationCount = this.GeneratedInventory.UsesLeft;
          pwActivateItemReq.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
          this.PostPurchaseRequest = (PWRequestBase) pwActivateItemReq;
          return pwActivateItemReq.SubmitServerQuery();
        }
      }
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      if (this.FundRemovalRequest != null)
        this.Log("FundRemovalRequest: " + this.FundRemovalRequest.Successful.ToString(), false, ELoggingLevel.ELL_Informative);
      if (this.InventoryAddRequest != null)
        this.Log("InventoryAddRequest: " + this.InventoryAddRequest.Successful.ToString(), false, ELoggingLevel.ELL_Informative);
      if (this.PostPurchaseRequest != null)
        this.Log("PostPurchaseRequest: " + this.PostPurchaseRequest.Successful.ToString(), false, ELoggingLevel.ELL_Informative);
      if (this.FundRemovalRequest == null || !this.FundRemovalRequest.Successful || (this.InventoryAddRequest == null || !this.InventoryAddRequest.Successful))
        return false;
      return this.PostPurchaseRequest == null || this.PostPurchaseRequest.Successful;
    }

    protected override bool ProcessServerResult()
    {
      if (this.PostPurchaseRequest is PWActivateItemReq postPurchaseRequest)
        this.PurchaseResult = postPurchaseRequest.ActivateResult;
      List<PWInventoryItem> InUpdatedItems = (List<PWInventoryItem>) null;
      if (this.Successful)
      {
        this.PurchaseResult = EPurchaseResult.EPR_Success;
        this.LogBuyItem();
        PWTransactionAddReq transactionAddReq = new PWTransactionAddReq(this.Connection);
        transactionAddReq.Item = new PWTransaction(this.userid, this.Currency == ECurrencyType.EC_GP ? ETransactionType.ETT_PurchaseGP : ETransactionType.ETT_PurchaseZP, this.PurchaseLength, this.ItemGuid, this.GeneratedInventory.InstanceId);
        transactionAddReq.SubmitServerQuery();
        /*if (this.PurchaseType == EPurchaseType.EPT_Activate && PostPurchaseRequest != null)
          InUpdatedItems = postPurchaseRequest.UpdateItems;
        else */if (this.PurchaseType == EPurchaseType.EPT_Inventory)
        {
          InUpdatedItems = new List<PWInventoryItem>(1);
          InUpdatedItems.Add(this.GeneratedInventory);
        }
        if (this.PurchaseType != EPurchaseType.EPT_Mail && InUpdatedItems.Count > 0)
          new PWInvUpdatedReq(this.Connection, this.userid, InUpdatedItems).SubmitServerQuery();
      }
      else
      {
        if (this.PurchaseType == EPurchaseType.EPT_Activate && this.InventoryAddRequest != null && this.InventoryAddRequest.Successful)
          new PWInvUpdatedReq(this.Connection, this.userid, new List<PWInventoryItem>(1)
          {
            this.GeneratedInventory
          }).SubmitServerQuery();
        this.Log("Purchase Item Failed", false, ELoggingLevel.ELL_Informative);
      }
      return this.GeneratedInventory != null ? this.SendMessage(this.SerializeMessage(((byte) this.PurchaseResult).ToString() + ":" + this.GeneratedInventory.InstanceId.ToString())) : this.SendMessage(this.SerializeMessage(((byte) this.PurchaseResult).ToString() + ":" + Guid.Empty.ToString()));
    }

    private void LogBuyItem()
    {
      string str = "buyitem" + " serverid=" + (object) this.Connection.OwningServer.PWServerID + " userid=" + (object) this.userid + " itemid=" + (object) this.ItemGuid + " unlockid=" + (object) this.TargetStoreItem.ItemId + " count=" + (object) this.Count + " time=" + (object) this.PurchaseLength + " location=" + (object) this.PurchaseLocation + " usegp=" + (this.Currency == ECurrencyType.EC_GP ? "true" : "false") + " cost=" + (object) this.PurchaseCost;
      int num = -1;
      if (this.Currency == ECurrencyType.EC_GP)
        num = this.GPRequest.Funds - this.PurchaseCost;
      else if (this.Currency == ECurrencyType.EC_ZP)
        num = (this.FundRemovalRequest as PWZPRemoveReq).TotalFunds;
      this.Log(str + " balance=" + (object) num, true, ELoggingLevel.ELL_Verbose);
    }
  }
}
