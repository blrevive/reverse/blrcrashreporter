﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.FuseInventoryHandlerReqBase
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Queries.Inventory;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries
{
  [ComVisible(true)]
  public abstract class FuseInventoryHandlerReqBase : PWRequestBase
  {
    protected PWInventoryItem ItemA;
    protected PWInventoryItem ItemB;
    protected string FusionData;
    protected long ForUserID;
    public Guid ResultingInventoryGuid;
    private List<PWInventoryItem> UpdatedInventoryItems;

    public FuseInventoryHandlerReqBase()
      : base((PWConnectionBase) null)
    {
    }

    public FuseInventoryHandlerReqBase(PWConnectionBase InClient)
      : base(InClient)
    {
    }

    public FuseInventoryHandlerReqBase(
      PWConnectionBase InConnection,
      long UserId,
      PWInventoryItem InvItemA,
      PWInventoryItem InvItemB,
      string AdditionalFusionData)
      : base(InConnection)
    {
      this.ForUserID = UserId;
      this.ItemA = InvItemA;
      this.ItemB = InvItemB;
      this.FusionData = AdditionalFusionData;
    }

    public override bool ParseServerQuery(byte[] InMessage) => false;

    public override bool SubmitServerQuery() => base.SubmitServerQuery();

    protected void AddChangedInventoryItem(PWInventoryItem ChangedItem)
    {
      if (this.UpdatedInventoryItems == null)
        this.UpdatedInventoryItems = new List<PWInventoryItem>(1);
      this.UpdatedInventoryItems.Add(ChangedItem);
    }

    protected void PushInventoryChanges()
    {
      if (this.UpdatedInventoryItems != null)
      {
        foreach (PWInventoryItem updatedInventoryItem in this.UpdatedInventoryItems)
          this.Log("Pushing notification for item guid " + updatedInventoryItem.InstanceId.ToString(), false, ELoggingLevel.ELL_Informative);
        new PWInvUpdatedReq(this.Connection, this.ForUserID, this.UpdatedInventoryItems).SubmitServerQuery();
      }
      this.OnInventoryChangesPushed((PWRequestBase) null);
    }

    private bool OnInventoryChangesPushed(PWRequestBase ForRequest) => this.ProcessServerResultDefault();

    public override bool ParseServerResult(PWRequestBase ForRequest) => false;

    protected override bool ProcessServerResult() => this.SendDefaultMessage();
  }
}
