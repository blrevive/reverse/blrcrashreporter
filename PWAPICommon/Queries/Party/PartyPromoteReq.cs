﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Party.PartyPromoteReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Party
{
  [ComVisible(true)]
  public class PartyPromoteReq : PWSocialReqBase
  {
    public long PromoteUserId;
    private EPartyError ErrorCode;

    public PartyPromoteReq()
      : base((PWConnectionBase) null)
    {
    }

    public PartyPromoteReq(PWConnectionBase InClient)
      : base(InClient)
    {
    }

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      return strArray.Length == 1 && long.TryParse(strArray[0], out this.PromoteUserId) && this.PromoteUserId > 0L;
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_PromotePartyLeader;

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      this.ErrorCode = EPartyError.EPE_UnknownError;
      if (this.SocialServer != null && this.ClientData != null)
        this.ErrorCode = this.SocialServer.PromoteMember(this.ClientData, this.PromoteUserId);
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => this.ErrorCode == EPartyError.EPE_Ok;

    protected override bool ProcessServerResult() => this.SendDefaultMessage();
  }
}
