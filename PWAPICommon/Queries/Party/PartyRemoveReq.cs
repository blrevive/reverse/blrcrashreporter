﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Party.PartyRemoveReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using System;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Party
{
  [ComVisible(true)]
  public class PartyRemoveReq : PWSocialReqBase
  {
    public long RemoveUserId;
    private EPartyError ErrorCode;

    public PartyRemoveReq()
      : base((PWConnectionBase) null)
    {
    }

    public PartyRemoveReq(PWConnectionBase InClient)
      : base(InClient)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_RemoveFromParty;

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      return strArray.Length == 1 && long.TryParse(strArray[0], out this.RemoveUserId) && this.RemoveUserId > 0L;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      this.ErrorCode = EPartyError.EPE_UnknownError;
      if (this.SocialServer != null && this.ClientData != null)
        this.ErrorCode = this.SocialServer.RemoveMemberFromParty(this.ClientData, this.RemoveUserId);
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => this.ErrorCode == EPartyError.EPE_Ok;

    protected override bool ProcessServerResult() => this.SendMessage(this.CompressMessage(this.SerializeMessage(Convert.ToByte((object) this.ErrorCode).ToString())));
  }
}
