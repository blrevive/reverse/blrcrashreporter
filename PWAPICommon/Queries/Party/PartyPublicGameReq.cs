﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Party.PartyPublicGameReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Party
{
  [ComVisible(true)]
  public class PartyPublicGameReq : PWSocialReqBase
  {
    private string IPString;
    private int Port;

    public PartyPublicGameReq()
      : base((PWConnectionBase) null)
    {
    }

    public PartyPublicGameReq(PWConnectionBase InClient)
      : base(InClient)
    {
    }

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      if (strArray.Length != 2 || !int.TryParse(strArray[1], out this.Port))
        return false;
      this.IPString = strArray[0];
      return true;
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_PartySendPublicGameInvite;

    public override bool SubmitServerQuery()
    {
      if (this.SocialServer != null && this.ClientData != null)
        this.SocialServer.NotifyPartyOfPublicGame(this.ClientData, this.IPString, this.Port);
      base.SubmitServerQuery();
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => this.IPString != "";

    protected override bool ProcessServerResult() => this.SendDefaultMessage();
  }
}
