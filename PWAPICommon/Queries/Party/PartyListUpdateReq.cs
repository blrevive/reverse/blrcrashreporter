﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Party.PartyListUpdateReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using System.Collections.Generic;

namespace PWAPICommon.Queries.Party
{
  internal class PartyListUpdateReq : PWSocialReqBase
  {
    public long UserId;
    public List<PartyMember> MembersToUpdate;

    public PartyListUpdateReq()
      : base((PWConnectionBase) null)
    {
    }

    public PartyListUpdateReq(PWConnectionBase InClient)
      : base(InClient)
      => this.MembersToUpdate = new List<PartyMember>();

    protected override EMessageType GetMessageType() => EMessageType.EMT_PartyListUpdate;

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      return strArray.Length == 1 && long.TryParse(strArray[0], out this.UserId) && this.UserId > 0L;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => this.MembersToUpdate != null;

    protected override bool ProcessServerResult() => this.SendMessage(this.CompressMessage(this.SerializeMessage(this.Serializer.SerializeObject<List<PartyMember>>(this.MembersToUpdate))));
  }
}
