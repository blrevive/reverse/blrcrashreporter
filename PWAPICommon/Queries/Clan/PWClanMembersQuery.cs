﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Clan.PWClanMembersQuery
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Clan
{
  [ComVisible(true)]
  public class PWClanMembersQuery : PWRequestBase
  {
    public Guid ClanID;
    private List<PWClanMemberInfo> ClanMembers;

    public PWClanMembersQuery()
      : this((PWConnectionBase) null)
    {
    }

    public PWClanMembersQuery(PWConnectionBase InConnection)
      : base(InConnection)
      => this.ClanMembers = new List<PWClanMemberInfo>();

    protected override EMessageType GetMessageType() => EMessageType.EMT_Invalid;

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      SqlCommand sqlCommand = new SqlCommand("SP_ClanMembers_Get_By_ClanID");
      sqlCommand.CommandType = CommandType.StoredProcedure;
      sqlCommand.AddParameter<byte[]>("@ClanID", SqlDbType.VarBinary, this.ClanID.ToByteArray());
      PWSQLReq pwsqlReq = new PWSQLReq(sqlCommand, this.Connection);
      pwsqlReq.ServerParseOverride = new ProcessDelegate(this.ParseDBQuery);
      pwsqlReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return pwsqlReq.SubmitServerQuery();
    }

    protected bool ParseDBQuery(PWRequestBase ForRequest)
    {
      PWSQLReq pwsqlReq = ForRequest as PWSQLReq;
      return pwsqlReq.ResponseValues != null && pwsqlReq.ResponseValues.Count > 0 && !((object[]) pwsqlReq.ResponseValues[0])[0].ToString().Contains("ClanID");
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      PWSQLReq pwsqlReq = ForRequest as PWSQLReq;
      if (ForRequest.Successful)
      {
        for (int index = 0; index < pwsqlReq.ResponseValues.Count; ++index)
        {
          object[] responseValue = (object[]) pwsqlReq.ResponseValues[index];
          PWClanMemberInfo pwClanMemberInfo = new PWClanMemberInfo();
          if (pwClanMemberInfo.ParseFromArray(responseValue))
          {
            this.ClanMembers.Add(pwClanMemberInfo);
          }
          else
          {
            this.Log("ClanMemberQuery - Failed to parse clan members for clan with ID " + this.ClanID.ToString(), false, ELoggingLevel.ELL_Errors);
            return false;
          }
        }
        return true;
      }
      this.Log("ClanMembersQuery - query failed for clan with ID " + this.ClanID.ToString(), false, ELoggingLevel.ELL_Errors);
      return false;
    }

    protected override bool ProcessServerResult() => true;

    public List<PWClanMemberInfo> GetMemberList() => this.ClanMembers;
  }
}
