﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Clan.PWClanGetInfo
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Caches;
using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Queries.General;
using PWAPICommon.Queries.Player;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Clan
{
  [ComVisible(true)]
  public class PWClanGetInfo : PWClanReqBase
  {
    public string InTag = "";
    private bool InfoRetrievedEarly;
    private PWClanInfo ClanInfo;
    private PWCombinedReq ProfileQueries;
    private PWClanGetTag TagQuery;

    public PWClanGetInfo()
      : this((PWConnectionBase) null)
    {
    }

    public PWClanGetInfo(PWConnectionBase InConnection)
      : base(InConnection)
      => this.InfoRetrievedEarly = false;

    protected override EMessageType GetMessageType() => EMessageType.EMT_GetClanInfo;

    public override bool ParseServerQuery(byte[] InData)
    {
      base.ParseServerQuery(InData);
      if (this.ClientData != null)
      {
        if (this.ClientData.ClanInfo != null)
          this.InTag = this.ClientData.ClanInfo.ClanItem.ClanTag;
        return true;
      }
      this.Log("No client data in ClanGetInfo, failing", false, ELoggingLevel.ELL_Warnings);
      return false;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      if (this.InTag == "")
      {
        if (this.TagQuery == null)
        {
          this.TagQuery = new PWClanGetTag(this.Connection);
          this.TagQuery.ServerResultProcessor = new ProcessDelegate(this.ProcessGetClanTag);
          return this.TagQuery.SubmitServerQuery();
        }
        this.Log("Clearing client's clan info", false, ELoggingLevel.ELL_Verbose);
        return this.ProcessServerResultDefault();
      }
      this.Log("Retrieving infromation for clan with tag: " + this.InTag, false, ELoggingLevel.ELL_Informative);
      this.ClanInfo = this.GetClanByTag(this.InTag);
      if (this.ClanInfo == null)
        return true;
      if (this.ClanCacheReady && (this.ClanInfo.Members == null || this.ClanInfo.Members.Count == 0))
      {
        this.ClanCache.UpdateCompleteCallback = new ClanCacheDelegate(this.ClanInfoReady);
        this.ClanInfo.AddMember(new PWClanMemberInfo(this.ClientData.PlayerInfo, this.ClanInfo.ClanItem.OwnerID == this.ClientData.PlayerInfo.UserID ? EClanRank.CR_LEADER : EClanRank.CR_MEMBER));
      }
      this.InfoRetrievedEarly = true;
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      if (this.InfoRetrievedEarly)
        return true;
      if (this.InTag == "")
      {
        this.Log("ClanGetInfo - ParseServerResult has blank clan tag", false, ELoggingLevel.ELL_Verbose);
        return false;
      }
      if (this.ClanInfo == null)
      {
        this.Log("ClanGetINfo - ParseServerResult has null ClanInfo", false, ELoggingLevel.ELL_Warnings);
        return false;
      }
      if (this.ProfileQueries == null)
      {
        PWSQLReq pwsqlReq = (PWSQLReq) ForRequest;
        if (pwsqlReq != null && pwsqlReq.Successful)
        {
          for (int index = 0; index < pwsqlReq.ResponseValues.Count; ++index)
          {
            PWClanMemberInfo ToAdd = new PWClanMemberInfo();
            if (ToAdd.ParseFromArray((object[]) pwsqlReq.ResponseValues[index]))
              this.ClanInfo.AddMember(ToAdd);
            else
              this.Log("ClanGetInfo - Failed to parse a clan member", false, ELoggingLevel.ELL_Errors);
          }
          if (this.ClanInfo.Members.Count == 0)
          {
            this.Log("ClanGetInfo - no members in a parsed clan", false, ELoggingLevel.ELL_Errors);
            return false;
          }
          this.Log("Member information query successful, marking online players and sending message", false, ELoggingLevel.ELL_Verbose);
          if (this.ClientData == null)
          {
            this.Log("Client data was null", false, ELoggingLevel.ELL_Errors);
            return false;
          }
          this.ClanInfo.SetMemberCount();
          this.ProfileQueries = new PWCombinedReq(this.Connection);
          this.ProfileQueries.ServerResultProcessor = new ProcessDelegate(this.ProcessProfileQuery);
          foreach (PWClanMemberInfo member in this.ClanInfo.Members)
          {
            SocialLoginData loginData = this.SocialServer.GetLoginData(member.PlayerInfo.UserID);
            if (loginData != null)
              member.PlayerInfo = loginData.PlayerInfo;
            else
              member.PlayerInfo.OnlineStatus.StatusContext = Convert.ToByte((object) SocialStatus.EOnlineContext.EOT_Offline);
            this.ProfileQueries.AddSubRequest((PWRequestBase) new PWProfileQueryBasicReq(this.Connection)
            {
              ForUserID = member.PlayerInfo.UserID
            });
          }
          return this.ProfileQueries.SubmitServerQuery();
        }
      }
      else if (this.ProfileQueries.Completed)
        return true;
      return false;
    }

    protected override bool ProcessServerResult()
    {
      if (this.InfoRetrievedEarly || this.ProfileQueries != null && this.ProfileQueries.Completed)
      {
        this.Log("Info gathered, sending to user", false, ELoggingLevel.ELL_Informative);
        this.ClientData.ClanInfo = this.ClanInfo;
        return this.SendMessage(this.CompressMessage(this.SerializeMessage(this.Serializer.SerializeObject<PWClanInfo>(this.ClanInfo))));
      }
      if (this.Successful)
        return true;
      this.Log("ClanGetInfo - no info to gather, sending blank info back", false, ELoggingLevel.ELL_Informative);
      return this.SendDefaultMessage();
    }

    private bool ProcessProfileQuery(PWRequestBase ForRequest)
    {
      foreach (PWProfileQueryBasicReq subRequest in this.ProfileQueries.SubRequests)
      {
        if (subRequest.BasicProfile != null)
          this.ClanInfo.FindMember(subRequest.BasicProfile.UserId).PlayerInfo.CopyFromUserProfile(subRequest.BasicProfile);
        else
          this.Log("Missing player in clan member info query", false, ELoggingLevel.ELL_Errors);
      }
      return this.ProcessServerResultDefault();
    }

    private bool QueryClanMembers()
    {
      if (this.ClanInfo != null)
      {
        this.Log("Clan exists, getting member information", false, ELoggingLevel.ELL_Verbose);
        SqlCommand sqlCommand = new SqlCommand("SP_ClanMembers_Get_By_ClanID");
        sqlCommand.CommandType = CommandType.StoredProcedure;
        sqlCommand.AddParameter<byte[]>("@ClanID", SqlDbType.VarBinary, this.ClanInfo.ClanItem.ClanID.ToByteArray());
        PWSQLReq pwsqlReq = new PWSQLReq(sqlCommand, this.Connection);
        pwsqlReq.ServerParseOverride = new ProcessDelegate(this.ParseDBReq);
        pwsqlReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
        return pwsqlReq.SubmitServerQuery();
      }
      this.Log("No such clan exists, returning fail", false, ELoggingLevel.ELL_Informative);
      return this.SendMessage(this.Serializer.Serialize("F:" + this.InTag));
    }

    protected bool ParseDBReq(PWRequestBase Request)
    {
      PWSQLReq pwsqlReq = Request as PWSQLReq;
      return pwsqlReq.ResponseValues != null && pwsqlReq.ResponseValues.Count > 0;
    }

    protected override bool ProcessGetClan(PWRequestBase Request)
    {
      if (this.ClanCacheReady)
        this.ClanCache.UpdateCompleteCallback = new ClanCacheDelegate(this.ClanInfoReady);
      if (base.ProcessGetClan(Request))
      {
        this.ClanInfo = this.FetchedClanInfo;
      }
      else
      {
        this.ClanInfo = (PWClanInfo) null;
        if (this.ClanCacheReady)
          this.ClanCache.UpdateCompleteCallback = (ClanCacheDelegate) null;
      }
      return this.ClanCacheReady && this.ClanCache.HasCachedMembers || this.ClanInfo == null || this.QueryClanMembers();
    }

    private void ClanInfoReady()
    {
      this.InfoRetrievedEarly = true;
      this.ProcessServerResultDefault();
    }

    private bool ProcessGetClanTag(PWRequestBase Request)
    {
      if (this.TagQuery != null && this.TagQuery.Successful)
        this.InTag = this.TagQuery.ValidClanInfo.ClanItem.ClanTag;
      this.SubmitServerQuery();
      return true;
    }
  }
}
