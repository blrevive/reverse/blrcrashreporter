﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Clan.PWClanKick
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Caches;
using PWAPICommon.Clients;
using PWAPICommon.Common;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Clan
{
  [ComVisible(true)]
  public class PWClanKick : PWClanReqBase
  {
    public long PlayerID;
    private SocialLoginData OtherPlayer;
    private PWClanKick.ClanKickFailReasons FailReason = PWClanKick.ClanKickFailReasons.CKFR_Unknown;

    public PWClanKick()
      : base((PWConnectionBase) null)
    {
    }

    public PWClanKick(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    public override bool ParseServerQuery(byte[] InData)
    {
      base.ParseServerQuery(InData);
      if (this.MsgArray.Length != 1)
      {
        this.Log("ClanKick - invalid parameter count", false, ELoggingLevel.ELL_Informative);
        return false;
      }
      this.PlayerID = Convert.ToInt64(this.MsgArray[0]);
      return true;
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_ClanKick;

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      this.OtherPlayer = this.SocialServer.GetLoginData(this.PlayerID);
      if (this.DetectFail())
        return this.ProcessServerResultDefault();
      this.Log("Kicking player with ID " + (object) this.PlayerID + " from clan", false, ELoggingLevel.ELL_Informative);
      SqlCommand sqlCommand = new SqlCommand("SP_ClanMembers_Remove");
      sqlCommand.CommandType = CommandType.StoredProcedure;
      sqlCommand.AddParameter<long>("@UserID", SqlDbType.BigInt, this.PlayerID);
      PWSQLReq pwsqlReq = new PWSQLReq(sqlCommand, this.Connection);
      pwsqlReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return pwsqlReq.SubmitServerQuery();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      PWSQLReq pwsqlReq = (PWSQLReq) ForRequest;
      if (pwsqlReq != null && pwsqlReq.Successful)
      {
        this.Log("clankick userid=" + Convert.ToString(this.ClientData.PlayerInfo.UserID) + " targetplayerid=" + (object) this.PlayerID + " " + this.ClientData.ClanInfo.ClanItem.ToPWString(), true, ELoggingLevel.ELL_Verbose);
        if (this.OtherPlayer != null)
        {
          this.SocialServer.RemoveClanListener(this.OtherPlayer);
          this.OtherPlayer.ClanInfo = (PWClanInfo) null;
        }
        this.SocialServer.SendClanMemberUpdate(this.ClientData.ClanInfo.FindMember(this.PlayerID), this.ClientData.ClanChat.Listeners, EClanMemberUpdateType.ECMUT_LeaveClan);
        this.ClientData.ClanInfo.RemoveMember(this.PlayerID);
        return true;
      }
      this.Log("ClanKick - Null request or did not pass default parse. ClientData is " + this.ClientData.ToString() + "attempted to kick user with ID: " + this.PlayerID.ToString(), false, ELoggingLevel.ELL_Errors);
      return false;
    }

    protected override bool ProcessServerResult()
    {
      bool flag = this.Connection.SendMessage(EMessageType.EMT_ClanKickResponse, this.SerializeMessage(Convert.ToByte((object) this.FailReason).ToString()));
      if (this.Successful && this.OtherPlayer != null && this.OtherPlayer.Connection != null)
        flag = this.OtherPlayer.Connection.SendMessage(this.MessageType, this.SerializeMessage()) && flag;
      return flag;
    }

    protected bool DetectFail()
    {
      if (this.ClientData.ClanInfo == null)
      {
        this.Log("Kick failed, player isn't in a clan", false, ELoggingLevel.ELL_Informative);
        return true;
      }
      PWClanMemberInfo member1 = this.ClientData.ClanInfo.FindMember(this.ClientData.PlayerInfo.UserID);
      PWClanMemberInfo member2 = this.ClientData.ClanInfo.FindMember(this.PlayerID);
      if (!PWClanCache.HasClanPermissions(member1))
      {
        this.Log("Kick failed, insufficient permissions", false, ELoggingLevel.ELL_Informative);
        this.FailReason = PWClanKick.ClanKickFailReasons.CKFR_NoPermission;
      }
      else if (member1 != null && member2 != null && (int) member1.ClanPosition == (int) member2.ClanPosition)
      {
        this.Log("Kick failed, tried to kick someone with same rank", false, ELoggingLevel.ELL_Informative);
        this.FailReason = PWClanKick.ClanKickFailReasons.CKFR_NoPermission;
      }
      else if (this.OtherPlayer == this.ClientData)
      {
        this.Log("Leader attempting to kick himself, disregarding.", false, ELoggingLevel.ELL_Informative);
        this.FailReason = PWClanKick.ClanKickFailReasons.CKFR_IsLeader;
      }
      else
        this.FailReason = PWClanKick.ClanKickFailReasons.CKFR_NoFail;
      this.Log("Inviting player's information is: " + this.ClientData.ToString(), false, ELoggingLevel.ELL_Verbose);
      return this.FailReason != PWClanKick.ClanKickFailReasons.CKFR_NoFail;
    }

    public enum ClanKickFailReasons
    {
      CKFR_NoPermission = 0,
      CKFR_IsLeader = 1,
      CKFR_Unknown = 2,
      CKFR_NoFail = 255, // 0x000000FF
    }
  }
}
