﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Clan.PWClanGetTag
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using System;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Clan
{
  [ComVisible(true)]
  public class PWClanGetTag : PWClanReqBase
  {
    private long UserID;
    private PWClanInfo ClanInfo;
    private PWClanMemberItem ClanMember;
    private bool IsServerRequest;

    public PWClanInfo ValidClanInfo => this.ClanInfo;

    public PWClanGetTag()
      : base((PWConnectionBase) null)
    {
    }

    public PWClanGetTag(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    public override bool ParseServerQuery(byte[] InData) => base.ParseServerQuery(InData) && this.MsgArray.Length > 0 && long.TryParse(Convert.ToString(this.MsgArray[0]), out this.UserID);

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      this.IsServerRequest = this.ClientData == null;
      if (this.IsServerRequest)
      {
        this.ClientData = this.SocialServer.GetLoginData(this.UserID);
        if (this.ClientData == null)
          this.Log("Client data null during server query - cross region communication, hopefully", false, ELoggingLevel.ELL_Verbose);
      }
      else if (this.ClientData.PlayerInfo != null)
      {
        this.UserID = this.ClientData.PlayerInfo.UserID;
      }
      else
      {
        this.Log("Unable to find ClientData in ClanGetTag during a client query", false, ELoggingLevel.ELL_Warnings);
        return false;
      }
      if (this.UserID == 0L)
      {
        this.Log("User Id was zero during clan tag look up", false, ELoggingLevel.ELL_Errors);
        return false;
      }
      this.ClanMember = this.GetClanMemberItem(this.UserID);
      return this.ClanMember == null || this.GetClanInfo();
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_ClanGetTag;

    protected bool GetClanInfo()
    {
      if (this.ClanMember != null)
      {
        this.ClanInfo = this.GetClanByID(this.ClanMember.ClanID);
        if (this.ClanInfo == null)
          return true;
      }
      else
        this.Log("User not in a clan, sending blank tag back", false, ELoggingLevel.ELL_Verbose);
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase Request) => this.ClanMember != null && this.ClanInfo != null && this.UpdateClanInfo();

    protected override bool ProcessServerResult()
    {
      string ExtraString = Convert.ToString(this.UserID) + ":";
      if (this.Successful)
        ExtraString += this.ClanInfo.ClanItem.ClanTag;
      return this.SendMessage(this.SerializeMessage(ExtraString));
    }

    protected override bool ProcessGetClanMember(PWRequestBase ForRequest)
    {
      this.ClanMember = !base.ProcessGetClanMember(ForRequest) ? (PWClanMemberItem) null : ((PWClanSingleMemberQuery) ForRequest).ClanMember;
      return this.GetClanInfo();
    }

    protected override bool ProcessGetClan(PWRequestBase Request)
    {
      if (base.ProcessGetClan(Request))
      {
        this.ClanInfo = this.FetchedClanInfo;
        if (!this.ClanCacheReady)
          this.ClanInfo.AddMember(new PWClanMemberInfo()
          {
            PlayerInfo = {
              UserID = this.ClanMember.MemberID
            }
          });
      }
      else
        this.ClanInfo = (PWClanInfo) null;
      return this.ProcessServerResultDefault();
    }

    private bool UpdateClanInfo()
    {
      if (this.ClanInfo != null)
      {
        if (this.ClientData != null)
        {
          lock (this.ClientData)
          {
            this.ClientData.ClanInfo = this.ClanInfo;
            this.SocialServer.AddClanListener(this.ClientData);
            SocialStatus onlineStatus = this.ClientData.PlayerInfo.OnlineStatus;
            PWPlayerInfo membersPlayerInfo = this.ClientData.ClanInfo.GetMembersPlayerInfo(this.ClientData.PlayerInfo.UserID);
            if (membersPlayerInfo != null)
            {
              this.ClientData.PlayerInfo = membersPlayerInfo;
              this.ClientData.PlayerInfo.OnlineStatus = onlineStatus;
            }
            this.ClientData.RefreshOurDataToClan();
          }
          this.Log("Player " + this.ClientData.ToString() + "retrieved clan tag", false, ELoggingLevel.ELL_Verbose);
        }
        else if (!this.IsServerRequest)
        {
          this.Log("Client data is null during UpdateClanInfo and was not a ServerRequest", false, ELoggingLevel.ELL_Warnings);
          return false;
        }
        return true;
      }
      this.Log("User is in ClanMembers, but clan does not exist in cache, member data is " + this.PrintClientInfo(), false, ELoggingLevel.ELL_Errors);
      return false;
    }
  }
}
