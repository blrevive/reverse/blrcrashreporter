﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Clan.PWClanJoin
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Clan
{
  [ComVisible(true)]
  public class PWClanJoin : PWClanReqBase
  {
    public Guid ClanID;
    public long PlayerID;
    public bool SendResponse = true;
    private EClanRank ClanRank;
    private PWClanInfo TheClan;

    public PWClanJoin()
      : base((PWConnectionBase) null)
    {
    }

    public PWClanJoin(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_ClanJoin;

    public override bool ParseServerQuery(byte[] InData)
    {
      base.ParseServerQuery(InData);
      if (this.MsgArray.Length < 1)
      {
        this.Log("ClanJoin - Invalid parameter count", false, ELoggingLevel.ELL_Informative);
        return false;
      }
      if (this.ClientData == null)
      {
        this.Log("ClanJoin - no client data exists", false, ELoggingLevel.ELL_Warnings);
        return false;
      }
      this.PlayerID = this.ClientData.PlayerInfo.UserID;
      if (Guid.TryParse(Convert.ToString(this.MsgArray[0]), out this.ClanID))
      {
        this.Log("ClanJoin - Received request to join clan: " + Convert.ToString((object) this.ClanID), false, ELoggingLevel.ELL_Informative);
        return true;
      }
      this.Log("ClanJoin - request failed, bad Guid entry", false, ELoggingLevel.ELL_Warnings);
      this.ProcessServerResultDefault();
      return false;
    }

    public override bool SubmitServerQuery()
    {
      if (this.PlayerID == 0L)
      {
        this.Log("Create clan request, PlayerID is zero", false, ELoggingLevel.ELL_Errors);
        return false;
      }
      this.TheClan = this.GetClanByID(this.ClanID);
      return this.TheClan == null || this.SendJoinQuery();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      PWSQLReq pwsqlReq = (PWSQLReq) ForRequest;
      if (pwsqlReq != null && pwsqlReq.Successful)
      {
        this.Log("clanjoin userid=" + Convert.ToString(this.PlayerID) + " " + this.TheClan.ClanItem.ToPWString(), true, ELoggingLevel.ELL_Verbose);
        if (this.ClientData != null)
        {
          lock (this.ClientData)
          {
            this.ClientData.ClanInfo = this.TheClan;
            this.TheClan.AddMember(new PWClanMemberInfo(this.ClientData.PlayerInfo, this.ClanRank));
            this.SocialServer.AddClanListener(this.ClientData);
            this.ClientData.RefreshOurDataToClan(EClanMemberUpdateType.ECMUT_JoinClan);
          }
        }
        else
          this.Log("ClanJoin - ClientData null during parse server result", false, ELoggingLevel.ELL_Informative);
        return true;
      }
      this.Log("ClanJoin - null dbreq or did not pass default parse. Client data is: " + this.ClientData.ToString(), false, ELoggingLevel.ELL_Errors);
      return false;
    }

    protected override bool ProcessServerResult()
    {
      if (!this.SendResponse)
        return true;
      return this.Successful ? this.SendMessage(this.SerializeMessage(this.TheClan.ClanItem.ClanTag)) : this.SendDefaultMessage();
    }

    private bool SendJoinQuery()
    {
      base.SubmitServerQuery();
      if (this.TheClan == null)
      {
        this.Log("ClanJoin - Attempting to join a clan that doesn't exist", false, ELoggingLevel.ELL_Errors);
        return this.ProcessServerResultDefault();
      }
      this.ClanRank = this.TheClan.ClanItem.OwnerID == this.PlayerID ? EClanRank.CR_LEADER : EClanRank.CR_MEMBER;
      this.Log("ClanJoin - Clan join should be valid, updating DB", false, ELoggingLevel.ELL_Verbose);
      SqlCommand sqlCommand = new SqlCommand("SP_ClanMembers_Add");
      sqlCommand.CommandType = CommandType.StoredProcedure;
      sqlCommand.AddParameter<long>("@UserID", SqlDbType.BigInt, this.PlayerID);
      sqlCommand.AddParameter<byte[]>("@ClanID", SqlDbType.VarBinary, this.TheClan.ClanItem.ClanID.ToByteArray());
      sqlCommand.AddParameter<byte>("@Rank", SqlDbType.TinyInt, Convert.ToByte((object) this.ClanRank));
      PWSQLReq pwsqlReq = new PWSQLReq(sqlCommand, this.Connection);
      pwsqlReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return pwsqlReq.SubmitServerQuery();
    }

    protected override bool ProcessGetClan(PWRequestBase Request)
    {
      base.ProcessGetClan(Request);
      this.TheClan = new PWClanInfo((Request as PWClanQueryWrapper).ClanItem);
      return this.SendJoinQuery();
    }
  }
}
