﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Clan.PWClanQueryWrapper
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Clan
{
  [ComVisible(true)]
  public class PWClanQueryWrapper : PWRequestBase
  {
    public PWClanItem ClanItem;
    public SqlCommand DBCommand;

    public PWClanQueryWrapper()
      : base((PWConnectionBase) null)
    {
    }

    public PWClanQueryWrapper(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_Invalid;

    public override bool ParseServerQuery(byte[] InMessage) => false;

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      if (this.DBCommand == null)
        return false;
      PWSQLReq pwsqlReq = new PWSQLReq(this.DBCommand, this.Connection);
      pwsqlReq.ServerParseOverride = new ProcessDelegate(this.ParseDBReq);
      pwsqlReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return pwsqlReq.SubmitServerQuery();
    }

    protected bool ParseDBReq(PWRequestBase ForRequest) => ForRequest is PWSQLReq pwsqlReq && pwsqlReq.ResponseValues != null && pwsqlReq.ResponseValues.Count == 1;

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      if (ForRequest is PWSQLReq pwsqlReq && pwsqlReq.Successful)
      {
        this.ClanItem = new PWClanItem();
        if (!this.ClanItem.ParseFromArray((object[]) pwsqlReq.ResponseValues[0]))
        {
          this.Log("ClanQueryWrapper - Received an item, but it failed to parse", false, ELoggingLevel.ELL_Errors);
          this.ClanItem = (PWClanItem) null;
        }
      }
      return this.ClanItem != null;
    }
  }
}
