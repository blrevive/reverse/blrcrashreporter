﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Clan.PWClanReqBase
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Caches;
using PWAPICommon.Clients;
using PWAPICommon.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Clan
{
  [ComVisible(true)]
  public class PWClanReqBase : PWSocialReqBase
  {
    protected PWClanCache ClanCache;
    protected bool ClanCacheReady;
    protected PWClanInfo FetchedClanInfo;

    public PWClanReqBase()
      : this((PWConnectionBase) null)
    {
    }

    public PWClanReqBase(PWConnectionBase InConnection)
      : base(InConnection)
    {
      this.ClanCacheReady = false;
      this.CheckClanCache();
    }

    protected bool CheckClanCache()
    {
      if (!this.ClanCacheReady)
      {
        this.ClanCache = this.Connection.OwningServer.GetResource<PWClanCache>();
        this.ClanCacheReady = this.ClanCache != null;
      }
      return this.ClanCacheReady;
    }

    protected void ForceGetClanCache(bool GetClanMembers = false)
    {
      if (!this.CheckClanCache())
        this.ClanCache = new PWClanCache(this.Connection.OwningServer, this.Connection, GetClanMembers, new ClanCacheDelegate(this.SetCacheReady));
      else
        this.ClanCache.ForceUpdate(new ClanCacheDelegate(this.SetCacheReady));
    }

    protected virtual void SetCacheReady() => this.ClanCacheReady = true;

    protected override EMessageType GetMessageType() => EMessageType.EMT_Invalid;

    public override bool ParseServerQuery(byte[] InData)
    {
      this.CheckClanCache();
      return base.ParseServerQuery(InData);
    }

    protected PWClanInfo GetClanByID(Guid ClanID)
    {
      if (this.ClanCacheReady)
      {
        PWClanInfo pwClanInfo = this.ClanCache.GetCachedItems().Find((Predicate<PWClanInfo>) (TheItem => TheItem.ClanItem.ClanID == ClanID));
        if (pwClanInfo != null)
          return pwClanInfo;
      }
      SqlCommand command = new SqlCommand("SP_Clan_Select_By_ID");
      command.CommandType = CommandType.StoredProcedure;
      command.AddParameter<byte[]>("@ClanID", SqlDbType.VarBinary, ClanID.ToByteArray());
      PWClanQueryWrapper clanQueryWrapper = new PWClanQueryWrapper(this.Connection);
      clanQueryWrapper.ServerResultProcessor = new ProcessDelegate(this.ProcessGetClan);
      clanQueryWrapper.DBCommand = command;
      clanQueryWrapper.SubmitServerQuery();
      return (PWClanInfo) null;
    }

    protected PWClanInfo GetClanByName(string ClanName)
    {
      if (this.ClanCacheReady)
      {
        PWClanInfo pwClanInfo = this.ClanCache.GetCachedItems().Find((Predicate<PWClanInfo>) (TheItem => TheItem.ClanItem.ClanName == ClanName));
        if (pwClanInfo != null)
          return pwClanInfo;
      }
      SqlCommand command = new SqlCommand("SP_Clan_Select_By_FullName");
      command.CommandType = CommandType.StoredProcedure;
      command.AddParameter<string>("@ClanName", SqlDbType.VarChar, ClanName);
      PWClanQueryWrapper clanQueryWrapper = new PWClanQueryWrapper(this.Connection);
      clanQueryWrapper.ServerResultProcessor = new ProcessDelegate(this.ProcessGetClan);
      clanQueryWrapper.DBCommand = command;
      clanQueryWrapper.SubmitServerQuery();
      return (PWClanInfo) null;
    }

    protected PWClanInfo GetClanByTag(string ClanTag)
    {
      if (this.ClanCacheReady)
      {
        PWClanInfo pwClanInfo = this.ClanCache.GetCachedItems().Find((Predicate<PWClanInfo>) (TheItem => TheItem.ClanItem.ClanTag == ClanTag));
        if (pwClanInfo != null)
          return pwClanInfo;
      }
      SqlCommand command = new SqlCommand("SP_Clan_Select_By_Tag");
      command.CommandType = CommandType.StoredProcedure;
      command.AddParameter<string>("@Tag", SqlDbType.VarChar, ClanTag);
      PWClanQueryWrapper clanQueryWrapper = new PWClanQueryWrapper(this.Connection);
      clanQueryWrapper.ServerResultProcessor = new ProcessDelegate(this.ProcessGetClan);
      clanQueryWrapper.DBCommand = command;
      clanQueryWrapper.SubmitServerQuery();
      return (PWClanInfo) null;
    }

    protected PWClanInfo GetClanByOwnerID(long OwnerID)
    {
      if (this.ClanCacheReady)
      {
        PWClanInfo pwClanInfo = this.ClanCache.GetCachedItems().Find((Predicate<PWClanInfo>) (TheItem => TheItem.ClanItem.OwnerID == OwnerID));
        if (pwClanInfo != null)
          return pwClanInfo;
      }
      SqlCommand command = new SqlCommand("SP_Clan_Select_By_OwnerID");
      command.CommandType = CommandType.StoredProcedure;
      command.AddParameter<long>("@OwnerID", SqlDbType.BigInt, OwnerID);
      PWClanQueryWrapper clanQueryWrapper = new PWClanQueryWrapper(this.Connection);
      clanQueryWrapper.ServerResultProcessor = new ProcessDelegate(this.ProcessGetClan);
      clanQueryWrapper.DBCommand = command;
      clanQueryWrapper.SubmitServerQuery();
      return (PWClanInfo) null;
    }

    protected virtual bool ProcessGetClan(PWRequestBase Request)
    {
      if (!(Request is PWClanQueryWrapper clanQueryWrapper) || !clanQueryWrapper.Successful || clanQueryWrapper.ClanItem == null)
        return false;
      this.FetchedClanInfo = new PWClanInfo();
      this.FetchedClanInfo.ClanItem = clanQueryWrapper.ClanItem;
      if (this.ClanCacheReady)
        this.ClanCache.AddItem(this.FetchedClanInfo);
      return true;
    }

    protected virtual PWClanMemberItem GetClanMemberItem(long UserID)
    {
      if (this.ClanCacheReady)
      {
        List<PWClanInfo> cachedItems = this.ClanCache.GetCachedItems();
        foreach (PWClanInfo pwClanInfo in cachedItems)
        {
          if (pwClanInfo.Members != null)
          {
            PWClanMemberInfo pwClanMemberInfo = pwClanInfo.Members.Find((Predicate<PWClanMemberInfo>) (x => x.PlayerInfo.UserID == UserID));
            if (pwClanMemberInfo != null)
              return new PWClanMemberItem()
              {
                ClanID = pwClanInfo.ClanItem.ClanID,
                MemberID = pwClanMemberInfo.PlayerInfo.UserID
              };
          }
        }
      }
      PWClanSingleMemberQuery singleMemberQuery = new PWClanSingleMemberQuery(this.Connection);
      singleMemberQuery.UserID = UserID;
      singleMemberQuery.ServerResultProcessor = new ProcessDelegate(this.ProcessGetClanMember);
      singleMemberQuery.SubmitServerQuery();
      return (PWClanMemberItem) null;
    }

    protected virtual bool ProcessGetClanMember(PWRequestBase ForRequest)
    {
      PWClanSingleMemberQuery MemberQuery = ForRequest as PWClanSingleMemberQuery;
      if (MemberQuery == null || !MemberQuery.Successful || MemberQuery.ClanMember == null)
        return false;
      if (this.ClanCacheReady)
      {
        PWClanInfo pwClanInfo = this.ClanCache.GetCachedItems().Find((Predicate<PWClanInfo>) (x => x.ClanItem.ClanID == MemberQuery.ClanMember.ClanID));
        if (pwClanInfo != null)
        {
          PWClanMemberInfo ToAdd = new PWClanMemberInfo();
          ToAdd.PlayerInfo.UserID = MemberQuery.ClanMember.MemberID;
          pwClanInfo.AddMember(ToAdd);
          this.ClanCache.QueryMemberProfiles(new List<PWClanMemberInfo>()
          {
            ToAdd
          });
        }
      }
      return true;
    }

    public override bool SubmitServerQuery() => base.SubmitServerQuery();
  }
}
