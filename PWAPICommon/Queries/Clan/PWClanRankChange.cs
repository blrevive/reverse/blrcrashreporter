﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Clan.PWClanRankChange
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Clan
{
  [ComVisible(true)]
  public class PWClanRankChange : PWClanReqBase
  {
    public long PlayerID;
    public byte NewRank;
    private PWClanMemberInfo TheirInfo;

    public PWClanRankChange()
      : this((PWConnectionBase) null)
    {
    }

    public PWClanRankChange(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_ClanRankChange;

    public override bool ParseServerQuery(byte[] InData)
    {
      base.ParseServerQuery(InData);
      if (this.MsgArray.Length == 2)
      {
        if (!long.TryParse(this.MsgArray[0].ToString(), out this.PlayerID))
        {
          this.Log("ClanRankChange - PlayerID failed to parse", false, ELoggingLevel.ELL_Warnings);
          return false;
        }
        if (byte.TryParse(this.MsgArray[1].ToString(), out this.NewRank))
          return true;
        this.Log("ClanRankChange - NewRank failed to parse", false, ELoggingLevel.ELL_Warnings);
        return false;
      }
      this.Log("ClanRankChange - Invalid parameter count in ParseSeverQuery", false, ELoggingLevel.ELL_Informative);
      return false;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      if (!this.ClientCanChangeRank())
        return this.ProcessServerResultBase((PWRequestBase) null);
      SqlCommand sqlCommand = new SqlCommand("SP_ClanMembers_Rank_Update");
      sqlCommand.CommandType = CommandType.StoredProcedure;
      sqlCommand.AddParameter<long>("@UserID", SqlDbType.BigInt, this.PlayerID);
      sqlCommand.AddParameter<byte>("@Rank", SqlDbType.TinyInt, this.NewRank);
      PWSQLReq pwsqlReq = new PWSQLReq(sqlCommand, this.Connection);
      pwsqlReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return pwsqlReq.SubmitServerQuery();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => ForRequest.Successful;

    protected override bool ProcessServerResult()
    {
      if (this.Successful && this.TheirInfo != null)
      {
        this.Log("ClanrankChange - Rank Change complete", false, ELoggingLevel.ELL_Verbose);
        this.TheirInfo.ClanPosition = this.NewRank;
        this.SocialServer.SendClanMemberUpdate(this.TheirInfo, this.ClientData.ClanChat.Listeners, EClanMemberUpdateType.ECMUT_RankChange);
        return true;
      }
      this.Log("ClanRankChange - Request completed, but did not pass default parse. ClientData is " + this.ClientData.ToString(), false, ELoggingLevel.ELL_Errors);
      return false;
    }

    private bool ClientCanChangeRank()
    {
      if (this.ClientData == null || this.ClientData.ClanInfo == null)
      {
        this.Log("ClanrankChange - Client data or clan info were null during a rank change", false, ELoggingLevel.ELL_Warnings);
        return false;
      }
      PWClanMemberInfo member = this.ClientData.ClanInfo.FindMember(this.ClientData.PlayerInfo.UserID);
      this.TheirInfo = this.ClientData.ClanInfo.FindMember(this.PlayerID);
      if (member != null && this.TheirInfo != null)
      {
        if (PWSocialServer.HasClanPermissions(member) && (int) member.ClanPosition < (int) this.NewRank && (int) this.NewRank < (int) Convert.ToByte((object) EClanRank.CR_MAX))
        {
          this.Log("ClanrankChange - Valid rank change, allowing", false, ELoggingLevel.ELL_Verbose);
          return true;
        }
        this.Log("ClanRankChange - Invalid promote attempt - user lacked permissions, didn't have sufficient rank, or was trying to demote below min rank", false, ELoggingLevel.ELL_Informative);
      }
      else
        this.Log("ClanRankChange - Could not find either user's clan member info or target's clan member info", false, ELoggingLevel.ELL_Warnings);
      return false;
    }
  }
}
