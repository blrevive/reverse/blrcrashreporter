﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Clan.PWClanValidateInfo
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Clan
{
  [ComVisible(true)]
  public class PWClanValidateInfo : PWClanReqBase
  {
    private ClanInfoTypes InfoType;
    private string Info;
    private bool ValidInfo;
    private PWClanInfo ClanInfo;

    public PWClanValidateInfo()
      : base((PWConnectionBase) null)
    {
    }

    public PWClanValidateInfo(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_ClanValidateInfo;

    public override bool ParseServerQuery(byte[] InData)
    {
      base.ParseServerQuery(InData);
      if (this.MsgArray.Length < 2)
      {
        this.Log("ClanValidateInfo - Invalid parameter count", false, ELoggingLevel.ELL_Informative);
        return false;
      }
      this.InfoType = (ClanInfoTypes) Convert.ToInt32(this.MsgArray[0]);
      this.Info = Convert.ToString(this.MsgArray[1]);
      this.Log("ClanValidateInfo - Validating info type " + Convert.ToString((object) this.InfoType) + "with entry" + Convert.ToString(this.Info), false, ELoggingLevel.ELL_Verbose);
      return true;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      switch (this.InfoType)
      {
        case ClanInfoTypes.CIT_TAG:
          this.ClanInfo = this.GetClanByTag(this.Info);
          break;
        case ClanInfoTypes.CIT_NAME:
          this.ClanInfo = this.GetClanByName(this.Info);
          break;
        default:
          this.ValidInfo = false;
          this.Log("ClanValidateInfo - Unexpected InfoType(" + (object) this.InfoType + ") requested", false, ELoggingLevel.ELL_Warnings);
          break;
      }
      if (!this.ClanCacheReady || this.ClanInfo == null)
        return true;
      this.ValidInfo = false;
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => true;

    protected override bool ProcessServerResult()
    {
      this.Log("ClanValidateInfo - Valid info is " + Convert.ToString(this.ValidInfo), false, ELoggingLevel.ELL_Informative);
      return this.ValidInfo ? this.SendMessage(this.Serializer.Serialize("T:T:" + Convert.ToString((int) this.InfoType))) : this.SendMessage(this.Serializer.Serialize("T:F:" + Convert.ToString((int) this.InfoType)));
    }

    protected override bool ProcessGetClan(PWRequestBase Request)
    {
      base.ProcessGetClan(Request);
      this.ValidInfo = (Request as PWClanQueryWrapper).ClanItem == null;
      return this.ProcessServerResultDefault();
    }
  }
}
