﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Clan.PWClanQueryReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace PWAPICommon.Queries.Clan
{
  internal class PWClanQueryReq : PWRequestBase
  {
    public List<PWClanInfo> Clans;

    public PWClanQueryReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWClanQueryReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_Invalid;

    public override bool ParseServerQuery(byte[] InData) => false;

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      SqlCommand InQuery = new SqlCommand("SP_Clan_Select_All");
      InQuery.CommandType = CommandType.StoredProcedure;
      PWSQLReq pwsqlReq = new PWSQLReq(InQuery, this.Connection);
      pwsqlReq.ServerParseOverride = new ProcessDelegate(this.ParseDBReq);
      pwsqlReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return pwsqlReq.SubmitServerQuery();
    }

    protected bool ParseDBReq(PWRequestBase Request) => Request is PWSQLReq pwsqlReq && pwsqlReq.ResponseValues != null;

    public override bool ParseServerResult(PWRequestBase Request)
    {
      if (!(Request is PWSQLReq pwsqlReq) || !pwsqlReq.Successful)
        return false;
      this.Clans = new List<PWClanInfo>();
      for (int index = 0; index < pwsqlReq.ResponseValues.Count; ++index)
      {
        object[] responseValue = (object[]) pwsqlReq.ResponseValues[index];
        PWClanInfo pwClanInfo = new PWClanInfo(new PWClanItem());
        if (pwClanInfo.ClanItem.ParseFromArray(responseValue))
          this.Clans.Add(pwClanInfo);
        else
          this.Log("ClanQueryReq - Failed to parse a clan item that was received", false, ELoggingLevel.ELL_Errors);
      }
      if (this.Clans.Count > 0)
        return true;
      return pwsqlReq != null && pwsqlReq.ResponseValues != null && pwsqlReq.ResponseValues.Count == 0;
    }

    protected override bool ProcessServerResult() => true;

    public override bool ParseClientQuery(byte[] InData) => throw new NotImplementedException();

    public override bool SubmitClientQuery() => throw new NotImplementedException();

    protected override bool ProcessClientResult(PWRequestBase ForRequest) => throw new NotImplementedException();
  }
}
