﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Clan.PWUpdateClanMemberPushReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Queries.General;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Clan
{
  [ComVisible(true)]
  public class PWUpdateClanMemberPushReq : PWNotificationReq<PWClanMemberInfo>
  {
    public EClanMemberUpdateType UpdateType;

    public PWUpdateClanMemberPushReq()
    {
    }

    public PWUpdateClanMemberPushReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    public PWUpdateClanMemberPushReq(
      PWConnectionBase InConnection,
      long InUserID,
      PWClanMemberInfo InUpdatedItem)
      : this(InConnection, InUserID, new List<PWClanMemberInfo>()
      {
        InUpdatedItem
      })
    {
    }

    public PWUpdateClanMemberPushReq(
      PWConnectionBase InConnection,
      long InUserID,
      List<PWClanMemberInfo> InUpdatedItems)
      : base(InConnection, InUserID, (IEnumerable<PWClanMemberInfo>) InUpdatedItems)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_ClanMemberUpdate;

    public override object Clone()
    {
      PWUpdateClanMemberPushReq clanMemberPushReq = new PWUpdateClanMemberPushReq();
      clanMemberPushReq.UserID = this.UserID;
      clanMemberPushReq.Items = this.Items;
      clanMemberPushReq.UpdateType = this.UpdateType;
      return (object) clanMemberPushReq;
    }

    protected override bool ProcessServerResult() => this.SendMessage(this.CompressMessage(this.SerializeMessage(this.EnumToString((Enum) this.UpdateType), this.Serializer.SerializeObject<PWClanMemberInfo[]>(this.Items))));
  }
}
