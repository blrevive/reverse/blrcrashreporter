﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Clan.PWClanSingleMemberQuery
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Clan
{
  [ComVisible(true)]
  public class PWClanSingleMemberQuery : PWRequestBase
  {
    public long UserID;
    private PWClanMemberItem MemberInfo;

    public PWClanMemberItem ClanMember => this.MemberInfo;

    public PWClanSingleMemberQuery()
      : base((PWConnectionBase) null)
    {
    }

    public PWClanSingleMemberQuery(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_Invalid;

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      SqlCommand sqlCommand = new SqlCommand("SP_ClanMembers_Get_By_UserID");
      sqlCommand.CommandType = CommandType.StoredProcedure;
      sqlCommand.AddParameter<long>("@UserID", SqlDbType.BigInt, this.UserID);
      PWSQLReq pwsqlReq = new PWSQLReq(sqlCommand, this.Connection);
      pwsqlReq.ServerParseOverride = new ProcessDelegate(this.ParseDBReq);
      pwsqlReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return pwsqlReq.SubmitServerQuery();
    }

    protected bool ParseDBReq(PWRequestBase ForRequest)
    {
      PWSQLReq pwsqlReq = (PWSQLReq) ForRequest;
      return pwsqlReq != null && pwsqlReq.ResponseValues != null && pwsqlReq.ResponseValues.Count > 0 && !((object[]) pwsqlReq.ResponseValues[0])[0].ToString().Contains("UserID");
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      PWSQLReq pwsqlReq = (PWSQLReq) ForRequest;
      if (pwsqlReq != null && pwsqlReq.Successful)
      {
        this.MemberInfo = new PWClanMemberItem();
        if (this.MemberInfo.ParseFromArray((object[]) pwsqlReq.ResponseValues[0]))
          return true;
        this.Log("ClanSingleMemberQuery - parse failed (player not in a clan)", false, ELoggingLevel.ELL_Informative);
        return false;
      }
      this.Log("ClanSingleMemberQuery - player not in a clan", false, ELoggingLevel.ELL_Informative);
      return false;
    }

    protected override bool ProcessServerResult() => true;
  }
}
