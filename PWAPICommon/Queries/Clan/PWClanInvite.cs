﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Clan.PWClanInvite
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Caches;
using PWAPICommon.Clients;
using System;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Clan
{
  [ComVisible(true)]
  public class PWClanInvite : PWClanReqBase
  {
    public string PlayerName;
    public long PlayerID;
    private SocialLoginData OtherPlayer;
    private PWClanInvite.ClanInviteFailReasons FailReason = PWClanInvite.ClanInviteFailReasons.CIFR_UnknownReason;

    public PWClanInvite()
      : base((PWConnectionBase) null)
    {
    }

    public PWClanInvite(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_ClanInvite;

    public override bool ParseServerQuery(byte[] InData)
    {
      base.ParseServerQuery(InData);
      if (this.MsgArray.Length < 2)
      {
        this.Log("ClanInvite - invalid parameter count", false, ELoggingLevel.ELL_Informative);
        return false;
      }
      if (long.TryParse(Convert.ToString(this.MsgArray[1]), out this.PlayerID))
      {
        this.PlayerName = Convert.ToString(this.MsgArray[0]);
        return true;
      }
      this.Log("ClanInvite - could not parse PlayerID", false, ELoggingLevel.ELL_Warnings);
      return false;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      this.OtherPlayer = this.PlayerID == 0L ? this.SocialServer.GetLoginData(this.PlayerName) : this.SocialServer.GetLoginData(this.PlayerID);
      if (!this.DetectInviteFail())
      {
        this.Log("Player " + this.OtherPlayer.ToString() + " is being invited to clan " + this.ClientData.ClanInfo.ToString(), false, ELoggingLevel.ELL_Informative);
        this.Log("claninvite userid=" + Convert.ToString(this.ClientData.PlayerInfo.UserID) + " targetplayerid=" + Convert.ToString(this.OtherPlayer.PlayerInfo.UserID) + " " + this.ClientData.ClanInfo.ClanItem.ToString(), true, ELoggingLevel.ELL_Verbose);
      }
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => this.FailReason == PWClanInvite.ClanInviteFailReasons.CIFR_NoFail;

    protected override bool ProcessServerResult()
    {
      bool flag = this.Connection.SendMessage(EMessageType.EMT_ClanInviteResponse, this.SerializeMessage(Convert.ToByte((object) this.FailReason).ToString()));
      if (this.Successful)
        flag = this.OtherPlayer.Connection.SendMessage(this.MessageType, this.SerializeMessage(this.ClientData.ClanInfo.ClanItem.ToString() + ":" + this.ClientData.PlayerInfo.UserName)) && flag;
      return flag;
    }

    protected bool DetectInviteFail()
    {
      if (this.ClientData.ClanInfo == null)
      {
        this.Log("Player not in a clan, can't invite - this should have been blocked client side", false, ELoggingLevel.ELL_Informative);
        return true;
      }
      if (!PWClanCache.HasClanPermissions(this.ClientData.ClanInfo.FindMember(this.ClientData.PlayerInfo.UserID)))
      {
        this.Log("Player does not have clan permissions, can't invite", false, ELoggingLevel.ELL_Informative);
        this.FailReason = PWClanInvite.ClanInviteFailReasons.CIFR_NoPermissions;
      }
      else if (this.OtherPlayer == null)
      {
        this.Log("Offline player being invited, can't do that yet", false, ELoggingLevel.ELL_Informative);
        this.FailReason = PWClanInvite.ClanInviteFailReasons.CIFR_Offline;
      }
      else if (this.OtherPlayer.ClanInfo != null)
      {
        if (this.OtherPlayer.ClanInfo.ClanItem.ClanTag == this.ClientData.ClanInfo.ClanItem.ClanTag)
        {
          this.Log("Invited player is already in this clan, cannot invite", false, ELoggingLevel.ELL_Informative);
          this.FailReason = PWClanInvite.ClanInviteFailReasons.CIFR_InYourClan;
        }
        else
        {
          this.Log("Player is already in a clan, cannot invite", false, ELoggingLevel.ELL_Informative);
          this.FailReason = PWClanInvite.ClanInviteFailReasons.CIFR_InClan;
        }
      }
      else
        this.FailReason = PWClanInvite.ClanInviteFailReasons.CIFR_NoFail;
      if (this.OtherPlayer != null)
        this.Log("Invited player's information was: " + this.OtherPlayer.ToString(), false, ELoggingLevel.ELL_Verbose);
      return this.FailReason != PWClanInvite.ClanInviteFailReasons.CIFR_NoFail;
    }

    public enum ClanInviteFailReasons
    {
      CIFR_Offline = 0,
      CIFR_InClan = 1,
      CIFR_InYourClan = 2,
      CIFR_NoPermissions = 3,
      CIFR_UnknownReason = 4,
      CIFR_NoFail = 255, // 0x000000FF
    }
  }
}
