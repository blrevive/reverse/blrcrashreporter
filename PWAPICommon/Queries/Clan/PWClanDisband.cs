﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Clan.PWClanDisband
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Clan
{
  [ComVisible(true)]
  public class PWClanDisband : PWClanReqBase
  {
    public long OwnerID;
    private PWClanInfo TheClan;

    public PWClanDisband()
      : base((PWConnectionBase) null)
    {
    }

    public PWClanDisband(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    public override bool ParseServerQuery(byte[] InData)
    {
      base.ParseServerQuery(InData);
      if (this.ClientData == null || this.ClientData.PlayerInfo == null)
        return false;
      this.OwnerID = this.ClientData.PlayerInfo.UserID;
      return true;
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_ClanDisband;

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      this.TheClan = this.GetClanByOwnerID(this.OwnerID);
      return this.TheClan == null || this.DisbandClan();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => ForRequest.Successful;

    protected override bool ProcessServerResult()
    {
      if (!this.Successful)
      {
        this.Log("ClanDisband - failed", false, ELoggingLevel.ELL_Errors);
        return this.SendDefaultMessage();
      }
      this.Log("clandisband userid=" + Convert.ToString(this.TheClan.ClanItem.OwnerID) + " " + this.TheClan.ClanItem.ToPWString(), true, ELoggingLevel.ELL_Verbose);
      this.SocialServer.DisbandClan(this.TheClan.ClanItem.ClanTag, this.Serializer.Serialize("T"));
      if (this.ClanCacheReady)
        this.ClanCache.RemoveItem(this.TheClan);
      return true;
    }

    private bool DisbandClan()
    {
      base.SubmitServerQuery();
      if (this.TheClan == null)
      {
        this.Log("ClanDisgband- Attempt to disband clan that does not exist", false, ELoggingLevel.ELL_Warnings);
        return false;
      }
      this.Log("ClanDisband - Valid disband request, sending to DB", false, ELoggingLevel.ELL_Verbose);
      SqlCommand sqlCommand = new SqlCommand("SP_Clan_Remove");
      sqlCommand.CommandType = CommandType.StoredProcedure;
      sqlCommand.AddParameter<long>("@Owner", SqlDbType.BigInt, this.OwnerID);
      PWSQLReq pwsqlReq = new PWSQLReq(sqlCommand, this.Connection);
      pwsqlReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return pwsqlReq.SubmitServerQuery();
    }

    protected override bool ProcessGetClan(PWRequestBase Request)
    {
      if (!base.ProcessGetClan(Request))
        return false;
      this.Log("ClanDisband - Retrieved clan that wasn't in cache, retrying request", false, ELoggingLevel.ELL_Verbose);
      this.TheClan = this.GetClanByOwnerID(this.OwnerID);
      return this.DisbandClan();
    }
  }
}
