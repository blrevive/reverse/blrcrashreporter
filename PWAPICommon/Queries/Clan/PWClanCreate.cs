﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Clan.PWClanCreate
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Clan
{
  [ComVisible(true)]
  public class PWClanCreate : PWClanReqBase
  {
    private PWClanInfo NewClan;
    private bool ValidTag;
    private bool ValidName;

    public PWClanCreate()
      : base((PWConnectionBase) null)
    {
    }

    public PWClanCreate(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_ClanCreate;

    public override bool ParseServerQuery(byte[] InData)
    {
      base.ParseServerQuery(InData);
      if (this.MsgArray.Length != 2)
      {
        this.Log("ClanCreate - Not enough elements in MsgArray, invalid request", false, ELoggingLevel.ELL_Warnings);
        return false;
      }
      this.ClientData = new SocialLoginData((PWConnectionBase) null);
      if (!long.TryParse(this.MsgArray[0].ToString(), out this.ClientData.PlayerInfo.UserID))
        return false;
      string[] strArray = this.MsgArray[1].ToString().Split(';');
      if (strArray.Length != 3)
      {
        this.Log("ClanCreate - Not enough parameters in ClanParams to create, invalid request", false, ELoggingLevel.ELL_Warnings);
        return false;
      }
      this.NewClan = new PWClanInfo(new PWClanItem());
      this.NewClan.ClanItem.ClanName = strArray[0];
      this.NewClan.ClanItem.ClanTag = strArray[1];
      this.NewClan.ClanItem.MOTD = strArray[2] + " " + this.NewClan.ClanItem.ClanName;
      this.NewClan.ClanItem.OwnerID = this.ClientData.PlayerInfo.UserID;
      this.Log("ClanCreate - Attempting to create clan: " + Convert.ToString((object) this.NewClan), false, ELoggingLevel.ELL_Verbose);
      return true;
    }

    public override bool SubmitServerQuery()
    {
      this.ForceGetClanCache();
      return true;
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      if (ForRequest == this)
      {
        this.Log("ClanCreate - Create request failed due to invalid name/tag", false, ELoggingLevel.ELL_Informative);
        return false;
      }
      if (ForRequest is PWSQLReq pwsqlReq && pwsqlReq.Successful)
      {
        this.Log("clancreate userid=" + Convert.ToString(this.ClientData.PlayerInfo.UserID) + " " + this.NewClan.ClanItem.ToPWString(), true, ELoggingLevel.ELL_Verbose);
        this.HandleClanCreated();
        return true;
      }
      this.Log("ClanCreate - Failed create attempt from DB", false, ELoggingLevel.ELL_Errors);
      return false;
    }

    protected override bool ProcessServerResult() => !this.Successful ? this.SendMessage(this.SerializeMessage("" + (this.ValidTag ? "1:" : "0:") + (this.ValidName ? "1" : "0"))) : this.SendDefaultMessage();

    private bool DetectCreateFail()
    {
      this.ValidTag = this.ValidName = true;
      if (this.ClanCache.ClanExistsbyTag(this.NewClan.ClanItem.ClanTag))
      {
        this.Log("ClanCreate - ClanTag taken", false, ELoggingLevel.ELL_Informative);
        this.ValidTag = false;
      }
      if (this.ClanCache.ClanExistsByName(this.NewClan.ClanItem.ClanName))
      {
        this.Log("ClanCreate - ClanName taken", false, ELoggingLevel.ELL_Informative);
        this.ValidName = false;
      }
      if (this.ClanCache.ClanExistsByOwnerID(this.NewClan.ClanItem.OwnerID))
      {
        this.Log("ClanCreate - This player is already the leader of a clan, attempting to display clan information", false, ELoggingLevel.ELL_Warnings);
        if (this.ClientData.ClanInfo != null)
          this.Log(this.ClientData.ClanInfo.ToString(), false, ELoggingLevel.ELL_Warnings);
        else
          this.Log("Clan information not stored locally, couldn't display.", false, ELoggingLevel.ELL_Warnings);
        this.Log("ClanCreate - Aborting request.", false, ELoggingLevel.ELL_Informative);
        return false;
      }
      return !this.ValidName || !this.ValidTag;
    }

    private bool CreateNewClan()
    {
      if (this.DetectCreateFail())
        return this.ProcessServerResultDefault();
      base.SubmitServerQuery();
      this.Log("Clan Name and Tag valid, sending query to DB", false, ELoggingLevel.ELL_Verbose);
      this.NewClan.ClanItem.ClanID = Guid.NewGuid();
      SqlCommand sqlCommand = new SqlCommand("SP_Clan_Add");
      sqlCommand.CommandType = CommandType.StoredProcedure;
      sqlCommand.AddParameter<string>("@FullName", SqlDbType.VarChar, this.NewClan.ClanItem.ClanName);
      sqlCommand.AddParameter<string>("@Tag", SqlDbType.VarChar, this.NewClan.ClanItem.ClanTag);
      sqlCommand.AddParameter<long>("@Owner", SqlDbType.BigInt, this.NewClan.ClanItem.OwnerID);
      sqlCommand.AddParameter<string>("@Message", SqlDbType.VarChar, this.NewClan.ClanItem.MOTD);
      sqlCommand.AddParameter<byte[]>("@ID", SqlDbType.VarBinary, this.NewClan.ClanItem.ClanID.ToByteArray());
      PWSQLReq pwsqlReq = new PWSQLReq(sqlCommand, this.Connection);
      pwsqlReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return pwsqlReq.SubmitServerQuery();
    }

    private void HandleClanCreated()
    {
      this.ClanCache.AddItemNoQuery(this.NewClan);
      new PWClanJoin(this.Connection)
      {
        ClanID = this.NewClan.ClanItem.ClanID,
        PlayerID = this.ClientData.PlayerInfo.UserID
      }.SubmitServerQuery();
    }

    protected override void SetCacheReady()
    {
      base.SetCacheReady();
      this.CreateNewClan();
    }

    protected override bool ProcessGetClan(PWRequestBase Request)
    {
      if (!base.ProcessGetClan(Request))
        return false;
      this.Log("Retrieved clan that wasn't in cache, retrying request", false, ELoggingLevel.ELL_Informative);
      return this.CreateNewClan();
    }
  }
}
