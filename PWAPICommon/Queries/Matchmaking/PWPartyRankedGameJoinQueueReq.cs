﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Matchmaking.PWPartyRankedGameJoinQueueReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Resources;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Matchmaking
{
  [ComVisible(true)]
  public class PWPartyRankedGameJoinQueueReq : PWRequestBase
  {
    public string QueueString;
    public List<long> PartyMembers;
    public int PlaceInQueue;
    public float EstimatedWaitTime;
    public ERankedJoinResponse ResponseCode;

    private PWPartyRankedGameJoinQueueReq()
      : base((PWConnectionBase) null)
    {
      this.PlaceInQueue = -1;
      this.EstimatedWaitTime = -1f;
      this.ResponseCode = ERankedJoinResponse.ERJR_UnknownError;
    }

    public PWPartyRankedGameJoinQueueReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
      this.PlaceInQueue = -1;
      this.EstimatedWaitTime = -1f;
      this.ResponseCode = ERankedJoinResponse.ERJR_UnknownError;
      this.PartyMembers = new List<long>();
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_PartyGameSearch;

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      string Text = "PartyRankedQueueJoin for UserIDs ";
      if (strArray.Length < 2)
        return false;
      this.QueueString = strArray[0];
      for (int index = 1; index < strArray.Length; ++index)
      {
        long result;
        long.TryParse(strArray[index], out result);
        this.PartyMembers.Add(result);
        Text += strArray[index];
        if (index < strArray.Length - 1)
          Text += ", ";
      }
      this.Log(Text, false, ELoggingLevel.ELL_Informative);
      return true;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      PWRankedManager resource = this.Connection.OwningServer.GetResource<PWRankedManager>();
      if (resource != null)
      {
        this.ResponseCode = resource.PartyAddToQueue(this);
        if (this.ResponseCode == ERankedJoinResponse.ERJR_Success)
          return true;
      }
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => this.ResponseCode == ERankedJoinResponse.ERJR_Success;

    protected override bool ProcessServerResult()
    {
      PWSuperServer resource = this.OwningServer.GetResource<PWSuperServer>();
      for (int index = 0; index < this.PartyMembers.Count; ++index)
        resource.GetLoggedInConnFromId(this.PartyMembers[index])?.SendMessage(EMessageType.EMT_PartyGameSearch, this.SerializeMessage(this.PlaceInQueue.ToString() + ":" + this.EstimatedWaitTime.ToString()));
      return true;
    }
  }
}
