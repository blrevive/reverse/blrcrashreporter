﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Matchmaking.PWPremiumServerQueryUserReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Wrappers;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Matchmaking
{
  [ComVisible(true)]
  public class PWPremiumServerQueryUserReq : PWRequestBase
  {
    public long OwnerID;
    private PWSqlItemQueryKeyedReq<PremiumMatchItem, long> QueryReq;
    public List<PremiumMatchItem> OutItems;

    public PWPremiumServerQueryUserReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWPremiumServerQueryUserReq(PWConnectionBase InConn)
      : base(InConn)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_PremiumServerQuery;

    public override bool ParseServerQuery(byte[] InMessage) => long.TryParse(this.Serializer.DeserializeToString(InMessage), out this.OwnerID);

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      if (this.QueryReq == null)
      {
        this.OutItems = new List<PremiumMatchItem>();
        this.QueryReq = new PWSqlItemQueryKeyedReq<PremiumMatchItem, long>(this.Connection);
        this.QueryReq.Key = this.OwnerID;
        this.QueryReq.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
        return this.QueryReq.SubmitServerQuery();
      }
      if (this.QueryReq.Successful)
        this.OutItems = this.QueryReq.ResponseItems;
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => this.QueryReq != null && this.QueryReq.Successful;

    protected override bool ProcessServerResult() => this.SendMessage(this.Serializer.SerializeObject<List<PremiumMatchItem>>(this.OutItems));
  }
}
