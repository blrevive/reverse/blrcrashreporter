﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Matchmaking.MasterServerStats
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Matchmaking
{
  [ComVisible(true)]
  public struct MasterServerStats
  {
    public float CPULoad;
    public float RamLoad;
    public int CurPopulation;
    public int MaxPopulation;
    public int CurTemplates;
    public int MaxTemplates;
    public int TotalServers;
    public int NumRankedMatches;
    public int NumRankedMatchSlots;
    public int NumPrivateMatches;
    public int NumPrivateMatchSlots;
    public int NumPremiumMatches;
    public int NumPremiumMatchSlots;
    public int NumGenericMatches;
    public int NumGenericMatchSlots;
  }
}
