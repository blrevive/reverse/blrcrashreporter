﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Matchmaking.PWStatsQuery
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Matchmaking
{
  [ComVisible(true)]
  public class PWStatsQuery : PWRequestBase
  {
    public StatInput Input;
    public StatOutput Output;
    private string ResultString;

    public override string WebResponse => this.ResultString;

    public PWStatsQuery()
      : base((PWConnectionBase) null)
    {
    }

    public PWStatsQuery(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_ReadStatsForUser;

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      return strArray.Length == 1 && long.TryParse(strArray[0], out this.Input.UserID);
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      string OutValue;
      if (!this.OwningServer.GetConfigValue<string>("StatsURL", out OutValue))
        return this.ProcessServerResultDefault();
      PWWebReq pwWebReq = new PWWebReq(this.Connection, OutValue, false, EWebRequestType.EWRT_POST, (object) this.Input, typeof (StatOutput));
      pwWebReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return pwWebReq.SubmitServerQuery();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      this.Output.UserID = this.Input.UserID;
      if (ForRequest != null && ForRequest.Successful)
      {
        PWWebReq pwWebReq = (PWWebReq) ForRequest;
        if (pwWebReq.Result != null)
        {
          this.Output = (StatOutput) pwWebReq.Result;
          this.Output.UserID = this.Input.UserID;
          return true;
        }
      }
      return false;
    }

    protected override bool ProcessServerResult() => this.SendMessage(this.SerializeMessage<StatOutput>("", this.Output));

    public override bool SubmitClientQuery() => this.SendMessage(this.Serializer.SerializeRaw(this.Input.UserID.ToString()));

    public override bool ParseClientQuery(byte[] InMessage)
    {
      string str = this.Serializer.DeserializeToString(InMessage);
      if (!(str != null & str.StartsWith("T:")))
        return false;
      this.ResultString = str.Remove(0, 2);
      this.Output = this.Serializer.DeSerializeStringToObject<StatOutput>(this.ResultString);
      return true;
    }
  }
}
