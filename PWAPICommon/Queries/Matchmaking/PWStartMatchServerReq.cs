﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Matchmaking.PWStartMatchServerReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Matchmaking
{
  [ComVisible(true)]
  public class PWStartMatchServerReq : PWRequestBase
  {
    private string GameRules;
    private bool bSuccess;

    public PWStartMatchServerReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWStartMatchServerReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_StartMatch;

    public override bool ParseServerQuery(byte[] InMessage) => true;

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      PWSuperServer resource = this.Connection.OwningServer.GetResource<PWSuperServer>();
      if (resource != null)
      {
        Dictionary<string, string> OutRules;
        this.bSuccess = resource.GameServerStartMatch(this.Connection, out OutRules);
        if (this.bSuccess)
        {
          this.GameRules = "";
          foreach (KeyValuePair<string, string> keyValuePair in OutRules)
          {
            PWStartMatchServerReq startMatchServerReq = this;
            startMatchServerReq.GameRules = startMatchServerReq.GameRules + keyValuePair.Key + "=" + keyValuePair.Value + ":";
          }
        }
      }
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => this.bSuccess;

    protected override bool ProcessServerResult() => this.SendMessage(this.SerializeMessage(this.GameRules));
  }
}
