﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Matchmaking.PWCreatePrivateServerReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Config;
using PWAPICommon.Resources;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Matchmaking
{
  [ComVisible(true)]
  public class PWCreatePrivateServerReq : PWCreateServerReqBase
  {
    private int[] PlaylistBuildInfo;
    private string Password;
    private string ServerName;
    private long OwnerID;
    private string OwnerName;
    private int MaxPlayers;
    private int MaxSpectators;
    private KVPDictionary ServerRules;
    private Guid PremiumGuid;
    private PWPremiumServerCreateReq PremiumReq;

    public PWCreatePrivateServerReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWCreatePrivateServerReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
      this.ServerRules = new KVPDictionary();
      this.PlaylistBuildInfo = new int[4];
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_CreatePrivateMatch;

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      if (strArray.Length != 12)
        return false;
      long.TryParse(strArray[0], out this.OwnerID);
      Guid.TryParse(strArray[1], out this.PremiumGuid);
      this.Password = strArray[2];
      this.ServerName = strArray[3];
      this.OwnerName = strArray[4];
      int.TryParse(strArray[5], out this.MaxPlayers);
      int.TryParse(strArray[6], out this.MaxSpectators);
      int.TryParse(strArray[7], out this.PlaylistBuildInfo[0]);
      int.TryParse(strArray[8], out this.PlaylistBuildInfo[1]);
      int.TryParse(strArray[9], out this.PlaylistBuildInfo[2]);
      int.TryParse(strArray[10], out this.PlaylistBuildInfo[3]);
      this.ServerRules.FromString(strArray[11]);
      return true;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      if (this.Connection.UserID == 0L)
      {
        this.Log("Failed to create private match. Owner ID is 0", false, ELoggingLevel.ELL_Warnings);
        return this.ProcessServerResultDefault();
      }
      PWSuperServer resource1 = this.Connection.OwningServer.GetResource<PWSuperServer>();
      PWPrivateServerManager resource2 = this.Connection.OwningServer.GetResource<PWPrivateServerManager>();
      if (this.PremiumGuid != Guid.Empty && this.PremiumReq == null)
      {
        this.PremiumReq = new PWPremiumServerCreateReq(this.Connection);
        this.PremiumReq.InventoryGuid = this.PremiumGuid;
        this.PremiumReq.OwnerID = this.OwnerID;
        this.ServerRules.Add("OwnerName", this.OwnerName);
        this.ServerRules.Add("MaxPlayers", this.MaxPlayers.ToString());
        this.ServerRules.Add("MaxSpectators", this.MaxSpectators.ToString());
        this.PremiumReq.GameRules = this.ServerRules.ToString();
        this.PremiumReq.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
        return this.PremiumReq.SubmitServerQuery();
      }
      if (this.PremiumReq != null)
      {
        if (!this.PremiumReq.Successful)
          return this.ProcessServerResultDefault();
        this.RequestInfo = this.PremiumReq.Item.GenerateRequestObject();
      }
      else
      {
        this.RequestInfo = new PWServerInfoReq();
        this.RequestInfo.bPrivate = true;
        this.RequestInfo.Password = this.Password;
        this.RequestInfo.ServerName = this.ServerName;
        this.RequestInfo.OwnerName = this.OwnerName;
        this.RequestInfo.CreatorId = this.Connection.UserID;
        this.RequestInfo.MaxPlayers = this.MaxPlayers;
        this.RequestInfo.MaxSpectators = this.MaxSpectators;
        this.RequestInfo.RequestId = Guid.NewGuid();
        Array.Copy((Array) this.PlaylistBuildInfo, (Array) this.RequestInfo.PlaylistBuildInfo, 4);
      }
      if (this.PremiumReq == null && this.RequestInfo != null && this.RequestInfo.PlaylistBuildInfo.Length != 4 || this.RequestInfo.PlaylistBuildInfo[0] == 0)
      {
        this.Log("Failed to create private match when no playlist info is set for creator ID " + (object) this.RequestInfo.CreatorId, true, ELoggingLevel.ELL_Critical);
        return this.ProcessServerResultDefault();
      }
      if (this.OwnerName == "")
      {
        this.Log("Failed to create private match when no owner name is set for creator ID " + (object) this.RequestInfo.CreatorId, true, ELoggingLevel.ELL_Critical);
        return this.ProcessServerResultDefault();
      }
      if (this.RequestInfo != null)
      {
        foreach (KeyValuePair<string, string> serverRule in (Dictionary<string, string>) this.ServerRules)
          this.RequestInfo.ServerProperties.Add(serverRule.Key + "=" + serverRule.Value);
      }
      if (resource2 != null && !resource2.CreatePrivateServer(this))
        return this.ProcessServerResultDefault();
      if (resource1 != null)
      {
        this.Log("Gonna try to create private match for user ID " + (object) this.RequestInfo.CreatorId, false, ELoggingLevel.ELL_Informative);
        if (resource1.CreateServer((PWCreateServerReqBase) this))
          return true;
        resource2?.NotifyPrivateMatchCreateFail(this);
      }
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      PWPrivateServerManager resource = this.Connection.OwningServer.GetResource<PWPrivateServerManager>();
      if (this.ResultInfo == null)
      {
        this.Log("Create private match with null result info", false, ELoggingLevel.ELL_Warnings);
        resource.NotifyPrivateMatchCreateFail(this);
      }
      return this.ResultInfo != null;
    }

    protected override bool ProcessServerResult() => this.ResultInfo != null ? this.SendMessage(this.SerializeMessage(this.ResultInfo.PublicIP + ":" + (object) this.ResultInfo.GamePort)) : this.SendDefaultMessage();
  }
}
