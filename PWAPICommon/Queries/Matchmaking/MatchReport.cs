﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Matchmaking.MatchReport
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System.Runtime.InteropServices;
using System.Xml.Serialization;

namespace PWAPICommon.Queries.Matchmaking
{
  [XmlRoot("match-complete")]
  [ComVisible(true)]
  public struct MatchReport
  {
    [XmlElement("matchid")]
    public int MatchID;
    [XmlElement("finaldata")]
    public MatchData MatchData;
  }
}
