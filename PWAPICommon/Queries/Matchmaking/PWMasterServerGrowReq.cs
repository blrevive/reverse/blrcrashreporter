﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Matchmaking.PWMasterServerGrowReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Matchmaking
{
  [ComVisible(true)]
  public class PWMasterServerGrowReq : PWRequestBase
  {
    public PWMasterServerGrowReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      return this.ProcessServerResultDefault();
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_MasterServerGrow;

    protected override bool ProcessServerResult() => this.SendDefaultMessage();

    public override bool ParseClientQuery(byte[] InMessage) => true;
  }
}
