﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Matchmaking.PWUpdateServerReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Matchmaking
{
  [ComVisible(true)]
  public class PWUpdateServerReq : PWRequestBase
  {
    public PWServerInfoUpdate UpdateInfo;
    private Dictionary<string, string> UpdatedProperties;

    public PWUpdateServerReq()
      : base((PWConnectionBase) null)
      => this.UpdatedProperties = new Dictionary<string, string>();

    public PWUpdateServerReq(PWConnectionBase InConnection)
      : base(InConnection)
      => this.UpdatedProperties = new Dictionary<string, string>();

    protected override EMessageType GetMessageType() => EMessageType.EMT_UpdateServer;

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] strArray1 = this.Serializer.Deserialize(InMessage);
      if (strArray1.Length < 3 || !byte.TryParse(strArray1[2], out this.UpdateInfo.PlayerNum))
        return false;
      string str1 = strArray1[3];
      char[] chArray1 = new char[1]{ ',' };
      foreach (string str2 in str1.Split(chArray1))
      {
        char[] chArray2 = new char[1]{ '=' };
        string[] strArray2 = str2.Split(chArray2);
        if (strArray2.Length == 2)
          this.UpdatedProperties.Add(strArray2[0], strArray2[1]);
      }
      this.UpdateInfo.Timestamp = PWServerBase.CurrentTime;
      return true;
    }

    protected override bool ProcessServerResult() => this.SendDefaultMessage();

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      PWSuperServer resource = this.Connection.OwningServer.GetResource<PWSuperServer>();
      return resource != null && resource.UpdateGameServer(this.UpdateInfo, this.UpdatedProperties, this.Connection);
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      return this.ProcessServerResultDefault();
    }
  }
}
