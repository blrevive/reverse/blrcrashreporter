﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Matchmaking.PWSetGameRuleReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Matchmaking
{
  [ComVisible(true)]
  public class PWSetGameRuleReq : PWRequestBase
  {
    public string PropertyName;
    public string NewValue;
    public EGameRuleAction Action;
    public bool bResult;

    public override string WebResponse => !this.bResult ? base.WebResponse : "Game Rule Set Successfully";

    public PWSetGameRuleReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWSetGameRuleReq(PWConnectionBase InClient)
      : base(InClient)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_SetGameRules;

    public override bool SubmitClientQuery() => this.SendMessage(this.SerializeSelf());

    public override bool ParseServerQuery(byte[] InMessage)
    {
      PWSetGameRuleReq pwSetGameRuleReq = this.DeserializeSelf<PWSetGameRuleReq>(InMessage);
      if (pwSetGameRuleReq == null)
        return false;
      this.PropertyName = pwSetGameRuleReq.PropertyName;
      this.NewValue = pwSetGameRuleReq.NewValue;
      this.Action = pwSetGameRuleReq.Action;
      return true;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      this.bResult = false;
      PWSuperServer resource = this.OwningServer.GetResource<PWSuperServer>();
      if (resource != null)
        this.bResult = resource.SetGameRule(this.PropertyName, this.NewValue, this.Action);
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => this.bResult;

    protected override bool ProcessServerResult() => this.SendDefaultMessage();

    public override bool ParseClientQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      if (strArray == null || strArray.Length != 1)
        return false;
      this.bResult = strArray[0] == "T";
      return true;
    }
  }
}
