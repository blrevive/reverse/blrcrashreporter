﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Matchmaking.PWUnregisterMasterServerReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Matchmaking
{
  [ComVisible(true)]
  public class PWUnregisterMasterServerReq : PWRequestBase
  {
    public PWMachineID MachineId;

    public PWUnregisterMasterServerReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWUnregisterMasterServerReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    public override bool ParseClientQuery(byte[] InMessage)
    {
      this.MachineId = this.Connection.OwningServer.HardwareID;
      return true;
    }

    public override bool ParseServerQuery(byte[] InMessage)
    {
      PWUnregisterMasterServerReq unregisterMasterServerReq = this.DeserializeSelf<PWUnregisterMasterServerReq>(InMessage);
      if (unregisterMasterServerReq == null)
        return false;
      this.MachineId = unregisterMasterServerReq.MachineId;
      return true;
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_MasterServerUnregister;

    public override bool SubmitClientQuery()
    {
      base.SubmitClientQuery();
      return this.SendMessage(this.SerializeSelf());
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      PWSuperServer resource = this.Connection.OwningServer.GetResource<PWSuperServer>();
      return resource != null && resource.UnregisterMasterServer(this);
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      return this.ProcessServerResultDefault();
    }

    protected override bool ProcessServerResult() => this.SendDefaultMessage();
  }
}
