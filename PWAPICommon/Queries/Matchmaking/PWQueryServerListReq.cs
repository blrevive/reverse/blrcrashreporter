﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Matchmaking.PWQueryServerListReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Matchmaking
{
  [ComVisible(true)]
  public class PWQueryServerListReq : PWRequestBase
  {
    public string Filter;
    private byte[] OutList;

    public PWQueryServerListReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWQueryServerListReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    public override bool ParseServerQuery(byte[] InMessage) => true;

    protected override EMessageType GetMessageType() => EMessageType.EMT_QueryServers;

    public override bool ParseServerResult(PWRequestBase ForRequest) => ForRequest != null;

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      PWSuperServer resource = this.Connection.OwningServer.GetResource<PWSuperServer>();
      if (resource != null)
        this.OutList = resource.QueryServerListData();
      return this.ProcessServerResultDefault();
    }

    protected override bool ProcessServerResult() => this.SendMessage(this.OutList);
  }
}
