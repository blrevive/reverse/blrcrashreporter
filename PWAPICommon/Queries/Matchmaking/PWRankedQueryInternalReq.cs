﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Matchmaking.PWRankedQueryInternalReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;

namespace PWAPICommon.Queries.Matchmaking
{
  internal class PWRankedQueryInternalReq : PWRequestBase
  {
    public long ForUserID;
    public UserRankData OutData;

    public PWRankedQueryInternalReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWRankedQueryInternalReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_RankedQuery;

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      string OutValue;
      if (!this.OwningServer.GetConfigValue<string>("MatchURL", out OutValue))
        return this.ProcessServerResultDefault();
      UserRankReq userRankReq;
      userRankReq.UserID = this.ForUserID;
      PWWebReq pwWebReq = new PWWebReq(this.Connection, OutValue, false, EWebRequestType.EWRT_POST, (object) userRankReq, typeof (UserRankData));
      pwWebReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return pwWebReq.SubmitServerQuery();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      PWWebReq pwWebReq = (PWWebReq) ForRequest;
      if (pwWebReq != null && pwWebReq.Result != null && pwWebReq.Result.GetType() == typeof (UserRankData))
      {
        this.OutData = (UserRankData) pwWebReq.Result;
        if (this.OutData.Status == "success")
          return true;
      }
      return false;
    }

    protected override bool ProcessServerResult() => this.SendMessage(this.SerializeMessage(this.ForUserID.ToString() + ":" + this.OutData.Rank.ToString()));
  }
}
