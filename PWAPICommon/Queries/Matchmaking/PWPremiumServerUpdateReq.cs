﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Matchmaking.PWPremiumServerUpdateReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Resources;
using PWAPICommon.Wrappers;
using System;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Matchmaking
{
  [ComVisible(true)]
  public class PWPremiumServerUpdateReq : PWRequestBase
  {
    private long OwnerID;
    private Guid TargetGuid;
    private bool bRestart;
    private string NewGameRules;
    private PremiumMatchItem FoundItem;
    private PWPremiumServerQueryUserReq QueryReq;
    private PWSqlItemUpdateReq<PremiumMatchItem> UpdateReq;

    public PWPremiumServerUpdateReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWPremiumServerUpdateReq(PWConnectionBase InConn)
      : base(InConn)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_PremiumServerQuery;

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      if (strArray == null || strArray.Length != 4 || (!long.TryParse(strArray[0], out this.OwnerID) || !Guid.TryParse(strArray[1], out this.TargetGuid)) || !bool.TryParse(strArray[2], out this.bRestart))
        return false;
      this.NewGameRules = strArray[3];
      return true;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      if (this.QueryReq == null)
      {
        this.QueryReq = new PWPremiumServerQueryUserReq(this.Connection);
        this.QueryReq.OwnerID = this.OwnerID;
        this.QueryReq.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
        return this.QueryReq.SubmitServerQuery();
      }
      if (this.QueryReq != null && this.QueryReq.Successful)
      {
        this.FoundItem = this.QueryReq.OutItems.Find((Predicate<PremiumMatchItem>) (x => x.UniqueID == this.TargetGuid));
        if (this.FoundItem != null)
        {
          this.FoundItem.GameRules = this.NewGameRules;
          this.UpdateReq = new PWSqlItemUpdateReq<PremiumMatchItem>(this.Connection);
          this.UpdateReq.Item = this.FoundItem;
          this.UpdateReq.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
          return this.UpdateReq.SubmitServerQuery();
        }
      }
      else if (this.UpdateReq != null && this.UpdateReq.Successful)
        this.OwningServer.GetResource<PWPrivateServerManager>()?.PremiumServerUpdated(this.FoundItem, this.bRestart);
      return this.ProcessServerResultDefault();
    }

    protected override bool ProcessServerResult() => this.SendDefaultMessage();
  }
}
