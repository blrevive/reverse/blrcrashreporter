﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Matchmaking.PWCreateCustomServerCom
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Matchmaking
{
  [ComVisible(true)]
  public class PWCreateCustomServerCom : PWRequestBase
  {
    public PWServerInfoReq StartInfo;
    public PWServerInfoRunning ServerProcess;
    public PWServerInfoFull CreatedServer;

    protected override EMessageType GetMessageType() => EMessageType.EMT_MasterServerCreateGameComplete;

    public PWCreateCustomServerCom()
      : base((PWConnectionBase) null)
    {
    }

    public PWCreateCustomServerCom(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    public override bool ParseServerQuery(byte[] InMessage)
    {
      object[] objArray = this.Serializer.Deserialize(InMessage, typeof (PWServerInfoFull));
      if (objArray.Length != 1 || objArray[0] == null || !(objArray[0].GetType() == typeof (PWServerInfoFull)))
        return false;
      this.CreatedServer = (PWServerInfoFull) objArray[0];
      return true;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      if (this.StartInfo != null)
        return this.SendMessage(this.Serializer.SerializeObject<PWServerInfoReq>(this.StartInfo));
      if (this.CreatedServer != null)
      {
        PWSuperServer resource = this.Connection.OwningServer.GetResource<PWSuperServer>();
        if (resource != null)
          return resource.GameServerCreated(this);
      }
      return false;
    }

    public override bool ParseClientQuery(byte[] InMessage)
    {
      object[] objArray = this.Serializer.Deserialize(InMessage, typeof (PWServerInfoReq));
      if (objArray.Length != 1 || objArray[0] == null || !(objArray[0].GetType() == typeof (PWServerInfoReq)))
        return false;
      this.StartInfo = (PWServerInfoReq) objArray[0];
      return true;
    }

    public override bool SubmitClientQuery()
    {
      base.SubmitClientQuery();
      return true;
    }

    public override bool ParseClientResult(PWRequestBase ForRequest) => this.ServerProcess != null && this.ServerProcess.Proc.Responding;

    protected override bool ProcessClientResult(PWRequestBase ForRequest) => !this.Successful || this.SendMessage(this.Serializer.SerializeObject<PWServerInfoFull>(this.CreatedServer));
  }
}
