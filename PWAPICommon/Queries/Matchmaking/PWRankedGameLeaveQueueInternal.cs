﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Matchmaking.PWRankedGameLeaveQueueInternal
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Matchmaking
{
  [ComVisible(true)]
  public class PWRankedGameLeaveQueueInternal : PWRequestBase
  {
    public LeaveReq InData;
    public LeaveResp OutData;

    public PWRankedGameLeaveQueueInternal()
      : base((PWConnectionBase) null)
    {
    }

    public PWRankedGameLeaveQueueInternal(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_RankedLeave;

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      if (strArray.Length != 1 || !long.TryParse(strArray[0], out this.InData.UserID))
        return false;
      this.InData.IP = this.Connection.EndPoint;
      return true;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      string OutValue;
      if (!this.OwningServer.GetConfigValue<string>("MatchURL", out OutValue))
        return this.ProcessServerResultDefault();
      PWWebReq pwWebReq = new PWWebReq(this.Connection, OutValue, false, EWebRequestType.EWRT_POST, (object) this.InData, typeof (LeaveResp));
      pwWebReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return pwWebReq.SubmitServerQuery();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      this.OutData = (LeaveResp) ((PWWebReq) ForRequest).Result;
      return this.OutData.Status == "success";
    }

    protected override bool ProcessServerResult() => this.SendDefaultMessage();
  }
}
