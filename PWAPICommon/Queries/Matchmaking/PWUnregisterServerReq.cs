﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Matchmaking.PWUnregisterServerReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Matchmaking
{
  [ComVisible(true)]
  public class PWUnregisterServerReq : PWRequestBase
  {
    private bool bUnregisteredSuccessfully;

    public PWUnregisterServerReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWUnregisterServerReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_UnregisterServer;

    public override bool ParseServerQuery(byte[] InMessage) => true;

    public override bool ParseServerResult(PWRequestBase ForRequest) => this.bUnregisteredSuccessfully;

    protected override bool ProcessServerResult() => this.SendDefaultMessage();

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      PWSuperServer resource = this.Connection.OwningServer.GetResource<PWSuperServer>();
      if (resource != null)
        this.bUnregisteredSuccessfully = resource.UnregisterGameServer(this.Connection);
      return this.ProcessServerResultDefault();
    }
  }
}
