﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Matchmaking.PWRankedGameJoinQueueInternal
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Matchmaking
{
  [ComVisible(true)]
  public class PWRankedGameJoinQueueInternal : PWRequestBase
  {
    public JoinReq InData;
    public JoinResp OutData;
    public PWRankedGameJoinQueueReq BaseRequest;

    private PWRankedGameJoinQueueInternal()
      : base((PWConnectionBase) null)
    {
    }

    public PWRankedGameJoinQueueInternal(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_RankedJoin;

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      if (strArray.Length != 2 || !long.TryParse(strArray[0], out this.InData.UserID))
        return false;
      this.InData.TeamSize = 2;
      this.InData.Level = -1;
      this.InData.IP = this.Connection.EndPoint;
      return true;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      string OutValue;
      this.OwningServer.GetConfigValue<string>("MatchURL", out OutValue);
      PWWebReq pwWebReq = new PWWebReq(this.Connection, OutValue, false, EWebRequestType.EWRT_POST, (object) this.InData, typeof (JoinResp));
      pwWebReq.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
      return pwWebReq.SubmitServerQuery();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      if (ForRequest is PWWebReq pwWebReq && pwWebReq.Result != null)
      {
        this.OutData = (JoinResp) pwWebReq.Result;
        this.Log("Join Queue PW Result " + this.OutData.Status + " for UserID " + (object) this.InData.UserID, false, ELoggingLevel.ELL_Informative);
        if (this.OutData.Status == "success")
          return true;
      }
      else if (this.OutData.Status == "success")
        return true;
      return false;
    }

    protected override bool ProcessServerResult() => this.SendDefaultMessage();
  }
}
