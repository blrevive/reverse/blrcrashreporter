﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Matchmaking.PWRankedGameJoinQueueReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Resources;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Matchmaking
{
  [ComVisible(true)]
  public class PWRankedGameJoinQueueReq : PWRequestBase
  {
    public long UserID;
    public string QueueString;
    public int PlaceInQueue;
    public float EstimatedWaitTime;
    public ERankedJoinResponse ResponseCode;

    private PWRankedGameJoinQueueReq()
      : base((PWConnectionBase) null)
    {
      this.PlaceInQueue = -1;
      this.EstimatedWaitTime = -1f;
      this.ResponseCode = ERankedJoinResponse.ERJR_UnknownError;
    }

    public PWRankedGameJoinQueueReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
      this.PlaceInQueue = -1;
      this.EstimatedWaitTime = -1f;
      this.ResponseCode = ERankedJoinResponse.ERJR_UnknownError;
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_RankedJoin;

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      if (strArray.Length != 2 || !long.TryParse(strArray[0], out this.UserID))
        return false;
      this.QueueString = strArray[1];
      return true;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      PWRankedManager resource = this.Connection.OwningServer.GetResource<PWRankedManager>();
      if (resource != null)
      {
        this.ResponseCode = resource.AddToQueue(this);
        if (this.ResponseCode == ERankedJoinResponse.ERJR_Success)
          return true;
      }
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => this.ResponseCode == ERankedJoinResponse.ERJR_Success;

    protected override bool ProcessServerResult()
    {
      this.Log("Sending Queue Response to UserID " + (object) this.UserID + " Place " + this.PlaceInQueue.ToString() + " Wait " + this.EstimatedWaitTime.ToString(), false, ELoggingLevel.ELL_Informative);
      return this.SendMessage(this.SerializeMessage(this.PlaceInQueue.ToString() + ":" + this.EstimatedWaitTime.ToString()));
    }
  }
}
