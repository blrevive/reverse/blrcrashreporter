﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Matchmaking.Stats
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Matchmaking
{
  [ComVisible(true)]
  public struct Stats
  {
    public int kills;
    public int deaths;
    public int assists;
    public int suicides;
    public int headshots;
    public int matches;
    public int fired;
    public int hits;
    public int kill_streak;
    public int wins;
    public int losses;
    public int win_streak;
    public int lose_streak;
    public int score;
    public int playtime;
    public int total_cp;
    public int max_cp;
    public int most_purchased_depot;
    public int most_purchased_depot_times;
    public int fave_primary_weapon;
    public int fave_primary_weapon_kills;
    public int fave_secondary_weapon;
    public int fave_secondary_weapon_kills;
    public int most_kills_gear_id;
    public int most_kills_gear_kills;
    public string creation_date;
  }
}
