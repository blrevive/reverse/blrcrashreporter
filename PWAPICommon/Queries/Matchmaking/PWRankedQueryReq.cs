﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.Matchmaking.PWRankedQueryReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Queries.General;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.Matchmaking
{
  [ComVisible(true)]
  public class PWRankedQueryReq : PWRequestBase
  {
    public List<long> InForUsers;
    public List<UserRankData> OutRanks;
    private PWCombinedReq QueryRequest;
    private List<PWRankedQueryInternalReq> InternalRequests;

    public PWRankedQueryReq()
      : base((PWConnectionBase) null)
      => this.InForUsers = new List<long>();

    public PWRankedQueryReq(PWConnectionBase InConnection)
      : base(InConnection)
      => this.InForUsers = new List<long>();

    protected override EMessageType GetMessageType() => EMessageType.EMT_RankedQuery;

    public override bool SubmitClientQuery()
    {
      base.SubmitClientQuery();
      string str = "";
      foreach (long inForUser in this.InForUsers)
        str = str + inForUser.ToString() + ":";
      return this.SendMessage(this.Serializer.SerializeRaw(str.Remove(str.Length - 1)));
    }

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] strArray = this.Serializer.Deserialize(InMessage);
      if (strArray.Length < 1)
        return false;
      this.InForUsers = new List<long>(strArray.Length);
      foreach (string s in strArray)
      {
        long result = 0;
        if (!long.TryParse(s, out result))
          return false;
        this.InForUsers.Add(result);
      }
      return this.InForUsers.Count > 0;
    }

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      if (this.InForUsers.Count > 0 && this.QueryRequest == null)
      {
        this.QueryRequest = new PWCombinedReq(this.Connection);
        this.InternalRequests = new List<PWRankedQueryInternalReq>(this.InForUsers.Count);
        this.OutRanks = new List<UserRankData>(this.InForUsers.Count);
        foreach (long inForUser in this.InForUsers)
        {
          PWRankedQueryInternalReq queryInternalReq = new PWRankedQueryInternalReq(this.Connection);
          queryInternalReq.ForUserID = inForUser;
          this.QueryRequest.AddSubRequest((PWRequestBase) queryInternalReq);
          this.InternalRequests.Add(queryInternalReq);
        }
        this.QueryRequest.ServerResultProcessor = new ProcessDelegate(this.ProcessServerResultBase);
        return this.QueryRequest.SubmitServerQuery();
      }
      if (this.QueryRequest.Successful)
      {
        foreach (PWRankedQueryInternalReq internalRequest in this.InternalRequests)
        {
          if (internalRequest.Successful)
            this.OutRanks.Add(internalRequest.OutData);
        }
      }
      return this.ProcessServerResultDefault();
    }

    protected override bool ProcessServerResult()
    {
      if (this.OutRanks.Count <= 0)
        return this.SendDefaultMessage();
      string ExtraString = "";
      foreach (UserRankData outRank in this.OutRanks)
      {
        ExtraString = ExtraString + outRank.UserID.ToString() + ":";
        ExtraString = ExtraString + outRank.Rank.ToString() + ":";
      }
      ExtraString.Remove(ExtraString.Length - 1, 1);
      return this.SendMessage(this.SerializeMessage(ExtraString));
    }
  }
}
