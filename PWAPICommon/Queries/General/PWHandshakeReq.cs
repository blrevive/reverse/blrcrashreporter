﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.General.PWHandshakeReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries.General
{
  [ComVisible(true)]
  public class PWHandshakeReq : PWRequestBase
  {
    private byte[] HandshakeData = new byte[4]
    {
      (byte) 66,
      (byte) 55,
      (byte) 114,
      (byte) 33
    };

    public PWHandshakeReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWHandshakeReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_Hello;

    public override bool ParseServerQuery(byte[] InMessage) => InMessage.Length == 4 && (int) InMessage[0] == (int) this.HandshakeData[0] && ((int) InMessage[1] == (int) this.HandshakeData[1] && (int) InMessage[2] == (int) this.HandshakeData[2]) && (int) InMessage[3] == (int) this.HandshakeData[3];

    public override bool ParseClientQuery(byte[] InMessage) => InMessage.Length == 1 && InMessage[0] == (byte) 84;

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      return this.ProcessServerResultDefault();
    }

    public override bool SubmitClientQuery()
    {
      base.SubmitClientQuery();
      return this.Successful || this.SendMessage(this.HandshakeData);
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => true;

    protected override bool ProcessServerResult() => this.SendDefaultMessage();
  }
}
