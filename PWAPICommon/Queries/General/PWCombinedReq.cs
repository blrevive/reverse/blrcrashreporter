﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.General.PWCombinedReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Xml.Serialization;

namespace PWAPICommon.Queries.General
{
  [ComVisible(true)]
  public class PWCombinedReq : PWRequestBase
  {
    public List<PWRequestBase> SubRequests;

    [XmlIgnore]
    public int NumSubRequests => this.SubRequests.Count;

    public PWCombinedReq(PWConnectionBase InConnection)
      : base(InConnection)
      => this.SubRequests = new List<PWRequestBase>();

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      this.SubRequests.RemoveAll((Predicate<PWRequestBase>) (x => x == null));
      if (this.SubRequests.Count == 0)
        return this.ProcessServerResultDefault();
      bool flag = true;
      foreach (PWRequestBase subRequest in this.SubRequests)
      {
        subRequest.Connection = this.Connection;
        subRequest.ServerResultProcessor = new ProcessDelegate(this.SubReqProcessor);
        flag = flag && subRequest.SubmitServerQuery();
      }
      return flag;
    }

    protected override EMessageType GetMessageType() => EMessageType.EMT_Invalid;

    public void AddSubRequest(PWRequestBase NewRequest) => this.SubRequests.Add(NewRequest);

    private bool SubReqProcessor(PWRequestBase ForRequest)
    {
      if (this.AreAllCompleted())
        this.ProcessServerResultDefault();
      if (ForRequest != null)
        return ForRequest.Successful;
      this.Log("ForRequest Null in SubReqProcessor, assuming Success", false, ELoggingLevel.ELL_Verbose);
      return true;
    }

    public override bool ParseServerResult(PWRequestBase ForRequest) => this.AreAllCompleted() && this.AreAllSuccessful();

    private bool AreAllSuccessful()
    {
      foreach (PWRequestBase subRequest in this.SubRequests)
      {
        if (!subRequest.Successful)
          return false;
      }
      return true;
    }

    private bool AreAllCompleted()
    {
      foreach (PWRequestBase subRequest in this.SubRequests)
      {
        if (!subRequest.Completed)
          return false;
      }
      return true;
    }
  }
}
