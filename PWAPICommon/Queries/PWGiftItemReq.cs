﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Queries.PWGiftItemReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Queries.General;
using PWAPICommon.Queries.Inventory;
using PWAPICommon.Queries.Store;
using PWAPICommon.Wrappers;
using System;
using System.Runtime.InteropServices;

namespace PWAPICommon.Queries
{
  [ComVisible(true)]
  public class PWGiftItemReq : PWRequestBase
  {
    public long UserID;
    public Guid StoreItemGuid;
    public EPurchaseLength PurchaseLength;
    public int Count = -1;
    public bool bUserConnection;
    public bool bAutoActivate;
    public bool bEnforceCap = true;
    private PWCombinedReq CombinedLookupReq;
    private PWInvQueryReq InvQueryReq;
    private PWStoreQueryUserReq StoreQueryReq;
    private PWSqlItemAddReq<PWInventoryItem> InvAddReq;
    private PWActivateItemReq ActivateItemReq;
    public StoreItem StoreItemToAdd;
    public PWInventoryItem ItemToAdd;

    public PWGiftItemReq()
      : base((PWConnectionBase) null)
    {
    }

    public PWGiftItemReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    public override bool ParseServerQuery(byte[] InMessage)
    {
      string[] stringArray = this.Serializer.DeserializeToStringArray(InMessage);
      if (stringArray.Length != 3 || !long.TryParse(stringArray[0], out this.UserID) || !Guid.TryParse(stringArray[1], out this.StoreItemGuid))
        return false;
      this.PurchaseLength = (EPurchaseLength) int.Parse(stringArray[2]);
      return true;
    }

    public override bool ParseClientQuery(byte[] InMessage) => this.Serializer.DeserializeToStringArray(InMessage).Length == 1;

    protected override EMessageType GetMessageType() => EMessageType.EMT_GiftStoreItem;

    public override bool SubmitClientQuery() => this.Connection.SendMessage(EMessageType.EMT_GiftStoreItem, this.Serializer.SerializeRaw(this.UserID.ToString() + ":" + this.StoreItemGuid.ToString() + ":" + ((int) this.PurchaseLength).ToString()));

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      if (this.CombinedLookupReq == null)
      {
        this.Log("Gifting Item " + this.StoreItemGuid.ToString() + " to User " + this.UserID.ToString(), false, ELoggingLevel.ELL_Informative);
        this.CombinedLookupReq = new PWCombinedReq(this.Connection);
        this.CombinedLookupReq.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
        this.StoreQueryReq = new PWStoreQueryUserReq(this.Connection);
        this.StoreQueryReq.UserId = this.UserID;
        this.StoreQueryReq.UserType = (byte) 0;
        this.StoreQueryReq.Filter = "";
        if (this.bEnforceCap)
        {
          this.InvQueryReq = new PWInvQueryReq(this.Connection);
          this.InvQueryReq.UserId = this.UserID;
          this.CombinedLookupReq.AddSubRequest((PWRequestBase) this.InvQueryReq);
        }
        this.CombinedLookupReq.AddSubRequest((PWRequestBase) this.StoreQueryReq);
        return this.CombinedLookupReq.SubmitServerQuery();
      }
      if (this.CombinedLookupReq != null && this.CombinedLookupReq.Completed && (this.CombinedLookupReq.Successful && this.ItemToAdd == null))
      {
        if (this.StoreItemToAdd == (StoreItem) null)
        {
          this.StoreItemToAdd = this.StoreQueryReq.Items.Find((Predicate<StoreItem>) (x => x.ItemGuid == this.StoreItemGuid));
          return this.StoreItemToAdd != (StoreItem) null ? this.SubmitServerQuery() : this.ProcessServerResultDefault();
        }
        this.ItemToAdd = this.StoreItemToAdd.GeneratePurchaseItem(this.UserID, this.PurchaseLength, this.Count);
        return this.SubmitServerQuery();
      }
      if (this.ItemToAdd != null && this.InvAddReq == null)
      {
        if (this.InvQueryReq != null && this.InvQueryReq.ReadInventory.CapReached)
        {
          this.Log("Inventory Cap Reached for User " + this.UserID.ToString(), false, ELoggingLevel.ELL_Informative);
          return this.ProcessServerResultDefault();
        }
        this.InvAddReq = new PWSqlItemAddReq<PWInventoryItem>(this.Connection);
        this.InvAddReq.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
        this.InvAddReq.Item = this.ItemToAdd;
        return this.InvAddReq.SubmitServerQuery();
      }
      if (this.InvAddReq == null || !this.InvAddReq.Successful || (!this.InvAddReq.Completed || this.ActivateItemReq != null))
        return this.ProcessServerResultDefault();
      PWTransactionAddReq transactionAddReq = new PWTransactionAddReq(this.Connection);
      transactionAddReq.Item = new PWTransaction(this.UserID, ETransactionType.ETT_Gift, this.PurchaseLength, this.StoreItemGuid, this.ItemToAdd.InstanceId);
      transactionAddReq.SubmitServerQuery();
      if (!this.bAutoActivate)
        return this.ProcessServerResultDefault();
      this.ActivateItemReq = new PWActivateItemReq(this.Connection);
      this.ActivateItemReq.UserId = this.UserID;
      this.ActivateItemReq.ActivationData = this.StoreItemToAdd.ActivationData;
      this.ActivateItemReq.TargetInstanceId = this.ItemToAdd.InstanceId;
      this.ActivateItemReq.ActivationCount = this.Count;
      this.ActivateItemReq.ServerResultProcessor = new ProcessDelegate(((PWRequestBase) this).ResubmitServerQuery);
      return this.ActivateItemReq.SubmitServerQuery();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      if (this.InvAddReq == null || !this.InvAddReq.Successful || !this.InvAddReq.Completed)
        return false;
      if (!this.bAutoActivate)
        return true;
      return this.ActivateItemReq != null && this.ActivateItemReq.Successful && this.ActivateItemReq.Completed;
    }

    protected override bool ProcessServerResult()
    {
      if (this.Successful)
        (this.ActivateItemReq == null ? (PWRequestBase) new PWInvUpdatedReq(this.Connection, this.UserID, this.ItemToAdd) : (PWRequestBase) new PWInvUpdatedReq(this.Connection, this.UserID, this.ActivateItemReq.UpdateItems)).SubmitServerQuery();
      return this.Connection.UserID == this.UserID || this.SendDefaultMessage();
    }
  }
}
