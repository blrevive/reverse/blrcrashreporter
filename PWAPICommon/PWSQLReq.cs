﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.PWSQLReq
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Queries;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using System.Xml.Serialization;

namespace PWAPICommon
{
  [ComVisible(true)]
  public class PWSQLReq : PWRequestWrapper
  {
    [XmlIgnore]
    public SqlCommand RequestQuery;
    [XmlIgnore]
    public bool bBatchQuery;
    private List<object> responseValues;

    [XmlIgnore]
    public IList<object> ResponseValues
    {
      get => (IList<object>) this.responseValues;
      set => this.responseValues = value as List<object>;
    }

    public PWSQLReq()
      : this((PWConnectionBase) null)
    {
    }

    public PWSQLReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    public PWSQLReq(SqlCommand InQuery, PWConnectionBase InConnection)
      : base(InConnection)
      => this.RequestQuery = InQuery;

    protected override EMessageType GetMessageType() => EMessageType.EMT_SQLWrapper;

    public override string ToString() => "PWSQLReq:" + (this.RequestQuery ?? new SqlCommand()).CommandText;

    public override bool SubmitServerQuery()
    {
      base.SubmitServerQuery();
      PWResourceSQL resource = this.OwningServer.GetResource<PWResourceSQL>();
      if (resource == null)
        this.Log("Missing DB Connection Resource", false, ELoggingLevel.ELL_Errors);
      else if (this.RequestQuery == null)
      {
        this.Log("Missing DB Command Specified", false, ELoggingLevel.ELL_Errors);
      }
      else
      {
        this.ResponseValues = (IList<object>) new List<object>();
        return resource.SubmitRequest(this);
      }
      return this.ProcessServerResultDefault();
    }

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      if (this.bBatchQuery)
        return true;
      if (this.ResponseValues != null && this.ResponseValues.Count > 0)
      {
        string str = (this.ResponseValues[0] as object[])[0].ToString();
        if (str.IndexOf("Succeeded", StringComparison.CurrentCultureIgnoreCase) >= 0 || str.IndexOf("Success", StringComparison.CurrentCultureIgnoreCase) >= 0)
          return true;
      }
      return false;
    }
  }
}
