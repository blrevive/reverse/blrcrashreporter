﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.PWMasterServer
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Queries.Matchmaking;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace PWAPICommon
{
  [ComVisible(true)]
  public class PWMasterServer
  {
    private List<PWServerInfoRunning> ActiveServers;
    private RefreshServerListCallback RefreshCallback;
    private PWServer OwningServer;

    public PWMasterServer(PWServer InOwningServer, RefreshServerListCallback InCallback)
    {
      this.OwningServer = InOwningServer;
      this.RefreshCallback = InCallback;
      this.ActiveServers = new List<PWServerInfoRunning>();
      this.OwningServer.AddRefreshConnectionsFunc(new PWServerBase.RefreshConnectionsFunc(this.ConnectionAdded));
    }

    private void RefreshServers() => this.RefreshCallback((IList<PWServerInfoRunning>) this.ActiveServers);

    public bool RegisterServer(
      PWServerInfoFull InInfo,
      PWMachineID InMachineID,
      PWConnectionBase InConnection)
    {
      if (this.FindServer(InConnection) != null)
      {
        this.OwningServer.Log("Server already registered.", InConnection, false, ELoggingLevel.ELL_Warnings);
        return false;
      }
      PWServerInfoRunning serverInfoRunning = new PWServerInfoRunning(InConnection, InInfo);
      this.ActiveServers.Add(serverInfoRunning);
      this.OwningServer.Log("Registered Server:" + serverInfoRunning.ToString(), InConnection, false, ELoggingLevel.ELL_Verbose);
      this.RefreshServers();
      return true;
    }

    public bool UnregisterServer(PWConnectionBase InConnection)
    {
      PWServerInfoRunning server = this.FindServer(InConnection);
      this.OwningServer.Log("Unregistering Server:", InConnection, false, ELoggingLevel.ELL_Informative);
      if (server != null)
      {
        this.ActiveServers.Remove(server);
        this.OwningServer.Log("Unregistered Server:" + (object) InConnection, InConnection, false, ELoggingLevel.ELL_Verbose);
        this.RefreshServers();
        return true;
      }
      this.OwningServer.Log("Server not registered!" + (object) InConnection, InConnection, false, ELoggingLevel.ELL_Warnings);
      return false;
    }

    public List<PWServerInfoRunning> QueryServerList()
    {
      this.OwningServer.Log("Querying server list...", (PWConnectionBase) null, false, ELoggingLevel.ELL_Informative);
      return this.ActiveServers;
    }

    public bool UpdateServer(PWUpdateServerReq InRequest)
    {
      this.OwningServer.Log("Updating Server:", InRequest.Connection, false, ELoggingLevel.ELL_Verbose);
      this.RefreshServers();
      return false;
    }

    private PWServerInfoRunning FindServer(PWConnectionBase InServerConn) => this.ActiveServers.Find((Predicate<PWServerInfoRunning>) (x => x.GameConnection == InServerConn));

    public void ConnectionAdded(PWServerBase InServer)
    {
    }

    public void ConnectionRemoved(PWServerBase InServer, PWConnectionBase InConnection)
    {
      PWServerInfoRunning server = this.FindServer(InConnection);
      if (server == null)
        return;
      this.OwningServer.Log("Server went offline, removing from list! " + server.ToString(), InConnection, false, ELoggingLevel.ELL_Informative);
      this.ActiveServers.Remove(server);
      this.RefreshServers();
    }
  }
}
