﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Resources.PWPrivateServerManager
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Queries.Matchmaking;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace PWAPICommon.Resources
{
  [ComVisible(true)]
  public class PWPrivateServerManager : IPWResource
  {
    private PWConnectionBase LocalConnection;
    private PWServerBase OwningServer;
    private List<PWPrivateServerManager.PrivateServerInfo> CurrentMatches;

    public PWPrivateServerManager(PWServerBase InServer, PWConnectionBase InConnection)
    {
      this.LocalConnection = InConnection;
      this.OwningServer = InServer;
      this.CurrentMatches = new List<PWPrivateServerManager.PrivateServerInfo>();
    }

    public virtual void StartResource()
    {
    }

    public virtual void StopResource()
    {
    }

    private void Log(string Text, ELoggingLevel InLogLevel) => this.OwningServer.Log(Text, (PWConnectionBase) null, false, InLogLevel);

    public bool CreatePrivateServer(PWCreatePrivateServerReq InRequest)
    {
      lock (this.CurrentMatches)
      {
        for (int index = 0; index < this.CurrentMatches.Count; ++index)
        {
          if (this.CurrentMatches[index].MatchOwnerId == InRequest.RequestInfo.CreatorId)
          {
            this.Log("Failed to create private match for owner ID " + (object) InRequest.RequestInfo.CreatorId + ". They already have a match open", ELoggingLevel.ELL_Informative);
            return false;
          }
        }
      }
      PWPrivateServerManager.PrivateServerInfo privateServerInfo = new PWPrivateServerManager.PrivateServerInfo(InRequest.RequestInfo);
      lock (this.CurrentMatches)
      {
        this.CurrentMatches.Add(privateServerInfo);
        this.Log("Adding new private match with owner ID " + (object) InRequest.RequestInfo.CreatorId + " and we have a list size of " + (object) this.CurrentMatches.Count, ELoggingLevel.ELL_Informative);
      }
      return true;
    }

    public void NotifyPrivateMatchCreateFail(PWCreatePrivateServerReq InRequest)
    {
      lock (this.CurrentMatches)
      {
        for (int index = 0; index < this.CurrentMatches.Count; ++index)
        {
          if (this.CurrentMatches[index].MatchOwnerId == InRequest.RequestInfo.CreatorId)
          {
            this.CurrentMatches.RemoveAt(index);
            this.Log("Notified private match create fail for owner ID " + (object) InRequest.RequestInfo.CreatorId + ". Removing private match to get a list size of " + (object) this.CurrentMatches.Count, ELoggingLevel.ELL_Informative);
            break;
          }
        }
      }
    }

    public bool NotifyGameRegistered(PWConnectionBase GameConnection, PWServerInfoFull InInfo)
    {
      PWPrivateServerManager.PrivateServerInfo privateServerInfo = (PWPrivateServerManager.PrivateServerInfo) null;
      lock (this.CurrentMatches)
        privateServerInfo = this.CurrentMatches.Find((Predicate<PWPrivateServerManager.PrivateServerInfo>) (x => x.StartInfo.Equals(InInfo.BaseInfo)));
      if (privateServerInfo == null)
        return false;
      this.Log("NotifyGameRegistered Match " + privateServerInfo.StartInfo.MatchID.ToString(), ELoggingLevel.ELL_Informative);
      privateServerInfo.SetRegistered(InInfo, GameConnection);
      return true;
    }

    public void NotifyDisconnected(PWConnectionBase InConnection)
    {
      PWPrivateServerManager.PrivateServerInfo privateServerInfo = (PWPrivateServerManager.PrivateServerInfo) null;
      lock (this.CurrentMatches)
        privateServerInfo = this.CurrentMatches.Find((Predicate<PWPrivateServerManager.PrivateServerInfo>) (x => x.GameConnection == InConnection));
      if (privateServerInfo == null)
        return;
      this.Log("Game " + privateServerInfo.StartInfo.ToString() + " Destroyed", ELoggingLevel.ELL_Informative);
      lock (this.CurrentMatches)
      {
        this.CurrentMatches.Remove(privateServerInfo);
        this.Log("Removing private match and we have a list size of " + (object) this.CurrentMatches.Count, ELoggingLevel.ELL_Informative);
      }
    }

    public void PremiumServerUpdated(PremiumMatchItem InItem, bool bRestart)
    {
    }

    public Dictionary<string, string> CheckHealth()
    {
      Dictionary<string, string> dictionary = new Dictionary<string, string>();
      lock (this.CurrentMatches)
        dictionary["Private:Size"] = this.CurrentMatches.Count.ToString();
      return dictionary;
    }

    private class PrivateServerInfo
    {
      private PWServerInfoReq startInfo;
      public PWConnectionBase GameConnection;
      private long OwnerId;

      public PWServerInfoReq StartInfo => this.startInfo;

      public PrivateServerInfo(PWServerInfoReq InRequest)
      {
        this.startInfo = InRequest;
        this.OwnerId = InRequest.CreatorId;
      }

      public void SetRegistered(PWServerInfoFull RegisterInfo, PWConnectionBase InConnection) => this.GameConnection = InConnection;

      public long MatchOwnerId => this.OwnerId;
    }
  }
}
