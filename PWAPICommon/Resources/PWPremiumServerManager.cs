﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Resources.PWPremiumServerManager
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace PWAPICommon.Resources
{
  [ComVisible(true)]
  public class PWPremiumServerManager : IPWResource
  {
    private PWConnectionBase LocalConnection;
    private PWServerBase OwningServer;

    public PWPremiumServerManager(PWServerBase InServer, PWConnectionBase InConnection)
    {
      this.LocalConnection = InConnection;
      this.OwningServer = InServer;
    }

    public virtual void StartResource()
    {
    }

    public virtual void StopResource()
    {
    }

    public Dictionary<string, string> CheckHealth()
    {
      Dictionary<string, string> dictionary = new Dictionary<string, string>();
      dictionary.Clear();
      return dictionary;
    }
  }
}
