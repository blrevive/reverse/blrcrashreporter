﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Resources.PWPresenceCache`1
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Queries;
using PWAPICommon.Queries.General;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace PWAPICommon.Resources
{
  [ComVisible(true)]
  public class PWPresenceCache<T> : IPWPresenceCache<T>, IPWPresenceCache, IPWResource
    where T : PWPresenceInfo
  {
    private bool bCrossRegion;
    private ConcurrentDictionary<long, T> PresenceInfo;
    private PWServerBase OwningServer;
    private PWRegionMap.RegionInfo RegionInfo;

    public bool CrossRegion => this.bCrossRegion;

    public PWPresenceCache(PWServerBase InOwningServer)
      : this(InOwningServer, true)
    {
    }

    public PWPresenceCache(PWServerBase InOwningServer, bool InCrossRegion)
    {
      this.OwningServer = InOwningServer;
      this.PresenceInfo = new ConcurrentDictionary<long, T>();
      if (!PWRegionMap.GetRegionInfo(this.OwningServer.ServerRegionName, out this.RegionInfo))
        this.Log("Invalid Region Name " + this.OwningServer.ServerRegionName, (PWConnectionBase) null, false, ELoggingLevel.ELL_Warnings);
      this.bCrossRegion = InCrossRegion;
    }

    public virtual void StartResource()
    {
    }

    public virtual void StopResource() => this.PurgePresenceInfo();

    public Dictionary<string, string> CheckHealth() => new Dictionary<string, string>();

    public void NotifyConnected(PWConnectionBase Connection, T Presence)
    {
      if (Connection == null || Connection.UserID == 0L || (object) Presence == null)
        return;
      Presence.UserID = Connection.UserID;
      Presence.RegionName = this.OwningServer.ServerRegionName;
      this.ApplyPresence(Connection.UserID, EPresenceUpdateType.EPUT_Add, Presence);
    }

    public void NotifyDisconnected(PWConnectionBase Connection)
    {
      if (Connection == null || Connection.UserID == 0L)
        return;
      this.ApplyPresence(Connection.UserID, EPresenceUpdateType.EPUT_Remove, default (T));
    }

    public void ApplyPresence(long UserID, EPresenceUpdateType UpdateType, T Presence)
    {
      bool flag = false;
      switch (UpdateType)
      {
        case EPresenceUpdateType.EPUT_Add:
          flag = this.AddPresence(UserID, Presence);
          break;
        case EPresenceUpdateType.EPUT_Update:
          flag = this.UpdatePresence(UserID, Presence);
          break;
        case EPresenceUpdateType.EPUT_Remove:
          flag = this.RemovePresence(UserID, out Presence);
          break;
      }
      if (!flag || (object) Presence == null || !(Presence.RegionName == this.OwningServer.ServerRegionName))
        return;
      this.SendPresenceUpdate(UserID, Presence, UpdateType);
    }

    public T GetPresence(long UserID)
    {
      T obj = default (T);
      this.PresenceInfo.TryGetValue(UserID, out obj);
      return obj;
    }

    private bool AddPresence(long UserID, T Presence)
    {
      if ((object) Presence == null || Presence.UserID != UserID || !this.PresenceInfo.TryAdd(UserID, Presence))
        return false;
      this.Log("Added Presence Info: " + Presence.ToString(), (PWConnectionBase) null, false, ELoggingLevel.ELL_Verbose);
      return true;
    }

    private bool UpdatePresence(long UserID, T Presence)
    {
      T obj = default (T);
      if ((object) Presence == null)
        return false;
      T presence = this.GetPresence(UserID);
      if ((object) presence == null)
        return this.AddPresence(UserID, Presence);
      presence.CopyFrom((PWPresenceInfo) Presence);
      this.Log("Updated Presence Info: " + presence.ToString(), (PWConnectionBase) null, false, ELoggingLevel.ELL_Verbose);
      return true;
    }

    private bool RemovePresence(long UserID, out T Presence)
    {
      if (!this.PresenceInfo.TryRemove(UserID, out Presence) || (object) Presence == null)
        return false;
      this.Log("Removed Presence Info: " + Presence.ToString(), (PWConnectionBase) null, false, ELoggingLevel.ELL_Verbose);
      return true;
    }

    private void PurgePresenceInfo() => this.PresenceInfo.Clear();

    public bool SendExternalQuery<U>(U InRequest) where U : PWRequestBase, ICloneable
    {
      if (!this.bCrossRegion || (object) InRequest == null)
        return false;
      foreach (string visibleRegion in this.RegionInfo.VisibleRegions)
      {
        if (InRequest.Clone() is U u)
          this.OwningServer.SendExternalQuery((PWRequestBase) u, visibleRegion, 5);
      }
      return true;
    }

    public bool SendExternalQuery<U>(long UserID, U InRequest) where U : PWRequestBase, ICloneable
    {
      T obj;
      return this.bCrossRegion && this.PresenceInfo.TryGetValue(UserID, out obj) && ((object) obj != null && obj.RegionName != this.OwningServer.ServerRegionName) && InRequest.Clone() is U u && this.OwningServer.SendExternalQuery((PWRequestBase) u, obj.RegionName, 5);
    }

    private void SendPresenceUpdate(long UserID, T Presence, EPresenceUpdateType UpdateType) => this.SendExternalQuery<PWPresenceUpdateReq<T>>(new PWPresenceUpdateReq<T>()
    {
      UserID = UserID,
      UpdateType = UpdateType,
      PresenceInfo = Presence
    });

    private void Log(
      string Text,
      PWConnectionBase Connection,
      bool bLogToRemoteServer,
      ELoggingLevel LogLevel)
    {
      if (this.OwningServer == null)
        return;
      this.OwningServer.Log(Text, Connection, bLogToRemoteServer, LogLevel);
    }
  }
}
