﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Resources.PW509Cert
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;

namespace PWAPICommon.Resources
{
  [ComVisible(true)]
  public class PW509Cert : X509Certificate2, IPWResource
  {
    public PW509Cert(string Filename, string password)
      : base(Filename, password)
    {
    }

    public virtual void StartResource()
    {
    }

    public virtual void StopResource()
    {
    }

    public Dictionary<string, string> CheckHealth() => new Dictionary<string, string>()
    {
      ["X509:Status"] = this.HasPrivateKey ? "OK" : "MISSING",
      ["X509:Name"] = this.FriendlyName
    };
  }
}
