﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Resources.PWConsoleManager
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;

namespace PWAPICommon.Resources
{
  [ComVisible(true)]
  public static class PWConsoleManager
  {
    private const string Kernel32_DllName = "kernel32.dll";

    [DllImport("kernel32.dll")]
    private static extern bool AllocConsole();

    [DllImport("kernel32.dll")]
    private static extern bool FreeConsole();

    [DllImport("kernel32.dll")]
    private static extern IntPtr GetConsoleWindow();

    [DllImport("kernel32.dll")]
    private static extern int GetConsoleOutputCP();

    public static bool HasConsole => PWConsoleManager.GetConsoleWindow() != IntPtr.Zero;

    public static void Show()
    {
      if (PWConsoleManager.HasConsole)
        return;
      PWConsoleManager.AllocConsole();
      PWConsoleManager.InvalidateOutAndError();
    }

    public static void Hide()
    {
      if (!PWConsoleManager.HasConsole)
        return;
      PWConsoleManager.SetOutAndErrorNull();
      PWConsoleManager.FreeConsole();
    }

    public static void Toggle()
    {
      if (PWConsoleManager.HasConsole)
        PWConsoleManager.Hide();
      else
        PWConsoleManager.Show();
    }

    private static void InvalidateOutAndError()
    {
      Type type = typeof (Console);
      FieldInfo field1 = type.GetField("_out", BindingFlags.Static | BindingFlags.NonPublic);
      FieldInfo field2 = type.GetField("_error", BindingFlags.Static | BindingFlags.NonPublic);
      MethodInfo method = type.GetMethod("InitializeStdOutError", BindingFlags.Static | BindingFlags.NonPublic);
      field1.SetValue((object) null, (object) null);
      field2.SetValue((object) null, (object) null);
      method.Invoke((object) null, new object[1]
      {
        (object) true
      });
    }

    private static void SetOutAndErrorNull()
    {
      Console.SetOut(TextWriter.Null);
      Console.SetError(TextWriter.Null);
    }
  }
}
