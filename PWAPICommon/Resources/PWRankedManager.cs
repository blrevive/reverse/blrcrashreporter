﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Resources.PWRankedManager
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Queries;
using PWAPICommon.Queries.Matchmaking;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Timers;

namespace PWAPICommon.Resources
{
  [ComVisible(true)]
  public class PWRankedManager : IPWResource
  {
    private List<PWRankedManager.QueuedPlayerInfo> PlayerQueue;
    private List<PWRankedManager.RankedGameInfo> CurrentMatches;
    private List<PWRankedManager.RankedGameInfo> EndedMatches;
    private PWConnectionBase LocalConnection;
    private List<PWRankedManager.WaitTimeInfo> RecentWaitTimes;
    private PWServerBase OwningServer;
    private Timer LocalMatchmakingTimer;
    private PWRankedManager.LocalMatchmakingConfig MatchConfig;

    public PWRankedManager(PWServerBase InServer, PWConnectionBase InConnection)
    {
      this.LocalConnection = InConnection;
      this.OwningServer = InServer;
      this.PlayerQueue = new List<PWRankedManager.QueuedPlayerInfo>();
      this.CurrentMatches = new List<PWRankedManager.RankedGameInfo>(100);
      this.EndedMatches = new List<PWRankedManager.RankedGameInfo>(1000);
      this.RecentWaitTimes = new List<PWRankedManager.WaitTimeInfo>(1000);
      this.LocalMatchmakingTimer = new Timer(30000.0);
      this.LocalMatchmakingTimer.Elapsed += new ElapsedEventHandler(this.OnMatchmakingTimer);
      this.MatchConfig = new PWRankedManager.LocalMatchmakingConfig();
      this.MatchConfig.AddGame("KC");
      this.MatchConfig.AddGame("TDM");
      this.MatchConfig.AddGame("TKOTH");
      this.MatchConfig.AddMap("HeavyMetal");
      this.MatchConfig.AddMap("SeaPort");
      this.MatchConfig.AddMap("Deadlock");
      this.MatchConfig.AddMap("Vertigo");
      this.MatchConfig.AddTeamSize(1);
    }

    private void Log(string Text, ELoggingLevel InLogLevel) => this.OwningServer.Log(Text, (PWConnectionBase) null, false, InLogLevel);

    private float GetAverageWaitTimeSeconds()
    {
      float num = 0.0f;
      foreach (PWRankedManager.WaitTimeInfo recentWaitTime in this.RecentWaitTimes)
        num += recentWaitTime.WaitTime;
      return num / (float) this.RecentWaitTimes.Count / 1000f;
    }

    private void OnMatchmakingTimer(object Source, ElapsedEventArgs args)
    {
      bool OutValue = false;
      this.OwningServer.GetConfigValue<bool>("LocalMatchmaking", out OutValue, false);
      if (!OutValue)
        return;
      this.Log("Local Matchmaking Check Queue Size " + (object) this.PlayerQueue.Count, ELoggingLevel.ELL_Informative);
      IList<PWRankedManager.QueuedPlayerInfo> list = (IList<PWRankedManager.QueuedPlayerInfo>) this.PlayerQueue.Where<PWRankedManager.QueuedPlayerInfo>((Func<PWRankedManager.QueuedPlayerInfo, bool>) (x => !x.bClaimed)).ToList<PWRankedManager.QueuedPlayerInfo>();
      for (PWCreateCustomServerReq nextmatch = this.MatchConfig.GenerateNextmatch(list); nextmatch != null; nextmatch = this.MatchConfig.GenerateNextmatch(list))
      {
        this.Log("Local Matchmaking Creating Game " + nextmatch.ToString(), ELoggingLevel.ELL_Informative);
        nextmatch.Connection = this.LocalConnection;
        nextmatch.SubmitServerQuery();
        list = (IList<PWRankedManager.QueuedPlayerInfo>) list.Where<PWRankedManager.QueuedPlayerInfo>((Func<PWRankedManager.QueuedPlayerInfo, bool>) (x => !x.bClaimed)).ToList<PWRankedManager.QueuedPlayerInfo>();
      }
    }

    public ERankedJoinResponse AddToQueue(PWRankedGameJoinQueueReq InRequest)
    {
      this.Log("Trying to add player to Ranked Queue UserID " + InRequest.UserID.ToString(), ELoggingLevel.ELL_Informative);
      lock (this.PlayerQueue)
      {
        if (this.PlayerQueue.FindIndex((Predicate<PWRankedManager.QueuedPlayerInfo>) (x => x.Connection == InRequest.Connection)) > 0)
        {
          this.Log("Player " + InRequest.UserID.ToString() + " already in queue, ignoring!", ELoggingLevel.ELL_Warnings);
          return ERankedJoinResponse.ERJR_AlreadyInQueue;
        }
      }
      this.SendQueueRequest(InRequest);
      return ERankedJoinResponse.ERJR_Success;
    }

    private bool SendQueueRequest(PWRankedGameJoinQueueReq InBaseRequest)
    {
      bool OutValue = false;
      this.OwningServer.GetConfigValue<bool>("LocalMatchmaking", out OutValue, false);
      this.Log("Sending Ranked Queue Request to Backend for UserID " + InBaseRequest.UserID.ToString(), ELoggingLevel.ELL_Informative);
      PWRankedGameJoinQueueInternal joinQueueInternal = new PWRankedGameJoinQueueInternal(InBaseRequest.Connection);
      joinQueueInternal.BaseRequest = InBaseRequest;
      joinQueueInternal.InData.IP = InBaseRequest.Connection.EndPoint;
      joinQueueInternal.InData.Level = -1;
      joinQueueInternal.InData.TeamSize = 2;
      joinQueueInternal.InData.UserID = InBaseRequest.UserID;
      joinQueueInternal.ServerResultProcessor = new ProcessDelegate(this.QueuedRequestComplete);
      if (!OutValue)
        return joinQueueInternal.SubmitServerQuery();
      joinQueueInternal.OutData.Status = "success";
      return joinQueueInternal.ProcessServerResultDefault();
    }

    private bool QueuedRequestComplete(PWRequestBase InWrapper)
    {
      PWRankedGameJoinQueueInternal joinQueueInternal = InWrapper as PWRankedGameJoinQueueInternal;
      PWRankedGameJoinQueueReq baseRequest = joinQueueInternal.BaseRequest;
      this.Log("Ranked Queue Request response from Backend for UserID " + joinQueueInternal.InData.UserID.ToString(), ELoggingLevel.ELL_Informative);
      baseRequest.EstimatedWaitTime = this.GetAverageWaitTimeSeconds();
      if (joinQueueInternal.Successful)
      {
        baseRequest.ResponseCode = ERankedJoinResponse.ERJR_Success;
        PWRankedManager.QueuedPlayerInfo queuedPlayerInfo = new PWRankedManager.QueuedPlayerInfo(joinQueueInternal.Connection, joinQueueInternal.InData.UserID);
        queuedPlayerInfo.StartQueue();
        lock (this.PlayerQueue)
        {
          this.PlayerQueue.Add(queuedPlayerInfo);
          baseRequest.PlaceInQueue = this.PlayerQueue.Count;
        }
        this.Log("Adding Player " + joinQueueInternal.InData.UserID.ToString() + " to matchmaking Queue. New Length:" + baseRequest.PlaceInQueue.ToString() + " Wait: " + baseRequest.EstimatedWaitTime.ToString(), ELoggingLevel.ELL_Informative);
      }
      else
      {
        switch (joinQueueInternal.OutData.ErrorCode)
        {
          case -99:
            baseRequest.ResponseCode = ERankedJoinResponse.ERJR_InternalDBErrorConn;
            break;
          case -98:
            baseRequest.ResponseCode = ERankedJoinResponse.ERJR_InternalDBErrorParam;
            break;
          case -9:
            baseRequest.ResponseCode = ERankedJoinResponse.ERJR_NoActiveSeason;
            break;
          case -5:
            baseRequest.ResponseCode = ERankedJoinResponse.ERJR_InvalidTeamSize;
            break;
          case -3:
            baseRequest.ResponseCode = ERankedJoinResponse.ERJR_InvalidIP;
            break;
          case -2:
            baseRequest.ResponseCode = ERankedJoinResponse.ERJR_InvalidUser;
            break;
          default:
            baseRequest.ResponseCode = ERankedJoinResponse.ERJR_UnknownError;
            break;
        }
        this.Log("PWRankedGameJoinQueueInternal failed with Response Code: " + baseRequest.ResponseCode.ToString(), ELoggingLevel.ELL_Informative);
      }
      return baseRequest.ProcessServerResultDefault();
    }

    public ERankedJoinResponse PartyAddToQueue(
      PWPartyRankedGameJoinQueueReq InRequest)
    {
      this.Log("PWRankedManager adding party to queue", ELoggingLevel.ELL_Informative);
      lock (this.PlayerQueue)
      {
        PWSuperServer resource = this.OwningServer.GetResource<PWSuperServer>();
        if (resource != null)
        {
          for (int index = 0; index < InRequest.PartyMembers.Count; ++index)
          {
            PWConnectionBase Conn = resource.GetLoggedInConnFromId(InRequest.PartyMembers[index]);
            if (this.PlayerQueue.FindIndex((Predicate<PWRankedManager.QueuedPlayerInfo>) (x => x.Connection == Conn)) > 0)
            {
              this.Log("Player " + InRequest.PartyMembers[index].ToString() + " already in queue, ignoring!", ELoggingLevel.ELL_Warnings);
              return ERankedJoinResponse.ERJR_AlreadyInQueue;
            }
          }
        }
        else
          this.Log("PWRankedManager couldn't add party to queue. No Super Server", ELoggingLevel.ELL_Informative);
      }
      this.SendPartyQueueRequest(InRequest);
      return ERankedJoinResponse.ERJR_Success;
    }

    private bool SendPartyQueueRequest(PWPartyRankedGameJoinQueueReq InBaseRequest)
    {
      PWPartyRankedGameJoinQueueInternalReq queueInternalReq = new PWPartyRankedGameJoinQueueInternalReq(InBaseRequest.Connection);
      queueInternalReq.BaseRequest = InBaseRequest;
      queueInternalReq.PartyMembers = InBaseRequest.PartyMembers;
      queueInternalReq.ServerResultProcessor = new ProcessDelegate(this.PartyQueuedRequestComplete);
      this.Log("PWRankedManager sending party queue request", ELoggingLevel.ELL_Informative);
      return queueInternalReq.SubmitServerQuery();
    }

    private bool PartyQueuedRequestComplete(PWRequestBase InWrapper)
    {
      PWPartyRankedGameJoinQueueInternalReq queueInternalReq = InWrapper as PWPartyRankedGameJoinQueueInternalReq;
      PWPartyRankedGameJoinQueueReq baseRequest = queueInternalReq.BaseRequest;
      baseRequest.EstimatedWaitTime = this.GetAverageWaitTimeSeconds();
      if (queueInternalReq.Successful)
      {
        baseRequest.ResponseCode = ERankedJoinResponse.ERJR_Success;
        this.Log("PWRankedManager Party queue request complete. Adding players to queue now.", ELoggingLevel.ELL_Informative);
        lock (this.PlayerQueue)
        {
          PWSuperServer resource = this.OwningServer.GetResource<PWSuperServer>();
          if (resource != null)
          {
            for (int index = 0; index < queueInternalReq.PartyMembers.Count; ++index)
            {
              PWRankedManager.QueuedPlayerInfo queuedPlayerInfo = new PWRankedManager.QueuedPlayerInfo(resource.GetLoggedInConnFromId(queueInternalReq.PartyMembers[index]), queueInternalReq.PartyMembers[index]);
              queuedPlayerInfo.StartQueue();
              this.PlayerQueue.Add(queuedPlayerInfo);
              baseRequest.PlaceInQueue = this.PlayerQueue.Count;
            }
          }
        }
      }
      else
      {
        switch (queueInternalReq.OutData.ErrorCode)
        {
          case -99:
            baseRequest.ResponseCode = ERankedJoinResponse.ERJR_InternalDBErrorConn;
            break;
          case -98:
            baseRequest.ResponseCode = ERankedJoinResponse.ERJR_InternalDBErrorParam;
            break;
          case -9:
            baseRequest.ResponseCode = ERankedJoinResponse.ERJR_NoActiveSeason;
            break;
          case -5:
            baseRequest.ResponseCode = ERankedJoinResponse.ERJR_InvalidTeamSize;
            break;
          case -3:
            baseRequest.ResponseCode = ERankedJoinResponse.ERJR_InvalidIP;
            break;
          case -2:
            baseRequest.ResponseCode = ERankedJoinResponse.ERJR_InvalidUser;
            break;
          default:
            baseRequest.ResponseCode = ERankedJoinResponse.ERJR_UnknownError;
            break;
        }
        this.Log("PWRankedGameJoinQueueInternal failed with Response Code: " + baseRequest.ResponseCode.ToString(), ELoggingLevel.ELL_Informative);
      }
      return baseRequest.ProcessServerResultDefault();
    }

    public void RemoveFromQueue(PWConnectionBase InConnection, long UserID, bool bMatchFound)
    {
      this.Log("Trying to remove player from Ranked Queue UserID " + UserID.ToString(), ELoggingLevel.ELL_Informative);
      PWRankedManager.QueuedPlayerInfo queuedPlayerInfo = (PWRankedManager.QueuedPlayerInfo) null;
      lock (this.PlayerQueue)
      {
        queuedPlayerInfo = this.PlayerQueue.Find((Predicate<PWRankedManager.QueuedPlayerInfo>) (x => x.Connection == InConnection));
        this.PlayerQueue.RemoveAll((Predicate<PWRankedManager.QueuedPlayerInfo>) (x => x.Connection == InConnection));
      }
      if (queuedPlayerInfo == null)
        return;
      this.Log("Removing Player " + queuedPlayerInfo.UserID.ToString() + " from matchmaking Queue", ELoggingLevel.ELL_Informative);
      if (bMatchFound)
      {
        queuedPlayerInfo.StopQueue();
        this.RecentWaitTimes.Add(new PWRankedManager.WaitTimeInfo()
        {
          TimeMatchFound = PWServerBase.CurrentTime,
          WaitTime = queuedPlayerInfo.TimeWaiting
        });
      }
      PWRankedGameLeaveQueueInternal leaveQueueInternal = new PWRankedGameLeaveQueueInternal(InConnection);
      leaveQueueInternal.InData.UserID = UserID;
      leaveQueueInternal.InData.IP = InConnection.EndPoint;
      leaveQueueInternal.ServerResultProcessor = new ProcessDelegate(this.OnRemoveFromQueueComplete);
      leaveQueueInternal.SubmitServerQuery();
    }

    private bool OnRemoveFromQueueComplete(PWRequestBase InWrapper)
    {
      PWRankedGameLeaveQueueInternal leaveQueueInternal = InWrapper as PWRankedGameLeaveQueueInternal;
      this.Log("Ranked Queue Leave response from Backend for UserID [" + leaveQueueInternal.InData.UserID.ToString() + "] Message [" + leaveQueueInternal.OutData.Message + "] Error [" + (object) leaveQueueInternal.OutData.Error + "] Status [" + leaveQueueInternal.OutData.Status + "]", ELoggingLevel.ELL_Informative);
      return true;
    }

    public bool CreateRankedGame(PWCreateCustomServerReq InRequest)
    {
      this.Log("Creating a ranked game: " + InRequest.RequestInfo.ToString(), ELoggingLevel.ELL_Informative);
      PWRankedManager.RankedGameInfo rankedGameInfo = new PWRankedManager.RankedGameInfo(InRequest.RequestInfo);
      lock (this.PlayerQueue)
      {
        for (int index = 0; index < this.PlayerQueue.Count; ++index)
        {
          PWRankedManager.QueuedPlayerInfo player = this.PlayerQueue[index];
          if (InRequest.RequestInfo.ArbitratedTeamAPlayers.Contains(player.UserID) || InRequest.RequestInfo.ArbitratedTeamBPlayers.Contains(player.UserID))
            rankedGameInfo.Players.Add(new KeyValuePair<PWConnectionBase, long>(player.Connection, player.UserID));
        }
      }
      int num = InRequest.RequestInfo.ArbitratedTeamBPlayers.Count + InRequest.RequestInfo.ArbitratedTeamAPlayers.Count;
      int count = rankedGameInfo.Players.Count;
      bool OutValue = false;
      this.OwningServer.GetConfigValue<bool>("AllowRankedQueueErrors", out OutValue, false);
      if (num == count || OutValue)
      {
        lock (this.CurrentMatches)
          this.CurrentMatches.Add(rankedGameInfo);
        return true;
      }
      foreach (long arbitratedTeamAplayer in InRequest.RequestInfo.ArbitratedTeamAPlayers)
      {
        bool flag = false;
        foreach (KeyValuePair<PWConnectionBase, long> player in (IEnumerable<KeyValuePair<PWConnectionBase, long>>) rankedGameInfo.Players)
        {
          if (player.Value == arbitratedTeamAplayer)
          {
            flag = true;
            break;
          }
        }
        if (!flag)
          this.Log("Match " + (object) rankedGameInfo.StartInfo.MatchID + " Player " + arbitratedTeamAplayer.ToString() + " not found!", ELoggingLevel.ELL_Errors);
      }
      return false;
    }

    public void NotifyGameCreated(PWServerInfoFull InInfo)
    {
      this.Log("NotifyGameCreated " + InInfo.BaseInfo.RequestId.ToString(), ELoggingLevel.ELL_Informative);
      lock (this.CurrentMatches)
        this.CurrentMatches.Find((Predicate<PWRankedManager.RankedGameInfo>) (x => x.StartInfo.Equals(InInfo.BaseInfo)))?.SetCreated(InInfo);
    }

    public bool NotifyGameRegistered(PWConnectionBase GameConnection, PWServerInfoFull InInfo)
    {
      PWRankedManager.RankedGameInfo rankedGameInfo = (PWRankedManager.RankedGameInfo) null;
      lock (this.CurrentMatches)
        rankedGameInfo = this.CurrentMatches.Find((Predicate<PWRankedManager.RankedGameInfo>) (x => x.StartInfo.Equals(InInfo.BaseInfo)));
      if (rankedGameInfo == null)
        return false;
      if (rankedGameInfo.GameState == PWRankedManager.RankedGameState.RGS_Created && rankedGameInfo.FullInfo != null)
      {
        this.Log("NotifyGameRegistered Match " + rankedGameInfo.StartInfo.MatchID.ToString(), ELoggingLevel.ELL_Informative);
        rankedGameInfo.SetRegistered(InInfo, GameConnection);
        if (rankedGameInfo.Players != null)
        {
          foreach (KeyValuePair<PWConnectionBase, long> player in (IEnumerable<KeyValuePair<PWConnectionBase, long>>) rankedGameInfo.Players)
          {
            if (player.Key != null)
            {
              this.Log("Telling player " + player.Value.ToString() + " to join " + rankedGameInfo.FullInfo.GetConnectString() + " for match " + rankedGameInfo.StartInfo.MatchID.ToString(), ELoggingLevel.ELL_Informative);
              this.RemoveFromQueue(player.Key, player.Value, true);
              player.Key.SendMessage(EMessageType.EMT_RankedStarted, new UTF8Encoding().GetBytes("T:" + rankedGameInfo.FullInfo.GetConnectString()));
            }
            else
              this.Log("Player or Player Connection null in PWRankedManager::NotifyGameRegistered!", ELoggingLevel.ELL_Errors);
          }
        }
        else
          this.Log("Info.Players == null in PWRankedManager::NotifyGameRegistered", ELoggingLevel.ELL_Errors);
      }
      else
        this.Log("NotifyGameRegistered found game in invalid state " + rankedGameInfo.GameState.ToString(), ELoggingLevel.ELL_Errors);
      return true;
    }

    public void NotifyGameEnded(PWConnectionBase GameConnection, PWServerInfoFull InInfo)
    {
      PWRankedManager.RankedGameInfo rankedGameInfo = (PWRankedManager.RankedGameInfo) null;
      lock (this.CurrentMatches)
        rankedGameInfo = this.CurrentMatches.Find((Predicate<PWRankedManager.RankedGameInfo>) (x => x.StartInfo.Equals(InInfo.BaseInfo)));
      this.Log("NotifyGameEnded " + InInfo.BaseInfo.RequestId.ToString(), ELoggingLevel.ELL_Informative);
      rankedGameInfo?.SetEnded();
    }

    public void NotifyReportedStats(PWConnectionBase InConnection, PWRankedGameReportReq Report)
    {
    }

    public void NotifyGameDestroyed(PWConnectionBase InConnection)
    {
      PWRankedManager.RankedGameInfo rankedGameInfo = (PWRankedManager.RankedGameInfo) null;
      lock (this.CurrentMatches)
        rankedGameInfo = this.CurrentMatches.Find((Predicate<PWRankedManager.RankedGameInfo>) (x => x.GameConnection == InConnection));
      if (rankedGameInfo == null)
        return;
      this.Log("Game " + rankedGameInfo.StartInfo.ToString() + " Destroyed", ELoggingLevel.ELL_Informative);
      rankedGameInfo.SetDestroyed();
      this.CurrentMatches.Remove(rankedGameInfo);
      this.EndedMatches.Add(rankedGameInfo);
    }

    public void NotifyDisconnected(PWConnectionBase InConnection)
    {
      PWRankedManager.QueuedPlayerInfo queuedPlayerInfo = (PWRankedManager.QueuedPlayerInfo) null;
      lock (this.PlayerQueue)
        queuedPlayerInfo = this.PlayerQueue.Find((Predicate<PWRankedManager.QueuedPlayerInfo>) (x => x.Connection == InConnection));
      if (queuedPlayerInfo != null)
      {
        this.Log("User " + queuedPlayerInfo.UserID.ToString() + " Disconnected while in Queue!", ELoggingLevel.ELL_Informative);
        this.RemoveFromQueue(InConnection, queuedPlayerInfo.UserID, false);
        new PWRankedGameLeaveQueueReq(this.LocalConnection)
        {
          UserID = queuedPlayerInfo.UserID
        }.SubmitServerQuery();
      }
      else
        this.NotifyGameDestroyed(InConnection);
    }

    public virtual void StartResource() => this.LocalMatchmakingTimer.Start();

    public virtual void StopResource() => this.LocalMatchmakingTimer.Stop();

    public Dictionary<string, string> CheckHealth()
    {
      Dictionary<string, string> dictionary = new Dictionary<string, string>();
      lock (this.PlayerQueue)
        dictionary["Ranked:Queue"] = this.PlayerQueue.Count.ToString();
      dictionary["Ranked:Wait"] = this.GetAverageWaitTimeSeconds().ToString();
      dictionary["Ranked:MatchesInProgress"] = this.CurrentMatches.Count.ToString();
      dictionary["Ranked:MatchedEnded"] = this.EndedMatches.Count.ToString();
      return dictionary;
    }

    private enum RankedGameState
    {
      RGS_Initial,
      RGS_Created,
      RGS_Registered,
      RGS_Ended,
      RGS_Destroyed,
    }

    private class LocalMatchmakingConfig
    {
      private Queue<string> MapNameRotation;
      private Queue<string> GameTypeRotation;
      private Queue<int> TeamSizeRotation;
      private int MatchID;
      private int SeasonID;

      public LocalMatchmakingConfig()
      {
        this.MapNameRotation = new Queue<string>();
        this.GameTypeRotation = new Queue<string>();
        this.TeamSizeRotation = new Queue<int>();
        this.MatchID = 100000;
        this.SeasonID = 123;
      }

      public void AddMap(string MapName) => this.MapNameRotation.Enqueue(MapName);

      public void AddGame(string GameType) => this.GameTypeRotation.Enqueue(GameType);

      public void AddTeamSize(int TeamSize) => this.TeamSizeRotation.Enqueue(TeamSize);

      public PWCreateCustomServerReq GenerateNextmatch(
        IList<PWRankedManager.QueuedPlayerInfo> PlayerQueue)
      {
        if (PlayerQueue.Count < this.TeamSizeRotation.Peek() * 2)
          return (PWCreateCustomServerReq) null;
        string str1 = this.GameTypeRotation.Dequeue();
        this.GameTypeRotation.Enqueue(str1);
        string str2 = this.MapNameRotation.Dequeue();
        this.MapNameRotation.Enqueue(str2);
        int num1 = this.TeamSizeRotation.Dequeue();
        this.TeamSizeRotation.Enqueue(num1);
        int num2 = this.MatchID++;
        PWCreateCustomServerReq createCustomServerReq = new PWCreateCustomServerReq();
        createCustomServerReq.RequestInfo = new PWServerInfoReq();
        createCustomServerReq.RequestInfo.bArbitrated = true;
        createCustomServerReq.RequestInfo.bPrivate = true;
        createCustomServerReq.RequestInfo.GameType = str1;
        createCustomServerReq.RequestInfo.MapName = str2;
        createCustomServerReq.RequestInfo.MatchID = num2;
        createCustomServerReq.RequestInfo.ServerName = "RankedMatch";
        createCustomServerReq.RequestInfo.SeasonID = this.SeasonID;
        createCustomServerReq.RequestInfo.RequestId = Guid.NewGuid();
        createCustomServerReq.RequestInfo.Password = createCustomServerReq.RequestInfo.RequestId.ToString();
        createCustomServerReq.RequestInfo.OwnerName = PWServerBase.AdminName;
        createCustomServerReq.RequestInfo.CreatorId = PWServerBase.AdminUserID;
        createCustomServerReq.RequestInfo.MaxPlayers = num1 * 2;
        createCustomServerReq.RequestInfo.MaxSpectators = 0;
        for (int index = 0; index < num1 * 2; ++index)
        {
          PlayerQueue[index].bClaimed = true;
          if (index % 2 == 1)
            createCustomServerReq.RequestInfo.ArbitratedTeamAPlayers.Add(PlayerQueue[index].UserID);
          else
            createCustomServerReq.RequestInfo.ArbitratedTeamBPlayers.Add(PlayerQueue[index].UserID);
        }
        return createCustomServerReq;
      }
    }

    private class QueuedPlayerInfo
    {
      public PWConnectionBase Connection;
      public long UserID;
      public bool bClaimed;
      private Stopwatch timeWaiting;

      public QueuedPlayerInfo(PWConnectionBase InConnection, long InUserID)
      {
        this.Connection = InConnection;
        this.UserID = InUserID;
        this.timeWaiting = new Stopwatch();
        this.bClaimed = false;
      }

      public void StartQueue() => this.timeWaiting.Start();

      public void StopQueue() => this.timeWaiting.Stop();

      public float TimeWaiting => (float) this.timeWaiting.ElapsedMilliseconds;
    }

    public class WaitTimeInfo
    {
      public DateTime TimeMatchFound;
      public float WaitTime;
    }

    private class RankedGameInfo
    {
      private List<long> EndedGamePlayerList;
      private List<KeyValuePair<PWConnectionBase, long>> PlayersInGame;
      public PWConnectionBase GameConnection;
      private PWRankedManager.RankedGameState gameState;
      private PWServerInfoReq startInfo;
      private PWServerInfoFull fullInfo;

      public RankedGameInfo(PWServerInfoReq InRequest)
      {
        this.startInfo = InRequest;
        this.gameState = PWRankedManager.RankedGameState.RGS_Initial;
        this.PlayersInGame = new List<KeyValuePair<PWConnectionBase, long>>();
      }

      public void SetCreated(PWServerInfoFull InFullInfo)
      {
        this.gameState = PWRankedManager.RankedGameState.RGS_Created;
        this.fullInfo = InFullInfo;
      }

      public void SetEnded() => this.gameState = PWRankedManager.RankedGameState.RGS_Ended;

      public void SetRegistered(PWServerInfoFull RegisterInfo, PWConnectionBase InConnection)
      {
        this.gameState = PWRankedManager.RankedGameState.RGS_Registered;
        this.GameConnection = InConnection;
      }

      public void SetDestroyed()
      {
        this.gameState = PWRankedManager.RankedGameState.RGS_Destroyed;
        this.EndedGamePlayerList = new List<long>(this.PlayersInGame.Count);
        foreach (KeyValuePair<PWConnectionBase, long> keyValuePair in this.PlayersInGame)
          this.EndedGamePlayerList.Add(keyValuePair.Value);
        this.PlayersInGame = (List<KeyValuePair<PWConnectionBase, long>>) null;
      }

      public IList<KeyValuePair<PWConnectionBase, long>> Players => (IList<KeyValuePair<PWConnectionBase, long>>) this.PlayersInGame;

      public PWRankedManager.RankedGameState GameState => this.gameState;

      public PWServerInfoReq StartInfo => this.startInfo;

      public PWServerInfoFull FullInfo => this.fullInfo;
    }
  }
}
