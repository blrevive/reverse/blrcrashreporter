﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Caches.CachedItem`1
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System;
using System.Runtime.InteropServices;

namespace PWAPICommon.Caches
{
  [ComVisible(true)]
  public class CachedItem<T>
  {
    private T CachedObject;
    private TimeSpan MaxCacheAge;
    private DateTime LastCacheTime;

    public CachedItem(TimeSpan Lifetime, T InObject)
    {
      this.MaxCacheAge = Lifetime;
      this.Item = InObject;
    }

    public void UpdateLifetime(TimeSpan Lifetime)
    {
      this.MaxCacheAge = Lifetime;
      this.LastCacheTime = PWServerBase.CurrentTime;
    }

    public bool Dirty
    {
      get => PWServerBase.CurrentTime - this.LastCacheTime > this.MaxCacheAge;
      set
      {
        if (value)
          this.LastCacheTime = PWServerBase.CurrentTime - new TimeSpan(365, 0, 0, 0);
        else
          this.LastCacheTime = PWServerBase.CurrentTime;
      }
    }

    public TimeSpan Age => PWServerBase.CurrentTime - this.LastCacheTime;

    public TimeSpan RemainingTime => this.LastCacheTime.AddSeconds(this.MaxCacheAge.TotalSeconds) - PWServerBase.CurrentTime;

    public T Item
    {
      get => this.Age > this.MaxCacheAge ? default (T) : this.CachedObject;
      set
      {
        this.CachedObject = value;
        this.LastCacheTime = PWServerBase.CurrentTime;
      }
    }

    public T ExpiredItem => this.CachedObject;

    public override bool Equals(object obj)
    {
      if (obj == null)
        return false;
      if (obj.GetType() == this.CachedObject.GetType())
        return obj.Equals((object) this.CachedObject);
      return obj.GetType() == this.GetType() ? this.CachedObject.Equals((object) (obj as CachedItem<T>).CachedObject) : base.Equals(obj);
    }

    public override int GetHashCode() => this.CachedObject.GetHashCode();
  }
}
