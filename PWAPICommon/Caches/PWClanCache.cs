﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Caches.PWClanCache
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Queries;
using PWAPICommon.Queries.Clan;
using PWAPICommon.Queries.Player;
using PWAPICommon.Resources;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace PWAPICommon.Caches
{
  [ComVisible(true)]
  public class PWClanCache : PWCacheBase
  {
    private int OutStandingMemberQueries;
    private List<PWClanInfo> ClanCache;
    private bool bWantsMembersCached;
    public ClanCacheDelegate UpdateCompleteCallback;

    public bool HasCachedMembers => this.bWantsMembersCached;

    public PWClanCache(PWServerBase InServer, PWConnectionBase InConnection)
      : base(InServer, InConnection)
    {
    }

    public PWClanCache(PWServerBase InServer, PWConnectionBase InConnection, bool InWantsMembers)
      : this(InServer, InConnection, InWantsMembers, (ClanCacheDelegate) null)
    {
    }

    public PWClanCache(
      PWServerBase InServer,
      PWConnectionBase InConnection,
      bool InWantsMembers,
      ClanCacheDelegate InCallback)
      : this(InServer, InConnection)
    {
      this.UpdateCompleteCallback = InCallback;
      this.bWantsMembersCached = InWantsMembers;
      this.OutStandingMemberQueries = 0;
    }

    protected override PWRequestBase CreateRefreshRequest()
    {
      this.Log("Clan Cache out of date, Refreshing...", ELoggingLevel.ELL_Informative);
      return (PWRequestBase) new PWClanQueryReq(this.LocalConnection);
    }

    public List<PWClanInfo> GetCachedItems()
    {
      this.ConditionalRefreshCache();
      return this.ClanCache;
    }

    public void AddItem(PWClanInfo NewItem)
    {
      this.AddItemNoQuery(NewItem);
      this.QueryClanMembers(NewItem);
    }

    public void AddItemNoQuery(PWClanInfo NewItem)
    {
      if (this.ClanCache == null)
        return;
      lock (this.ClanCache)
      {
        if (this.ClanCache.Contains(NewItem))
          return;
        this.ClanCache.Add(NewItem);
      }
    }

    public void RemoveItem(PWClanInfo NewItem)
    {
      if (this.ClanCache == null)
        return;
      lock (this.ClanCache)
      {
        if (!this.ClanCache.Contains(NewItem))
          return;
        this.ClanCache.Remove(NewItem);
      }
    }

    protected override bool ParseCacheRefreshResults(PWRequestBase Wrapper)
    {
      PWClanQueryReq pwClanQueryReq = Wrapper as PWClanQueryReq;
      if (this.ClanCache != null)
      {
        lock (this.ClanCache)
          this.ClanCache = pwClanQueryReq.Clans;
      }
      else
        this.ClanCache = pwClanQueryReq.Clans;
      if (this.bWantsMembersCached)
        this.QueryAllClanMembers();
      else
        this.UpdateComplete();
      return true;
    }

    private void QueryAllClanMembers()
    {
      this.OutStandingMemberQueries = 0;
      foreach (PWClanInfo ToQuery in this.ClanCache)
      {
        ++this.OutStandingMemberQueries;
        this.QueryClanMembers(ToQuery);
      }
    }

    public void QueryClanMembers(PWClanInfo ToQuery)
    {
      PWClanMembersQuery clanMembersQuery = new PWClanMembersQuery(this.LocalConnection);
      clanMembersQuery.ClanID = ToQuery.ClanItem.ClanID;
      clanMembersQuery.ServerResultProcessor = new ProcessDelegate(this.ProcessClanMembers);
      clanMembersQuery.SubmitServerQuery();
    }

    private bool ProcessClanMembers(PWRequestBase Wrapper)
    {
      PWClanMembersQuery MemberQuery = (PWClanMembersQuery) Wrapper;
      if (MemberQuery != null)
      {
        PWClanInfo pwClanInfo = this.ClanCache.Find((Predicate<PWClanInfo>) (x => x.ClanItem.ClanID == MemberQuery.ClanID));
        if (pwClanInfo != null)
        {
          pwClanInfo.Members = MemberQuery.GetMemberList();
          pwClanInfo.ClanItem.MemberCount = pwClanInfo.Members.Count;
          this.QueryMemberProfiles(pwClanInfo.Members);
          return true;
        }
      }
      return false;
    }

    public void QueryMemberProfiles(List<PWClanMemberInfo> MemberList)
    {
      PWSocialServer owningServer = this.OwningServer as PWSocialServer;
      PWQueryProfileBasicList profileBasicList = new PWQueryProfileBasicList(this.LocalConnection);
      profileBasicList.PlayerList = new List<PWPlayerInfo>();
      profileBasicList.ServerResultProcessor = new ProcessDelegate(this.ProfileQueryComplete);
      this.OwningServer.GetResource<PWPresenceCache<SocialStatus>>();
      foreach (PWClanMemberInfo member in MemberList)
      {
        if (owningServer != null)
        {
          SocialLoginData loginData = owningServer.GetLoginData(member.PlayerInfo.UserID);
          if (loginData != null)
            member.PlayerInfo = loginData.PlayerInfo;
        }
        profileBasicList.PlayerList.Add(member.PlayerInfo);
      }
      profileBasicList.SubmitServerQuery();
    }

    private bool ProfileQueryComplete(PWRequestBase ForRequest)
    {
      --this.OutStandingMemberQueries;
      if (this.OutStandingMemberQueries == 0)
        this.UpdateComplete();
      return true;
    }

    private void UpdateComplete()
    {
      if (this.UpdateCompleteCallback == null)
        return;
      this.UpdateCompleteCallback();
      this.UpdateCompleteCallback = (ClanCacheDelegate) null;
    }

    public void ForceUpdate(ClanCacheDelegate InCallback)
    {
      this.UpdateCompleteCallback = InCallback;
      this.MarkDirty();
      this.ConditionalRefreshCache();
    }

    public override Dictionary<string, string> CheckHealth()
    {
      Dictionary<string, string> dictionary = new Dictionary<string, string>();
      if (this.ClanCache != null)
        dictionary.Add("ClanCacheSize", this.ClanCache.Count.ToString());
      return dictionary;
    }

    public static bool HasClanPermissions(PWClanMemberInfo ToCheck)
    {
      if (ToCheck == null)
        return false;
      return (int) ToCheck.ClanPosition == (int) Convert.ToByte((object) EClanRank.CR_LEADER) || (int) ToCheck.ClanPosition == (int) Convert.ToByte((object) EClanRank.CR_OFFICER);
    }

    public bool ClanExistsbyTag(string ClanTag) => this.ClanCache.Exists((Predicate<PWClanInfo>) (TheItem => TheItem.ClanItem.ClanTag == ClanTag));

    public bool ClanExistsByName(string ClanName) => this.ClanCache.Exists((Predicate<PWClanInfo>) (TheItem => TheItem.ClanItem.ClanName == ClanName));

    public bool ClanExistsByOwnerID(long OwnerID) => this.ClanCache.Exists((Predicate<PWClanInfo>) (TheItem => TheItem.ClanItem.OwnerID == OwnerID));
  }
}
