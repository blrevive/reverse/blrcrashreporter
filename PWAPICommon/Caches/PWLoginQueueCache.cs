﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Caches.PWLoginQueueCache
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Config;
using PWAPICommon.Resources;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Timers;

namespace PWAPICommon.Caches
{
  [ComVisible(true)]
  public class PWLoginQueueCache : IPWResource
  {
    private List<CachedItem<string>> QueuedLogins;
    private List<CachedItem<string>> ClearedLogins;
    private Timer PurgeTimer;
    private PWServerBase OwningServer;
    private float AverageWaitTimeSeconds;

    public int TotalQueueSize => this.QueuedLogins.Count + this.ClearedLogins.Count;

    public float NewUserWaitTime => (float) (this.TotalQueueSize + 1) * this.AverageWaitTimeSeconds;

    public PWLoginQueueCache(PWServerBase InServer)
    {
      this.OwningServer = InServer;
      this.OwningServer.Log("Starting Login Queue Cache", (PWConnectionBase) null, false, ELoggingLevel.ELL_Informative);
      this.QueuedLogins = new List<CachedItem<string>>();
      this.ClearedLogins = new List<CachedItem<string>>();
      this.AverageWaitTimeSeconds = 1f;
      this.PurgeTimer = new Timer(15000.0);
      this.PurgeTimer.Elapsed += new ElapsedEventHandler(this.OnPurgeTimer);
      ConfigSettings.AddConfigChangedListener(new ConfigurationChangedFunc(this.ConfigChanged));
    }

    public Dictionary<string, string> CheckHealth() => new Dictionary<string, string>()
    {
      {
        "Queue:Count",
        this.QueuedLogins.Count.ToString()
      },
      {
        "QueueCleared:Count",
        this.ClearedLogins.Count.ToString()
      },
      {
        "QueueCleared:AvgWait",
        ((float) this.QueuedLogins.Count * this.AverageWaitTimeSeconds).ToString()
      }
    };

    public void StartResource() => this.PurgeTimer.Start();

    public void StopResource()
    {
      this.QueuedLogins.Clear();
      this.ClearedLogins.Clear();
      this.PurgeTimer.Stop();
    }

    public bool TryQueueUser(
      string AccountName,
      out int OutQueueTime,
      out int OutQueueSize,
      out int OutPositionInQueue)
    {
      this.OwningServer.Log("Account " + AccountName + " Checking Queue", (PWConnectionBase) null, false, ELoggingLevel.ELL_Informative);
      AccountName = AccountName.ToLower();
      int OutValue = 0;
      this.OwningServer.GetConfigValue<int>("queuetimeperplayerseconds", out OutValue, 15);
      TimeSpan Lifetime = new TimeSpan(0, 0, OutValue + 5);
      lock (this.ClearedLogins)
      {
        if (this.ClearedLogins.Find((Predicate<CachedItem<string>>) (x => x.ExpiredItem == AccountName)) != null)
        {
          this.OwningServer.Log("Account " + AccountName + " Cleared for Login", (PWConnectionBase) null, false, ELoggingLevel.ELL_Informative);
          OutQueueTime = 0;
          OutQueueSize = this.TotalQueueSize;
          OutPositionInQueue = 0;
          return true;
        }
      }
      lock (this.QueuedLogins)
      {
        CachedItem<string> cachedItem = this.QueuedLogins.Find((Predicate<CachedItem<string>>) (x => x.ExpiredItem == AccountName));
        if (cachedItem == null)
        {
          this.OwningServer.Log("Account " + AccountName + " Added to Queue", (PWConnectionBase) null, false, ELoggingLevel.ELL_Informative);
          cachedItem = new CachedItem<string>(Lifetime, AccountName);
          this.QueuedLogins.Add(cachedItem);
        }
        OutPositionInQueue = this.QueuedLogins.IndexOf(cachedItem) + 1;
        OutQueueTime = OutValue;
        OutQueueSize = this.TotalQueueSize;
        cachedItem.UpdateLifetime(Lifetime);
        this.OwningServer.Log("Account " + AccountName + " In Queue Position " + (object) OutPositionInQueue, (PWConnectionBase) null, false, ELoggingLevel.ELL_Informative);
        return false;
      }
    }

    public void UserLoggedIn(string AccountName)
    {
      this.OwningServer.Log("Account " + AccountName + " Logged in, removing from Queues", (PWConnectionBase) null, false, ELoggingLevel.ELL_Informative);
      lock (this.QueuedLogins)
        this.QueuedLogins.RemoveAll((Predicate<CachedItem<string>>) (x => x.ExpiredItem == AccountName));
      lock (this.ClearedLogins)
        this.ClearedLogins.RemoveAll((Predicate<CachedItem<string>>) (x => x.ExpiredItem == AccountName));
    }

    public void UserLoggedOut(string AccountName)
    {
      this.OwningServer.Log("Account " + AccountName + " Logged out, Clearning next user", (PWConnectionBase) null, false, ELoggingLevel.ELL_Informative);
      this.ClearUsersForLogin(1);
    }

    private void ConfigChanged(string Key, string OldValue, string NewValue)
    {
      if (!Key.Equals("LoginQueuePlayerThreshold", StringComparison.CurrentCultureIgnoreCase))
        return;
      int result1 = 0;
      int result2 = 0;
      int.TryParse(OldValue, out result1);
      int.TryParse(NewValue, out result2);
      int Count = result2 - result1;
      if (Count <= 0)
        return;
      this.OwningServer.Log("LoginQueuePlayerThreshold Increased, clearing " + (object) Count + " users to login immediately", (PWConnectionBase) null, false, ELoggingLevel.ELL_Informative);
      this.ClearUsersForLogin(Count);
    }

    private void ClearUserForLogin(string AccountName)
    {
      this.OwningServer.Log("Clearing Account " + AccountName + " for Login", (PWConnectionBase) null, false, ELoggingLevel.ELL_Informative);
      int OutValue = 0;
      this.OwningServer.GetConfigValue<int>("queueclearedplayertime", out OutValue, 120);
      TimeSpan Lifetime = new TimeSpan(0, 0, OutValue);
      lock (this.QueuedLogins)
        this.QueuedLogins.RemoveAll((Predicate<CachedItem<string>>) (x => x.ExpiredItem == AccountName));
      lock (this.ClearedLogins)
        this.ClearedLogins.Add(new CachedItem<string>(Lifetime, AccountName));
    }

    private void ClearUsersForLogin(int Count)
    {
      for (; Count > 0 && this.QueuedLogins.Count > 0; --Count)
        this.ClearUserForLogin(this.QueuedLogins[0].ExpiredItem);
    }

    private void OnPurgeTimer(object Source, ElapsedEventArgs args)
    {
      lock (this.ClearedLogins)
      {
        int Count = this.ClearedLogins.RemoveAll((Predicate<CachedItem<string>>) (x => x.Dirty));
        if (Count > 0)
          this.OwningServer.Log("LoginQueue Purged " + (object) Count + " Cleared Logins", (PWConnectionBase) null, false, ELoggingLevel.ELL_Informative);
        this.ClearUsersForLogin(Count);
      }
      lock (this.QueuedLogins)
      {
        int num = this.QueuedLogins.RemoveAll((Predicate<CachedItem<string>>) (x => x.Dirty));
        if (num <= 0)
          return;
        this.OwningServer.Log("LoginQueue Purged " + (object) num + " Queued Logins", (PWConnectionBase) null, false, ELoggingLevel.ELL_Informative);
      }
    }

    private string PrintQueueStats(int Position, int Size, int Time) => "Queue Position/Size: " + (object) Position + "/" + (object) Size + " - Wait Time: " + (object) Time;
  }
}
