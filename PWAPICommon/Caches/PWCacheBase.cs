﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Caches.PWCacheBase
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Queries;
using PWAPICommon.Resources;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace PWAPICommon.Caches
{
  [ComVisible(true)]
  public abstract class PWCacheBase : IPWResource
  {
    private DateTime CacheReadTime;
    private DateTime NextCacheReadTime;
    protected PWServerBase OwningServer;
    protected PWConnectionBase LocalConnection;
    private TimeSpan MaxCacheAge;
    private PWRequestBase RefreshCacheRequest;

    public TimeSpan Age => PWServerBase.CurrentTime - this.CacheReadTime;

    public bool IsDirty => PWServerBase.CurrentTime > this.NextCacheReadTime;

    public DateTime NextReadTime => this.NextCacheReadTime;

    protected TimeSpan NextReadTimeSpan
    {
      set => this.NextCacheReadTime = this.CacheReadTime + value;
    }

    public PWCacheBase(PWServerBase InServer, PWConnectionBase InConnection)
      : this(InServer, InConnection, new TimeSpan(1, 0, 0))
    {
    }

    public PWCacheBase(PWServerBase InServer, PWConnectionBase InConnection, TimeSpan InCacheAge)
    {
      this.OwningServer = InServer;
      this.LocalConnection = InConnection;
      this.MaxCacheAge = InCacheAge;
    }

    protected void ConditionalRefreshCache()
    {
      if (!this.IsDirty || this.RefreshCacheRequest != null)
        return;
      this.RefreshCacheRequest = this.CreateRefreshRequest();
      if (this.RefreshCacheRequest == null)
        return;
      this.RefreshCacheRequest.ServerResultProcessor = new ProcessDelegate(this.RefreshCacheCompleted);
      this.RefreshCacheRequest.Connection = this.LocalConnection;
      this.RefreshCacheRequest.SubmitServerQuery();
      this.CacheReadTime = PWServerBase.CurrentTime;
      this.NextCacheReadTime = this.CacheReadTime + this.MaxCacheAge;
    }

    protected abstract PWRequestBase CreateRefreshRequest();

    protected abstract bool ParseCacheRefreshResults(PWRequestBase Wrapper);

    protected void Log(string LogString, ELoggingLevel LoggingLevel) => this.OwningServer.Log(LogString, (PWConnectionBase) null, false, LoggingLevel);

    public virtual void MarkDirty()
    {
      this.Log(this.GetType().ToString() + " Cache Marked Dirty", ELoggingLevel.ELL_Informative);
      this.NextCacheReadTime = PWServerBase.CurrentTime - new TimeSpan(1, 0, 0);
    }

    private bool RefreshCacheCompleted(PWRequestBase Wrapper)
    {
      this.CacheReadTime = PWServerBase.CurrentTime;
      this.NextCacheReadTime = this.CacheReadTime + this.MaxCacheAge;
      this.RefreshCacheRequest = (PWRequestBase) null;
      return this.ParseCacheRefreshResults(Wrapper);
    }

    public virtual void StartResource() => this.ConditionalRefreshCache();

    public virtual void StopResource()
    {
    }

    public abstract Dictionary<string, string> CheckHealth();
  }
}
