﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Config.ConfigSettings
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Caches;
using PWAPICommon.Common;
using PWAPICommon.Servers;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;

namespace PWAPICommon.Config
{
  [ComVisible(true)]
  public static class ConfigSettings
  {
    private static ConcurrentDictionary<string, string> Lookup;
    private static Dictionary<string, ConfigData> ReadConfigs;
    private static ConfigurationChangedFunc ConfigChangedCallbacks;

    public static ConcurrentDictionary<string, string> Values => ConfigSettings.Lookup;

    public static List<HttpData> GetWebData()
    {
      List<HttpData> httpDataList = new List<HttpData>();
      HttpData httpData;
      httpData.SectionName = "Configuration";
      httpData.SectionData = ConfigSettings.Lookup.PrintHttpTable<string, string>();
      httpDataList.Add(httpData);
      return httpDataList;
    }

    public static void SetKey(string InKey, string InValue)
    {
      string lower = InKey.ToLower();
      string OldValue;
      if (ConfigSettings.Lookup.TryGetValue(lower, out OldValue))
      {
        if (!(OldValue != InValue))
          return;
        ConfigSettings.Lookup[lower] = InValue;
        ConfigSettings.ConfigChanged(lower, OldValue, InValue);
      }
      else
      {
        ConfigSettings.Lookup.TryAdd(lower, InValue);
        ConfigSettings.ConfigChanged(lower, "", InValue);
      }
    }

    private static void ConfigChanged(string Key, string OldValue, string NewValue)
    {
      if (ConfigSettings.ConfigChangedCallbacks == null)
        return;
      ConfigSettings.ConfigChangedCallbacks(Key, OldValue, NewValue);
    }

    public static void AddConfigChangedListener(ConfigurationChangedFunc InDelegate) => ConfigSettings.ConfigChangedCallbacks += InDelegate;

    public static void RemoveConfigChangedListener(ConfigurationChangedFunc InDelegate) => ConfigSettings.ConfigChangedCallbacks -= InDelegate;

    public static void Reload()
    {
      foreach (string Path in ConfigSettings.ReadConfigs.Keys.ToList<string>())
        ConfigSettings.Load(Path);
    }

    public static bool Load(string Path)
    {
      if (ConfigSettings.ReadConfigs == null)
        ConfigSettings.ReadConfigs = new Dictionary<string, ConfigData>();
      if (ConfigSettings.Lookup == null)
        ConfigSettings.Lookup = new ConcurrentDictionary<string, string>();
      try
      {
        FileStream fileStream = new FileStream(Path, FileMode.Open, FileAccess.Read);
        if (fileStream != null)
        {
          byte[] numArray = new byte[fileStream.Length];
          fileStream.Read(numArray, 0, numArray.Length);
          MessageSerializer messageSerializer = new MessageSerializer();
          string Data = messageSerializer.DeserializeToString(numArray);
          ConfigData configData = messageSerializer.DeSerializeStringToObject<ConfigData>(Data);
          if (configData != null && configData.Settings != null && configData.Settings.Count > 0)
          {
            ConfigSettings.ReadConfigs[Path] = configData;
            foreach (KVP<string, string> setting in configData.Settings)
              ConfigSettings.SetKey(setting.key, setting.value);
          }
          fileStream.Close();
          return true;
        }
      }
      catch (Exception ex)
      {
      }
      return false;
    }

    public static bool GetSetting(string Key, ref string OutValue) => ConfigSettings.Lookup != null && ConfigSettings.Lookup.TryGetValue(Key.ToLower(), out OutValue);

    public static bool GetSetting(string Key, ref string OutValue, string DefaultValue)
    {
      if (ConfigSettings.GetSetting(Key, ref OutValue))
        return true;
      OutValue = DefaultValue;
      return false;
    }
  }
}
