﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Config.KVPDictionary
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace PWAPICommon.Config
{
  [ComVisible(true)]
  public class KVPDictionary : Dictionary<string, string>
  {
    public override string ToString()
    {
      string str = "";
      foreach (KeyValuePair<string, string> keyValuePair in (Dictionary<string, string>) this)
        str = str + keyValuePair.Key.ToString() + "=" + keyValuePair.Value.ToString() + (object) ',';
      if (str.EndsWith(","))
        str.Remove(str.Length - 1);
      return str;
    }

    public string GetValue(string Key, string Default)
    {
      string str = Default;
      if (!this.TryGetValue(Key, out str))
        str = Default;
      return str;
    }

    public int GetValue(string Key, int Default)
    {
      int result = Default;
      string s = Default.ToString();
      this.TryGetValue(Key, out s);
      if (!int.TryParse(s, out result))
        result = Default;
      return result;
    }

    public void FromString(string Input)
    {
      string str1 = Input;
      char[] chArray1 = new char[1]{ ',' };
      foreach (string str2 in str1.Split(chArray1))
      {
        char[] chArray2 = new char[1]{ '=' };
        string[] strArray = str2.Split(chArray2);
        if (strArray.Length == 2)
          this.Add(strArray[0], strArray[1]);
      }
    }
  }
}
