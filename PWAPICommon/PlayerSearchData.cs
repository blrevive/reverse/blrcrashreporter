﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.PlayerSearchData
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System.Runtime.InteropServices;

namespace PWAPICommon
{
  [ComVisible(true)]
  public struct PlayerSearchData
  {
    public string UserName;
    public int Experience;

    public PlayerSearchData(string InName, int InExp)
    {
      this.UserName = InName;
      this.Experience = InExp;
    }

    public override string ToString() => this.UserName;
  }
}
