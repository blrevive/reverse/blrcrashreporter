﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.PWServerBase
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using Microsoft.Win32;
using PWAPICommon.Caches;
using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Queries;
using PWAPICommon.Queries.General;
using PWAPICommon.Resources;
using PWAPICommon.Servers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace PWAPICommon
{
  [ComVisible(true)]
  public class PWServerBase : IHttpResource
  {
    private int GameID;
    private int ServerID;
    private PWServerApp OwningApp;
    private bool bStarted;
    protected Socket MainSocket;
    protected int TargetPort;
    private string Name;
    private string RegionName;
    private PWMachineID MachineId;
    private int ConnectionPoolSize;
    private bool bCheckingHealth;
    private PWLockFreeStack<SocketAsyncEventArgs> ConnectionPool;
    private List<SocketAsyncEventArgs> CurrentConnections;
    private Dictionary<Type, IPWResource> ServerResources;
    private Dictionary<string, string> ServerHealthState;
    private Dictionary<EMessageType, Type> MessageMap;
    private System.Timers.Timer RefreshTimer;
    private System.Timers.Timer HealthMonitorTimer;
    private System.Timers.Timer TimeChangeTimer;
    private ELoggingLevel LogLevel;
    private PWAsyncTask ConnectionPoolGrowthTask;
    private bool bFrozen;
    protected PWServerBase.RefreshConnectionsFunc RefreshConnectionFunc;
    protected PWServerBase.AddLogMessageCallback LogCallbackFunc;
    private static readonly string FreeToken = "Free";
    private static readonly string PreConnectionToken = "Popped";

    public bool Frozen
    {
      get => this.bFrozen;
      set => this.SetFrozen(value);
    }

    public static DateTime CurrentTime => DateTime.UtcNow;

    public static long AdminUserID => -666;

    public static long GlobalUserID => -999;

    public static long EmptyUserID => 0;

    public ELoggingLevel LoggingLevel
    {
      get => this.LogLevel;
      set => this.LogLevel = value;
    }

    public static string AdminName => "Admin";

    public bool TimeoutsDisabled => this.TimeChangeTimer.Enabled;

    public static string PurchaseSenderName => "Marketplace";

    public static string Version => "v0.982";

    public int PWGameID => this.GameID;

    public int PWServerID => this.ServerID;

    public Dictionary<EMessageType, Type> MessagesAccepted => this.MessageMap;

    public string ServerName => this.Name;

    public string ServerRegionName => this.RegionName;

    public bool IsRunning => this.MainSocket != null;

    public int NumConnections => this.ConnectionPoolSize - this.ConnectionPool.NumElements;

    public int TotalConnections => this.ConnectionPoolSize;

    public PWMachineID HardwareID => this.MachineId;

    public Dictionary<string, string> ServerHealth => this.ServerHealthState;

    public string HealthStatus
    {
      get
      {
        lock (this.ServerHealthState)
        {
          string str = "";
          foreach (string key in this.ServerHealthState.Keys)
            str = str + this.Name + " " + key.ToString() + " = " + this.ServerHealthState[key] + Environment.NewLine;
          return str;
        }
      }
    }

    public bool Started => this.bStarted;

    public PWServerApp App => this.OwningApp;

    public int Port
    {
      get => this.TargetPort;
      set => this.TargetPort = value;
    }

    public PWRequestBase CreateRequestObject(
      EMessageType InMessageType,
      PWConnectionBase InConnection)
    {
      try
      {
        return (PWRequestBase) Activator.CreateInstance(this.MessageMap[InMessageType], (object) InConnection);
      }
      catch (Exception ex)
      {
        this.Log("Activator Exception: " + ex.ToString(), InConnection, false, ELoggingLevel.ELL_Errors);
        return (PWRequestBase) null;
      }
    }

    public void AddRefreshConnectionsFunc(PWServerBase.RefreshConnectionsFunc InCallback) => this.RefreshConnectionFunc += InCallback;

    public void AddLogCallback(PWServerBase.AddLogMessageCallback InCallback) => this.LogCallbackFunc += InCallback;

    public void RemoveLogCallback(PWServerBase.AddLogMessageCallback InCallback) => this.LogCallbackFunc -= InCallback;

    public void RefreshConnectionList()
    {
      if (this.RefreshConnectionFunc == null)
        return;
      this.RefreshConnectionFunc(this);
    }

    private string GetTimestamp(DateTime value) => value.ToString("yyyy-MM-dd HH:mm:ss:ff");

    private string GetLogLevelName(ELoggingLevel InLogLevel)
    {
      switch (InLogLevel)
      {
        case ELoggingLevel.ELL_Critical:
          return "CRI";
        case ELoggingLevel.ELL_Errors:
          return "ERR";
        case ELoggingLevel.ELL_Warnings:
          return "WRN";
        case ELoggingLevel.ELL_Informative:
          return "INF";
        case ELoggingLevel.ELL_Verbose:
          return "VRB";
        default:
          throw new Exception("Invalid Log level specified");
      }
    }

    public static EventLogEntryType GetEventLogType(ELoggingLevel InLogLevel)
    {
      switch (InLogLevel)
      {
        case ELoggingLevel.ELL_Critical:
          return EventLogEntryType.Error;
        case ELoggingLevel.ELL_Errors:
          return EventLogEntryType.Error;
        case ELoggingLevel.ELL_Warnings:
          return EventLogEntryType.Warning;
        case ELoggingLevel.ELL_Informative:
          return EventLogEntryType.Information;
        case ELoggingLevel.ELL_Verbose:
          return EventLogEntryType.Information;
        default:
          return EventLogEntryType.Information;
      }
    }

    public void Log(
      string InLogText,
      PWConnectionBase InConnection,
      bool bLogToRemoteServer,
      ELoggingLevel MessageLogLevel)
    {
      if ((this.LogCallbackFunc == null || this.LogLevel < MessageLogLevel) && !bLogToRemoteServer)
        return;
      if (InLogText.EndsWith("\0"))
        InLogText = InLogText.Remove(InLogText.Length - 1, 1);
      string InLogText1;
      if (InConnection != null)
        InLogText1 = "[" + this.GetTimestamp(PWServerBase.CurrentTime) + "] [" + this.GetLogLevelName(MessageLogLevel) + "] [" + this.Name + "]  [" + InConnection.ToString() + "] " + InLogText;
      else
        InLogText1 = "[" + this.GetTimestamp(PWServerBase.CurrentTime) + "] [" + this.GetLogLevelName(MessageLogLevel) + "] [" + this.Name + "] " + InLogText;
      if (this.LogCallbackFunc != null && this.LogLevel >= MessageLogLevel)
        this.LogCallbackFunc(InLogText1, MessageLogLevel);
      if (!bLogToRemoteServer)
        return;
      this.LogRemote(InLogText1);
    }

    protected virtual void LogRemote(string InLogText)
    {
    }

    public PWServerBase(
      int InConnectionPoolSize,
      string InServerName,
      string InRegionName,
      int InGameID,
      int InServerID,
      ELoggingLevel InLogLevel,
      PWServerApp InApp)
    {
      this.bStarted = false;
      this.OwningApp = InApp;
      this.Name = InServerName;
      this.RegionName = InRegionName;
      this.ConnectionPoolSize = 0;
      this.CurrentConnections = (List<SocketAsyncEventArgs>) null;
      this.ConnectionPool = new PWLockFreeStack<SocketAsyncEventArgs>();
      this.MessageMap = new Dictionary<EMessageType, Type>();
      this.ServerResources = new Dictionary<Type, IPWResource>();
      this.MachineId = this.GenerateMachineID();
      this.GameID = InGameID;
      this.ServerID = InServerID;
      this.LogLevel = InLogLevel;
      this.RefreshTimer = new System.Timers.Timer(1000.0);
      this.RefreshTimer.Elapsed += new ElapsedEventHandler(this.OnRefreshTimer);
      this.ServerHealthState = new Dictionary<string, string>();
      this.HealthMonitorTimer = new System.Timers.Timer(15000.0);
      this.HealthMonitorTimer.Elapsed += new ElapsedEventHandler(this.OnCheckHealth);
      this.HealthMonitorTimer.Start();
      this.TimeChangeTimer = new System.Timers.Timer(120000.0);
      this.TimeChangeTimer.Elapsed += new ElapsedEventHandler(this.OnTimeChangeEnded);
      SystemEvents.TimeChanged += new EventHandler(this.SystemEvents_TimeChanged);
      this.GrowConnectionPool(InConnectionPoolSize);
      this.ConnectionPoolGrowthTask = new PWAsyncTask((Action) (() =>
      {
        int OutValue = 0;
        this.GetConfigValue<int>("ConnectionPoolGrowthSize", out OutValue, 100);
        this.Log("Growing Connection Pool by " + (object) OutValue, (PWConnectionBase) null, false, ELoggingLevel.ELL_Informative);
        this.GrowConnectionPool(OutValue);
      }));
      this.ConnectionPoolGrowthTask.BlockSubsequent = true;
    }

    public virtual bool SendQuery(PWRequestBase InRequest) => false;

    public virtual bool CancelQuery(PWRequestBase InRequest) => false;

    public bool SendExternalQuery(PWRequestBase InRequest, string InRegionName, int TimeoutSeconds) => this.SendExternalQuery(InRequest, InRegionName, TimeoutSeconds, false);

    public virtual bool SendExternalQuery(
      PWRequestBase InRequest,
      string InRegionName,
      int TimeoutSeconds,
      bool bWaitForResponse)
    {
      return false;
    }

    public bool SendInternalQuery(
      PWRequestBase InRequest,
      EServerType InServerType,
      int TimeoutSeconds)
    {
      return this.SendInternalQuery(InRequest, InServerType, TimeoutSeconds, false);
    }

    public virtual bool SendInternalQuery(
      PWRequestBase InRequest,
      EServerType InServerType,
      int TimeoutSeconds,
      bool bWaitForResponse)
    {
      return false;
    }

    private void OnTimeChangeEnded(object sender, ElapsedEventArgs e)
    {
      if (!this.TimeChangeTimer.Enabled)
        return;
      this.Log("Time Shift Freeze Ended.", (PWConnectionBase) null, false, ELoggingLevel.ELL_Warnings);
      this.TimeChangeTimer.Stop();
      this.TimeChangeTimer.Enabled = false;
    }

    private void SystemEvents_TimeChanged(object sender, EventArgs e)
    {
      if (this.TimeChangeTimer.Enabled)
        return;
      this.Log("Detected Time Shift, Freezing Time-outs for 2 minutes", (PWConnectionBase) null, false, ELoggingLevel.ELL_Warnings);
      this.TimeChangeTimer.Enabled = true;
      this.TimeChangeTimer.Start();
    }

    public void AddConnectionList() => this.CurrentConnections = new List<SocketAsyncEventArgs>(this.ConnectionPoolSize);

    public void RemoveConnectionList() => this.CurrentConnections = (List<SocketAsyncEventArgs>) null;

    private void OnRefreshTimer(object Source, ElapsedEventArgs args)
    {
      if (!this.Started)
        return;
      this.RefreshConnectionList();
    }

    protected void SetHealthString(string key, string value)
    {
      lock (this.ServerHealthState)
        this.ServerHealthState[key] = value;
    }

    protected virtual void OnCheckHealth(object Source, ElapsedEventArgs args)
    {
      if (this.bCheckingHealth)
        return;
      this.bCheckingHealth = true;
      Stopwatch stopwatch = new Stopwatch();
      stopwatch.Start();
      this.SetHealthString("Server", this.bStarted ? "RUNNING" : "STOPPED");
      this.SetHealthString("Socket", this.MainSocket == null ? "NULL" : "OK");
      this.SetHealthString("Socket:Connections", this.NumConnections.ToString());
      if (this.OwningApp != null)
      {
        this.SetHealthString("App:CPU", this.OwningApp.CPUUsagePercent.ToString("00.0"));
        this.SetHealthString("App:RAM", this.OwningApp.RAMUsagePercent.ToString("00.0"));
        this.SetHealthString("App:DeadThreads", this.OwningApp.DeadThreadCount.ToString());
        this.SetHealthString("App:Version", this.OwningApp.AssemblyString);
        this.SetHealthString("App:CheckHealthMs", "0");
      }
      if (this.bStarted && this.MainSocket == null)
        this.Log("Socket is null!", (PWConnectionBase) null, false, ELoggingLevel.ELL_Critical);
      foreach (object obj in this.ServerResources.Values)
      {
        if (obj is IPWResource pwResource)
        {
          foreach (KeyValuePair<string, string> keyValuePair in pwResource.CheckHealth())
            this.SetHealthString(keyValuePair.Key, keyValuePair.Value);
        }
      }
      stopwatch.Stop();
      this.SetHealthString("App:CheckCostMs", stopwatch.ElapsedMilliseconds.ToString());
      this.bCheckingHealth = false;
    }

    public List<HttpData> GetWebData()
    {
      List<HttpData> httpDataList = new List<HttpData>();
      if (this.OwningApp != null)
        httpDataList.AddRange((IEnumerable<HttpData>) this.OwningApp.GetWebData());
      foreach (object obj in this.ServerResources.Values)
      {
        if (obj is IHttpResource httpResource)
          httpDataList.AddRange((IEnumerable<HttpData>) httpResource.GetWebData());
      }
      HttpData httpData;
      httpData.SectionName = "Server Health";
      httpData.SectionData = this.ServerHealth.PrintHttpTable<string, string>();
      httpDataList.Add(httpData);
      return httpDataList;
    }

    public bool IsValidMessageType(EMessageType InMessageType) => InMessageType != EMessageType.EMT_Invalid && this.MessageMap.ContainsKey(InMessageType);

    public void AddMessageMap(EMessageType InMessageType, Type NewRequestHandler)
    {
      string InLogText = "Unable to Map Message Type '" + InMessageType.ToString() + "'";
      if (InMessageType == EMessageType.EMT_Invalid)
        this.Log(InLogText, (PWConnectionBase) null, false, ELoggingLevel.ELL_Errors);
      else if (NewRequestHandler == (Type) null)
        this.Log(InLogText + ": Invalid Request Handler", (PWConnectionBase) null, false, ELoggingLevel.ELL_Errors);
      else if (this.MessageMap.ContainsKey(InMessageType))
        this.Log(InLogText + ": Message Type Already Mapped to a Request Handler", (PWConnectionBase) null, false, ELoggingLevel.ELL_Verbose);
      else
        this.MessageMap.Add(InMessageType, NewRequestHandler);
    }

    public string[] GetConnectionList()
    {
      if (this.CurrentConnections == null)
        return (string[]) null;
      string[] strArray = new string[this.CurrentConnections.Count];
      lock (this.CurrentConnections)
      {
        for (int index = 0; index < this.CurrentConnections.Count; ++index)
        {
          SocketAsyncEventArgs currentConnection = this.CurrentConnections[index];
          strArray[index] = currentConnection == null ? "null" : (!(currentConnection.UserToken is PWConnectionBase userToken) ? "Utility" : userToken.ToString());
        }
      }
      return strArray;
    }

    public object AddResource(string InResourceName, IPWResource InObject)
    {
      if (this.ServerResources == null)
        this.ServerResources = new Dictionary<Type, IPWResource>();
      if (this.ServerResources.ContainsKey(InObject.GetType()))
        this.Log("Resource already exists, ignoring add of" + InResourceName, (PWConnectionBase) null, false, ELoggingLevel.ELL_Warnings);
      else
        this.ServerResources.Add(InObject.GetType(), InObject);
      return (object) InObject;
    }

    public T GetResource<T>() => this.ServerResources.ContainsKey(typeof (T)) ? (T) this.ServerResources[typeof (T)] : default (T);

    public IEnumerable<T> GetResources<T>() => this.ServerResources.Values.OfType<T>() ?? (IEnumerable<T>) new List<T>();

    public IPAddress GetIPAddressFromString(string InAddress)
    {
      try
      {
        return PWServerBase.StaticGetIPAddressFromString(InAddress);
      }
      catch (Exception ex)
      {
        this.Log("Failed to resolve DNS for host: " + InAddress + " Exception:" + ex.ToString(), (PWConnectionBase) null, false, ELoggingLevel.ELL_Errors);
        return (IPAddress) null;
      }
    }

    public static IPAddress StaticGetIPAddressFromString(string InAddress)
    {
      IPAddress address = IPAddress.Loopback;
      if (!IPAddress.TryParse(InAddress, out address))
        address = new List<IPAddress>((IEnumerable<IPAddress>) Dns.GetHostAddresses(InAddress)).Find((Predicate<IPAddress>) (x => x.AddressFamily == AddressFamily.InterNetwork));
      return address;
    }

    public virtual void StartServer()
    {
      try
      {
        this.Log(this.Name + " Server starting on Tool Version " + PWServerBase.Version.ToString() + " [" + this.RegionName + "] ", (PWConnectionBase) null, false, ELoggingLevel.ELL_Informative);
        this.bStarted = true;
        this.CreateMainSocket();
        this.RefreshTimer.Start();
        Parallel.ForEach<IPWResource>((IEnumerable<IPWResource>) this.ServerResources.Values, (Action<IPWResource>) (x =>
        {
          try
          {
            x.StartResource();
          }
          catch (Exception ex)
          {
            this.Log("Resource " + (object) x + " failed to start! Exception: " + ex.ToString(), (PWConnectionBase) null, false, ELoggingLevel.ELL_Critical);
          }
        }));
        this.Log(this.Name + " server started listening on port " + (object) this.TargetPort, (PWConnectionBase) null, false, ELoggingLevel.ELL_Informative);
      }
      catch (Exception ex)
      {
        this.Log(this.Name + " server failed to start with exception: " + ex.ToString(), (PWConnectionBase) null, false, ELoggingLevel.ELL_Critical);
      }
    }

    public virtual void StopServer(string Reason)
    {
      try
      {
        this.Log(this.Name + " stopping for reason: " + Reason, (PWConnectionBase) null, false, ELoggingLevel.ELL_Informative);
        this.bStarted = false;
        this.RefreshTimer.Stop();
        if (this.CurrentConnections != null)
        {
          lock (this.CurrentConnections)
          {
            while (this.CurrentConnections.Count > 0)
            {
              SocketAsyncEventArgs currentConnection = this.CurrentConnections[0];
              this.DisconnectClient(currentConnection.UserToken as PWConnectionBase);
              this.FreeConnection(currentConnection);
            }
          }
        }
        Parallel.ForEach<IPWResource>((IEnumerable<IPWResource>) this.ServerResources.Values, (Action<IPWResource>) (x =>
        {
          try
          {
            x.StopResource();
          }
          catch (Exception ex)
          {
            this.Log("Resource " + (object) x + " failed to stop! Exception: " + ex.ToString(), (PWConnectionBase) null, false, ELoggingLevel.ELL_Critical);
          }
        }));
        this.DestroyMainSocket();
        this.Log(this.Name + " stopped.", (PWConnectionBase) null, false, ELoggingLevel.ELL_Informative);
      }
      catch (Exception ex)
      {
        this.Log(this.Name + " server failed to stop with exception: " + ex.ToString(), (PWConnectionBase) null, false, ELoggingLevel.ELL_Critical);
      }
    }

    protected virtual void CreateMainSocket()
    {
      try
      {
        this.MainSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        IPEndPoint ipEndPoint = new IPEndPoint(IPAddress.Any, this.TargetPort);
        this.MainSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Linger, (object) new LingerOption(false, 0));
        this.MainSocket.Bind((EndPoint) ipEndPoint);
        this.Log("Socket Bound to Port " + (object) this.Port, (PWConnectionBase) null, false, ELoggingLevel.ELL_Errors);
      }
      catch (Exception ex)
      {
        this.Log("Create Main Socket Exception: " + ex.ToString(), (PWConnectionBase) null, false, ELoggingLevel.ELL_Critical);
      }
    }

    protected virtual void DestroyMainSocket()
    {
      if (this.MainSocket == null)
        return;
      lock (this.MainSocket)
      {
        this.MainSocket.Close(0);
        this.MainSocket = (Socket) null;
        this.Log("Socket Destroyed", (PWConnectionBase) null, false, ELoggingLevel.ELL_Errors);
      }
    }

    protected void RestartServer()
    {
      this.Log("Attempting to restart listening on port " + (object) this.TargetPort, (PWConnectionBase) null, false, ELoggingLevel.ELL_Errors);
      this.StopServer("Restarting");
      this.StartServer();
    }

    protected virtual void OnIOCompleted(object sender, SocketAsyncEventArgs e)
    {
      this.Log("OnIoCompleted " + (object) e.LastOperation, (PWConnectionBase) null, false, ELoggingLevel.ELL_Verbose);
      PWConnectionTCPBase userToken = e.UserToken as PWConnectionTCPBase;
      switch (e.LastOperation)
      {
        case SocketAsyncOperation.Receive:
          userToken.FinishReceive(e);
          break;
        case SocketAsyncOperation.Send:
          userToken.FinishSend(e);
          this.FinishSend(e);
          break;
        default:
          throw new ArgumentException("The last operation completed on the socket was not a receive or send");
      }
    }

    public PWMachineID GenerateMachineID()
    {
      PWMachineID pwMachineId = new PWMachineID();
      RegistryKey inKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Cryptography");
      if (inKey != null)
      {
        string regKey32 = inKey.GetRegKey32("MachineGuid");
        string regKey64 = inKey.GetRegKey64("MachineGuid");
        pwMachineId.MachineGuid = !(regKey32 != "") ? regKey64.ToUpper() : regKey32.ToUpper();
      }
      IPGlobalProperties.GetIPGlobalProperties();
      foreach (NetworkInterface networkInterface in NetworkInterface.GetAllNetworkInterfaces())
      {
        if (networkInterface.NetworkInterfaceType == NetworkInterfaceType.Ethernet)
        {
          PhysicalAddress physicalAddress = networkInterface.GetPhysicalAddress();
          pwMachineId.Nics.Add(physicalAddress.ToString().ToUpper());
        }
      }
      return pwMachineId;
    }

    protected virtual void SetFrozen(bool bNewValue) => this.bFrozen = bNewValue;

    public SocketAsyncEventArgs GetConnection()
    {
      SocketAsyncEventArgs socketAsyncEventArgs = this.ConnectionPool.Pop();
label_4:
      while (socketAsyncEventArgs == null)
      {
        this.ConnectionPoolGrowthTask.Start();
        while (true)
        {
          if (socketAsyncEventArgs == null && this.ConnectionPoolGrowthTask.Running)
          {
            Thread.Sleep(5);
            socketAsyncEventArgs = this.ConnectionPool.Pop();
          }
          else
            goto label_4;
        }
      }
      if (this.CurrentConnections != null)
      {
        lock (this.CurrentConnections)
          this.CurrentConnections.Add(socketAsyncEventArgs);
      }
      if (socketAsyncEventArgs.UserToken != null && !socketAsyncEventArgs.UserToken.Equals((object) PWServerBase.FreeToken))
        this.Log("Found Non-Freed Args:" + socketAsyncEventArgs.UserToken.ToString(), (PWConnectionBase) null, false, ELoggingLevel.ELL_Errors);
      socketAsyncEventArgs.UserToken = (object) PWServerBase.PreConnectionToken;
      return socketAsyncEventArgs;
    }

    protected void GrowConnectionPool(int Amount)
    {
      for (int index = 0; index < Amount; ++index)
      {
        SocketAsyncEventArgs socketAsyncEventArgs = new SocketAsyncEventArgs();
        socketAsyncEventArgs.UserToken = (object) PWServerBase.FreeToken;
        socketAsyncEventArgs.Completed += new EventHandler<SocketAsyncEventArgs>(this.OnIOCompleted);
        ++this.ConnectionPoolSize;
        this.ConnectionPool.Push(socketAsyncEventArgs);
      }
    }

    protected void FreeConnection(SocketAsyncEventArgs InArgs)
    {
      if (InArgs == null)
        this.Log("FreeConnection trying to free NULL!", (PWConnectionBase) null, false, ELoggingLevel.ELL_Errors);
      else if (InArgs.UserToken != null && InArgs.UserToken.Equals((object) PWServerBase.FreeToken))
      {
        this.Log("Found Double-freed Args, ignoring!", (PWConnectionBase) null, false, ELoggingLevel.ELL_Errors);
      }
      else
      {
        InArgs.UserToken = (object) PWServerBase.FreeToken;
        InArgs.AcceptSocket = (Socket) null;
        InArgs.RemoteEndPoint = (EndPoint) null;
        if (this.CurrentConnections != null)
        {
          lock (this.CurrentConnections)
            this.CurrentConnections.Remove(InArgs);
        }
        this.ConnectionPool.Push(InArgs);
      }
    }

    protected virtual void FinishSend(SocketAsyncEventArgs InArgs)
    {
      InArgs.SetBuffer(new byte[1], 0, 1);
      this.FreeConnection(InArgs);
    }

    public virtual void StartReceive(SocketAsyncEventArgs ReceiveArgs)
    {
      if (!(ReceiveArgs.UserToken is PWConnectionTCPBase userToken))
      {
        this.Log("StartReceive: Null connection to receive on", (PWConnectionBase) null, false, ELoggingLevel.ELL_Critical);
        this.FreeConnection(ReceiveArgs);
      }
      else
      {
        try
        {
          switch (userToken.StartReceive(ReceiveArgs))
          {
            case -1:
              this.DisconnectClient((PWConnectionBase) userToken);
              this.FreeConnection(ReceiveArgs);
              break;
            case 0:
              userToken.FinishReceive(ReceiveArgs);
              break;
          }
        }
        catch (ObjectDisposedException ex)
        {
          this.Log("Tried to receive on a disposed socket, aborting this connection! Exception: " + ex.ToString(), (PWConnectionBase) null, false, ELoggingLevel.ELL_Errors);
          this.DisconnectClient((PWConnectionBase) userToken);
          this.FreeConnection(ReceiveArgs);
        }
      }
    }

    public void ProcessError(SocketAsyncEventArgs e)
    {
      PWConnectionBase userToken1 = e.UserToken as PWConnectionBase;
      if (e.SocketError != SocketError.Success)
        this.Log("Disconnected with error " + e.SocketError.ToString(), userToken1, false, ELoggingLevel.ELL_Informative);
      else
        this.Log("Disconnected Normally", userToken1, false, ELoggingLevel.ELL_Verbose);
      if (e.UserToken is PWConnectionBase userToken2)
        this.DisconnectClient(userToken2);
      this.FreeConnection(e);
    }

    public bool GetConfigValue<T>(string KeyName, out T OutValue)
    {
      OutValue = default (T);
      return PWServerApp.GetConfigValue<T>(KeyName, out OutValue, this.LogCallbackFunc);
    }

    public bool GetConfigValue<T>(string KeyName, out T OutValue, T DefaultValue)
    {
      if (this.GetConfigValue<T>(KeyName, out OutValue))
        return true;
      OutValue = DefaultValue;
      return false;
    }

    public virtual void ApplyUserIdentity(PWConnectionBase Connection, long UserID) => this.ApplyUserIdentity<PWPresenceInfo>(Connection, UserID, new PWPresenceInfo());

    public virtual void ApplyUserIdentity<T>(PWConnectionBase Connection, long UserID, T Presence) where T : PWPresenceInfo
    {
      if (Connection == null || UserID == PWServerBase.EmptyUserID)
        return;
      Connection.UserID = UserID;
      Connection.Name = Connection.UserID.ToString();
      foreach (IPWNotificationCache resource in this.GetResources<IPWNotificationCache>())
        resource.NotifyConnected(Connection);
      foreach (IPWPresenceCache<T> resource in this.GetResources<IPWPresenceCache<T>>())
        resource.NotifyConnected(Connection, Presence);
    }

    public virtual void RemoveUserIdentity(PWConnectionBase Connection)
    {
      if (Connection == null || Connection.UserID == PWServerBase.EmptyUserID)
        return;
      foreach (IPWNotificationCache resource in this.GetResources<IPWNotificationCache>())
        resource.NotifyDisconnected(Connection);
      foreach (IPWPresenceCache resource in this.GetResources<IPWPresenceCache>())
        resource.NotifyDisconnected(Connection);
      Connection.UserID = PWServerBase.EmptyUserID;
      Connection.Name = string.Empty;
    }

    public virtual void DisconnectClient(PWConnectionBase InConnection)
    {
      this.GetResource<PWLoginCache>()?.NotifyDisconnected(InConnection);
      this.GetResource<PWSuperServer>()?.NotifyDisconnected(InConnection);
      this.GetResource<PWVoiceServer>()?.NotifyDisconnected(InConnection);
      this.RemoveUserIdentity(InConnection);
      InConnection?.Destroy();
    }

    public delegate void AddLogMessageCallback(string InLogText, ELoggingLevel InLogLevel);

    public delegate void RefreshConnectionsFunc(PWServerBase InServer);

    public delegate PWServerInfoFull StartInstanceCallback(
      PWServerInfoReq InInfo,
      out PWServerInfoRunning StartedProcess,
      bool bTemplate,
      bool bKeepAlive);
  }
}
