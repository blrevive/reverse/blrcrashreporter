﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.PWClanInfo
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Common;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Xml.Serialization;

namespace PWAPICommon
{
  [ComVisible(true)]
  public class PWClanInfo
  {
    [XmlElement("CI")]
    public PWClanItem ClanItem;
    [XmlArray("ML")]
    public List<PWClanMemberInfo> Members;

    public PWClanInfo()
      : this((PWClanItem) null)
    {
    }

    public PWClanInfo(PWClanItem InClanItem) => this.ClanItem = InClanItem;

    public void SetMemberCount()
    {
      if (this.ClanItem == null || this.Members == null)
        return;
      this.ClanItem.MemberCount = this.Members.Count;
    }

    public PWClanMemberInfo FindMember(long ID)
    {
      if (this.Members == null)
      {
        Console.WriteLine("cbruner::Tried to get member from null Member list in PWClanInfo");
        return (PWClanMemberInfo) null;
      }
      lock (this.Members)
        return this.Members.Find((Predicate<PWClanMemberInfo>) (TheItem => TheItem.PlayerInfo.UserID == ID));
    }

    public void AddMember(PWClanMemberInfo ToAdd)
    {
      if (this.Members == null)
      {
        Console.WriteLine("cbruner::Initializing null Member list in PWClanInfo");
        this.Members = new List<PWClanMemberInfo>();
      }
      if (ToAdd == null)
        return;
      lock (this.Members)
      {
        if (this.Members.Contains(ToAdd))
          return;
        this.Members.Add(ToAdd);
        this.ClanItem.MemberCount = this.Members.Count;
      }
    }

    public PWPlayerInfo GetMembersPlayerInfo(long ToFind)
    {
      if (this.Members == null)
      {
        Console.WriteLine("cbruner::Tried to get player info from null Member list in PWClanInfo");
        return (PWPlayerInfo) null;
      }
      lock (this.Members)
      {
        PWClanMemberInfo pwClanMemberInfo = this.Members.Find((Predicate<PWClanMemberInfo>) (x => x.PlayerInfo.UserID == ToFind));
        if (pwClanMemberInfo != null)
          return pwClanMemberInfo.PlayerInfo;
      }
      return (PWPlayerInfo) null;
    }

    public void RemoveMember(long ToRemove)
    {
      if (this.Members == null)
      {
        Console.WriteLine("cbruner::Tried to remove from null Member list in PWClanInfo");
      }
      else
      {
        lock (this.Members)
        {
          PWClanMemberInfo pwClanMemberInfo = this.Members.Find((Predicate<PWClanMemberInfo>) (x => x.PlayerInfo.UserID == ToRemove));
          if (pwClanMemberInfo == null)
            return;
          this.Members.Remove(pwClanMemberInfo);
          this.ClanItem.MemberCount = this.Members.Count;
        }
      }
    }

    public override string ToString()
    {
      if (this.ClanItem == null)
        return "";
      return this.ClanItem.ClanTag + ":" + this.ClanItem.ClanName + ":" + (object) this.ClanItem.ClanID;
    }
  }
}
