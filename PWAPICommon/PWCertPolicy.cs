﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.PWCertPolicy
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System.Net.Security;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;

namespace PWAPICommon
{
  [ComVisible(true)]
  public class PWCertPolicy
  {
    public static bool CertificateValidationCallBack(
      object sender,
      X509Certificate certificate,
      X509Chain chain,
      SslPolicyErrors sslPolicyErrors)
    {
      bool flag = true;
      if (sslPolicyErrors != SslPolicyErrors.None)
      {
        if ((sslPolicyErrors & SslPolicyErrors.RemoteCertificateChainErrors) != SslPolicyErrors.None)
        {
          if (chain != null && chain.ChainStatus != null)
          {
            foreach (X509ChainStatus chainStatu in chain.ChainStatus)
            {
              if ((!(certificate.Subject == certificate.Issuer) || chainStatu.Status != X509ChainStatusFlags.UntrustedRoot) && chainStatu.Status != X509ChainStatusFlags.NoError)
                break;
            }
          }
          flag = true;
        }
        else
          flag = false;
      }
      int num = flag ? 1 : 0;
      return true;
    }
  }
}
