﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Clients.PWConnectionTCPBase
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Common;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Timers;

namespace PWAPICommon.Clients
{
  [ComVisible(true)]
  public abstract class PWConnectionTCPBase : PWConnectionBase
  {
    private Socket UsedSocket;
    private List<byte> TransferBuffer;
    private string SocketEndPoint;
    private System.Timers.Timer PollTimer;
    private DateTime HeartbeatTimeout;
    private double HeartbeatTimeoutMinutes = 1.25;

    public override string EndPoint => this.SocketEndPoint;

    public PWConnectionTCPBase(PWServerBase InOwningServer, Socket InSocket)
      : base(InOwningServer)
    {
      this.UsedSocket = InSocket;
      if (this.UsedSocket != null)
      {
        if (this.UsedSocket.RemoteEndPoint != null)
        {
          try
          {
            this.SocketEndPoint = ((IPEndPoint) this.UsedSocket.RemoteEndPoint).Address.ToString();
          }
          catch (Exception ex)
          {
            this.Log("Could not get Endpoint for socket.  Exception: " + ex.ToString(), false, ELoggingLevel.ELL_Errors);
            this.SocketEndPoint = "INVALID";
          }
        }
      }
      this.HeartbeatTimeout = PWServerBase.CurrentTime.AddMinutes(1.1);
      this.PollTimer = new System.Timers.Timer(15000.0);
      this.PollTimer.Elapsed += new ElapsedEventHandler(this.OnPollTimer);
      this.PollTimer.Start();
      this.TransferBuffer = new List<byte>(256);
    }

    public override string ToString() => "[" + this.Name + "] " + this.EndPoint;

    public override bool SendMessage(EMessageType MessageType, byte[] BinaryData)
    {
      byte[] FinalBlob = (byte[]) null;
      MessageSerializer.Encode(MessageType, BinaryData, out FinalBlob);
      base.SendMessage(MessageType, FinalBlob);
      return this.SendEncodedMessage(MessageType, FinalBlob);
    }

    public override bool SendEncodedMessage(EMessageType MessageType, byte[] FinalBlob)
    {
      base.SendEncodedMessage(MessageType, FinalBlob);
      if (this.UsedSocket == null)
      {
        this.OwningServer.Log("SendEncodedMessage: Socket NULL", (PWConnectionBase) this, false, ELoggingLevel.ELL_Verbose);
        return false;
      }
      lock (this.UsedSocket)
      {
        SocketAsyncEventArgs connection = this.OwningServer.GetConnection();
        if (connection == null)
        {
          this.Log("New Args are NULL, aborting send!", false, ELoggingLevel.ELL_Errors);
          return false;
        }
        connection.SetBuffer(FinalBlob, 0, FinalBlob.Length);
        connection.UserToken = (object) this;
        try
        {
          return this.UsedSocket.SendAsync(connection);
        }
        catch (InvalidOperationException ex)
        {
          this.Log("SendAsync Exception: " + ex.ToString() + " LastOp:" + connection.LastOperation.ToString(), false, ELoggingLevel.ELL_Errors);
          return this.SendEncodedMessage(MessageType, FinalBlob);
        }
      }
    }

    public int StartReceive(SocketAsyncEventArgs InArgs)
    {
      if (this.UsedSocket == null)
      {
        this.Log("StartReceive: Socket NULL", false, ELoggingLevel.ELL_Verbose);
        return -1;
      }
      lock (this.UsedSocket)
      {
        InArgs.SetBuffer(new byte[1024], 0, 1024);
        return !this.UsedSocket.ReceiveAsync(InArgs) ? 0 : 1;
      }
    }

    public void FinishReceive(SocketAsyncEventArgs args)
    {
      int num = -1;
      bool flag = false;
      if (this.UsedSocket == null)
      {
        flag = true;
      }
      else
      {
        lock (this.UsedSocket)
        {
          try
          {
            num = this.UsedSocket.Available;
          }
          catch (ObjectDisposedException ex)
          {
            flag = true;
            this.UsedSocket = (Socket) null;
          }
        }
      }
      if (args.BytesTransferred <= 0 || args.SocketError != SocketError.Success || flag)
      {
        this.OwningServer.ProcessError(args);
      }
      else
      {
        byte[] numArray = new byte[args.BytesTransferred];
        Array.Copy((Array) args.Buffer, (Array) numArray, args.BytesTransferred);
        this.TransferBuffer.AddRange((IEnumerable<byte>) numArray);
        if (num == 0)
        {
          byte[] array = this.TransferBuffer.ToArray();
          do
            ;
          while (this.ProcessData(ref array));
          if (array.Length == 0)
            this.TransferBuffer.Clear();
          else
            this.TransferBuffer.RemoveRange(0, this.TransferBuffer.Count - array.Length);
        }
        this.OwningServer.StartReceive(args);
      }
    }

    public bool FinishSend(SocketAsyncEventArgs args) => true;

    public override void Destroy()
    {
      base.Destroy();
      if (this.PollTimer != null)
      {
        this.PollTimer.Stop();
        this.PollTimer = (System.Timers.Timer) null;
      }
      if (this.UsedSocket == null)
        return;
      lock (this.UsedSocket)
      {
        Socket usedSocket = this.UsedSocket;
        this.UsedSocket = (Socket) null;
        try
        {
          if (usedSocket.Connected)
            usedSocket.Shutdown(SocketShutdown.Both);
          usedSocket.Close(1);
        }
        catch (Exception ex)
        {
        }
      }
    }

    public void HeartbeatReceived()
    {
      this.HeartbeatTimeout = PWServerBase.CurrentTime.AddMinutes(this.HeartbeatTimeoutMinutes);
      this.Log("Heartbeat at" + PWServerBase.CurrentTime.ToString("hh:mm:ss") + " setting timeout to: " + this.HeartbeatTimeout.ToString("hh:mm:ss"), false, ELoggingLevel.ELL_Verbose);
    }

    private void OnPollTimer(object Source, ElapsedEventArgs args)
    {
      if (!(PWServerBase.CurrentTime > this.HeartbeatTimeout) || this.OwningServer.TimeoutsDisabled)
        return;
      this.Log("Timed out at" + PWServerBase.CurrentTime.ToString("hh:mm:ss") + " with Heartbeat Cutoff at: " + this.HeartbeatTimeout.ToString("hh:mm:ss"), false, ELoggingLevel.ELL_Verbose);
      this.OwningServer.DisconnectClient((PWConnectionBase) this);
    }
  }
}
