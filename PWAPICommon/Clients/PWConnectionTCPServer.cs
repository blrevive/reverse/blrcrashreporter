﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Clients.PWConnectionTCPServer
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Queries;
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Runtime.InteropServices;

namespace PWAPICommon.Clients
{
  [ComVisible(true)]
  public class PWConnectionTCPServer : PWConnectionTCPBase
  {
    private bool bReceivedHandshake;
    private List<PWRequestBase> PendingMessages;

    public PWConnectionTCPServer(PWServerBase InOwningServer, Socket InSocket)
      : base(InOwningServer, InSocket)
      => this.PendingMessages = new List<PWRequestBase>();

    private void ProcessPendingMessages()
    {
      if (this.PendingMessages.Count <= 0)
        return;
      this.OwningServer.Log("Processing Out Of Order Messages", (PWConnectionBase) this, false, ELoggingLevel.ELL_Informative);
      foreach (PWRequestBase pendingMessage in this.PendingMessages)
        this.ReceiveValidMessage(pendingMessage, (byte[]) null);
      this.PendingMessages.Clear();
    }

    protected override bool ReceiveValidMessage(PWRequestBase InRequest, byte[] InOriginalData)
    {
      bool flag = false;
      if (!this.bReceivedHandshake)
      {
        if (InRequest.MessageType == EMessageType.EMT_Hello)
        {
          this.bReceivedHandshake = true;
          this.ProcessPendingMessages();
          flag = this.SubmitQuery(InRequest);
        }
        else
        {
          this.OwningServer.Log("OOO Message: " + InRequest.ToString(), (PWConnectionBase) this, false, ELoggingLevel.ELL_Warnings);
          this.PendingMessages.Add(InRequest);
        }
      }
      else
        flag = this.SubmitQuery(InRequest);
      return flag;
    }

    protected override bool ParseMessage(PWRequestBase InRequest, byte[] InMessage) => InRequest.ParseServerQuery(InMessage);

    public override bool SubmitQuery(PWRequestBase InQuery)
    {
      try
      {
        return InQuery.SubmitServerQuery();
      }
      catch (Exception ex)
      {
        this.Log("SubmitQuery Exception: " + ex.ToString(), false, ELoggingLevel.ELL_Errors);
      }
      return false;
    }
  }
}
