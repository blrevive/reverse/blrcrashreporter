﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Clients.PWConnectionBase
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Queries;
using System;
using System.Runtime.InteropServices;

namespace PWAPICommon.Clients
{
  [ComVisible(true)]
  public abstract class PWConnectionBase
  {
    private string ConnectionName;
    public GeoLocation GeoLoc;
    private PWServerBase owningServer;
    private long userID;

    public string Name
    {
      get => this.ConnectionName;
      set => this.ConnectionName = value;
    }

    public long UserID
    {
      get => this.userID;
      set => this.userID = value;
    }

    public PWServerBase OwningServer => this.owningServer;

    public virtual string EndPoint => "none";

    public PWConnectionBase(PWServerBase InOwningServer)
    {
      this.owningServer = InOwningServer;
      this.ConnectionName = "";
    }

    public abstract bool SubmitQuery(PWRequestBase InQuery);

    public virtual bool SendMessage(EMessageType MessageType, byte[] BinaryData) => true;

    protected bool ReceiveMessage(EMessageType InMessageType, byte[] Message)
    {
      PWRequestBase requestObject = this.OwningServer.CreateRequestObject(InMessageType, this);
      if (requestObject == null)
      {
        this.OwningServer.Log("Request is NULL for message type " + InMessageType.ToString(), this, false, ELoggingLevel.ELL_Warnings);
        return false;
      }
      bool flag;
      try
      {
        flag = this.ParseMessage(requestObject, Message);
      }
      catch (Exception ex)
      {
        this.Log("Invalid Message of Type " + (object) InMessageType + " Failed to Parse " + requestObject.ToString() + " Exception: " + ex.ToString(), false, ELoggingLevel.ELL_Errors);
        flag = false;
      }
      this.Log(requestObject.ToString() + " Created Valid=" + (object) flag, false, ELoggingLevel.ELL_Verbose);
      if (flag)
        return this.ReceiveValidMessage(requestObject, Message);
      this.OwningServer.Log("Request " + requestObject.ToString() + " is not valid, ignoring!", this, false, ELoggingLevel.ELL_Warnings);
      PWServerStats resource = this.OwningServer.GetResource<PWServerStats>();
      if (resource != null)
      {
        try
        {
          resource.MessageInvalid(InMessageType);
        }
        catch (NotImplementedException ex)
        {
        }
      }
      return false;
    }

    protected abstract bool ParseMessage(PWRequestBase InRequest, byte[] InMessage);

    protected abstract bool ReceiveValidMessage(PWRequestBase InMessage, byte[] InOriginalData);

    public virtual void EncodeMessage(
      EMessageType MessageType,
      byte[] BinaryData,
      out byte[] FinalBlob)
    {
      FinalBlob = (byte[]) null;
    }

    public virtual bool SendEncodedMessage(EMessageType MessageType, byte[] FinalBlob)
    {
      if (this.OwningServer != null)
        this.OwningServer.GetResource<PWServerStats>()?.MessageSent(MessageType, (long) FinalBlob.Length);
      return false;
    }

    public virtual void WaitForResponse(PWRequestBase InRequest)
    {
    }

    public override string ToString() => this.ConnectionName;

    public virtual void Destroy()
    {
    }

    private int GetHeaderIndex(byte[] Data, int BeginIndex)
    {
      for (int index = BeginIndex; index < Data.Length - 1; ++index)
      {
        if (Data[index] == (byte) 60 && Data[index + 1] == (byte) 60)
          return index;
      }
      return -1;
    }

    private int GetFooterIndex(byte[] Data, int HeaderIndex)
    {
      if (HeaderIndex >= 0 && HeaderIndex < Data.Length)
      {
        for (int index = HeaderIndex; index < Data.Length - 1; ++index)
        {
          if (Data[index] == (byte) 62 && Data[index + 1] == (byte) 62)
            return index;
        }
      }
      return -1;
    }

    private bool IsValidPadding(byte InPadding) => InPadding < (byte) 4;

    protected bool ProcessData(ref byte[] ReceivedData)
    {
      int headerIndex = this.GetHeaderIndex(ReceivedData, 0);
      int footerIndex = this.GetFooterIndex(ReceivedData, headerIndex);
      bool flag1 = headerIndex >= 0;
      bool flag2 = footerIndex >= 0;
      bool flag3 = false;
      for (; flag1 && flag2; flag2 = footerIndex >= 0)
      {
        flag3 = true;
        while (footerIndex < ReceivedData.Length && footerIndex > 0)
        {
          int length1 = footerIndex - headerIndex - 2;
          bool flag4;
          if (length1 < 4)
          {
            footerIndex = this.GetFooterIndex(ReceivedData, footerIndex + 1);
            flag4 = footerIndex >= 0;
          }
          else
          {
            byte[] InOutData1 = new byte[length1];
            Array.Copy((Array) ReceivedData, headerIndex + 2, (Array) InOutData1, 0, length1);
            XXTEA.DoDecrypt(ref InOutData1);
            byte InPadding = InOutData1[1];
            byte num = InOutData1[2];
            int length2 = length1 - 4;
            int length3 = length2 - (int) InPadding;
            if (length2 < 0 || length3 < 0 || (!this.OwningServer.IsValidMessageType((EMessageType) num) || !this.IsValidPadding(InPadding)))
            {
              this.Log("Invalid Message: Type [" + (object) num + "] Padding [" + (object) InPadding + "]", false, ELoggingLevel.ELL_Verbose);
              footerIndex = this.GetFooterIndex(ReceivedData, footerIndex + 1);
              flag4 = footerIndex >= 0;
            }
            else
            {
              byte[] InOutData2 = new byte[length2];
              Array.Copy((Array) InOutData1, 4, (Array) InOutData2, 0, length2);
              XXTEA.DoDecrypt(ref InOutData2);
              byte[] Message = new byte[length3];
              Array.Copy((Array) InOutData2, 0, (Array) Message, 0, length3);
              if (!this.ReceiveMessage((EMessageType) num, Message))
              {
                this.Log("ReceiveMessage failed for Message Type " + num.ToString(), false, ELoggingLevel.ELL_Verbose);
                footerIndex = this.GetFooterIndex(ReceivedData, footerIndex + 1);
                flag4 = footerIndex >= 0;
              }
              else
              {
                int length4 = ReceivedData.Length - (footerIndex + 2);
                byte[] numArray = new byte[length4];
                Array.Copy((Array) ReceivedData, footerIndex + 2, (Array) numArray, 0, length4);
                ReceivedData = numArray;
                return true;
              }
            }
          }
        }
        headerIndex = this.GetHeaderIndex(ReceivedData, headerIndex + 1);
        footerIndex = this.GetFooterIndex(ReceivedData, headerIndex);
        flag1 = headerIndex >= 0;
      }
      if (flag3)
        this.Log("Invalid Message: Header / Footer", false, ELoggingLevel.ELL_Verbose);
      return false;
    }

    protected void Log(string Text, bool bLogToRemoteServer, ELoggingLevel InLoggingLevel)
    {
      if (this.OwningServer == null)
        return;
      this.OwningServer.Log(Text, this, bLogToRemoteServer, InLoggingLevel);
    }
  }
}
