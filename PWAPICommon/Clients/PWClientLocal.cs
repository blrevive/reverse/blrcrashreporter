﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Clients.PWClientLocal
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Queries;
using System;
using System.Runtime.InteropServices;

namespace PWAPICommon.Clients
{
  [ComVisible(true)]
  public class PWClientLocal : PWConnectionBase
  {
    private SendMessageCallback MessageCallbackFunc;

    public override string EndPoint => "Local";

    public PWClientLocal(PWServer InOwningServer, SendMessageCallback InCallback)
      : base((PWServerBase) InOwningServer)
    {
      this.MessageCallbackFunc = InCallback;
      this.Name = "Local";
    }

    public override bool SendMessage(EMessageType MessageType, byte[] BinaryData)
    {
      base.SendMessage(MessageType, BinaryData);
      return this.MessageCallbackFunc == null || this.MessageCallbackFunc(MessageType, BinaryData);
    }

    protected override bool ParseMessage(PWRequestBase InRequest, byte[] InMessage) => throw new NotImplementedException();

    protected override bool ReceiveValidMessage(PWRequestBase InMessage, byte[] InOriginalData) => throw new NotImplementedException();

    public override bool SubmitQuery(PWRequestBase InQuery)
    {
      try
      {
        return InQuery.SubmitServerQuery();
      }
      catch (Exception ex)
      {
        this.Log("SubmitQuery Exception: " + ex.ToString(), false, ELoggingLevel.ELL_Errors);
      }
      return false;
    }
  }
}
