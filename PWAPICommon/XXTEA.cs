﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.XXTEA
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System;
using System.Runtime.InteropServices;

namespace PWAPICommon
{
  [ComVisible(true)]
  public static class XXTEA
  {
    private static uint[] Key = new uint[4]
    {
      4271383519U,
      3752564771U,
      1127922790U,
      1752379389U
    };
    private static int Iterations = 12;

    private static void CodeInternal(ref uint[] v, uint[] k, XXTEA.XXTEAMode mode)
    {
      if (v.Length <= 1)
        return;
      uint y = 0;
      uint z = 0;
      uint sum = 0;
      uint p = 0;
      uint e = 0;
      int length = v.Length;
      Func<uint> func = (Func<uint>) (() => (uint) (((int) (z >> 5) ^ (int) y << 2) + ((int) (y >> 3) ^ (int) z << 4) ^ ((int) sum ^ (int) y) + ((int) k[(long)(IntPtr) (p & 3U ^ e)] ^ (int) z)));
      if (mode == XXTEA.XXTEAMode.Encrypt)
      {
        uint num = (uint) (6 + 52 / length);
        z = v[length - 1];
        do
        {
          sum += 2654435769U;
          e = sum >> 2 & 3U;
          for (p = 0U; (long) p < (long) (length - 1); ++p)
          {
            y = v[(long)(IntPtr) (p + 1U)];
            z = (v[(long)(IntPtr) p] += func());
          }
          y = v[0];
          z = (v[length - 1] += func());
        }
        while (--num > 0U);
      }
      else
      {
        sum = (uint) (6 + 52 / length) * 2654435769U;
        y = v[0];
        do
        {
          e = sum >> 2 & 3U;
          for (p = (uint) (length - 1); p > 0U; --p)
          {
            z = v[(long)(IntPtr) (p - 1U)];
            y = (v[(long)(IntPtr) p] -= func());
          }
          z = v[length - 1];
          y = (v[0] -= func());
        }
        while ((sum -= 2654435769U) != 0U);
      }
    }

    public static void DoDecrypt(ref byte[] InOutData)
    {
      uint[] v = new uint[(int) Math.Ceiling((double) InOutData.Length / 4.0)];
      Buffer.BlockCopy((Array) InOutData, 0, (Array) v, 0, InOutData.Length);
      for (int index = 0; index < XXTEA.Iterations; ++index)
        XXTEA.CodeInternal(ref v, XXTEA.Key, XXTEA.XXTEAMode.Decrypt);
      byte[] numArray = new byte[v.Length * 4];
      Buffer.BlockCopy((Array) v, 0, (Array) numArray, 0, numArray.Length);
      InOutData = numArray;
    }

    public static byte DoEncrypt(ref byte[] InOutData)
    {
      uint[] v = new uint[(int) Math.Ceiling((double) InOutData.Length / 4.0)];
      Buffer.BlockCopy((Array) InOutData, 0, (Array) v, 0, InOutData.Length);
      for (int index = 0; index < XXTEA.Iterations; ++index)
        XXTEA.CodeInternal(ref v, XXTEA.Key, XXTEA.XXTEAMode.Encrypt);
      byte[] numArray = new byte[v.Length * 4];
      Buffer.BlockCopy((Array) v, 0, (Array) numArray, 0, numArray.Length);
      byte num = (byte) (numArray.Length - InOutData.Length);
      InOutData = numArray;
      return num;
    }

    public enum XXTEAMode
    {
      Encrypt,
      Decrypt,
    }
  }
}
