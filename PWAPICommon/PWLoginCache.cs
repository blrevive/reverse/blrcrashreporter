﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.PWLoginCache
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Caches;
using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Config;
using PWAPICommon.Resources;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Timers;

namespace PWAPICommon
{
  [ComVisible(true)]
  public class PWLoginCache : IPWResource
  {
    private int LoggedInPlayerMax;
    private List<PWLoginCache.LoginData> PendingPlayers;
    private List<PWLoginCache.LoginData> LoggedInPlayers;
    private PWServerBase OwningServer;
    private Timer PopulationReportTimer;
    private List<PWLoginCache.LoginData> Blacklist;
    private List<PWLoginCache.LoginData> WhiteList;
    private PWLoginCache.MonitorStats HealthStats;
    private DateTime LastLogin;
    private bool bHadFirstLogin;
    private TimeSpan CacheIdleThreshold;
    private bool bBlacklistEnabled;
    private bool bWhitelistEnabled;

    public IList<PWLoginCache.LoginData> BlacklistedPlayers => (IList<PWLoginCache.LoginData>) this.Blacklist;

    public IList<PWLoginCache.LoginData> WhitelistedPlayers => (IList<PWLoginCache.LoginData>) this.WhiteList;

    public int NumPlayers => this.LoggedInPlayers.Count;

    public int NumAvailableSpots => this.LoggedInPlayerMax - (this.LoggedInPlayers.Count + this.PendingPlayers.Count);

    public bool ShouldQueueUser => this.LoggedInPlayers.Count + this.PendingPlayers.Count >= this.LoggedInPlayerMax;

    public bool WhiteListEnabled => this.bWhitelistEnabled;

    public PWLoginCache(PWServerBase InOwningServer)
    {
      this.bHadFirstLogin = false;
      this.OwningServer = InOwningServer;
      this.PendingPlayers = new List<PWLoginCache.LoginData>();
      this.LoggedInPlayers = new List<PWLoginCache.LoginData>();
      this.PopulationReportTimer = new Timer(120000.0);
      this.PopulationReportTimer.Elapsed += new ElapsedEventHandler(this.OnReportTimer);
      this.PopulationReportTimer.Start();
      this.CacheIdleThreshold = new TimeSpan(0, 5, 0);
      this.LoadFilterFiles();
      ConfigSettings.AddConfigChangedListener(new ConfigurationChangedFunc(this.ConfigChanged));
    }

    private List<PWLoginCache.LoginData> LoadUserInfoFromFile(string PathName)
    {
      List<PWLoginCache.LoginData> loginDataList = new List<PWLoginCache.LoginData>();
      if (PathName != "")
      {
        FileStream fileStream = new FileStream(PathName, FileMode.Open, FileAccess.Read);
        if (fileStream != null)
        {
          byte[] numArray = new byte[fileStream.Length];
          fileStream.Read(numArray, 0, numArray.Length);
          string str = new MessageSerializer().DeserializeToString(numArray);
          char[] separator = new char[7]
          {
            '\r',
            ' ',
            ';',
            '\n',
            ',',
            '\t',
            ':'
          };
          foreach (string s in str.Split(separator, StringSplitOptions.RemoveEmptyEntries))
          {
            PWLoginCache.LoginData loginData = new PWLoginCache.LoginData();
            if (!long.TryParse(s, out loginData.UserID))
              loginData.UserName = s;
            loginDataList.Add(loginData);
          }
        }
      }
      return loginDataList;
    }

    private void LoadFilterFiles()
    {
      this.OwningServer.GetConfigValue<bool>("BlackListEnabled", out this.bBlacklistEnabled, false);
      this.OwningServer.GetConfigValue<bool>("WhiteListEnabled", out this.bWhitelistEnabled, false);
      string OutValue;
      if (this.OwningServer.GetConfigValue<string>("BlacklistFileName", out OutValue, ""))
        this.Blacklist = this.LoadUserInfoFromFile(OutValue);
      if (this.OwningServer.GetConfigValue<string>("WhitelistFileName", out OutValue, ""))
        this.WhiteList = this.LoadUserInfoFromFile(OutValue);
      this.OwningServer.GetConfigValue<int>("LoginQueuePlayerThreshold", out this.LoggedInPlayerMax, 9000);
    }

    public void ConfigChanged(string Key, string OldValue, string NewValue) => this.LoadFilterFiles();

    public ELoginCacheResponse StartLogin(
      string InUserName,
      PWConnectionBase InConnection)
    {
      lock (this)
      {
        this.bHadFirstLogin = true;
        this.LastLogin = PWServerBase.CurrentTime;
        ELoginCacheResponse eloginCacheResponse = this.CheckFilter(InUserName, -1L);
        if (eloginCacheResponse != ELoginCacheResponse.ELCR_OK)
          return eloginCacheResponse;
        if (this.IsLoggedIn(InUserName) || this.IsPendingLogin(InUserName))
        {
          bool OutValue = false;
          this.OwningServer.GetConfigValue<bool>("ForceLogout", out OutValue);
          if (!OutValue)
            return ELoginCacheResponse.ELCR_Duplicate;
          PWLoginCache.LoginData loginData = this.LoggedInPlayers.Find((Predicate<PWLoginCache.LoginData>) (x => x.UserName == InUserName)) ?? this.PendingPlayers.Find((Predicate<PWLoginCache.LoginData>) (x => x.UserName == InUserName));
          if (loginData != null)
            this.ForceLogout(loginData.UserID);
        }
        this.PendingPlayers.Add(new PWLoginCache.LoginData()
        {
          UserID = -1L,
          UserName = InUserName,
          Connection = InConnection
        });
        return ELoginCacheResponse.ELCR_OK;
      }
    }

    public ELoginCacheResponse FinishLogin(
      string InUserName,
      long InUserID,
      bool bSuccess,
      string InUserIP,
      long SteamID,
      string LanguageExt)
    {
      lock (this)
      {
        if (!this.IsPendingLogin(InUserName))
          return ELoginCacheResponse.ELCR_NotPending;
        PWLoginCache.LoginData loginData = this.PendingPlayers.Find((Predicate<PWLoginCache.LoginData>) (x => x.UserName == InUserName));
        this.PendingPlayers.Remove(loginData);
        ELoginCacheResponse eloginCacheResponse = this.CheckFilter(InUserName, -1L);
        if (eloginCacheResponse != ELoginCacheResponse.ELCR_OK)
          return eloginCacheResponse;
        if (this.IsLoggedIn(InUserName))
          return ELoginCacheResponse.ELCR_Duplicate;
        if (!bSuccess)
          return ELoginCacheResponse.ELCR_Failed;
        this.OwningServer.GetResource<PWLoginQueueCache>()?.UserLoggedIn(InUserName);
        this.Log("login charid=" + (object) InUserID + " username=" + InUserName + " ip=" + InUserIP + " steamid=" + (object) SteamID + " client_lang=" + LanguageExt, (PWConnectionBase) null, true, ELoggingLevel.ELL_Verbose);
        loginData.UserID = InUserID;
        this.LoggedInPlayers.Add(loginData);
        return ELoginCacheResponse.ELCR_OK;
      }
    }

    private ELoginCacheResponse CheckFilter(string InName, long InId)
    {
      if (this.bWhitelistEnabled && (this.WhiteList == null || this.WhiteList.Count <= 0 || this.WhiteList.Find((Predicate<PWLoginCache.LoginData>) (x => x.UserID == InId || x.UserName.Equals(InName, StringComparison.CurrentCultureIgnoreCase))) == null))
        return ELoginCacheResponse.ELCR_Whitelisted;
      return this.bBlacklistEnabled && this.Blacklist != null && this.Blacklist.Count > 0 && this.Blacklist.Find((Predicate<PWLoginCache.LoginData>) (x => x.UserID == InId || x.UserName.Equals(InName, StringComparison.CurrentCultureIgnoreCase))) != null ? ELoginCacheResponse.ELCR_Blacklisted : ELoginCacheResponse.ELCR_OK;
    }

    private void Log(
      string Text,
      PWConnectionBase InConnection,
      bool bLogToRemoteServer,
      ELoggingLevel LogLevel)
    {
      this.OwningServer.Log(Text, InConnection, bLogToRemoteServer, LogLevel);
    }

    public bool Logout(PWConnectionBase InConnection)
    {
      lock (this)
      {
        PWLoginQueueCache resource = this.OwningServer.GetResource<PWLoginQueueCache>();
        PWLoginCache.LoginData loginData1 = this.PendingPlayers.Find((Predicate<PWLoginCache.LoginData>) (x => x.Connection == InConnection));
        if (loginData1 != null)
        {
          this.PendingPlayers.Remove(loginData1);
          this.OwningServer.Log("Pending login player " + (object) loginData1.UserID + " Disconnected", (PWConnectionBase) null, false, ELoggingLevel.ELL_Informative);
          resource?.UserLoggedOut(loginData1.UserName);
        }
        PWLoginCache.LoginData loginData2 = this.LoggedInPlayers.Find((Predicate<PWLoginCache.LoginData>) (x => x.Connection == InConnection));
        if (loginData2 != null)
        {
          this.LoggedInPlayers.Remove(loginData2);
          this.OwningServer.Log("Logged in player " + (object) loginData2.UserID + " Disconnected", (PWConnectionBase) null, false, ELoggingLevel.ELL_Informative);
          this.Log("logout charid=" + (object) loginData2.UserID + " username=" + loginData2.UserName + " ip=" + InConnection.EndPoint, (PWConnectionBase) null, true, ELoggingLevel.ELL_Verbose);
          resource?.UserLoggedOut(loginData2.UserName);
        }
      }
      return false;
    }

    public bool ForceLogout(long UserId)
    {
      bool flag = false;
      lock (this)
      {
        PWLoginCache.LoginData loginData1 = this.PendingPlayers.Find((Predicate<PWLoginCache.LoginData>) (x => x.UserID == UserId));
        if (loginData1 != null)
        {
          this.OwningServer.Log("Pending login player " + (object) loginData1.UserID + " Kicked", (PWConnectionBase) null, false, ELoggingLevel.ELL_Informative);
          this.OwningServer.DisconnectClient(loginData1.Connection);
          flag = true;
        }
        PWLoginCache.LoginData loginData2 = this.LoggedInPlayers.Find((Predicate<PWLoginCache.LoginData>) (x => x.UserID == UserId));
        if (loginData2 != null)
        {
          this.OwningServer.Log("Logged in player " + (object) loginData2.UserID + " Kicked", (PWConnectionBase) null, false, ELoggingLevel.ELL_Informative);
          this.OwningServer.DisconnectClient(loginData2.Connection);
          flag = true;
        }
      }
      return flag;
    }

    public bool ForceLogout(string PlayerName)
    {
      bool flag = false;
      lock (this)
      {
        PWLoginCache.LoginData loginData1 = this.PendingPlayers.Find((Predicate<PWLoginCache.LoginData>) (x => x.UserName == PlayerName));
        if (loginData1 != null)
        {
          this.OwningServer.Log("Pending login player " + (object) loginData1.UserID + " Kicked", (PWConnectionBase) null, false, ELoggingLevel.ELL_Informative);
          this.OwningServer.DisconnectClient(loginData1.Connection);
          flag = true;
        }
        PWLoginCache.LoginData loginData2 = this.LoggedInPlayers.Find((Predicate<PWLoginCache.LoginData>) (x => x.UserName == PlayerName));
        if (loginData2 != null)
        {
          this.OwningServer.Log("Logged in player " + (object) loginData2.UserID + " Kicked", (PWConnectionBase) null, false, ELoggingLevel.ELL_Informative);
          this.OwningServer.DisconnectClient(loginData2.Connection);
          flag = true;
        }
      }
      return flag;
    }

    private bool IsLoggedIn(string InUserName) => this.LoggedInPlayers.Find((Predicate<PWLoginCache.LoginData>) (x => x.UserName == InUserName)) != null;

    private bool IsLoggedIn(long InUserId) => this.LoggedInPlayers.Find((Predicate<PWLoginCache.LoginData>) (x => x.UserID == InUserId)) != null;

    private bool IsPendingLogin(string InUserName) => this.PendingPlayers.Find((Predicate<PWLoginCache.LoginData>) (x => x.UserName == InUserName)) != null;

    private bool IsPendingLogin(long InUserId) => this.PendingPlayers.Find((Predicate<PWLoginCache.LoginData>) (x => x.UserID == InUserId)) != null;

    public void NotifyDisconnected(PWConnectionBase Client) => this.Logout(Client);

    private void OnReportTimer(object Source, ElapsedEventArgs args) => this.Log("regionpopulation=" + this.NumPlayers.ToString(), (PWConnectionBase) null, true, ELoggingLevel.ELL_Verbose);

    public void KickAll()
    {
      while (this.LoggedInPlayers.Count > 0)
        this.ForceLogout(this.LoggedInPlayers[0].UserID);
    }

    public virtual void StartResource()
    {
    }

    public virtual void StopResource()
    {
    }

    public bool QueueUser(
      string Account,
      out int QueueTime,
      out int QueueSize,
      out int PositionInQueue)
    {
      PWLoginQueueCache resource = this.OwningServer.GetResource<PWLoginQueueCache>();
      if (resource != null && (resource.TotalQueueSize > 0 && this.NumAvailableSpots - resource.TotalQueueSize <= 0 || this.ShouldQueueUser))
        return resource.TryQueueUser(Account, out QueueTime, out QueueSize, out PositionInQueue);
      QueueTime = 0;
      QueueSize = 0;
      PositionInQueue = 0;
      return true;
    }

    public Dictionary<string, string> CheckHealth()
    {
      Dictionary<string, string> dictionary = new Dictionary<string, string>();
      dictionary["Login"] = "OK";
      dictionary["Login:Last"] = (PWServerBase.CurrentTime - this.LastLogin).TotalMinutes.ToString("0.0") + "m";
      if (!this.bHadFirstLogin)
        dictionary["Login:Last"] = "NEVER";
      else if (PWServerBase.CurrentTime - this.LastLogin > this.CacheIdleThreshold)
      {
        this.Log("Error: No Logins in the last " + this.CacheIdleThreshold.ToString(), (PWConnectionBase) null, false, ELoggingLevel.ELL_Verbose);
        dictionary["Login"] = "STALLED";
      }
      PWLoginCache.MonitorStats monitorStats = new PWLoginCache.MonitorStats();
      monitorStats.LastPlayerCount = this.LoggedInPlayers.Count;
      if (this.HealthStats != null)
      {
        int OutValue1 = 0;
        this.OwningServer.GetConfigValue<int>("MinPlayerDrop", out OutValue1, 10);
        float OutValue2 = 0.8f;
        this.OwningServer.GetConfigValue<float>("MinPctDrop", out OutValue2, 0.8f);
        int num1 = monitorStats.LastPlayerCount - this.HealthStats.LastPlayerCount;
        float num2 = (float) (100.0 * (1.0 - (this.HealthStats.LastPlayerCount > 0 ? (double) (monitorStats.LastPlayerCount / this.HealthStats.LastPlayerCount) : 0.0)));
        if (num1 > OutValue1 && (double) num2 > (double) OutValue2)
        {
          this.Log("Error: Logged in player count dropped " + (object) num2 + "% from " + (object) this.HealthStats.LastPlayerCount + " to " + (object) monitorStats.LastPlayerCount, (PWConnectionBase) null, false, ELoggingLevel.ELL_Errors);
          dictionary["Login"] = "DROP";
        }
      }
      dictionary["Login:Players"] = this.LoggedInPlayers.Count.ToString();
      this.HealthStats = monitorStats;
      return dictionary;
    }

    public class LoginData
    {
      public long UserID;
      public string UserName;
      public PWConnectionBase Connection;

      public LoginData()
      {
        this.UserName = "";
        this.UserID = -1L;
      }

      public override string ToString() => this.UserID.ToString() + " : " + this.UserName + " : ";
    }

    private class MonitorStats
    {
      public int LastPlayerCount;
    }
  }
}
