﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.ChatRoom
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System.Collections.Concurrent;
using System.Runtime.InteropServices;

namespace PWAPICommon
{
  [ComVisible(true)]
  public class ChatRoom
  {
    private int ID;
    private string Name;
    private bool IsPersistent;
    public ConcurrentDictionary<long, PWConnectionBase> Listeners;

    public int RoomID => this.ID;

    public string RoomName => this.Name;

    public bool bIsPersistent => this.IsPersistent;

    public ChatRoom(string InName, int InID, bool ShouldPersist = false)
    {
      this.Name = InName;
      this.ID = InID;
      this.IsPersistent = ShouldPersist;
      this.Listeners = new ConcurrentDictionary<long, PWConnectionBase>();
    }

    public override string ToString() => this.Name + ":" + (object) this.ID;
  }
}
