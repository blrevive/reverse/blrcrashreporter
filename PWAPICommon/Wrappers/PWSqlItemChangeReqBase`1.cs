﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Wrappers.PWSqlItemChangeReqBase`1
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Queries;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace PWAPICommon.Wrappers
{
  [ComVisible(true)]
  public abstract class PWSqlItemChangeReqBase<T> : PWSqlItemReqBase<T> where T : PWSqlItemBase<T>, new()
  {
    public T Item;

    public PWSqlItemChangeReqBase()
      : this((PWConnectionBase) null)
    {
    }

    public PWSqlItemChangeReqBase(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override SqlCommand GetCommand(ref string LogOutput) => (object) this.Item != null ? this.Item.GetCommand(this.SqlOp, ref LogOutput) : (SqlCommand) null;

    public override bool ParseServerResult(PWRequestBase ForRequest) => base.ParseServerResult(ForRequest) && (object) this.Item != null && this.Item.ParseServerResult((PWSqlItemReqBase<T>) this);
  }
}
