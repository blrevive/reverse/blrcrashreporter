﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Wrappers.PWSqlItemAddReq`1
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using System.Runtime.InteropServices;

namespace PWAPICommon.Wrappers
{
  [ComVisible(true)]
  public class PWSqlItemAddReq<T> : PWSqlItemChangeReqBase<T> where T : PWSqlItemBase<T>, new()
  {
    public override ESqlItemOp SqlOp => ESqlItemOp.ESIO_Add;

    public PWSqlItemAddReq()
      : this((PWConnectionBase) null)
    {
    }

    public PWSqlItemAddReq(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }
  }
}
