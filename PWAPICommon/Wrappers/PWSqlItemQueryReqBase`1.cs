﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Wrappers.PWSqlItemQueryReqBase`1
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Queries;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace PWAPICommon.Wrappers
{
  [ComVisible(true)]
  public abstract class PWSqlItemQueryReqBase<T> : PWSqlItemReqBase<T> where T : PWSqlItemBase<T>, new()
  {
    public List<T> ResponseItems;

    public PWSqlItemQueryReqBase()
      : this((PWConnectionBase) null)
    {
    }

    public PWSqlItemQueryReqBase(PWConnectionBase InConnection)
      : base(InConnection)
    {
    }

    protected override SqlCommand GetCommand(ref string LogOutput) => PWSqlItemReqBase<T>.DefaultObject.GetCommand(this.SqlOp, ref LogOutput);

    public override bool ParseServerResult(PWRequestBase ForRequest)
    {
      if (this.ResponseValues != null && this.ResponseValues.Count > 0)
      {
        string str = (this.ResponseValues[0] as object[])[0].ToString();
        if (str.IndexOf("Not Exists", StringComparison.CurrentCultureIgnoreCase) >= 0)
        {
          this.ResponseItems = new List<T>();
          return PWSqlItemReqBase<T>.DefaultObject.ParseServerResult((PWSqlItemReqBase<T>) this);
        }
        if (str.IndexOf("Failed", StringComparison.CurrentCultureIgnoreCase) < 0)
        {
          List<T> objList = new List<T>(this.ResponseValues.Count);
          foreach (object[] responseValue in (IEnumerable<object>) this.ResponseValues)
          {
            T obj = new T();
            if (obj.ParseFromArray(responseValue))
              objList.Add(obj);
          }
          this.ResponseItems = objList;
          return PWSqlItemReqBase<T>.DefaultObject.ParseServerResult((PWSqlItemReqBase<T>) this);
        }
      }
      return false;
    }
  }
}
