﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.PWTransactionCache
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Caches;
using PWAPICommon.Clients;
using PWAPICommon.Queries;
using PWAPICommon.Resources;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace PWAPICommon
{
  [ComVisible(true)]
  public class PWTransactionCache : PWCacheBase, IPWResource
  {
    private List<PWTransaction> CachedTransactions;
    private int MaxTransactionsToCache;
    private List<Guid> CachedPopularItems;
    private int NumPopularItems;
    private int PopularItemMaxAgeDays;

    public PWTransactionCache(
      PWServerBase InServer,
      PWConnectionBase InConnection,
      int InMaxTransactionsToCache,
      int InPopulatItemAgeLimit,
      int InNumPopularItems)
      : base(InServer, InConnection)
    {
      this.CachedPopularItems = new List<Guid>();
      this.PopularItemMaxAgeDays = InPopulatItemAgeLimit;
      this.NumPopularItems = InNumPopularItems;
      this.MaxTransactionsToCache = InMaxTransactionsToCache;
    }

    public List<Guid> GetPopularItems()
    {
      this.ConditionalRefreshCache();
      return this.CachedPopularItems;
    }

    protected override PWRequestBase CreateRefreshRequest()
    {
      this.Log("Transaction Cache out of date, Refreshing...", ELoggingLevel.ELL_Informative);
      return (PWRequestBase) new PWTransactionQueryReq();
    }

    private void ParsePopularPurchases(List<PWTransaction> InTransactions)
    {
      this.Log("Refreshing Popular Purchases...", ELoggingLevel.ELL_Informative);
      DateTime dateTime = PWServerBase.CurrentTime.AddDays((double) -this.PopularItemMaxAgeDays);
      Dictionary<Guid, uint> dictionary1 = new Dictionary<Guid, uint>();
      for (int index = 0; index < InTransactions.Count; ++index)
      {
        PWTransaction inTransaction = InTransactions[index];
        if ((inTransaction.Type == ETransactionType.ETT_PurchaseZP || inTransaction.Type == ETransactionType.ETT_PurchaseGP) && inTransaction.Time > dateTime)
        {
          if (dictionary1.ContainsKey(inTransaction.TargetItem))
          {
            Dictionary<Guid, uint> dictionary2;
            Guid targetItem;
            (dictionary2 = dictionary1)[targetItem = inTransaction.TargetItem] = dictionary2[targetItem] + 1U;
          }
          else
            dictionary1.Add(inTransaction.TargetItem, 1U);
        }
      }
      List<KeyValuePair<Guid, uint>> keyValuePairList = new List<KeyValuePair<Guid, uint>>(dictionary1.Count);
      foreach (KeyValuePair<Guid, uint> keyValuePair in dictionary1)
        keyValuePairList.Add(keyValuePair);
      keyValuePairList.Sort((Comparison<KeyValuePair<Guid, uint>>) ((a, b) => b.Value.CompareTo(a.Value)));
      this.CachedPopularItems.Clear();
      for (int index = 0; index < keyValuePairList.Count && index < this.NumPopularItems; ++index)
        this.CachedPopularItems.Add(keyValuePairList[index].Key);
      this.OwningServer.GetResource<PWStoreCache>()?.MarkDirty();
    }

    protected override bool ParseCacheRefreshResults(PWRequestBase Wrapper)
    {
      PWTransactionQueryReq transactionQueryReq = Wrapper as PWTransactionQueryReq;
      this.Log("Transaction Cache read, parsing...", ELoggingLevel.ELL_Informative);
      if (transactionQueryReq.ParseServerResult(Wrapper))
      {
        this.Log("Transaction Parsed, Processing...", ELoggingLevel.ELL_Informative);
        this.ParsePopularPurchases(transactionQueryReq.Transactions);
        this.CachedTransactions = transactionQueryReq.Transactions;
        if (this.CachedTransactions.Count > this.MaxTransactionsToCache)
          this.CachedTransactions.RemoveRange(0, this.CachedTransactions.Count - this.MaxTransactionsToCache);
        return true;
      }
      this.Log("Transaction Cache read FAILED!", ELoggingLevel.ELL_Warnings);
      return false;
    }

    public override Dictionary<string, string> CheckHealth()
    {
      Dictionary<string, string> dictionary = new Dictionary<string, string>();
      if (this.CachedTransactions == null)
      {
        dictionary["TransCache:Status"] = "PENDING";
      }
      else
      {
        dictionary["TransCache:Status"] = "OK";
        dictionary["TransCache:Count"] = this.CachedTransactions.Count.ToString();
        dictionary["TransCache:Age"] = this.Age.ToString();
      }
      return dictionary;
    }
  }
}
