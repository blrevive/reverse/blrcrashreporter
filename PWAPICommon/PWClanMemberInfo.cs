﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.PWClanMemberInfo
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Common;
using System;
using System.Runtime.InteropServices;
using System.Xml.Serialization;

namespace PWAPICommon
{
  [ComVisible(true)]
  public class PWClanMemberInfo : PWItemBase
  {
    [XmlAttribute("CP")]
    public byte ClanPosition;
    [XmlElement("PI")]
    public PWPlayerInfo PlayerInfo;

    public PWClanMemberInfo()
      : this(new PWPlayerInfo())
    {
    }

    public PWClanMemberInfo(PWPlayerInfo InPlayerInfo)
      : this(InPlayerInfo, EClanRank.CR_MEMBER)
    {
    }

    public PWClanMemberInfo(PWPlayerInfo InPlayerInfo, EClanRank InRank)
    {
      this.PlayerInfo = InPlayerInfo;
      this.ClanPosition = Convert.ToByte((object) InRank);
    }

    public override bool ParseFromArray(object[] InArray) => this.ValidatedAssign(ref this.PlayerInfo.UserID, InArray[0]) && this.ValidatedAssign<byte>(ref this.ClanPosition, InArray[2]);
  }
}
