﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.EMessageType
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System.Runtime.InteropServices;

namespace PWAPICommon
{
  [ComVisible(true)]
  public enum EMessageType
  {
    EMT_Hello = 0,
    EMT_Login = 1,
    EMT_ZPRemove = 2,
    EMT_QueryProfile = 3,
    EMT_WriteProfile = 4,
    EMT_ZPQuery = 5,
    EMT_QueryInventory = 6,
    EMT_WriteInventory = 7,
    EMT_QueryServerAvailability = 8,
    EMT_QueryStoreUser = 9,
    EMT_CreateStoreItem = 10, // 0x0000000A
    EMT_RemoveStoreItem = 11, // 0x0000000B
    EMT_UpdateStoreItem = 12, // 0x0000000C
    EMT_CreateInventory = 13, // 0x0000000D
    EMT_RemoveInventory = 14, // 0x0000000E
    EMT_UpdateInventory = 15, // 0x0000000F
    EMT_GPQuery = 16, // 0x00000010
    EMT_GPAdd = 17, // 0x00000011
    EMT_GPRemove = 18, // 0x00000012
    EMT_PurchaseItem = 19, // 0x00000013
    EMT_QueryAllFunds = 20, // 0x00000014
    EMT_QueryServers = 21, // 0x00000015
    EMT_QueryServerDetails = 22, // 0x00000016
    EMT_FindFriends = 23, // 0x00000017
    EMT_RegisterServer = 24, // 0x00000018
    EMT_UnregisterServer = 25, // 0x00000019
    EMT_UpdateServer = 26, // 0x0000001A
    EMT_WriteStats = 27, // 0x0000001B
    EMT_ActivateItem = 28, // 0x0000001C
    EMT_AddExperience = 29, // 0x0000001D
    EMT_ChangeName = 30, // 0x0000001E
    EMT_PlayerNameValidate = 31, // 0x0000001F
    EMT_QueryXP = 32, // 0x00000020
    EMT_Logout = 33, // 0x00000021
    EMT_MasterServerRegister = 34, // 0x00000022
    EMT_MasterServerUnregister = 35, // 0x00000023
    EMT_MasterServerUpdate = 36, // 0x00000024
    EMT_MasterServerCreateGame = 37, // 0x00000025
    EMT_MasterServerCreateGameComplete = 38, // 0x00000026
    EMT_QuerySkills = 39, // 0x00000027
    EMT_UpdateSkills = 40, // 0x00000028
    EMT_Chat = 41, // 0x00000029
    EMT_ClanValidateInfo = 42, // 0x0000002A
    EMT_Heartbeat = 43, // 0x0000002B
    EMT_GenderChange = 44, // 0x0000002C
    EMT_ChatChannel = 45, // 0x0000002D
    EMT_OfferQuery = 46, // 0x0000002E
    EMT_OfferCreate = 47, // 0x0000002F
    EMT_OfferUpdate = 48, // 0x00000030
    EMT_OfferRemove = 49, // 0x00000031
    EMT_RemoteCommand = 50, // 0x00000032
    EMT_InventoryUpdated = 51, // 0x00000033
    EMT_QueryVoice = 52, // 0x00000034
    EMT_InventoryRenew = 53, // 0x00000035
    EMT_InventoryDestroy = 54, // 0x00000036
    EMT_ClanMOTD = 55, // 0x00000037
    EMT_ClanCreate = 56, // 0x00000038
    EMT_ClanDisband = 57, // 0x00000039
    EMT_ClanInvite = 58, // 0x0000003A
    EMT_ClanInviteResponse = 59, // 0x0000003B
    EMT_ToggleProfileFlag = 60, // 0x0000003C
    EMT_QueryPlayerList = 61, // 0x0000003D
    EMT_TransactionAdd = 62, // 0x0000003E
    EMT_ClanMemberUpdate = 63, // 0x0000003F
    EMT_GetClanInfo = 64, // 0x00000040
    EMT_SetSocialInfo = 65, // 0x00000041
    EMT_GiftStoreItem = 66, // 0x00000042
    EMT_PlayerWhisper = 67, // 0x00000043
    EMT_MailCreate = 68, // 0x00000044
    EMT_MailDestroy = 69, // 0x00000045
    EMT_MailReceiveItems = 70, // 0x00000046
    EMT_MailQueryUser = 71, // 0x00000047
    EMT_MailMarkRead = 72, // 0x00000048
    EMT_RankedJoin = 73, // 0x00000049
    EMT_RankedLeave = 74, // 0x0000004A
    EMT_RankedStarted = 75, // 0x0000004B
    EMT_RankedStatReport = 76, // 0x0000004C
    EMT_RankedQuery = 77, // 0x0000004D
    EMT_RankedQueryTop = 78, // 0x0000004E
    EMT_ChatList = 79, // 0x0000004F
    EMT_MailNotification = 80, // 0x00000050
    EMT_ClanMemberQuery = 81, // 0x00000051
    EMT_TransactionQuery = 82, // 0x00000052
    EMT_AdminMessage = 83, // 0x00000053
    EMT_CreateCharacter = 84, // 0x00000054
    EMT_KickPlayer = 85, // 0x00000055
    EMT_ClientLog = 86, // 0x00000056
    EMT_ResetProfile = 87, // 0x00000057
    EMT_MasterServerGrow = 88, // 0x00000058
    EMT_ReadFile = 89, // 0x00000059
    EMT_QueryHealth = 90, // 0x0000005A
    EMT_FriendQuery = 91, // 0x0000005B
    EMT_FriendAdd = 92, // 0x0000005C
    EMT_FriendRemove = 93, // 0x0000005D
    EMT_FriendRespond = 94, // 0x0000005E
    EMT_FriendUpdate = 95, // 0x0000005F
    EMT_QueryBasicProfile = 96, // 0x00000060
    EMT_FuseInventory = 97, // 0x00000061
    EMT_FuseDatanodes = 98, // 0x00000062
    EMT_PlayerSearch = 99, // 0x00000063
    EMT_GameLog = 100, // 0x00000064
    EMT_StartMatch = 101, // 0x00000065
    EMT_EndMatch = 102, // 0x00000066
    EMT_QueryAccountIdByPlayerName = 103, // 0x00000067
    EMT_CheckLoginQueue = 104, // 0x00000068
    EMT_GiftOffer = 105, // 0x00000069
    EMT_InviteToParty = 106, // 0x0000006A
    EMT_PartyInviteResponse = 107, // 0x0000006B
    EMT_RemoveFromParty = 108, // 0x0000006C
    EMT_PartyMemberRemoved = 109, // 0x0000006D
    EMT_PartyListUpdate = 110, // 0x0000006E
    EMT_PromotePartyLeader = 111, // 0x0000006F
    EMT_PartyLeaderPromoted = 112, // 0x00000070
    EMT_PartyDisbanded = 113, // 0x00000071
    EMT_PartyGameSearch = 114, // 0x00000072
    EMT_PartyGameJoin = 115, // 0x00000073
    EMT_ClanGetTag = 116, // 0x00000074
    EMT_ClanJoin = 117, // 0x00000075
    EMT_ClanQuit = 118, // 0x00000076
    EMT_SetGameRules = 119, // 0x00000077
    EMT_InvalidateStoreCache = 120, // 0x00000078
    EMT_PresenceUpdate = 121, // 0x00000079
    EMT_ReadStatsForUser = 122, // 0x0000007A
    EMT_ClanKick = 123, // 0x0000007B
    EMT_ClanKickResponse = 124, // 0x0000007C
    EMT_FileUpload = 125, // 0x0000007D
    EMT_CreatePrivateMatch = 126, // 0x0000007E
    EMT_RouteMessage = 127, // 0x0000007F
    EMT_ZPAdd = 128, // 0x00000080
    EMT_GameInviteSend = 129, // 0x00000081
    EMT_GameInviteReceive = 130, // 0x00000082
    EMT_ClanRankChange = 131, // 0x00000083
    EMT_QueryIPLocation = 132, // 0x00000084
    EMT_PartySendPublicGameInvite = 133, // 0x00000085
    EMT_PartyReceivePublicGameInvite = 134, // 0x00000086
    EMT_FindInvItem = 135, // 0x00000087
    EMT_PremiumServerQuery = 136, // 0x00000088
    EMT_PremiumServerUpdate = 137, // 0x00000089
    EMT_PartyInviteReceive = 138, // 0x0000008A
    EMT_JoinChatChannel = 139, // 0x0000008B
    EMT_LeaveChatChannel = 140, // 0x0000008C
    EMT_SetConfig = 253, // 0x000000FD
    EMT_CrashReportID = 254, // 0x000000FE
    EMT_Invalid = 255, // 0x000000FF
    EMT_WebWrapper = 256, // 0x00000100
    EMT_SQLWrapper = 257, // 0x00000101
    EMT_OBJWrapper = 258, // 0x00000102
  }
}
