﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.RegistryWOW6432
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using Microsoft.Win32;
using System;
using System.Runtime.InteropServices;
using System.Text;

namespace PWAPICommon
{
  [ComVisible(true)]
  public static class RegistryWOW6432
  {
    public static UIntPtr HKEY_LOCAL_MACHINE = new UIntPtr(2147483650U);
    public static UIntPtr HKEY_CURRENT_USER = new UIntPtr(2147483649U);

    [DllImport("Advapi32.dll")]
    private static extern uint RegOpenKeyEx(
      UIntPtr hKey,
      string lpSubKey,
      uint ulOptions,
      int samDesired,
      out int phkResult);

    [DllImport("Advapi32.dll")]
    private static extern uint RegCloseKey(int hKey);

    [DllImport("Advapi32.dll")]
    public static extern int RegQueryValueEx(
      int hKey,
      string lpValueName,
      int lpReserved,
      ref uint lpType,
      StringBuilder lpData,
      ref uint lpcbData);

    public static string GetRegKey64(this RegistryKey inKey, string inPropertyName)
    {
      string str = inKey.ToString();
      string inString = str.Split('\\')[0];
      string inKeyName = str.Substring(str.IndexOf('\\') + 1);
      return RegistryWOW6432.GetRegKey64(RegistryWOW6432.GetRegHiveFromString(inString), inKeyName, RegistryWOW6432.RegSAM.WOW64_64Key, inPropertyName);
    }

    public static string GetRegKey32(this RegistryKey inKey, string inPropertyName)
    {
      string str = inKey.ToString();
      string inString = str.Split('\\')[0];
      string inKeyName = str.Substring(str.IndexOf('\\') + 1);
      return RegistryWOW6432.GetRegKey64(RegistryWOW6432.GetRegHiveFromString(inString), inKeyName, RegistryWOW6432.RegSAM.WOW64_32Key, inPropertyName);
    }

    private static UIntPtr GetRegHiveFromString(string inString)
    {
      if (inString == "HKEY_LOCAL_MACHINE")
        return RegistryWOW6432.HKEY_LOCAL_MACHINE;
      return inString == "HKEY_CURRENT_USER" ? RegistryWOW6432.HKEY_CURRENT_USER : UIntPtr.Zero;
    }

    public static string GetRegKey64(
      UIntPtr inHive,
      string inKeyName,
      RegistryWOW6432.RegSAM in32or64key,
      string inPropertyName)
    {
      int phkResult = 0;
      try
      {
        if (RegistryWOW6432.RegOpenKeyEx(inHive, inKeyName, 0U, (int) (RegistryWOW6432.RegSAM.QueryValue | in32or64key), out phkResult) != 0U)
          return (string) null;
        uint lpType = 0;
        uint lpcbData = 1024;
        StringBuilder lpData = new StringBuilder(1024);
        RegistryWOW6432.RegQueryValueEx(phkResult, inPropertyName, 0, ref lpType, lpData, ref lpcbData);
        return lpData.ToString();
      }
      finally
      {
        if (phkResult != 0)
        {
          int num = (int) RegistryWOW6432.RegCloseKey(phkResult);
        }
      }
    }

    public enum RegSAM
    {
      QueryValue = 1,
      SetValue = 2,
      CreateSubKey = 4,
      EnumerateSubKeys = 8,
      Notify = 16, // 0x00000010
      CreateLink = 32, // 0x00000020
      WOW64_64Key = 256, // 0x00000100
      WOW64_32Key = 512, // 0x00000200
      WOW64_Res = 768, // 0x00000300
      Write = 131078, // 0x00020006
      Execute = 131097, // 0x00020019
      Read = 131097, // 0x00020019
      AllAccess = 983103, // 0x000F003F
    }
  }
}
