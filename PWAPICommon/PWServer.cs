﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.PWServer
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Caches;
using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Queries;
using PWAPICommon.Resources;
using PWAPICommon.Servers;
using System;
using System.Collections.Concurrent;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Timers;

namespace PWAPICommon
{
  [ComVisible(true)]
  public class PWServer : PWServerBase
  {
    protected PWClientLocal LocalConnection;
    protected ConcurrentDictionary<EServerType, PWServerClient> InternalClients;
    protected ConcurrentDictionary<string, PWServerClient> ExternalClients;
    private PWLockFreeQueue<string> RemoteLogQueue;
    private EServerType ServerType;
    private bool bHasServerType;
    private Thread RemoteLogThread;

    public PWServer(
      int InMaxConnections,
      string InServerName,
      string InRegionName,
      int InGameID,
      int InServerID,
      PWServerBase.AddLogMessageCallback InCallback,
      ELoggingLevel InLogLevel,
      PWServerApp InApp)
      : base(InMaxConnections, InServerName, InRegionName, InGameID, InServerID, InLogLevel, InApp)
    {
      this.AddLogCallback(InCallback);
      this.LocalConnection = new PWClientLocal(this, (SendMessageCallback) null);
      this.RemoteLogQueue = new PWLockFreeQueue<string>();
      this.InternalClients = new ConcurrentDictionary<EServerType, PWServerClient>();
      this.ExternalClients = new ConcurrentDictionary<string, PWServerClient>();
      this.bHasServerType = false;
    }

    private void ProcessRemoteLogs(object StateObject)
    {
      Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
      EndPoint remoteEP = (EndPoint) (StateObject as IPEndPoint);
      UTF8Encoding utF8Encoding = new UTF8Encoding();
      Thread.CurrentThread.Name = "Remote Log Thread";
      while (true)
      {
        string s = (string) null;
        while (this.RemoteLogQueue.Dequeue(out s))
          socket.SendTo(utF8Encoding.GetBytes(s), remoteEP);
        Thread.Sleep(100);
      }
    }

    protected override void LogRemote(string InLogText) => this.RemoteLogQueue.Enqueue(PWServerBase.CurrentTime.ToString("yyyy-MM-dd HH:mm:ss") + " BL:R : " + InLogText);

    protected override void OnCheckHealth(object Source, ElapsedEventArgs args)
    {
      base.OnCheckHealth(Source, args);
      this.SetHealthString("Socket:Bind", "OK");
      this.SetHealthString("Socket:Listening", "OK");
      bool flag = false;
      if (!this.Started)
        return;
      if (this.MainSocket == null || !this.MainSocket.IsBound)
      {
        this.SetHealthString("Socket:Bind", "FAILED");
        this.Log("Socket is not bound!", (PWConnectionBase) null, false, ELoggingLevel.ELL_Critical);
        flag = true;
      }
      if (this.MainSocket != null && this.MainSocket.SocketType == SocketType.Stream)
      {
        TcpClient tcpClient = new TcpClient();
        if (this.Started)
        {
          try
          {
            tcpClient.Connect("localhost", this.TargetPort);
          }
          catch (Exception ex)
          {
            this.Log("Socket stopped listening, exception: " + ex.ToString(), (PWConnectionBase) null, false, ELoggingLevel.ELL_Errors);
            this.SetHealthString("Socket:Listening", "FAILED");
            flag = true;
          }
        }
        tcpClient.Close();
      }
      if (!flag)
        return;
      this.RestartServer();
    }

    public void ApplyServerType(EServerType InServerType)
    {
      this.ServerType = InServerType;
      this.bHasServerType = true;
    }

    public override bool SendQuery(PWRequestBase InRequest) => InRequest.SubmitServerQuery();

    public override bool SendExternalQuery(
      PWRequestBase InRequest,
      string InRegionName,
      int TimeoutSeconds,
      bool bWaitForResponse)
    {
      PWServerClient externalClient = this.GetExternalClient(InRegionName);
      if (externalClient == null)
        return false;
      externalClient.AddMessageMap(InRequest.MessageType, InRequest.GetType());
      if (!externalClient.WaitForConnect(TimeoutSeconds))
      {
        this.Log("Failed to Open Client Connection for " + this.ServerType.ToString() + " on Region " + InRegionName + ", Connection Timed Out", (PWConnectionBase) null, false, ELoggingLevel.ELL_Warnings);
        return false;
      }
      if (!externalClient.SendQuery(InRequest))
      {
        this.Log("Failed to Send Query to Client Connection for " + this.ServerType.ToString() + " on Region " + InRegionName, (PWConnectionBase) null, false, ELoggingLevel.ELL_Warnings);
        return false;
      }
      if (!bWaitForResponse || externalClient.WaitForQuery(InRequest, TimeoutSeconds))
        return true;
      this.Log("Failed to Send Query to Client Connection for " + this.ServerType.ToString() + " on Region " + InRegionName + ", Query Timed Out", (PWConnectionBase) null, false, ELoggingLevel.ELL_Warnings);
      return false;
    }

    public override bool SendInternalQuery(
      PWRequestBase InRequest,
      EServerType InServerType,
      int TimeoutSeconds,
      bool bWaitForResponse)
    {
      if (InServerType != this.ServerType)
      {
        PWServerClient internalClient = this.GetInternalClient(InServerType);
        if (internalClient != null)
        {
          internalClient.AddMessageMap(InRequest.MessageType, InRequest.GetType());
          if (!internalClient.WaitForConnect(TimeoutSeconds))
          {
            this.Log("Failed to Open Client Connection for " + InServerType.ToString() + " on Region " + this.ServerRegionName + ", Connection Timed Out", (PWConnectionBase) null, false, ELoggingLevel.ELL_Warnings);
            return false;
          }
          if (!internalClient.SendQuery(InRequest))
          {
            this.Log("Failed to Send Query To Client Connection for " + InServerType.ToString() + " on Region " + this.ServerRegionName, (PWConnectionBase) null, false, ELoggingLevel.ELL_Warnings);
            return false;
          }
          if (!bWaitForResponse || internalClient.WaitForQuery(InRequest, TimeoutSeconds))
            return true;
          this.Log("Failed to Send Query To Client Connection for " + InServerType.ToString() + " on Region " + this.ServerRegionName + ", Query Timed Out", (PWConnectionBase) null, false, ELoggingLevel.ELL_Warnings);
          return false;
        }
      }
      return false;
    }

    private PWServerClient SpawnClient(string InRegionName, EServerType InServerType)
    {
      try
      {
        return new PWServerClient(InRegionName, InServerType);
      }
      catch (Exception ex)
      {
        this.Log("Failed to Open Client Connection for " + InServerType.ToString() + " on Region " + InRegionName + ", Invalid Endpoint", (PWConnectionBase) null, false, ELoggingLevel.ELL_Warnings);
        return (PWServerClient) null;
      }
    }

    private PWServerClient GetInternalClient(EServerType InServerType)
    {
      PWServerClient pwServerClient = (PWServerClient) null;
      PWRegionMap.RegionInfo OutInfo;
      if (PWRegionMap.GetRegionInfo(this.ServerRegionName, out OutInfo) && (!this.InternalClients.TryGetValue(InServerType, out pwServerClient) || !pwServerClient.Connected))
      {
        pwServerClient = this.SpawnClient(OutInfo.InternalName, InServerType);
        if (pwServerClient != null)
        {
          pwServerClient = this.InternalClients.GetOrAdd(InServerType, pwServerClient);
          lock (pwServerClient)
          {
            if (!pwServerClient.Started)
              pwServerClient.StartServer();
          }
        }
      }
      return pwServerClient;
    }

    private PWServerClient GetExternalClient(string InRegionName)
    {
      PWServerClient pwServerClient = (PWServerClient) null;
      if (!this.bHasServerType || this.ExternalClients.TryGetValue(InRegionName, out pwServerClient) && pwServerClient.Connected)
        return pwServerClient;
      if (pwServerClient == null)
        pwServerClient = this.SpawnClient(InRegionName, this.ServerType);
      if (pwServerClient != null)
      {
        pwServerClient = this.ExternalClients.GetOrAdd(InRegionName, pwServerClient);
        lock (pwServerClient)
        {
          if (!pwServerClient.Started)
            pwServerClient.StartServer();
        }
      }
      return pwServerClient;
    }

    public override void StartServer()
    {
      base.StartServer();
      string OutValue1;
      int OutValue2;
      if (this.GetConfigValue<string>("LogIP", out OutValue1) && this.GetConfigValue<int>("LogPort", out OutValue2))
      {
        IPEndPoint ipEndPoint = new IPEndPoint(this.GetIPAddressFromString(OutValue1), OutValue2);
        if (this.RemoteLogThread == null)
          this.RemoteLogThread = new Thread(new ParameterizedThreadStart(this.ProcessRemoteLogs));
        this.RemoteLogThread.Start((object) ipEndPoint);
      }
      this.StartListening();
    }

    public override void StopServer(string Reason)
    {
      base.StopServer(Reason);
      if (this.RemoteLogThread == null)
        return;
      this.RemoteLogThread.Abort();
      this.RemoteLogThread = (Thread) null;
    }

    protected override void SetFrozen(bool bNewValue)
    {
      base.SetFrozen(bNewValue);
      this.Log("Server Setting Frozen state to: " + bNewValue.ToString(), (PWConnectionBase) null, false, ELoggingLevel.ELL_Informative);
      if (bNewValue)
      {
        this.DestroyMainSocket();
      }
      else
      {
        this.CreateMainSocket();
        this.StartListening();
      }
    }

    protected virtual void StartListening()
    {
      try
      {
        this.MainSocket.Listen(5000);
        this.Log("Socket Listening on Port " + (object) this.Port, (PWConnectionBase) null, false, ELoggingLevel.ELL_Errors);
        this.StartAccept(this.GetConnection());
      }
      catch (Exception ex)
      {
        this.Log("StartListening Exception: " + ex.ToString(), (PWConnectionBase) null, false, ELoggingLevel.ELL_Critical);
      }
    }

    private void StartAccept(SocketAsyncEventArgs AcceptArgs)
    {
      try
      {
        this.Log("Socket Starting Accept", (PWConnectionBase) null, false, ELoggingLevel.ELL_Verbose);
        AcceptArgs.AcceptSocket = (Socket) null;
        AcceptArgs.SetBuffer((byte[]) null, 0, 0);
        if (this.MainSocket == null)
          return;
        lock (this.MainSocket)
        {
          if (this.MainSocket.AcceptAsync(AcceptArgs))
            return;
          ThreadPool.QueueUserWorkItem(new WaitCallback(this.FinishAccept), (object) AcceptArgs);
          this.StartAccept(this.GetConnection());
        }
      }
      catch (Exception ex)
      {
        this.Log("StartAccept Exception: " + ex.ToString(), (PWConnectionBase) null, false, ELoggingLevel.ELL_Critical);
      }
    }

    private void FinishAccept(object StateObject)
    {
      SocketAsyncEventArgs args = (SocketAsyncEventArgs) StateObject;
      this.Log("FinishAccept " + (object) args.LastOperation, (PWConnectionBase) null, false, ELoggingLevel.ELL_Verbose);
      this.FinishAcceptInternal(args);
    }

    protected virtual PWConnectionBase FinishAcceptInternal(
      SocketAsyncEventArgs args)
    {
      this.Log("New Connection!", (PWConnectionBase) null, false, ELoggingLevel.ELL_Verbose);
      PWConnectionBase pwConnectionBase = (PWConnectionBase) null;
      Socket acceptSocket = args.AcceptSocket;
      if (acceptSocket != null)
      {
        try
        {
          this.Log("Socket Finishing Accept on: " + acceptSocket.ToString(), (PWConnectionBase) null, false, ELoggingLevel.ELL_Verbose);
          pwConnectionBase = this.ConnectionEstablished(acceptSocket);
          args.UserToken = (object) pwConnectionBase;
          PWConnectionTCPBase connectionTcpBase = pwConnectionBase as PWConnectionTCPBase;
          if (args.BytesTransferred > 0)
            connectionTcpBase.FinishReceive(args);
          else
            connectionTcpBase.StartReceive(args);
        }
        catch (Exception ex)
        {
          PWConnectionBase userToken = args.UserToken as PWConnectionBase;
          this.Log("Exception when processing data received:" + ex.ToString(), userToken, false, ELoggingLevel.ELL_Errors);
          this.FreeConnection(args);
        }
      }
      else
      {
        this.Log("Null Socket when accepting!", (PWConnectionBase) null, false, ELoggingLevel.ELL_Errors);
        this.FreeConnection(args);
      }
      return pwConnectionBase;
    }

    protected override void OnIOCompleted(object sender, SocketAsyncEventArgs e)
    {
      object userToken = e.UserToken;
      if (e.LastOperation == SocketAsyncOperation.Accept)
      {
        this.StartAccept(this.GetConnection());
        ThreadPool.QueueUserWorkItem(new WaitCallback(this.FinishAccept), (object) e);
      }
      else
        base.OnIOCompleted(sender, e);
    }

    protected virtual PWConnectionBase ConnectionEstablished(Socket s) => (PWConnectionBase) new PWConnectionTCPServer((PWServerBase) this, s);

    public void AddDBConnection(string InConnectionString, string InDBName)
    {
      this.Log("Adding Database Connection to " + InDBName, (PWConnectionBase) null, false, ELoggingLevel.ELL_Informative);
      PWResourceSQL pwResourceSql = new PWResourceSQL(InConnectionString, this, InDBName);
      this.AddResource(InDBName, (IPWResource) pwResourceSql);
    }

    public void AddWebCert(
      string InCertDir,
      string InCertFile,
      string InCertPass,
      string InCertName)
    {
      ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
      ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(PWCertPolicy.CertificateValidationCallBack);
      ServicePointManager.Expect100Continue = true;
      try
      {
        this.Log("Adding Cert File: " + InCertDir + "\\" + InCertFile, (PWConnectionBase) null, false, ELoggingLevel.ELL_Informative);
        PW509Cert pw509Cert = new PW509Cert(InCertDir + "\\" + InCertFile, InCertPass);
        if (pw509Cert == null)
          return;
        this.AddResource(InCertName, (IPWResource) pw509Cert);
      }
      catch (Exception ex)
      {
        this.Log("AddWebCert Exception: " + ex.ToString(), (PWConnectionBase) null, false, ELoggingLevel.ELL_Critical);
      }
    }

    public PWSuperServer AddSuperServer()
    {
      this.Log("Adding Master Server Connection", (PWConnectionBase) null, false, ELoggingLevel.ELL_Informative);
      PWSuperServer pwSuperServer = this.GetResource<PWSuperServer>();
      if (pwSuperServer == null)
      {
        pwSuperServer = new PWSuperServer(this);
        this.AddResource("SuperServer", (IPWResource) pwSuperServer);
      }
      return pwSuperServer;
    }

    public PWTransactionCache AddTransactionCache()
    {
      this.Log("Adding Transaction Cache", (PWConnectionBase) null, false, ELoggingLevel.ELL_Informative);
      PWTransactionCache transactionCache = this.GetResource<PWTransactionCache>();
      if (transactionCache == null)
      {
        int OutValue1 = 0;
        int OutValue2 = 0;
        int OutValue3 = 0;
        this.GetConfigValue<int>("MaxPopItemAge", out OutValue1, 14);
        this.GetConfigValue<int>("MaxPopItems", out OutValue2, 12);
        this.GetConfigValue<int>("MaxTransactions", out OutValue3, 10000);
        transactionCache = new PWTransactionCache((PWServerBase) this, (PWConnectionBase) this.LocalConnection, OutValue3, OutValue1, OutValue2);
        this.AddResource("TransCache", (IPWResource) transactionCache);
      }
      return transactionCache;
    }

    public PWOfferCache AddOfferCache()
    {
      this.Log("Adding Offer Cache", (PWConnectionBase) null, false, ELoggingLevel.ELL_Informative);
      PWOfferCache pwOfferCache = this.GetResource<PWOfferCache>();
      if (pwOfferCache == null)
      {
        pwOfferCache = new PWOfferCache((PWServerBase) this, (PWConnectionBase) this.LocalConnection);
        this.AddResource("OfferCache", (IPWResource) pwOfferCache);
      }
      return pwOfferCache;
    }

    public PWPremiumMatchCache AddPMCache()
    {
      this.Log("Adding PMCache Cache", (PWConnectionBase) null, false, ELoggingLevel.ELL_Informative);
      PWPremiumMatchCache premiumMatchCache = this.GetResource<PWPremiumMatchCache>();
      if (premiumMatchCache == null)
      {
        premiumMatchCache = new PWPremiumMatchCache((PWServerBase) this, (PWConnectionBase) this.LocalConnection);
        this.AddResource("PMCache", (IPWResource) premiumMatchCache);
      }
      return premiumMatchCache;
    }

    public PWRankedManager AddRankedManager()
    {
      PWRankedManager pwRankedManager = new PWRankedManager((PWServerBase) this, (PWConnectionBase) this.LocalConnection);
      this.AddResource("RankedManager", (IPWResource) pwRankedManager);
      return pwRankedManager;
    }

    public PWPrivateServerManager AddPrivateServerManager()
    {
      PWPrivateServerManager privateServerManager = new PWPrivateServerManager((PWServerBase) this, (PWConnectionBase) this.LocalConnection);
      this.AddResource("PrivateServerManager", (IPWResource) privateServerManager);
      return privateServerManager;
    }

    public PWPremiumServerManager AddPremiumServerManager()
    {
      PWPremiumServerManager premiumServerManager = new PWPremiumServerManager((PWServerBase) this, (PWConnectionBase) this.LocalConnection);
      this.AddResource("PremiumServerManager", (IPWResource) premiumServerManager);
      return premiumServerManager;
    }

    public PWStoreCache AddStoreCache()
    {
      this.Log("Adding Store Cache", (PWConnectionBase) null, false, ELoggingLevel.ELL_Informative);
      PWStoreCache pwStoreCache = this.GetResource<PWStoreCache>();
      if (pwStoreCache == null)
      {
        pwStoreCache = new PWStoreCache((PWServerBase) this, (PWConnectionBase) this.LocalConnection);
        this.AddResource("StoreCache", (IPWResource) pwStoreCache);
      }
      return pwStoreCache;
    }

    public PWClanCache AddClanCache(bool WantsMemberQueries = true)
    {
      PWClanCache pwClanCache = this.GetResource<PWClanCache>();
      if (pwClanCache == null)
      {
        this.Log("Adding clan cache", (PWConnectionBase) null, false, ELoggingLevel.ELL_Informative);
        pwClanCache = new PWClanCache((PWServerBase) this, (PWConnectionBase) this.LocalConnection, WantsMemberQueries);
        this.AddResource("ClanCache", (IPWResource) pwClanCache);
      }
      return pwClanCache;
    }
  }
}
