﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.PWServerClient
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Queries;
using PWAPICommon.Queries.General;
using PWAPICommon.Servers;
using System;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Threading;
using System.Timers;

namespace PWAPICommon
{
  [ComVisible(true)]
  public class PWServerClient : PWServerBase
  {
    private IPEndPoint RemoteEndPoint;
    public PWConnectionTCPClient Connection;
    public PWServerClient.FinishConnectDelegate OnFinishConnect;
    private bool bFinishedConnect;
    public bool bWaitForHeartbeatResponse;
    private EServerType ConnectedServerType;
    private System.Timers.Timer HeartbeatTimer;

    public EServerType ServerType => this.ConnectedServerType;

    public string EndPoint => this.RemoteEndPoint.ToString();

    public bool Connected => this.MainSocket != null && this.MainSocket.Connected && this.Connection != null && this.bFinishedConnect;

    public PWServerClient(string InRegionName, EServerType InServerIndex)
      : base(1000, "Client", "", 1001, 0, ELoggingLevel.ELL_Informative, (PWServerApp) null)
    {
      this.ConnectedServerType = InServerIndex;
      this.bFinishedConnect = false;
      PWRegionMap.RegionInfo OutInfo;
      if (PWRegionMap.GetRegionInfo(InRegionName, out OutInfo))
        this.RemoteEndPoint = OutInfo.GetDNSInfo(InServerIndex).Location;
      this.bWaitForHeartbeatResponse = false;
      this.HeartbeatTimer = new System.Timers.Timer(60000.0);
      this.HeartbeatTimer.Elapsed += new ElapsedEventHandler(this.OnHeartbeatTimer);
    }

    public PWServerClient(string InHost, int InPort)
      : base(1000, "Client", "", 1001, 0, ELoggingLevel.ELL_Informative, (PWServerApp) null)
    {
      this.bFinishedConnect = false;
      try
      {
        this.RemoteEndPoint = new IPEndPoint(this.GetIPAddressFromString(InHost), InPort);
      }
      catch (Exception ex)
      {
        this.Log("PWServerClient Failed to resolve Host: " + InHost + " Port: " + InPort.ToString() + " Exception: " + ex.ToString(), (PWConnectionBase) null, false, ELoggingLevel.ELL_Errors);
      }
      this.bWaitForHeartbeatResponse = false;
      this.HeartbeatTimer = new System.Timers.Timer(60000.0);
      this.HeartbeatTimer.Elapsed += new ElapsedEventHandler(this.OnHeartbeatTimer);
    }

    public PWServerClient(IPEndPoint InEndPoint)
      : base(1000, "Client", "", 1001, 0, ELoggingLevel.ELL_Informative, (PWServerApp) null)
    {
      this.RemoteEndPoint = InEndPoint;
      this.bWaitForHeartbeatResponse = false;
      this.HeartbeatTimer = new System.Timers.Timer(60000.0);
      this.HeartbeatTimer.Elapsed += new ElapsedEventHandler(this.OnHeartbeatTimer);
    }

    public override void StartServer()
    {
      base.StartServer();
      SocketAsyncEventArgs connection = this.GetConnection();
      connection.UserToken = (object) this.Connection;
      connection.RemoteEndPoint = (System.Net.EndPoint) this.RemoteEndPoint;
      this.DestroyMainSocket();
      this.CreateMainSocket();
      this.MainSocket.ConnectAsync(connection);
    }

    protected override void OnCheckHealth(object Source, ElapsedEventArgs args)
    {
      base.OnCheckHealth(Source, args);
      if (this.MainSocket == null)
        return;
      bool blocking = this.MainSocket.Blocking;
      bool flag = false;
      try
      {
        byte[] buffer = new byte[1];
        this.MainSocket.Blocking = false;
        this.MainSocket.Send(buffer, 0, SocketFlags.None);
        this.MainSocket.Blocking = blocking;
      }
      catch (SocketException ex)
      {
        if (!ex.NativeErrorCode.Equals(10035))
        {
          this.Log("Socket no longer connected with Exception: " + ex.ToString(), (PWConnectionBase) null, false, ELoggingLevel.ELL_Errors);
          flag = true;
        }
      }
      catch (Exception ex)
      {
        this.Log("Socket no longer connected with Exception: " + ex.ToString(), (PWConnectionBase) null, false, ELoggingLevel.ELL_Errors);
        flag = true;
      }
      if (!flag && this.MainSocket.Connected)
        return;
      this.Reconnect();
    }

    protected void Reconnect()
    {
      this.Log("Attempting to reconnect to " + this.RemoteEndPoint.ToString(), (PWConnectionBase) null, false, ELoggingLevel.ELL_Errors);
      this.StopServer("Disconnected");
      this.StartServer();
    }

    public bool WaitForConnect(int TimeoutSeconds)
    {
      Stopwatch stopwatch = new Stopwatch();
      stopwatch.Start();
      while (!this.Connected)
      {
        Thread.Sleep(100);
        if (stopwatch.Elapsed.TotalSeconds > (double) TimeoutSeconds)
          return false;
      }
      stopwatch.Stop();
      return true;
    }

    protected override void OnIOCompleted(object sender, SocketAsyncEventArgs e)
    {
      object userToken = e.UserToken;
      if (e.LastOperation == SocketAsyncOperation.Connect)
        this.FinishConnect(e);
      else
        base.OnIOCompleted(sender, e);
    }

    private void FinishConnect(SocketAsyncEventArgs InArgs)
    {
      if (this.MainSocket == null || !this.MainSocket.Connected || InArgs.SocketError != SocketError.Success)
      {
        this.Log("Socket Connection Failed!", (PWConnectionBase) null, false, ELoggingLevel.ELL_Errors);
        this.FreeConnection(InArgs);
      }
      else
      {
        this.RefreshConnectionList();
        this.HeartbeatTimer.Start();
        this.Connection = new PWConnectionTCPClient((PWServerBase) this, this.MainSocket);
        SocketAsyncEventArgs connection = this.GetConnection();
        connection.SetBuffer(new byte[1024], 0, 1024);
        connection.UserToken = (object) this.Connection;
        this.StartReceive(connection);
        this.SendQuery((PWRequestBase) new PWHandshakeReq((PWConnectionBase) this.Connection));
        this.SendHeartbeat();
        this.bFinishedConnect = true;
        if (this.OnFinishConnect != null)
          this.OnFinishConnect(this.MainSocket.Connected);
        this.Log("Socket Connection Completed Successfully", (PWConnectionBase) null, false, ELoggingLevel.ELL_Errors);
      }
    }

    private void OnHeartbeatTimer(object Source, ElapsedEventArgs args) => this.SendHeartbeat();

    private void SendHeartbeat()
    {
      PWHeartbeatReq pwHeartbeatReq = new PWHeartbeatReq((PWConnectionBase) this.Connection);
      if (this.bWaitForHeartbeatResponse)
        this.SendQuery((PWRequestBase) pwHeartbeatReq);
      else
        pwHeartbeatReq.SubmitClientQuery();
    }

    public override bool SendQuery(PWRequestBase InRequest)
    {
      if (this.Connection == null || InRequest == null)
        return false;
      this.AddMessageMap(InRequest.MessageType, InRequest.GetType());
      InRequest.Connection = (PWConnectionBase) this.Connection;
      InRequest.Connection.WaitForResponse(InRequest);
      return InRequest.SubmitClientQuery();
    }

    public override bool CancelQuery(PWRequestBase InRequest) => this.Connection.CancelQuery(InRequest);

    public bool WaitForQuery(PWRequestBase InRequest, int TimeoutSeconds)
    {
      Stopwatch stopwatch = new Stopwatch();
      stopwatch.Start();
      while (!InRequest.Completed)
      {
        Thread.Sleep(100);
        if (TimeoutSeconds > 0 && stopwatch.Elapsed.TotalSeconds > (double) TimeoutSeconds)
          return false;
      }
      stopwatch.Stop();
      return true;
    }

    public delegate void FinishConnectDelegate(bool bSuccess);
  }
}
