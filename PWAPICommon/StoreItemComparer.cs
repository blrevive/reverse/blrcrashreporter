﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.StoreItemComparer
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Common;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace PWAPICommon
{
  [ComVisible(true)]
  public class StoreItemComparer : IEqualityComparer<StoreItem>
  {
    public bool Equals(StoreItem InA, StoreItem InB) => InA.ItemGuid == InB.ItemGuid;

    public int GetHashCode(StoreItem InObj) => object.ReferenceEquals((object) InObj, (object) null) || object.ReferenceEquals((object) InObj.ItemGuid, (object) null) ? 0 : InObj.ItemGuid.GetHashCode();
  }
}
