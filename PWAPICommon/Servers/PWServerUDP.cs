﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.Servers.PWServerUDP
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Connections;
using PWAPICommon.Resources;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Threading;

namespace PWAPICommon.Servers
{
  [ComVisible(true)]
  public class PWServerUDP : PWServer, IPWResource
  {
    private PWConnectionUDPServer UDPConnection;

    public PWServerUDP(
      int InMaxConnections,
      string InServerName,
      string InRegionName,
      int InGameID,
      int InServerID,
      PWServerBase.AddLogMessageCallback InCallback,
      ELoggingLevel InLogLevel,
      PWServerApp InApp)
      : base(InMaxConnections, InServerName, InRegionName, InGameID, InServerID, InCallback, InLogLevel, InApp)
    {
    }

    public void SetTargetPort(int Port) => this.TargetPort = Port;

    public void StartResource()
    {
      if (this.IsRunning)
        return;
      this.StartServer();
      this.MainSocket.IOControl(-1744830452, new byte[1], new byte[1]);
    }

    public void StopResource()
    {
      if (!this.IsRunning)
        return;
      this.StopServer("Resource Told to Stop");
    }

    public Dictionary<string, string> CheckHealth()
    {
      Dictionary<string, string> dictionary = new Dictionary<string, string>();
      dictionary["UDP:Bind"] = "OK";
      dictionary["UDP:Listening"] = "OK";
      if (this.MainSocket != null && !this.MainSocket.IsBound)
      {
        dictionary["UDP:Bind"] = "FAILED";
        this.Log("Socket is not bound!", (PWConnectionBase) null, false, ELoggingLevel.ELL_Critical);
      }
      dictionary["UDP:Conn"] = this.NumConnections.ToString();
      UdpClient udpClient = new UdpClient();
      if (this.Started)
      {
        try
        {
          udpClient.Connect("localhost", this.TargetPort);
        }
        catch (Exception ex)
        {
          this.Log("UDP Socket stopped listening, exception: " + ex.ToString(), (PWConnectionBase) null, false, ELoggingLevel.ELL_Errors);
          this.SetHealthString("UDP:Listening", "FAILED");
          if (this.Started)
          {
            this.Log("Attempting to restart listening on port " + (object) this.TargetPort, (PWConnectionBase) null, false, ELoggingLevel.ELL_Errors);
            this.DestroyMainSocket();
            this.CreateMainSocket();
            this.StartListening();
          }
        }
      }
      udpClient.Close();
      return dictionary;
    }

    protected override void CreateMainSocket()
    {
      this.MainSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
      this.MainSocket.Bind((EndPoint) new IPEndPoint(IPAddress.Any, this.TargetPort));
    }

    protected override void OnIOCompleted(object sender, SocketAsyncEventArgs e)
    {
      this.Log("UDP OnIOCompleted " + (object) e.LastOperation, (PWConnectionBase) null, false, ELoggingLevel.ELL_Verbose);
      object userToken = e.UserToken;
      switch (e.LastOperation)
      {
        case SocketAsyncOperation.Receive:
          this.FinishReceive((object) e);
          break;
        case SocketAsyncOperation.ReceiveFrom:
          this.StartReceive(this.GetConnection());
          ThreadPool.QueueUserWorkItem(new WaitCallback(this.FinishReceive), (object) e);
          break;
        case SocketAsyncOperation.SendTo:
          this.FinishSend(e);
          break;
        default:
          throw new ArgumentException("The last operation completed on the socket was not a receive or send");
      }
    }

    public override void StartReceive(SocketAsyncEventArgs ReceiveArgs)
    {
      try
      {
        ReceiveArgs.SetBuffer(new byte[2048], 0, 2048);
        ReceiveArgs.RemoteEndPoint = (EndPoint) new IPEndPoint(IPAddress.Any, this.TargetPort);
        ReceiveArgs.UserToken = (object) null;
        if (this.MainSocket == null)
          return;
        lock (this.MainSocket)
        {
          if (this.MainSocket.ReceiveFromAsync(ReceiveArgs))
            return;
          ThreadPool.QueueUserWorkItem(new WaitCallback(this.FinishReceive), (object) ReceiveArgs);
          this.StartReceive(this.GetConnection());
        }
      }
      catch (Exception ex)
      {
        this.Log("UDP StartReceive Exception: " + ex.ToString(), (PWConnectionBase) null, false, ELoggingLevel.ELL_Critical);
      }
    }

    protected override void StartListening()
    {
      this.UDPConnection = new PWConnectionUDPServer((PWServerBase) this, this.MainSocket);
      this.StartReceive(this.GetConnection());
    }

    private void FinishReceive(object StateObject)
    {
      SocketAsyncEventArgs args = StateObject as SocketAsyncEventArgs;
      this.Log("UDP Finished InitialReceive " + (object) args.LastOperation, (PWConnectionBase) null, false, ELoggingLevel.ELL_Verbose);
      this.FinishReceiveInternal(args);
    }

    protected virtual void FinishReceiveInternal(SocketAsyncEventArgs args)
    {
      try
      {
        IPEndPoint remoteEndPoint = (IPEndPoint) args.RemoteEndPoint;
        if (remoteEndPoint == null)
          return;
        this.Log("Finished a receive on: " + remoteEndPoint.ToString(), (PWConnectionBase) null, false, ELoggingLevel.ELL_Verbose);
        if (args.BytesTransferred <= 0)
          return;
        byte[] InData = new byte[args.BytesTransferred];
        Array.Copy((Array) args.Buffer, (Array) InData, args.BytesTransferred);
        this.UDPConnection.ReceivedData(InData, args.RemoteEndPoint as IPEndPoint);
      }
      catch (Exception ex)
      {
        PWConnectionBase userToken = args.UserToken as PWConnectionBase;
        this.Log("UDP Exception when proceessing data received: " + ex.ToString(), userToken, false, ELoggingLevel.ELL_Errors);
      }
      finally
      {
        this.FreeConnection(args);
      }
    }
  }
}
