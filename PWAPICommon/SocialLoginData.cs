﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.SocialLoginData
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Queries;
using PWAPICommon.Queries.Clan;
using PWAPICommon.Queries.Friends;
using PWAPICommon.Queries.General;
using PWAPICommon.Queries.Player;
using PWAPICommon.Resources;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace PWAPICommon
{
  [ComVisible(true)]
  public class SocialLoginData
  {
    public PWConnectionBase Connection;
    public PWPlayerInfo PlayerInfo;
    public PartyInfo Party;
    public PWClanInfo ClanInfo;
    public List<ChatRoom> ListeningRooms;
    public ClanChatRoom ClanChat;
    private PWFriendQueryInternalReq FriendQueryReq;
    private PWProfileQueryBasicReq BasicProfileReq;

    public SocialLoginData() => this.PlayerInfo = new PWPlayerInfo();

    public SocialLoginData(PWConnectionBase InConnection)
      : this()
    {
      this.Connection = InConnection;
      this.ListeningRooms = new List<ChatRoom>();
    }

    public override string ToString()
    {
      string str = this.PlayerInfo.UserName + ":" + Convert.ToString(this.PlayerInfo.UserID);
      if (this.ClanInfo != null)
        str = str + ":" + this.ClanInfo.ToString();
      return str;
    }

    public void UpdateLocation(SocialStatus InStatus)
    {
      this.PlayerInfo.OnlineStatus = InStatus;
      this.Connection.OwningServer.GetResource<PWPresenceCache<SocialStatus>>()?.ApplyPresence(this.PlayerInfo.UserID, EPresenceUpdateType.EPUT_Update, this.PlayerInfo.OnlineStatus);
      this.RefreshOurDataToFriendsAndClan();
      if (this.Party == null)
        return;
      if (InStatus.StatusContext == (byte) 0)
      {
        PWSocialServer pwSocialServer = this.Connection.OwningServer != null ? (PWSocialServer) this.Connection.OwningServer : (PWSocialServer) null;
        if (pwSocialServer == null)
          return;
        int num = (int) pwSocialServer.RemoveMemberFromParty(this, this.PlayerInfo.UserID);
      }
      else
        this.RefreshDataToOurParty();
    }

    public void RefreshDataToOurParty()
    {
      PWSocialServer pwSocialServer = this.Connection.OwningServer != null ? (PWSocialServer) this.Connection.OwningServer : (PWSocialServer) null;
      if (pwSocialServer == null || this.Party == null)
        return;
      pwSocialServer.PushUpdatedPlayerToParty(this);
    }

    public void RefreshOurDataToClan(EClanMemberUpdateType UpdateType = EClanMemberUpdateType.ECMUT_Presence)
    {
      PWSocialServer pwSocialServer = this.Connection.OwningServer != null ? (PWSocialServer) this.Connection.OwningServer : (PWSocialServer) null;
      if (pwSocialServer == null || this.ClanInfo == null || this.ClanChat == null)
        return;
      pwSocialServer.SendClanMemberUpdate(this, UpdateType);
    }

    public void RefreshOurDataToFriendsAndClan()
    {
      if (this.BasicProfileReq != null)
        return;
      this.BasicProfileReq = new PWProfileQueryBasicReq(this.Connection);
      this.BasicProfileReq.ForUserID = this.PlayerInfo.UserID;
      this.BasicProfileReq.ServerResultProcessor = new ProcessDelegate(this.OnBasicProfileQueriedForUpdate);
      this.BasicProfileReq.SubmitServerQuery();
    }

    private bool OnBasicProfileQueriedForUpdate(PWRequestBase Wrapper)
    {
      if (this.BasicProfileReq == null || !this.BasicProfileReq.Successful)
        return false;
      if (this.PlayerInfo != null)
        this.PlayerInfo.CopyFromUserProfile(this.BasicProfileReq.BasicProfile);
      this.RefreshOurDataToClan();
      if (this.FriendQueryReq == null)
      {
        this.FriendQueryReq = new PWFriendQueryInternalReq(this.Connection);
        this.FriendQueryReq.UserId = this.PlayerInfo.UserID;
        this.FriendQueryReq.ServerResultProcessor = new ProcessDelegate(this.OnFriendListQueriedForUpdate);
        this.FriendQueryReq.SubmitServerQuery();
      }
      return true;
    }

    private bool OnFriendListQueriedForUpdate(PWRequestBase Wrapper)
    {
      if (this.FriendQueryReq == null || !this.FriendQueryReq.Successful)
        return false;
      foreach (PWFriendItem allFriend in this.FriendQueryReq.AllFriends)
        new PWSingleFriendUpdatePushReq(this.Connection, allFriend.OwnerID == this.PlayerInfo.UserID ? allFriend.FriendID : allFriend.OwnerID, new PWFriendItem()
        {
          State = allFriend.State,
          FriendID = allFriend.FriendID,
          OwnerID = allFriend.OwnerID,
          PlayerInfo = this.PlayerInfo
        }).SubmitServerQuery();
      this.FriendQueryReq = (PWFriendQueryInternalReq) null;
      this.BasicProfileReq = (PWProfileQueryBasicReq) null;
      return true;
    }
  }
}
