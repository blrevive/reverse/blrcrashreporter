﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.PWVoiceServer
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using PWAPICommon.Resources;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace PWAPICommon
{
  [ComVisible(true)]
  public class PWVoiceServer : IPWResource
  {
    private List<VirtualServer> VirtualServers;
    private ConcurrentDictionary<PWConnectionBase, int> CachedVoiceConnections;
    public int PortRangeStart;
    private PWServerBase OwningServer;
    private server_callback_struct TSCallbacks;

    [DllImport("ts3server_win32.dll", CallingConvention = CallingConvention.Cdecl)]
    public static extern uint ts3server_initServerLib(
      ref server_callback_struct arg0,
      LogTypes arg1,
      string arg2);

    [DllImport("ts3server_win32.dll", CallingConvention = CallingConvention.Cdecl)]
    public static extern uint ts3server_getServerLibVersion(out IntPtr arg0);

    [DllImport("ts3server_win32.dll", CallingConvention = CallingConvention.Cdecl)]
    public static extern uint ts3server_freeMemory(IntPtr arg0);

    [DllImport("ts3server_win32.dll", CallingConvention = CallingConvention.Cdecl)]
    public static extern uint ts3server_destroyServerLib();

    [DllImport("ts3server_win32.dll", CallingConvention = CallingConvention.Cdecl)]
    public static extern uint ts3server_getGlobalErrorMessage(
      uint errorcode,
      out IntPtr errormessage);

    [DllImport("ts3server_win32.dll", CallingConvention = CallingConvention.Cdecl)]
    public static extern uint ts3server_getClientVariableAsString(
      ulong ServerID,
      ushort ClientID,
      ClientProperties Flag,
      out IntPtr Result);

    [DllImport("ts3server_win32.dll", CallingConvention = CallingConvention.Cdecl)]
    public static extern unsafe uint ts3server_getVirtualServerList(ulong** Result);

    [DllImport("ts3server_win32.dll", CallingConvention = CallingConvention.Cdecl)]
    public static extern unsafe uint ts3server_getClientList(ulong ServerID, ushort** Result);

    [DllImport("ts3server_win32.dll", CallingConvention = CallingConvention.Cdecl)]
    public static extern unsafe uint ts3server_getChannelList(ulong ServerID, ulong** Result);

    [DllImport("ts3server_win32.dll", CallingConvention = CallingConvention.Cdecl)]
    public static extern unsafe uint ts3server_getChannelClientList(
      ulong ServerID,
      ulong ChannelID,
      ushort** Result);

    public PWVoiceServer(PWServerBase InOwningServer)
    {
      this.OwningServer = InOwningServer;
      this.CachedVoiceConnections = new ConcurrentDictionary<PWConnectionBase, int>();
      this.VirtualServers = new List<VirtualServer>();
    }

    public void Log(
      string LogMessage,
      PWConnectionBase InConnection,
      bool bLogToRemoteServer,
      ELoggingLevel InLogLevel)
    {
      this.OwningServer.Log(LogMessage, InConnection, bLogToRemoteServer, InLogLevel);
    }

    public string GetGlobalErrorMessage(uint ErrorType)
    {
      string str = "";
      IntPtr errormessage = IntPtr.Zero;
      int globalErrorMessage = (int) PWVoiceServer.ts3server_getGlobalErrorMessage(ErrorType, out errormessage);
      if (ErrorType == 0U)
      {
        str = Marshal.PtrToStringAnsi(errormessage);
        int num = (int) PWVoiceServer.ts3server_freeMemory(errormessage);
      }
      return str;
    }

    private int GenerateHandlerID(int ServerIndex, int ChannelIndex) => (ServerIndex << 8 | ChannelIndex) + 1;

    private void GetServerAndChannelFromHandlerID(
      int HandlerID,
      out int ServerIndex,
      out int ChannelIndex)
    {
      ServerIndex = HandlerID - 1 >> 8;
      ChannelIndex = HandlerID - 1 & (int) byte.MaxValue;
    }

    public void DoServerCreate(
      PWConnectionBase Connection,
      out int HandlerID,
      out char[] ChannelName,
      out int Port)
    {
      HandlerID = -1;
      ChannelName = (char[]) null;
      Port = -1;
      if (Connection == null)
        this.Log("Null connection when creating server", (PWConnectionBase) null, false, ELoggingLevel.ELL_Warnings);
      else if (this.VirtualServers == null)
        this.Log("Null VirtualServers when creating server", (PWConnectionBase) null, false, ELoggingLevel.ELL_Warnings);
      else if (this.CachedVoiceConnections.ContainsKey(Connection))
      {
        this.Log("Attempted to re-open connection for handler ID " + this.CachedVoiceConnections[Connection].ToString(), (PWConnectionBase) null, false, ELoggingLevel.ELL_Warnings);
      }
      else
      {
        lock (this.VirtualServers)
        {
          for (int index = 0; index < this.VirtualServers.Count; ++index)
          {
            VirtualServer virtualServer = this.VirtualServers[index];
            if (!virtualServer.AreAllChannelsActive)
            {
              int availableChannelIndex = virtualServer.GetAvailableChannelIndex();
              if (availableChannelIndex >= 0)
              {
                virtualServer.CreateChannel(availableChannelIndex);
                HandlerID = this.GenerateHandlerID(index, availableChannelIndex);
                Port = this.VirtualServers[index].Port;
                int num = availableChannelIndex;
                this.Log("Created virtual server with handler id " + HandlerID.ToString() + " on port " + Port.ToString() + " and channel index " + num.ToString(), (PWConnectionBase) null, false, ELoggingLevel.ELL_Informative);
                ChannelName = ("ch" + num.ToString()).ToCharArray();
                this.CachedVoiceConnections[Connection] = HandlerID;
                break;
              }
            }
          }
        }
      }
    }

    public void DoServerDestroy(int HandlerID)
    {
      int ServerIndex = 0;
      int ChannelIndex = 0;
      if (HandlerID == 0)
        return;
      this.GetServerAndChannelFromHandlerID(HandlerID, out ServerIndex, out ChannelIndex);
      lock (this.VirtualServers)
      {
        if (ServerIndex >= this.VirtualServers.Count || ChannelIndex >= 100)
          return;
        this.Log("Killing virtual server for handler ID " + HandlerID.ToString(), (PWConnectionBase) null, false, ELoggingLevel.ELL_Informative);
        this.VirtualServers[ServerIndex].KillChannel(ChannelIndex);
      }
    }

    public void onTSUserLoggingMessageEvent(
      string TheMessage,
      int InTSLogLevel,
      string logChannel,
      ulong logID,
      string logTime,
      string completeLogString)
    {
      ELoggingLevel InLogLevel = ELoggingLevel.ELL_Warnings;
      switch (InTSLogLevel)
      {
        case 0:
          InLogLevel = ELoggingLevel.ELL_Errors;
          break;
        case 1:
        case 2:
          InLogLevel = ELoggingLevel.ELL_Warnings;
          break;
        case 3:
          InLogLevel = ELoggingLevel.ELL_Verbose;
          break;
        case 4:
          InLogLevel = ELoggingLevel.ELL_Informative;
          break;
      }
      this.Log("TeamSpeak Log: " + TheMessage, (PWConnectionBase) null, false, InLogLevel);
    }

    public void NotifyDisconnected(PWConnectionBase InClientConnection)
    {
      if (InClientConnection != null)
      {
        int HandlerID = -1;
        if (!this.CachedVoiceConnections.TryRemove(InClientConnection, out HandlerID))
          return;
        this.Log("voice Disconnected. Closing connection handler ID " + HandlerID.ToString(), InClientConnection, false, ELoggingLevel.ELL_Verbose);
        this.DoServerDestroy(HandlerID);
      }
      else
        this.Log("Disconnecting client that did not have a virtual server open " + InClientConnection.Name, (PWConnectionBase) null, false, ELoggingLevel.ELL_Verbose);
    }

    public virtual void StartResource()
    {
      this.Log("Initializing voice server with port range starting at " + (object) this.PortRangeStart, (PWConnectionBase) null, false, ELoggingLevel.ELL_Informative);
      this.TSCallbacks = new server_callback_struct();
      this.TSCallbacks.onUserLoggingMessageEvent_delegate = new onUserLoggingMessageEvent_type(this.onTSUserLoggingMessageEvent);
      this.TSCallbacks.onClientConnected_delegate = (onClientConnected_type) null;
      this.TSCallbacks.onClientDisconnected_delegate = (onClientDisconnected_type) null;
      this.TSCallbacks.onClientMoved_delegate = (onClientMoved_type) null;
      this.TSCallbacks.onChannelCreated_delegate = (onChannelCreated_type) null;
      this.TSCallbacks.onChannelEdited_delegate = (onChannelEdited_type) null;
      this.TSCallbacks.onChannelDeleted_delegate = (onChannelDeleted_type) null;
      this.TSCallbacks.onServerTextMessageEvent_delegate = (onServerTextMessageEvent_type) null;
      this.TSCallbacks.onChannelTextMessageEvent_delegate = (onChannelTextMessageEvent_type) null;
      this.TSCallbacks.onClientStartTalkingEvent_delegate = (onClientStartTalkingEvent_type) null;
      this.TSCallbacks.onClientStopTalkingEvent_delegate = (onClientStopTalkingEvent_type) null;
      this.TSCallbacks.onAccountingErrorEvent_delegate = (onAccountingErrorEvent_type) null;
      uint ErrorType = PWVoiceServer.ts3server_initServerLib(ref this.TSCallbacks, LogTypes.LogType_FILE | LogTypes.LogType_CONSOLE | LogTypes.LogType_USERLOGGING, (string) null);
      if (ErrorType != 0U)
      {
        this.Log("Error initializing teamspeak server library: " + this.GetGlobalErrorMessage(ErrorType), (PWConnectionBase) null, false, ELoggingLevel.ELL_Errors);
      }
      else
      {
        int OutValue;
        this.OwningServer.GetConfigValue<int>("MaxVoipServers", out OutValue, 1);
        for (int InServerIndex = 0; InServerIndex < OutValue; ++InServerIndex)
          this.VirtualServers.Add(new VirtualServer(this, this.PortRangeStart + InServerIndex, InServerIndex));
      }
    }

    public virtual void StopResource()
    {
      this.Log("Stopping voice server", (PWConnectionBase) null, false, ELoggingLevel.ELL_Informative);
      foreach (VirtualServer virtualServer in this.VirtualServers)
      {
        if (virtualServer.ServerID != 0UL)
          virtualServer.DestroyServer();
      }
      uint ErrorType = PWVoiceServer.ts3server_destroyServerLib();
      if (ErrorType == 0U)
        return;
      this.Log("Failed to destroy server lib " + this.GetGlobalErrorMessage(ErrorType), (PWConnectionBase) null, false, ELoggingLevel.ELL_Warnings);
    }

    private unsafe List<ulong> ConvertToList(ulong* InArray)
    {
      List<ulong> ulongList = new List<ulong>();
      if ((IntPtr) InArray != IntPtr.Zero)
      {
        for (int index = 0; InArray[index] != 0UL; ++index)
        {
          ulong num = InArray[index];
          ulongList.Add(num);
        }
      }
      return ulongList;
    }

    private unsafe List<ushort> ConvertToList(ushort* InArray)
    {
      List<ushort> ushortList = new List<ushort>();
      if ((IntPtr) InArray != IntPtr.Zero)
      {
        for (int index = 0; InArray[index] != (ushort) 0; ++index)
        {
          ushort num = InArray[index];
          ushortList.Add(num);
        }
      }
      return ushortList;
    }

    private unsafe List<ulong> GetAllServers()
    {
      List<ulong> ulongList = new List<ulong>();
      ulong* InArray = (ulong*) null;
      if (PWVoiceServer.ts3server_getVirtualServerList(&InArray) == 0U)
      {
        ulongList = this.ConvertToList(InArray);
        int num = (int) PWVoiceServer.ts3server_freeMemory((IntPtr) (void*) InArray);
      }
      return ulongList;
    }

    private unsafe List<ulong> GetChannelsInServer(ulong InServerID)
    {
      List<ulong> ulongList = new List<ulong>();
      ulong* InArray = (ulong*) null;
      if (PWVoiceServer.ts3server_getChannelList(InServerID, &InArray) == 0U)
      {
        ulongList = this.ConvertToList(InArray);
        int num = (int) PWVoiceServer.ts3server_freeMemory((IntPtr) (void*) InArray);
      }
      return ulongList;
    }

    private unsafe List<ushort> GetUsersInChannel(ulong InServerID, ulong InChannelID)
    {
      List<ushort> ushortList = new List<ushort>();
      ushort* InArray = (ushort*) null;
      if (PWVoiceServer.ts3server_getChannelClientList(InServerID, InChannelID, &InArray) == 0U)
      {
        ushortList = this.ConvertToList(InArray);
        int num = (int) PWVoiceServer.ts3server_freeMemory((IntPtr) (void*) InArray);
      }
      return ushortList;
    }

    private unsafe List<ushort> GetUsers(ulong InServerID)
    {
      List<ushort> ushortList = new List<ushort>();
      ushort* InArray = (ushort*) null;
      if (PWVoiceServer.ts3server_getClientList(InServerID, &InArray) == 0U)
      {
        ushortList = this.ConvertToList(InArray);
        int num = (int) PWVoiceServer.ts3server_freeMemory((IntPtr) (void*) InArray);
      }
      return ushortList;
    }

    private string GetClientVariable(ulong ServerID, ushort ClientID, ClientProperties Flag)
    {
      IntPtr Result = IntPtr.Zero;
      string str = "NULL";
      if (PWVoiceServer.ts3server_getClientVariableAsString(ServerID, ClientID, Flag, out Result) == 0U)
      {
        str = Marshal.PtrToStringAnsi(Result);
        int num = (int) PWVoiceServer.ts3server_freeMemory(Result);
      }
      return str;
    }

    public Dictionary<string, string> CheckHealth()
    {
      Dictionary<string, string> dictionary = new Dictionary<string, string>();
      if (this.VirtualServers != null && this.VirtualServers.Count > 0)
      {
        foreach (ulong allServer in this.GetAllServers())
        {
          List<ulong> channelsInServer = this.GetChannelsInServer(allServer);
          this.GetUsers(allServer);
          foreach (ulong InChannelID in channelsInServer)
          {
            foreach (ushort ClientID in this.GetUsersInChannel(allServer, InChannelID))
              dictionary["s" + (object) allServer + ":c" + (object) InChannelID + ":u" + (object) ClientID] = this.GetClientVariable(allServer, ClientID, ClientProperties.CLIENT_NICKNAME);
          }
        }
        for (int index = 0; index < this.VirtualServers.Count; ++index)
        {
          VirtualServer virtualServer = this.VirtualServers[index];
          dictionary["Server " + (object) index + " ChannelNum"] = virtualServer.NumChannels.ToString();
        }
      }
      return dictionary;
    }
  }
}
