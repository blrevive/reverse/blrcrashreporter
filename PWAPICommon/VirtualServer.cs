﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.VirtualServer
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Clients;
using System;
using System.Runtime.InteropServices;

namespace PWAPICommon
{
  [ComVisible(true)]
  public class VirtualServer
  {
    public const int MAX_CHANNELS_PER_SERVER = 100;
    public const uint MAX_REMOTE_TALKERS = 32;
    public PWVoiceServer Parent;
    private byte NumChannelsUsed;
    private byte[] ChannelData;
    private ulong[] Channels;
    public ulong ServerID;
    public int Port;
    public int ServerIndex;

    [DllImport("ts3server_win32.dll", CallingConvention = CallingConvention.Cdecl)]
    public static extern uint ts3server_freeMemory(IntPtr arg0);

    [DllImport("ts3server_win32.dll", CallingConvention = CallingConvention.Cdecl)]
    public static extern uint ts3server_createVirtualServer(
      int serverPort,
      string ip,
      string serverName,
      string serverKeyPair,
      uint serverMaxClients,
      out ulong serverID);

    [DllImport("ts3server_win32.dll", CallingConvention = CallingConvention.Cdecl)]
    public static extern uint ts3server_getGlobalErrorMessage(
      uint errorcode,
      out IntPtr errormessage);

    [DllImport("ts3server_win32.dll", CallingConvention = CallingConvention.Cdecl)]
    public static extern uint ts3server_getVirtualServerKeyPair(ulong serverID, out IntPtr result);

    [DllImport("ts3server_win32.dll", CallingConvention = CallingConvention.Cdecl)]
    public static extern uint ts3server_setVirtualServerVariableAsString(
      ulong serverID,
      VirtualServerProperties flag,
      string result);

    [DllImport("ts3server_win32.dll", CallingConvention = CallingConvention.Cdecl)]
    public static extern uint ts3server_flushVirtualServerVariable(ulong serverID);

    [DllImport("ts3server_win32.dll", CallingConvention = CallingConvention.Cdecl)]
    public static extern uint ts3server_stopVirtualServer(ulong serverID);

    [DllImport("ts3server_win32.dll", CallingConvention = CallingConvention.Cdecl)]
    public static extern uint ts3server_setChannelVariableAsInt(
      ulong serverID,
      ulong channelID,
      ChannelProperties flag,
      int Value);

    [DllImport("ts3server_win32.dll", CallingConvention = CallingConvention.Cdecl)]
    public static extern uint ts3server_setChannelVariableAsString(
      ulong serverID,
      ulong channelID,
      ChannelProperties flag,
      string result);

    [DllImport("ts3server_win32.dll", CallingConvention = CallingConvention.Cdecl)]
    public static extern uint ts3server_flushChannelCreation(
      ulong serverID,
      ulong ChannelParentID,
      out ulong Result);

    [DllImport("ts3server_win32.dll", CallingConvention = CallingConvention.Cdecl)]
    public static extern uint ts3server_channelDelete(ulong serverID, ulong ChannelId, int Force);

    public VirtualServer()
    {
    }

    public VirtualServer(PWVoiceServer InParent, int InPort, int InServerIndex)
    {
      this.NumChannelsUsed = (byte) 0;
      this.ChannelData = new byte[13];
      for (int index = 0; index < 13; ++index)
        this.ChannelData[index] = (byte) 0;
      this.Channels = new ulong[100];
      for (int index = 0; index < 100; ++index)
        this.Channels[index] = 0UL;
      this.Parent = InParent;
      this.ServerID = 0UL;
      this.Port = InPort;
      this.ServerIndex = InServerIndex;
      uint virtualServer = VirtualServer.ts3server_createVirtualServer(this.Port, "0.0.0.0", "PWTSServer" + this.ServerIndex.ToString(), "", 32U, out this.ServerID);
      if (virtualServer == 0U)
        return;
      this.Parent.Log("Error creating virtual server at index " + this.ServerIndex.ToString() + ": " + this.GetGlobalErrorMessage(virtualServer), (PWConnectionBase) null, false, ELoggingLevel.ELL_Warnings);
    }

    public bool HasActiveChannels => this.NumChannelsUsed != (byte) 0;

    public int NumChannels => (int) this.NumChannelsUsed;

    public bool AreAllChannelsActive => this.NumChannelsUsed == (byte) 100;

    public int GetAvailableChannelIndex()
    {
      for (int ChannelIndex = 0; ChannelIndex < 100; ++ChannelIndex)
      {
        if (!this.IsChannelUsed(ChannelIndex))
          return ChannelIndex;
      }
      return -1;
    }

    private void SetChannelUsed(int ChannelIndex)
    {
      int index = ChannelIndex >> 3;
      int num = 1 << ChannelIndex - (index << 3);
      this.ChannelData[index] |= (byte) num;
    }

    private void ClearChannelUsed(int ChannelIndex)
    {
      int index = ChannelIndex >> 3;
      int num = 1 << ChannelIndex - (index << 3);
      this.ChannelData[index] &= (byte) ~num;
    }

    public bool IsChannelUsed(int ChannelIndex)
    {
      int index = ChannelIndex >> 3;
      int num = 1 << ChannelIndex - (index << 3);
      return ((int) this.ChannelData[index] & (int) (byte) num) > 0;
    }

    public string GetGlobalErrorMessage(uint ErrorType)
    {
      string str = "";
      IntPtr errormessage = IntPtr.Zero;
      int globalErrorMessage = (int) VirtualServer.ts3server_getGlobalErrorMessage(ErrorType, out errormessage);
      if (ErrorType == 0U)
      {
        str = Marshal.PtrToStringAnsi(errormessage);
        int num = (int) VirtualServer.ts3server_freeMemory(errormessage);
      }
      return str;
    }

    public void CreateChannel(int ChannelIndex)
    {
      if (this.NumChannelsUsed >= (byte) 100)
        this.Parent.Log("Failed to create channel at index " + ChannelIndex.ToString() + " for server index " + this.ServerIndex.ToString(), (PWConnectionBase) null, false, ELoggingLevel.ELL_Errors);
      this.SetChannelUsed(ChannelIndex);
      ++this.NumChannelsUsed;
      int num1 = (int) VirtualServer.ts3server_setChannelVariableAsString(this.ServerID, 0UL, ChannelProperties.CHANNEL_NAME, "ch" + ChannelIndex.ToString());
      int num2 = (int) VirtualServer.ts3server_setChannelVariableAsInt(this.ServerID, 0UL, ChannelProperties.CHANNEL_FLAG_PERMANENT, 1);
      int num3 = (int) VirtualServer.ts3server_setChannelVariableAsInt(this.ServerID, 0UL, ChannelProperties.CHANNEL_FLAG_SEMI_PERMANENT, 0);
      int num4 = (int) VirtualServer.ts3server_setChannelVariableAsInt(this.ServerID, 0UL, ChannelProperties.CHANNEL_FLAG_DEFAULT, 0);
      int num5 = (int) VirtualServer.ts3server_flushChannelCreation(this.ServerID, 0UL, out this.Channels[ChannelIndex]);
      this.Parent.Log("Created channel of ID " + this.Channels[ChannelIndex].ToString(), (PWConnectionBase) null, false, ELoggingLevel.ELL_Informative);
    }

    public void KillChannel(int ChannelIndex)
    {
      this.ClearChannelUsed(ChannelIndex);
      --this.NumChannelsUsed;
      uint ErrorType = VirtualServer.ts3server_channelDelete(this.ServerID, this.Channels[ChannelIndex], 1);
      if (ErrorType == 0U)
        return;
      this.Parent.Log("Error deleting channelID " + this.Channels[ChannelIndex].ToString() + ": " + this.GetGlobalErrorMessage(ErrorType), (PWConnectionBase) null, false, ELoggingLevel.ELL_Warnings);
    }

    public void DestroyServer()
    {
      if (this.ServerID == 0UL)
        return;
      uint ErrorType = VirtualServer.ts3server_stopVirtualServer(this.ServerID);
      if (ErrorType != 0U)
        this.Parent.Log("Error stopping virtual server" + this.ServerID.ToString() + ": " + this.GetGlobalErrorMessage(ErrorType), (PWConnectionBase) null, false, ELoggingLevel.ELL_Warnings);
      this.NumChannelsUsed = (byte) 0;
      for (int index = 0; index < 13; ++index)
        this.ChannelData[index] = (byte) 0;
      for (int index = 0; index < 100; ++index)
        this.Channels[index] = 0UL;
      this.ServerID = 0UL;
    }
  }
}
