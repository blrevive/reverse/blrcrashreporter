﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.PWServerStats
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Resources;
using PWAPICommon.Servers;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Timers;

namespace PWAPICommon
{
  [ComVisible(true)]
  public class PWServerStats : IPWResource, IHttpResource
  {
    private ConcurrentDictionary<EMessageType, PWServerStats.MessageStat> MessageStats;
    private DateTime ServerStartTime;
    private System.Timers.Timer PollTimer;
    private PWServerBase OwningServer;
    private long Downloaded;
    private long Uploaded;
    private long NumEvents;
    private List<PWServerStats.ConnectionHistoryNode> ConnectionHistory;
    private int MaxConnections;

    public TimeSpan Uptime => PWServerBase.CurrentTime - this.ServerStartTime;

    public DateTime StartTime => this.ServerStartTime;

    public int NumMaxConnections => this.MaxConnections;

    public List<PWServerStats.ConnectionHistoryNode> History => this.ConnectionHistory;

    public object this[int index] => (object) this.MessageStats.ElementAt<KeyValuePair<EMessageType, PWServerStats.MessageStat>>(index);

    public ConcurrentDictionary<EMessageType, PWServerStats.MessageStat> Stats => this.MessageStats;

    public long TotalDownload => this.Downloaded;

    public long TotalUploaded => this.Uploaded;

    public long TotalEvents => this.NumEvents;

    public PWServerStats(PWServerBase InServer)
    {
      this.MessageStats = new ConcurrentDictionary<EMessageType, PWServerStats.MessageStat>();
      this.ConnectionHistory = new List<PWServerStats.ConnectionHistoryNode>();
      this.ServerStartTime = PWServerBase.CurrentTime;
      this.OwningServer = InServer;
      this.PollTimer = new System.Timers.Timer(60000.0);
      this.PollTimer.Elapsed += new ElapsedEventHandler(this.OnPollTimer);
      this.PollTimer.Start();
      foreach (EMessageType key in InServer.MessagesAccepted.Keys)
        this.MessageStats[key] = new PWServerStats.MessageStat();
    }

    public void MessageSent(EMessageType InMessageType, long MessageLength)
    {
      PWServerStats.MessageStat stat = this.GetStat(InMessageType);
      Interlocked.Increment(ref stat.NumSent);
      Interlocked.Add(ref stat.BytesSent, MessageLength);
      Interlocked.Add(ref this.Uploaded, MessageLength);
      Interlocked.Increment(ref this.NumEvents);
    }

    public void MessageInvalid(EMessageType InMessageType) => Interlocked.Increment(ref this.GetStat(InMessageType).NumInvalid);

    public void MessageFailed(EMessageType InMessageType) => Interlocked.Increment(ref this.GetStat(InMessageType).NumFailed);

    public void MessageDuration(EMessageType InMessageType, TimeSpan InDuration)
    {
      PWServerStats.MessageStat stat = this.GetStat(InMessageType);
      lock (stat)
        stat.TotalTime += InDuration;
    }

    public void MessageTransactionStart(EMessageType InMessageType) => Interlocked.Add(ref this.GetStat(InMessageType).NumTransacting, 1L);

    public void MessageTransactionEnd(EMessageType InMessageType) => Interlocked.Decrement(ref this.GetStat(InMessageType).NumTransacting);

    public void MessageCreated(EMessageType InMessageType) => Interlocked.Increment(ref this.GetStat(InMessageType).NumCreated);

    public void MessageReceived(EMessageType InMessageType, long MessageLength)
    {
      PWServerStats.MessageStat stat = this.GetStat(InMessageType);
      Interlocked.Increment(ref stat.NumReceived);
      Interlocked.Add(ref stat.BytesReceived, MessageLength);
      Interlocked.Add(ref this.Downloaded, MessageLength);
      Interlocked.Increment(ref this.NumEvents);
    }

    public PWServerStats.MessageStat GetStat(EMessageType InMessageType) => this.MessageStats.GetOrAdd(InMessageType, new PWServerStats.MessageStat());

    public void LogConnections(int CurrentConnectionNum)
    {
      this.ConnectionHistory.Add(new PWServerStats.ConnectionHistoryNode(CurrentConnectionNum));
      this.MaxConnections = Math.Max(this.MaxConnections, CurrentConnectionNum);
    }

    private void OnPollTimer(object Source, ElapsedEventArgs args) => this.LogConnections(this.OwningServer.NumConnections);

    public virtual void StartResource()
    {
    }

    public virtual void StopResource()
    {
    }

    Dictionary<string, string> IPWResource.CheckHealth() => new Dictionary<string, string>();

    public List<HttpData> GetWebData()
    {
      List<HttpData> httpDataList = new List<HttpData>();
      HttpData httpData;
      httpData.SectionName = "Server Stats";
      string str1 = "" + "<table border=\"0\">" + "<tr><td><b>Message Type</b></td><td><b>Num Created</b></td><td><b>Bytes Sent</b></td><td><b>Bytes Received</b></td><td><b>Num Failed</b></td><td><b>Num Invalid</b></td><td><b>Num Succeeded</b></td><td><b>Num Transacting</b></td><td><b>Total Time</b></td></tr>";
      foreach (KeyValuePair<EMessageType, PWServerStats.MessageStat> stat in this.Stats)
      {
        PWServerStats.MessageStat messageStat = stat.Value;
        str1 = str1 + "<tr><td>" + stat.Key.ToString() + "</td><td>" + messageStat.NumCreated.ToString() + "</td><td>" + messageStat.BytesSent.ToString() + "</td><td>" + messageStat.BytesReceived.ToString() + "</td><td>" + messageStat.NumFailed.ToString() + "</td><td>" + messageStat.NumInvalid.ToString() + "</td><td>" + messageStat.NumSucceeded.ToString() + "</td><td>" + messageStat.NumTransacting.ToString() + "</td><td>" + messageStat.TotalTime.ToString() + "</td></tr>";
      }
      string str2 = str1 + "</table></p>";
      httpData.SectionData = str2;
      httpDataList.Add(httpData);
      return httpDataList;
    }

    public class MessageStat
    {
      public long BytesSent;
      public long BytesReceived;
      public long NumFailed;
      public long NumInvalid;
      public long NumSent;
      public long NumReceived;
      public long NumCreated;
      public long NumTransacting;
      public TimeSpan TotalTime;

      public long NumSucceeded => this.NumReceived + this.NumSent - this.NumFailed;
    }

    public struct ConnectionHistoryNode
    {
      public int NumConnections;
      private DateTime TimeStamp;

      public ConnectionHistoryNode(int InNum)
      {
        this.NumConnections = InNum;
        this.TimeStamp = PWServerBase.CurrentTime;
      }
    }
  }
}
