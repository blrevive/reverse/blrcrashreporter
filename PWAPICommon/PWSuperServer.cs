﻿// Decompiled with JetBrains decompiler
// Type: PWAPICommon.PWSuperServer
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using PWAPICommon.Caches;
using PWAPICommon.Clients;
using PWAPICommon.Common;
using PWAPICommon.Queries.Backend;
using PWAPICommon.Queries.Matchmaking;
using PWAPICommon.Resources;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Timers;

namespace PWAPICommon
{
  [ComVisible(true)]
  public class PWSuperServer : IPWResource
  {
    private Dictionary<string, string> CurrentGameRules;
    private PWServer OwningServer;
    private List<MasterServerInfo> MasterServers;
    private List<PWSuperServer.PendingCreate> PendingCreateRequests;
    private List<PWSuperServer.PendingRegister> PendingRegisterRequests;
    private ConcurrentDictionary<long, PWConnectionBase> LoggedInPlayers;
    private int NextGameServerID;
    private int NextMasterServerID;
    private System.Timers.Timer ReportTimer;
    private ReaderWriterLockSlim MasterLock;
    private ReaderWriterLockSlim ServerListLock;
    private float PlayerLoadThreshold;
    private PWSuperServer.MonitorStats HealthStats;
    private RefreshGenericListCallback RefreshCallback;
    private RefreshGenericListCallback RefreshMasterCallback;
    private byte[] PreserializedServerList;
    private System.Timers.Timer ServerListTimer;

    public IList<MasterServerInfo> CurrentMasterServers => (IList<MasterServerInfo>) this.MasterServers;

    public PWSuperServer(PWServer InServer)
    {
      int num = 1;
      PWRegionMap.RegionInfo OutInfo;
      if (PWRegionMap.GetRegionInfo(InServer.ServerRegionName, out OutInfo))
        num = OutInfo.ID;
      this.NextGameServerID = 1000 * num;
      this.NextMasterServerID = 10000;
      this.OwningServer = InServer;
      this.MasterServers = new List<MasterServerInfo>(10);
      this.PendingCreateRequests = new List<PWSuperServer.PendingCreate>(5);
      this.PendingRegisterRequests = new List<PWSuperServer.PendingRegister>(10);
      this.LoggedInPlayers = new ConcurrentDictionary<long, PWConnectionBase>();
      this.MasterLock = new ReaderWriterLockSlim();
      this.ServerListLock = new ReaderWriterLockSlim();
      this.OwningServer.GetConfigValue<float>("SuperLoadThreshold", out this.PlayerLoadThreshold);
      this.CurrentGameRules = new Dictionary<string, string>();
      this.ReportTimer = new System.Timers.Timer(120000.0);
      this.ReportTimer.Elapsed += new ElapsedEventHandler(this.OnReportTimer);
      this.PreserializedServerList = (byte[]) null;
      this.ServerListTimer = new System.Timers.Timer(5000.0);
      this.ServerListTimer.Elapsed += new ElapsedEventHandler(this.GenerateServerListData);
    }

    public void AddRefreshCallback(RefreshGenericListCallback InCallback) => this.RefreshCallback += InCallback;

    public void AddRefreshMasterCallback(RefreshGenericListCallback InCallback) => this.RefreshMasterCallback += InCallback;

    public void NotifyDisconnected(PWConnectionBase InConnection)
    {
      MasterServerInfo masterServer1 = this.FindMasterServer(InConnection);
      this.RemoveLoggedInPlayerByConn(InConnection);
      if (masterServer1 != null)
      {
        this.Log("Warning: MasterServer Disconnected! " + masterServer1.ToString(), InConnection, ELoggingLevel.ELL_Errors);
        this.RemoveMaster(masterServer1);
        this.RefreshServers();
      }
      else
      {
        this.MasterLock.EnterReadLock();
        foreach (MasterServerInfo masterServer2 in this.MasterServers)
        {
          PWServerInfoRunning server = masterServer2.FindServer(InConnection);
          if (server != null)
          {
            masterServer2.RemoveServer(InConnection);
            this.OwningServer.Log("GameServerStop EndPoint=" + InConnection.EndPoint + " AdvertisedIP=" + server.BaseInfo.PublicIP + ":" + (object) server.BaseInfo.GamePort + " ServerID=" + (object) server.BaseInfo.ServerID + " ServerName=\"" + server.BaseInfo.BaseInfo.ServerName + "\" ServerType=" + (object) server.BaseInfo.GameServerType + " Reason=Disconnected", InConnection, true, ELoggingLevel.ELL_Informative);
            this.RefreshServers();
            break;
          }
        }
        this.MasterLock.ExitReadLock();
      }
      this.OwningServer.GetResource<PWRankedManager>()?.NotifyDisconnected(InConnection);
      this.OwningServer.GetResource<PWPrivateServerManager>()?.NotifyDisconnected(InConnection);
    }

    public bool RegisterMasterServer(PWRegisterMasterServerReq InRequest, out int OutServerID)
    {
      MasterServerInfo masterServer = this.FindMasterServer(InRequest.MachineId);
      if (masterServer != null)
      {
        OutServerID = masterServer.ServerID;
        if (masterServer.Connection == null)
        {
          this.Log("Master server Reconnected!", InRequest.Connection, ELoggingLevel.ELL_Warnings);
          masterServer.Connection = InRequest.Connection;
          return true;
        }
        this.Log("Master server already registered", InRequest.Connection, ELoggingLevel.ELL_Warnings);
        return true;
      }
      this.Log("Master server registered for External IP " + InRequest.ExternalIP, InRequest.Connection, ELoggingLevel.ELL_Informative);
      OutServerID = this.NextMasterServerID++;
      this.AddMaster(new MasterServerInfo(InRequest.Connection, InRequest.MachineId, OutServerID, InRequest.ExternalIP, InRequest.RegionTag));
      this.Refreshmasters();
      if ((double) InRequest.LatOverride != 0.0 && (double) InRequest.LongOverride != 0.0)
        InRequest.Connection.GeoLoc = new GeoLocation((double) InRequest.LatOverride, (double) InRequest.LongOverride);
      else
        new PWQueryIPInfoDBReq(InRequest.Connection)
        {
          InIP = InRequest.ExternalIP
        }.SubmitServerQuery();
      return true;
    }

    public bool UnregisterMasterServer(PWUnregisterMasterServerReq InRequest)
    {
      MasterServerInfo masterServer = this.FindMasterServer(InRequest.MachineId);
      if (masterServer != null)
      {
        this.Log("Master Server Unregistered on Machine " + (object) InRequest.MachineId, InRequest.Connection, ELoggingLevel.ELL_Informative);
        this.RemoveMaster(masterServer);
        this.Refreshmasters();
        return true;
      }
      this.Log("Server not registered", InRequest.Connection, ELoggingLevel.ELL_Warnings);
      return false;
    }

    public bool UpdateMasterServer(PWMasterServerUpdateReq InRequest)
    {
      MasterServerInfo masterServer = this.FindMasterServer(InRequest.MachineId);
      if (masterServer == null)
        return false;
      masterServer.UpdateStats(InRequest.InStats);
      this.Refreshmasters();
      return true;
    }

    public bool RegisterGameServer(
      PWServerInfoFull InInfo,
      PWMachineID InMachineID,
      PWConnectionBase InConnection)
    {
      MasterServerInfo masterServer = this.FindMasterServer(InMachineID);
      if (masterServer != null)
      {
        if (masterServer.FindServer(InConnection) != null)
        {
          this.Log("Server already registered", InConnection, ELoggingLevel.ELL_Warnings);
          return true;
        }
        if (InInfo.ServerID != 0)
          masterServer.RemoveServer(InInfo);
        InInfo.ServerID = this.GenerateServerID();
        PWSuperServer.PendingRegister pendingRegister = this.PendingRegisterRequests.Find((Predicate<PWSuperServer.PendingRegister>) (x => x.ServerInfo.BaseInfo.Equals(InInfo.BaseInfo)));
        PWServerInfoRunning NewServer = new PWServerInfoRunning(InConnection, InInfo);
        if (pendingRegister != null)
        {
          this.Log("Found Pending Register: " + InInfo.BaseInfo.RequestId.ToString(), InConnection, ELoggingLevel.ELL_Verbose);
          NewServer.BaseInfo.BaseInfo = pendingRegister.ServerInfo.BaseInfo;
          if (pendingRegister.CreateRequest != null)
          {
            pendingRegister.CreateRequest.ResultInfo = InInfo;
            pendingRegister.CreateRequest.ProcessServerResultDefault();
          }
          lock (this.PendingRegisterRequests)
            this.PendingRegisterRequests.Remove(pendingRegister);
        }
        this.OwningServer.Log("GameServerStart EndPoint=" + InConnection.EndPoint + " AdvertisedIP=" + NewServer.BaseInfo.PublicIP + ":" + (object) NewServer.BaseInfo.GamePort + " ServerID=" + (object) NewServer.BaseInfo.ServerID + " ServerName=\"" + NewServer.BaseInfo.BaseInfo.ServerName + "\" ServerType=" + (object) NewServer.BaseInfo.GameServerType, InConnection, true, ELoggingLevel.ELL_Informative);
        this.OwningServer.GetResource<PWRankedManager>()?.NotifyGameRegistered(InConnection, InInfo);
        this.OwningServer.GetResource<PWPrivateServerManager>()?.NotifyGameRegistered(InConnection, InInfo);
        masterServer.AddServer(NewServer);
        this.RefreshServers();
        return true;
      }
      this.Log("Register Server Failed, not tied to a master server", InConnection, ELoggingLevel.ELL_Warnings);
      return false;
    }

    public bool UnregisterGameServer(PWConnectionBase InConnection)
    {
      this.Log("Unregistering Server on Machine ", InConnection, ELoggingLevel.ELL_Informative);
      bool flag = false;
      this.MasterLock.EnterReadLock();
      foreach (MasterServerInfo masterServer in this.MasterServers)
      {
        PWServerInfoRunning server = masterServer.FindServer(InConnection);
        if (server != null)
        {
          this.OwningServer.GetResource<PWRankedManager>()?.NotifyGameEnded(InConnection, server.BaseInfo);
          masterServer.RemoveServer(server);
          this.OwningServer.Log("GameServerStop EndPoint=" + InConnection.EndPoint + " AdvertisedIP=" + server.BaseInfo.PublicIP + ":" + (object) server.BaseInfo.GamePort + " ServerID=" + (object) server.BaseInfo.ServerID + " ServerName=\"" + server.BaseInfo.BaseInfo.ServerName + "\" ServerType=" + (object) server.BaseInfo.GameServerType + " Reason=Unregistered", InConnection, true, ELoggingLevel.ELL_Informative);
          flag = true;
          break;
        }
      }
      this.MasterLock.ExitReadLock();
      return flag;
    }

    public bool UpdateGameServer(
      PWServerInfoUpdate UpdateInfo,
      Dictionary<string, string> UpdatedProperties,
      PWConnectionBase InConnection)
    {
      bool flag = false;
      this.MasterLock.EnterReadLock();
      foreach (MasterServerInfo masterServer in this.MasterServers)
      {
        PWServerInfoRunning server = masterServer.FindServer(InConnection);
        if (server != null)
        {
          server.UpdateWith(UpdateInfo, UpdatedProperties);
          flag = true;
          break;
        }
      }
      this.MasterLock.ExitReadLock();
      return flag;
    }

    public bool GameServerStartMatch(
      PWConnectionBase InConnection,
      out Dictionary<string, string> OutRules)
    {
      OutRules = this.CurrentGameRules;
      bool flag = false;
      this.MasterLock.EnterReadLock();
      foreach (MasterServerInfo masterServer in this.MasterServers)
      {
        PWServerInfoRunning server = masterServer.FindServer(InConnection);
        if (server != null)
        {
          server.bMatchInProgress = true;
          flag = true;
          break;
        }
      }
      this.MasterLock.ExitReadLock();
      return flag;
    }

    public bool GameServerEndMatch(PWConnectionBase InConnection)
    {
      bool flag = false;
      this.MasterLock.EnterReadLock();
      foreach (MasterServerInfo masterServer in this.MasterServers)
      {
        PWServerInfoRunning server = masterServer.FindServer(InConnection);
        if (server != null)
        {
          server.bMatchInProgress = false;
          flag = true;
          break;
        }
      }
      this.MasterLock.ExitReadLock();
      return flag;
    }

    public PWServerInfoRunning FindGameServer(PWConnectionBase InConnection)
    {
      this.MasterLock.EnterReadLock();
      PWServerInfoRunning serverInfoRunning = (PWServerInfoRunning) null;
      foreach (MasterServerInfo masterServer in this.MasterServers)
      {
        serverInfoRunning = masterServer.FindServer(InConnection);
        if (serverInfoRunning != null)
          break;
      }
      this.MasterLock.ExitReadLock();
      return serverInfoRunning;
    }

    public bool SetGameRule(string PropertyName, string NewValue, EGameRuleAction Action)
    {
      if (Action == EGameRuleAction.EGR_Add)
      {
        this.CurrentGameRules[PropertyName] = NewValue;
        return true;
      }
      return Action == EGameRuleAction.EGR_Remove && this.CurrentGameRules.Remove(PropertyName);
    }

    public void AddLoggedInPlayer(long PlayerId, PWConnectionBase Conn)
    {
      MasterServerInfo masterServerInfo = this.ChooseMasterServerForNewGame(EGameServerType.EGST_Private, Conn.GeoLoc);
      if (masterServerInfo != null)
        Conn.GeoLoc.RegionTag = masterServerInfo.RegionTag;
      if (this.LoggedInPlayers.ContainsKey(PlayerId))
        this.LoggedInPlayers[PlayerId] = Conn;
      else
        this.LoggedInPlayers.TryAdd(PlayerId, Conn);
    }

    public PWConnectionBase GetLoggedInConnFromId(long LoggedInPlayerId)
    {
      PWConnectionBase pwConnectionBase;
      this.LoggedInPlayers.TryGetValue(LoggedInPlayerId, out pwConnectionBase);
      return pwConnectionBase;
    }

    public void RemoveLoggedInPlayerByID(long PlayerId)
    {
      if (!this.LoggedInPlayers.ContainsKey(PlayerId))
        return;
      this.LoggedInPlayers.TryRemove(PlayerId, out PWConnectionBase _);
    }

    public void RemoveLoggedInPlayerByConn(PWConnectionBase Conn)
    {
      foreach (KeyValuePair<long, PWConnectionBase> loggedInPlayer in this.LoggedInPlayers)
      {
        if (loggedInPlayer.Value == Conn)
          this.LoggedInPlayers.TryRemove(loggedInPlayer.Key, out PWConnectionBase _);
      }
    }

    private List<PWServerInfoRunning> QueryServerList()
    {
      List<PWServerInfoRunning> serverInfoRunningList = new List<PWServerInfoRunning>();
      this.MasterLock.EnterReadLock();
      foreach (MasterServerInfo masterServer in this.MasterServers)
        serverInfoRunningList.AddRange(masterServer.PublicServers);
      this.MasterLock.ExitReadLock();
      return serverInfoRunningList;
    }

    public byte[] QueryServerListData()
    {
      this.ServerListLock.EnterReadLock();
      byte[] preserializedServerList = this.PreserializedServerList;
      this.ServerListLock.ExitReadLock();
      return preserializedServerList;
    }

    private void GenerateServerListData(object Source, ElapsedEventArgs args)
    {
      this.ServerListLock.EnterWriteLock();
      List<PWServerInfoRunning> serverInfoRunningList = this.QueryServerList();
      StringBuilder stringBuilder = new StringBuilder(serverInfoRunningList.Count * 350);
      stringBuilder.Append("T:");
      for (int index = 0; index < serverInfoRunningList.Count; ++index)
      {
        PWServerInfoRunning serverInfoRunning = serverInfoRunningList[index];
        if (index > 0)
          stringBuilder.Append(":" + serverInfoRunning.GetAdvertiseString());
        else
          stringBuilder.Append(serverInfoRunning.GetAdvertiseString());
      }
      MessageSerializer messageSerializer = new MessageSerializer();
      this.PreserializedServerList = messageSerializer.Compress(messageSerializer.SerializeRaw(stringBuilder.ToString()));
      this.ServerListLock.ExitWriteLock();
      int OutValue;
      this.OwningServer.GetConfigValue<int>("ServerListCacheTimeMS", out OutValue, 5000);
      this.ServerListTimer.Interval = (double) OutValue;
    }

    public bool CreateServer(PWCreateServerReqBase InRequest)
    {
      if (this.MasterServers.Count == 0)
      {
        this.Log("Failed to create server.  No Master Servers connected.", InRequest.Connection, ELoggingLevel.ELL_Errors);
        return false;
      }
      MasterServerInfo masterServerInfo;
      if (!(InRequest.SubRegionTag == ""))
      {
        int num = InRequest.RequestInfo.bArbitrated ? 2 : 3;
        string subRegionTag = InRequest.SubRegionTag;
        if ((masterServerInfo = this.ChooseMasterServerForNewGame((EGameServerType) num, subRegionTag)) != null)
          goto label_5;
      }
      masterServerInfo = this.ChooseMasterServerForNewGame(InRequest.RequestInfo.bArbitrated ? EGameServerType.EGST_Ranked : EGameServerType.EGST_Private, InRequest.Connection.GeoLoc);
label_5:
      if (masterServerInfo != null)
      {
        this.Log("Creating Server: " + InRequest.RequestInfo.RequestId.ToString(), InRequest.Connection, ELoggingLevel.ELL_Informative);
        PWCreateCustomServerCom createCustomServerCom = new PWCreateCustomServerCom();
        createCustomServerCom.StartInfo = InRequest.RequestInfo;
        createCustomServerCom.Connection = masterServerInfo.Connection;
        PWSuperServer.PendingCreate pendingCreate = new PWSuperServer.PendingCreate();
        pendingCreate.CreateRequest = InRequest;
        pendingCreate.CreateCommand = createCustomServerCom;
        pendingCreate.OwningMaster = masterServerInfo;
        lock (this.PendingCreateRequests)
          this.PendingCreateRequests.Add(pendingCreate);
        return createCustomServerCom.SubmitServerQuery();
      }
      this.Log("Failed to create server.  No Available Slots", InRequest.Connection, ELoggingLevel.ELL_Warnings);
      return false;
    }

    public bool GameServerCreated(PWCreateCustomServerCom InRequest)
    {
      PWSuperServer.PendingCreate pendingCreate = this.PendingCreateRequests.Find((Predicate<PWSuperServer.PendingCreate>) (x => x.CreateRequest.RequestInfo.Equals(InRequest.CreatedServer.BaseInfo)));
      if (pendingCreate == null)
        return false;
      this.Log("Found Pending Create: " + pendingCreate.CreateRequest.RequestInfo.RequestId.ToString(), InRequest.Connection, ELoggingLevel.ELL_Verbose);
      lock (this.PendingCreateRequests)
        this.PendingCreateRequests.Remove(pendingCreate);
      pendingCreate.CreateRequest.ResultInfo = InRequest.CreatedServer;
      PWSuperServer.PendingRegister pendingRegister = new PWSuperServer.PendingRegister();
      pendingRegister.MachineID = pendingCreate.OwningMaster.MachineID;
      pendingRegister.ServerInfo = InRequest.CreatedServer;
      pendingRegister.CreateRequest = pendingCreate.CreateRequest;
      lock (this.PendingRegisterRequests)
        this.PendingRegisterRequests.Add(pendingRegister);
      PWRankedManager resource = this.OwningServer.GetResource<PWRankedManager>();
      this.OwningServer.GetResource<PWPrivateServerManager>();
      if (pendingCreate.CreateRequest.RequestInfo.bArbitrated && resource != null)
        resource.NotifyGameCreated(InRequest.CreatedServer);
      else if (pendingCreate.CreateRequest.RequestInfo.bPrivate)
        ;
      return true;
    }

    public virtual void StartResource()
    {
      this.ServerListTimer.Start();
      this.ReportTimer.Start();
    }

    public virtual void StopResource()
    {
      this.ServerListTimer.Stop();
      this.ReportTimer.Stop();
    }

    public Dictionary<string, string> CheckHealth()
    {
      Dictionary<string, string> dictionary = new Dictionary<string, string>();
      PWSuperServer.MonitorStats monitorStats = new PWSuperServer.MonitorStats();
      monitorStats.NumMasters = this.MasterServers.Count;
      monitorStats.NumRankedServers = 0;
      monitorStats.NumPublicServers = 0;
      monitorStats.NumPlayers = 0;
      monitorStats.SlotsAvailable = 0;
      monitorStats.CurAdvertisedPop = 0;
      monitorStats.MaxAdvertisedPop = 0;
      dictionary["Super"] = "OK";
      foreach (MasterServerInfo masterServer in this.MasterServers)
      {
        monitorStats.NumRankedServers += masterServer.NumRankedServers;
        monitorStats.NumPublicServers += masterServer.NumPublicServers;
        monitorStats.NumPrivateServers += masterServer.NumPrivateServers;
        monitorStats.NumPlayers += masterServer.CurrentPopulation;
        monitorStats.SlotsAvailable += masterServer.TotalSlotsAvailable;
      }
      foreach (PWServerInfoRunning queryServer in this.QueryServerList())
      {
        monitorStats.CurAdvertisedPop += (int) queryServer.NumPlayers;
        monitorStats.MaxAdvertisedPop += queryServer.BaseInfo.BaseInfo.MaxPlayers;
      }
      if (this.HealthStats != null && this.HealthStats.NumMasters > 0)
      {
        if (monitorStats.NumMasters < this.HealthStats.NumMasters)
        {
          this.Log("Error: Master server count dropped suddenly from " + (object) this.HealthStats.NumMasters + " to " + (object) monitorStats.NumMasters, (PWConnectionBase) null, ELoggingLevel.ELL_Errors);
          dictionary["Super"] = "LOSTMASTER";
        }
        if (monitorStats.NumPublicServers < this.HealthStats.NumPublicServers)
        {
          this.Log("Error: Public Game server count dropped suddenly from " + (object) this.HealthStats.NumPublicServers + " to " + (object) monitorStats.NumPublicServers, (PWConnectionBase) null, ELoggingLevel.ELL_Errors);
          dictionary["Super"] = "LOSTSERVER";
        }
        int OutValue1 = 0;
        this.OwningServer.GetConfigValue<int>("MinPlayerDrop", out OutValue1, 10);
        float OutValue2 = 0.8f;
        this.OwningServer.GetConfigValue<float>("MinPctDrop", out OutValue2, 0.8f);
        int num1 = monitorStats.NumPlayers - this.HealthStats.NumPlayers;
        float num2 = (float) (100.0 * (1.0 - (this.HealthStats.NumPlayers > 0 ? (double) (monitorStats.NumPlayers / this.HealthStats.NumPlayers) : 0.0)));
        if (num1 > OutValue1 && (double) num2 > (double) OutValue2)
        {
          this.Log("Error: Playing player count dropped " + (object) num2 + "% from " + (object) this.HealthStats.NumPlayers + " to " + (object) monitorStats.NumPlayers, (PWConnectionBase) null, ELoggingLevel.ELL_Errors);
          dictionary["Super"] = "LOSTPLAYERS";
        }
      }
      dictionary["Super:NumMasters"] = monitorStats.NumMasters.ToString();
      dictionary["Super:PublicServers"] = monitorStats.NumPublicServers.ToString();
      dictionary["Super:PrivateServers"] = monitorStats.NumPrivateServers.ToString();
      dictionary["Super:RankedServers"] = monitorStats.NumRankedServers.ToString();
      dictionary["Super:ServerSlotsAvailable"] = monitorStats.SlotsAvailable.ToString();
      dictionary["Super:PlayersInGame"] = monitorStats.NumPlayers.ToString();
      dictionary["Super:CurAdvPlayers"] = monitorStats.CurAdvertisedPop.ToString();
      dictionary["Super:MaxAdvPlayers"] = monitorStats.MaxAdvertisedPop.ToString();
      foreach (KeyValuePair<string, string> currentGameRule in this.CurrentGameRules)
        dictionary["Super:Rules:" + currentGameRule.Key] = currentGameRule.Value;
      this.HealthStats = monitorStats;
      return dictionary;
    }

    private void OnReportTimer(object Source, ElapsedEventArgs args)
    {
      float num1 = 0.0f;
      float num2 = 0.0f;
      this.MasterLock.EnterReadLock();
      foreach (MasterServerInfo masterServer in this.MasterServers)
      {
        num1 += (float) masterServer.CurrentPopulation;
        num2 += (float) masterServer.MaxPopulation;
      }
      float num3 = 1f;
      if ((double) num2 > 0.0)
        num3 = num1 / num2;
      if ((double) num3 > (double) this.PlayerLoadThreshold)
      {
        this.OwningServer.Log("Player Threshold Reached, growing servers, at " + (object) num1 + "/" + (object) num2, (PWConnectionBase) null, false, ELoggingLevel.ELL_Errors);
        float num4 = 0.0f;
        int index1 = -1;
        for (int index2 = 0; index2 < this.MasterServers.Count; ++index2)
        {
          MasterServerInfo masterServer = this.MasterServers[index2];
          if ((double) masterServer.Rating > (double) num4)
          {
            num4 = masterServer.Rating;
            index1 = index2;
          }
        }
        if (index1 >= 0)
          new PWMasterServerGrowReq(this.MasterServers[index1].Connection).SubmitServerQuery();
        else
          this.OwningServer.Log("Region wants to grow, but no Master available.  Masters [" + (object) this.MasterServers.Count + "]  Players [" + (object) num1 + "/" + (object) num2 + "]", (PWConnectionBase) null, false, ELoggingLevel.ELL_Errors);
        if ((double) num3 > 0.949999988079071)
          this.OwningServer.Log("Region load at " + (object) (int) ((double) num3 * 100.0) + "% Masters [" + (object) this.MasterServers.Count + "]  Players [" + (object) num1 + "/" + (object) num2 + "]", (PWConnectionBase) null, false, ELoggingLevel.ELL_Critical);
      }
      else
        this.OwningServer.Log("Region Load " + (object) num1 + "/" + (object) num2, (PWConnectionBase) null, false, ELoggingLevel.ELL_Informative);
      this.MasterLock.ExitReadLock();
      this.OwningServer.Log("regionpopulationinmatch=" + num1.ToString(), (PWConnectionBase) null, true, ELoggingLevel.ELL_Verbose);
      this.CheckPremiumMatches();
    }

    private void CheckPremiumMatches()
    {
      PWPremiumMatchCache resource = this.OwningServer.GetResource<PWPremiumMatchCache>();
      bool OutValue = false;
      this.OwningServer.GetConfigValue<bool>("RunPremiumServers", out OutValue, false);
      if (resource == null || !OutValue)
        return;
      this.Log("Updating Premium Matches", (PWConnectionBase) null, ELoggingLevel.ELL_Informative);
      List<PremiumMatchItem> premiumMatchItemList1 = new List<PremiumMatchItem>(resource.CachedMatches.Count / 2);
      List<PremiumMatchItem> premiumMatchItemList2 = new List<PremiumMatchItem>(resource.CachedMatches.Count / 2);
      Dictionary<Guid, PWServerInfoRunning> dictionary = this.QueryServerList().Where<PWServerInfoRunning>((Func<PWServerInfoRunning, bool>) (x => x.BaseInfo.GameServerType == EGameServerType.EGST_Premium)).ToDictionary<PWServerInfoRunning, Guid>((Func<PWServerInfoRunning, Guid>) (x => x.BaseInfo.BaseInfo.RequestId));
      foreach (PremiumMatchItem cachedMatch in resource.CachedMatches)
      {
        if (PWServerBase.CurrentTime > cachedMatch.ExpirationDate)
          premiumMatchItemList2.Add(cachedMatch);
        else if (!dictionary.ContainsKey(cachedMatch.UniqueID))
          premiumMatchItemList1.Add(cachedMatch);
      }
      if (premiumMatchItemList1.Count > 0)
      {
        this.Log("Updating Premium Matches found " + (object) premiumMatchItemList1.Count + " that need starting", (PWConnectionBase) null, ELoggingLevel.ELL_Informative);
        using (List<PremiumMatchItem>.Enumerator enumerator = premiumMatchItemList1.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            PremiumMatchItem match = enumerator.Current;
            PWServerInfoReq requestObject = match.GenerateRequestObject();
            this.Log("Starting Premium Match " + requestObject.ToString(), (PWConnectionBase) null, ELoggingLevel.ELL_Informative);
            MasterServerInfo masterServerInfo = this.MasterServers.Find((Predicate<MasterServerInfo>) (x => x.RegionTag == match.SubRegion));
            if (masterServerInfo != null)
            {
              PWCreateServerReqBase InRequest = new PWCreateServerReqBase(masterServerInfo.Connection);
              InRequest.RequestInfo = requestObject;
              InRequest.SubmitServerQuery();
              this.CreateServer(InRequest);
            }
          }
        }
      }
      if (premiumMatchItemList2.Count <= 0)
        return;
      this.Log("Updating Premium Matches found " + (object) premiumMatchItemList2.Count + " expired matches that need shutdown", (PWConnectionBase) null, ELoggingLevel.ELL_Informative);
      foreach (PremiumMatchItem InMatch in premiumMatchItemList2)
      {
        PWServerInfoRunning serverInfoRunning = (PWServerInfoRunning) null;
        dictionary.TryGetValue(InMatch.UniqueID, out serverInfoRunning);
        resource.RemoveExpiredMatch(InMatch);
      }
    }

    private void Log(string Message, PWConnectionBase InConnection, ELoggingLevel InLoggingLevel) => this.OwningServer.Log(Message, InConnection, false, InLoggingLevel);

    private MasterServerInfo FindMasterServer(PWMachineID InMachineId)
    {
      this.MasterLock.EnterReadLock();
      MasterServerInfo masterServerInfo = this.MasterServers.Find((Predicate<MasterServerInfo>) (x => x.MachineID.Equals(InMachineId)));
      this.MasterLock.ExitReadLock();
      return masterServerInfo;
    }

    private MasterServerInfo FindMasterServer(PWConnectionBase InConnection)
    {
      this.MasterLock.EnterReadLock();
      MasterServerInfo masterServerInfo = this.MasterServers.Find((Predicate<MasterServerInfo>) (x => InConnection.Equals((object) x.Connection)));
      this.MasterLock.ExitReadLock();
      return masterServerInfo;
    }

    private void AddMaster(MasterServerInfo NewMaster)
    {
      this.MasterLock.EnterWriteLock();
      this.MasterServers.Add(NewMaster);
      this.MasterLock.ExitWriteLock();
    }

    private bool RemoveMaster(MasterServerInfo CurrentMaster)
    {
      if (CurrentMaster.NumTotalServers > 0)
      {
        this.Log("Master server removed but has live servers, keeping it around", CurrentMaster.Connection, ELoggingLevel.ELL_Errors);
        CurrentMaster.Connection = (PWConnectionBase) null;
        return false;
      }
      this.MasterLock.EnterWriteLock();
      bool flag = this.MasterServers.Remove(CurrentMaster);
      this.MasterLock.ExitWriteLock();
      return flag;
    }

    private int GenerateServerID() => this.NextGameServerID++;

    private void RefreshServers()
    {
      if (this.RefreshCallback == null)
        return;
      List<string> stringList = new List<string>();
      this.MasterLock.EnterReadLock();
      foreach (MasterServerInfo masterServer in this.MasterServers)
        stringList.AddRange(masterServer.PrintServers());
      this.MasterLock.ExitReadLock();
      this.RefreshCallback((IList<string>) stringList);
    }

    private void Refreshmasters()
    {
      if (this.RefreshMasterCallback == null)
        return;
      List<string> stringList = new List<string>();
      this.MasterLock.EnterReadLock();
      foreach (MasterServerInfo masterServer in this.MasterServers)
        stringList.Add(masterServer.ToString());
      this.MasterLock.ExitReadLock();
      this.RefreshMasterCallback((IList<string>) stringList);
    }

    private MasterServerInfo ChooseMasterServerForNewGame(
      EGameServerType InType,
      GeoLocation InLocation)
    {
      MasterServerInfo masterServerInfo1 = (MasterServerInfo) null;
      int num1 = 0;
      MasterServerInfo masterServerInfo2 = (MasterServerInfo) null;
      double num2 = 20000.0;
      MasterServerInfo masterServerInfo3 = (MasterServerInfo) null;
      int num3 = 0;
      this.MasterLock.EnterReadLock();
      foreach (MasterServerInfo masterServer in this.MasterServers)
      {
        if (masterServer.Connection != null)
        {
          double num4 = 0.0;
          if (masterServer.Connection.GeoLoc != null && InLocation != null)
          {
            num4 = GeoLocation.ComputeDistance(masterServer.Connection.GeoLoc, InLocation);
            this.Log("Distance Between " + masterServer.Connection.GeoLoc.ToString() + " and " + InLocation.ToString() + " = " + (object) num4, (PWConnectionBase) null, ELoggingLevel.ELL_Informative);
          }
          if (num4 < num2)
          {
            num2 = num4;
            masterServerInfo2 = masterServer;
          }
          int slotsAvailableFor = masterServer.GetSlotsAvailableFor(InType);
          if (slotsAvailableFor > num1)
          {
            masterServerInfo1 = masterServer;
            num1 = slotsAvailableFor;
          }
          int genericSlotsAvailable = masterServer.GenericSlotsAvailable;
          if (genericSlotsAvailable > num3)
          {
            masterServerInfo3 = masterServer;
            num3 = genericSlotsAvailable;
          }
        }
      }
      this.MasterLock.ExitReadLock();
      if (masterServerInfo2 == null || masterServerInfo2.TotalSlotsAvailable <= 0)
        return masterServerInfo1 ?? masterServerInfo3;
      this.Log("Choosing Closest Master with RegionTag" + masterServerInfo2.RegionTag + " Distance: " + (object) num2 + " miles away.", (PWConnectionBase) null, ELoggingLevel.ELL_Informative);
      return masterServerInfo2;
    }

    private MasterServerInfo ChooseMasterServerForNewGame(
      EGameServerType InType,
      string RegionTag)
    {
      MasterServerInfo masterServerInfo1 = (MasterServerInfo) null;
      int num1 = 0;
      MasterServerInfo masterServerInfo2 = (MasterServerInfo) null;
      double num2 = 20000.0;
      MasterServerInfo masterServerInfo3 = (MasterServerInfo) null;
      int num3 = 0;
      lock (this.MasterServers)
      {
        foreach (MasterServerInfo masterServer in this.MasterServers)
        {
          if (masterServer.Connection != null && masterServer.RegionTag == RegionTag)
          {
            int slotsAvailableFor = masterServer.GetSlotsAvailableFor(InType);
            if (slotsAvailableFor > num1)
            {
              masterServerInfo1 = masterServer;
              num1 = slotsAvailableFor;
            }
            int genericSlotsAvailable = masterServer.GenericSlotsAvailable;
            if (genericSlotsAvailable > num3)
            {
              masterServerInfo3 = masterServer;
              num3 = genericSlotsAvailable;
            }
          }
        }
      }
      if (masterServerInfo2 == null || masterServerInfo2.TotalSlotsAvailable <= 0)
        return masterServerInfo1 ?? masterServerInfo3;
      this.Log("Choosing Closest Master with RegionTag" + masterServerInfo2.RegionTag + " Distance: " + (object) num2 + " miles away.", (PWConnectionBase) null, ELoggingLevel.ELL_Informative);
      return masterServerInfo2;
    }

    private class PendingCreate
    {
      public PWCreateServerReqBase CreateRequest;
      public PWCreateCustomServerCom CreateCommand;
      public MasterServerInfo OwningMaster;
    }

    private class PendingRegister
    {
      public PWMachineID MachineID;
      public PWServerInfoFull ServerInfo;
      public PWCreateServerReqBase CreateRequest;
    }

    private class MonitorStats
    {
      public int NumMasters;
      public int NumRankedServers;
      public int NumPublicServers;
      public int NumPrivateServers;
      public int NumPlayers;
      public int SlotsAvailable;
      public int CurAdvertisedPop;
      public int MaxAdvertisedPop;
    }
  }
}
