﻿// Decompiled with JetBrains decompiler
// Type: ComponentAce.Compression.Libs.zlib.ZInputStream
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System.IO;
using System.Runtime.InteropServices;

namespace ComponentAce.Compression.Libs.zlib
{
  [ComVisible(true)]
  public class ZInputStream : BinaryReader
  {
    protected ZStream z = new ZStream();
    protected int bufsize = 512;
    protected int flush;
    protected byte[] buf;
    protected byte[] buf1 = new byte[1];
    protected bool compress;
    internal Stream in_Renamed;
    internal bool nomoreinput;

    internal void InitBlock()
    {
      this.flush = 0;
      this.buf = new byte[this.bufsize];
    }

    public virtual int FlushMode
    {
      get => this.flush;
      set => this.flush = value;
    }

    public virtual long TotalIn => this.z.total_in;

    public virtual long TotalOut => this.z.total_out;

    public ZInputStream(Stream in_Renamed)
      : base(in_Renamed)
    {
      this.InitBlock();
      this.in_Renamed = in_Renamed;
      this.z.inflateInit();
      this.compress = false;
      this.z.next_in = this.buf;
      this.z.next_in_index = 0;
      this.z.avail_in = 0;
    }

    public ZInputStream(Stream in_Renamed, int level)
      : base(in_Renamed)
    {
      this.InitBlock();
      this.in_Renamed = in_Renamed;
      this.z.deflateInit(level);
      this.compress = true;
      this.z.next_in = this.buf;
      this.z.next_in_index = 0;
      this.z.avail_in = 0;
    }

    public override int Read() => this.read(this.buf1, 0, 1) == -1 ? -1 : (int) this.buf1[0] & (int) byte.MaxValue;

    public int read(byte[] b, int off, int len)
    {
      if (len == 0)
        return 0;
      this.z.next_out = b;
      this.z.next_out_index = off;
      this.z.avail_out = len;
      int num;
      do
      {
        if (this.z.avail_in == 0 && !this.nomoreinput)
        {
          this.z.next_in_index = 0;
          this.z.avail_in = SupportClass.ReadInput(this.in_Renamed, this.buf, 0, this.bufsize);
          if (this.z.avail_in == -1)
          {
            this.z.avail_in = 0;
            this.nomoreinput = true;
          }
        }
        num = !this.compress ? this.z.inflate(this.flush) : this.z.deflate(this.flush);
        if (this.nomoreinput && num == -5)
          return -1;
        if (num != 0 && num != 1)
          throw new ZStreamException((this.compress ? "de" : "in") + "flating: " + this.z.msg);
        if (this.nomoreinput && this.z.avail_out == len)
          return -1;
      }
      while (this.z.avail_out == len && num == 0);
      return len - this.z.avail_out;
    }

    public long skip(long n)
    {
      int length = 512;
      if (n < (long) length)
        length = (int) n;
      byte[] target = new byte[length];
      return (long) SupportClass.ReadInput(this.BaseStream, target, 0, target.Length);
    }

    public override void Close() => this.in_Renamed.Close();
  }
}
