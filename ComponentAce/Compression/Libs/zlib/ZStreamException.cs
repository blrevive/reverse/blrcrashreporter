﻿// Decompiled with JetBrains decompiler
// Type: ComponentAce.Compression.Libs.zlib.ZStreamException
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System.IO;
using System.Runtime.InteropServices;

namespace ComponentAce.Compression.Libs.zlib
{
  [ComVisible(true)]
  public class ZStreamException : IOException
  {
    public ZStreamException()
    {
    }

    public ZStreamException(string s)
      : base(s)
    {
    }
  }
}
