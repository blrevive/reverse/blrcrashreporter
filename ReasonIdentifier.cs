﻿// Decompiled with JetBrains decompiler
// Type: ReasonIdentifier
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System.Runtime.InteropServices;

[ComVisible(true)]
public enum ReasonIdentifier
{
  REASON_NONE,
  REASON_MOVED,
  REASON_SUBSCRIPTION,
  REASON_LOST_CONNECTION,
  REASON_KICK_CHANNEL,
  REASON_KICK_SERVER,
  REASON_KICK_SERVER_BAN,
  REASON_SERVERSTOP,
  REASON_CLIENTDISCONNECT,
  REASON_CHANNELUPDATE,
  REASON_CHANNELEDIT,
  REASON_CLIENTDISCONNECT_SERVER_SHUTDOWN,
}
