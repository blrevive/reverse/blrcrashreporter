﻿// Decompiled with JetBrains decompiler
// Type: SpeexCodec
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System.Runtime.InteropServices;

[ComVisible(true)]
public enum SpeexCodec
{
  CODEC_SPEEX_NARROWBAND,
  CODEC_SPEEX_WIDEBAND,
  CODEC_SPEEX_ULTRAWIDEBAND,
  CODEC_CELT_MONO,
  CODEC_DUMMY_MONO,
}
