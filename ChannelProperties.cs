﻿// Decompiled with JetBrains decompiler
// Type: ChannelProperties
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System.Runtime.InteropServices;

[ComVisible(true)]
public enum ChannelProperties
{
  CHANNEL_NAME,
  CHANNEL_TOPIC,
  CHANNEL_DESCRIPTION,
  CHANNEL_PASSWORD,
  CHANNEL_CODEC,
  CHANNEL_CODEC_QUALITY,
  CHANNEL_MAXCLIENTS,
  CHANNEL_MAXFAMILYCLIENTS,
  CHANNEL_ORDER,
  CHANNEL_FLAG_PERMANENT,
  CHANNEL_FLAG_SEMI_PERMANENT,
  CHANNEL_FLAG_DEFAULT,
  CHANNEL_FLAG_PASSWORD,
  CHANNEL_ENDMARKER,
}
