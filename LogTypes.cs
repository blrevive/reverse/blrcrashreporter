﻿// Decompiled with JetBrains decompiler
// Type: LogTypes
// Assembly: BLRCrashReporter, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C4DCEFA4-04D9-4613-B271-9E50BDA5C72F
// Assembly location: C:\Users\SuperEwald\Downloads\depotdownloader-2.3.6\depots\209871\2520205\Blacklight Retribution\Live\Binaries\Win32\BLRCrashReporter.exe

using System.Runtime.InteropServices;

[ComVisible(true)]
public enum LogTypes
{
  LogType_NONE = 0,
  LogType_FILE = 1,
  LogType_CONSOLE = 2,
  LogType_USERLOGGING = 4,
  LogType_NO_NETLOGGING = 8,
  LogType_DATABASE = 16, // 0x00000010
}
